<?php

namespace App\Http\Controllers\Event;
use App\Http\Controllers\Controller;
use App\Models\LiveGame;
use Illuminate\Http\Request;


class LiveGameController extends Controller
{

    /**
     * Manage
     */
    public function manage()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        $data = LiveGame::getList();

        if( $data != null ){
            $response = [ 'status' => 1, 'data' => $data ];
        }

        return response()->json($response, 200);
    }

    /**
     * Create
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'eventId' => 'required|min:1|unique:tbl_live_game',
            'name' => 'required|min:3',
            'slug' => 'required|min:3',
            'mType' => 'required|min:3',
        ]);

        $response = LiveGame::doCreate($request->input());

        return response()->json($response, 200);
    }

    /**
     * Update
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required|min:3',
        ]);

        $response = LiveGame::doUpdate($request->input());

        return response()->json($response, 200);
    }

    /**
     * Status
     */
    public function status(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'status' => 'required',
        ]);

        $response = LiveGame::doUpdate($request->input());

        return response()->json($response, 200);
    }

    /**
     * Get Data
     */
    public function getData($id)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        $data = LiveGame::where([['id',$id]])->first();

        if( $data != null ){
            $response = [ 'status' => 1, 'data' => $data ];
        }

        return response()->json($response, 200);
    }


}
