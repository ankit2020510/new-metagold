<?php

namespace App\Http\Controllers\SquareOff;
use DB;
use App\Models\Mcx;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;
use App\Models\CommonOrderProfit;


class SquareOffController extends Controller
{

  public function cronOrderProcessing(Request $request){

   
     $response = [ "status" => 0 , "code" => 400 , 'data' => null , "message" => "Bad Request !!" ];

     $startTime = time();
     $endTime = time() + 60;
     while ($endTime > time()) {
            usleep(200000);  

         $orders = DB::table('tbl_order')
                            ->select('*')
                            ->where('comm',0)
                            ->whereIn('status',[1,5])
                            ->get();
         

        $date = date('Y-m-d');
        $currentBuyPrice = 0;$currentSellPrice=0;
        foreach ($orders as $key => $val) {

           if($val->status == 1 && $val->comm == 0 ){

              $depth = $this->getCurrentPrice($val->tradingSymbol,$val->instrumentToken);
         
              if($depth != 0){
                $currentBuyPrice = $depth['currentBuy'];
                $currentSellPrice = $depth['currentSell'];
              }
          
              if($val->expiry <= $date){                  
                  $orderProfit = CommonOrderProfit::getProfit($val,$currentBuyPrice,$currentSellPrice);
                  $update = $this->updateOrder($val->uid,$val->instrumentToken,$orderProfit,$val->id,$currentSellPrice,$currentBuyPrice);     
               
              }elseif($val->stopLoss != 0 || $val->stopLoss != null || $val->takeProfit != 0 || $val->takeProfit != null){
           
                   $check = $this->checkStopLossTakeProfit($val,$currentBuyPrice,$currentSellPrice);
                }//close check expiry
          }
        }


  }//while loop
        //print_r($orders); die('aaaaaaaa');
       die('corn order procesing');
       
   return $response;

  }

  
  public function checkStopLossTakeProfit($val,$currentBuyPrice,$currentSellPrice){

    if($val->order_type == 'instant_execution'){
                  if($val->trans_type == 'BUY'){
                    if($val->stopLoss < $val->buy_price){
                       if($val->stopLoss >= $currentBuyPrice){
                        $orderProfit = CommonOrderProfit::getProfit($val,$currentBuyPrice,$currentSellPrice);
                        $update = $this->updateOrder($val->uid,$val->instrumentToken,$orderProfit,$val->id,$currentSellPrice,$currentBuyPrice);
                      }// stop loss
                    }
                    if($val->takeProfit > $val->buy_price){
                        if($val->takeProfit <= $currentBuyPrice){
                          $orderProfit = CommonOrderProfit::getProfit($val,$currentBuyPrice,$currentSellPrice);
                          $update = $this->updateOrder($val->uid,$val->instrumentToken,$orderProfit,$val->id,$currentSellPrice,$currentBuyPrice);
                        }// take profit
                    }
                  }else{
                    if($val->stopLoss > $val->sell_price){
                       if($currentSellPrice >= $val->stopLoss){
                        $orderProfit = CommonOrderProfit::getProfit($val,$currentBuyPrice,$currentSellPrice);
                        $update = $this->updateOrder($val->uid,$val->instrumentToken,$orderProfit,$val->id,$currentSellPrice,$currentBuyPrice);
                      }// stop loss
                    }
                    if($val->takeProfit < $val->sell_price){
                      if($val->takeProfit >= $currentSellPrice){
                        $orderProfit = CommonOrderProfit::getProfit($val,$currentBuyPrice,$currentSellPrice);
                        $update = $this->updateOrder($val->uid,$val->instrumentToken,$orderProfit,$val->id,$currentSellPrice,$currentBuyPrice);
                      }// take profit
                    }
                  }
               }elseif($val->order_type == 'buy_limit'){
                  if($val->trans_type == 'BUY'){
                    if($val->stopLoss < $val->price){
                        if($val->stopLoss >= $currentBuyPrice){
                          $orderProfit = CommonOrderProfit::getProfit($val,$currentBuyPrice,$currentSellPrice);
                          $update = $this->updateOrder($val->uid,$val->instrumentToken,$orderProfit,$val->id,$currentSellPrice,$currentBuyPrice);
                        }// stop loss
                    }
                     if($val->takeProfit > $val->price){
                        if($val->takeProfit <= $currentBuyPrice){
                        $orderProfit = CommonOrderProfit::getProfit($val,$currentBuyPrice,$currentSellPrice);
                        $update = $this->updateOrder($val->uid,$val->instrumentToken,$orderProfit,$val->id,$currentSellPrice,$currentBuyPrice);
                      }// take profit
                    }

                  }
               }else{
                  if($val->trans_type == 'SELL'){
                     if($val->stopLoss > $val->price){
                        if($currentSellPrice >= $val->stopLoss){
                          $orderProfit = CommonOrderProfit::getProfit($val,$currentBuyPrice,$currentSellPrice);
                          $update = $this->updateOrder($val->uid,$val->instrumentToken,$orderProfit,$val->id,$currentSellPrice,$currentBuyPrice);
                        }// stop loss
                    }
                    if($val->takeProfit < $val->price){
                        if($val->takeProfit >= $currentSellPrice){
                          $orderProfit = CommonOrderProfit::getProfit($val,$currentBuyPrice,$currentSellPrice);
                          $update = $this->updateOrder($val->uid,$val->instrumentToken,$orderProfit,$val->id,$currentSellPrice,$currentBuyPrice);
                        }// take profit
                    }
                  }
               }//instanzt
  }


   public function getCurrentPrice($tradingSymbol, $instrumentToken){

         
       $cache = Redis::connection();
       $key = 'Mcx_Market';
       $result=$cache->get($key);
       $arrData = json_decode($result);
       $currentBuy=0;
       $currentSell=0;
       if(!empty($arrData)){
       foreach ($arrData as $key => $val) {
          
         if($key == $tradingSymbol && $val->instrument_token == $instrumentToken){
              $depth = $val->depth;
              $currentBuy = $depth->buy[0]->price;
              $currentSell = $depth->sell[0]->price;      
                         
          }
       }
          $depth = ['currentBuy'=>$currentBuy,'currentSell'=>$currentSell];

          return $depth;
        }else{

          return 0;
        }

 }
  public function updateOrder($uid,$instrumentToken,$profit,$orderId,$currentSellPrice,$currentBuyPrice){

         
       /* $pl_balance = [
            'pl_balance' => DB::raw('pl_balance + ' . round($profit))
        ];*/
    
       $status = ['status'=>5,'profit' => $profit,'closeBuyPrice'=>$currentBuyPrice,'closeSellPrice'=>$currentSellPrice,'balanceLow'=>2];
       //print_r($uid); echo "<pre>"; print_r($userPl->pl_balance); die('ankit');
       $orderStatus = DB::table('tbl_order')->where([['id',$orderId],['uid',$uid]])->update($status);

       //$order = DB::table('tbl_user_info')->where('uid',$uid)->update($pl_balance);
        
      return '1';

  }

}












?>