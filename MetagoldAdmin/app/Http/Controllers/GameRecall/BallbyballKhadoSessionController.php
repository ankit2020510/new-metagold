<?php

namespace App\Http\Controllers\GameRecall;
use App\Http\Controllers\Controller;
use App\Models\CommonModel;
use Illuminate\Support\Facades\DB;


class BallbyballKhadoSessionController extends Controller
{

    // do Recall
    public function doRecall(){

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
        $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

        if( isset( $requestData[ 'eventId' ] ) && isset( $requestData['marketId'] ) && isset( $requestData['mType'] ) ){

            $eventId = $requestData['eventId'];
            $marketId = $requestData['marketId'];
            $mType = $requestData['mType'];

            if( $mType == 'khado' ){
                $tbl = 'tbl_khado_sesstion';
            }else{
                $tbl = 'tbl_ball_session';
            }

            $market = DB::table($tbl)
                ->where([['eid',$eventId],['marketId',$marketId],['game_over',1]])->first();

            if( $market != null ){
                $betTbl = 'tbl_bet_pending_3';
                if( CommonModel::gameRecall($marketId,$mType,$betTbl) == true ){

                    if( DB::table($tbl)->where([['eid',$eventId],['marketId',$marketId]])
                        ->update([ 'game_over' => 0 , 'result' => 'undefined' ]) ){

                        // Update User Expose
                        CommonModel::userExposeUpdateRecall($marketId,$betTbl);

                        $response = [
                            "status" => 1,
                            "success" => [
                                "message" => "Game Recall Successfully!"
                            ]
                        ];
                    }else{
                        $response[ "error" ] = [
                            "message" => "Something wrong! Yet game recall not done!"
                        ];
                    }

                }else{
                    $response[ "error" ] = [
                        "message" => "Something wrong! Yet game recall not done!"
                    ];
                }

            }

        }

        return $response;

    }

    /*
    //game Recall
    public function gameRecall($marketId,$mType){

        $transArr = [];
        $transClient = DB::table('tbl_transaction_client')->select(['userId', 'type', 'amount'])
            ->where([['status', 1],['mid', $marketId],['mType', $mType]])->get();

        if( $transClient != null ){
            $transArr = array_merge($transArr,$transClient);
        }

        $transParent = DB::table('tbl_transaction_parent')->select(['userId', 'type', 'amount'])
            ->where([['status', 1],['mid', $marketId],['mType', $mType]])->get();

        if( $transParent != null ){
            $transArr = array_merge($transArr,$transParent);
        }

        $transAdmin = DB::table('tbl_transaction_admin')->select(['userId', 'type', 'amount'])
            ->where([['status', 1],['mid', $marketId],['mType', $mType]])->get();

        if( $transAdmin != null ){
            $transArr = array_merge($transArr,$transAdmin);
        }

        if ($transArr != null) {

            foreach ($transArr as $trans) {

                $user = DB::table('tbl_user_info')->select(['pl_balance'])
                    ->where([['status', 1], ['uid', $trans->userId]])->first();

                if ($user != null) {
                    if ($trans->type == 'CREDIT') {
                        $plBalance = ($user->pl_balance - $trans->amount);
                    } else {
                        $plBalance = ($user->pl_balance + $trans->amount);
                    }

                    DB::table('tbl_user_info')->where([['uid', $trans->userId]])
                        ->update(['pl_balance' => $plBalance]);

                    $summaryData = [];

                    $summaryData['uid'] = $trans->userId;
                    $summaryData['type'] = $trans->type;

                    if( $trans->eType == 3 ){
                        $summaryData['ownComm'] = $trans->amount;
                        $summaryData['parentComm'] = $trans->p_amount;
                    }else{
                        $summaryData['ownPl'] = $trans->amount;
                        $summaryData['parentPl'] = $trans->p_amount;
                    }

                    $this->userSummaryUpdate($summaryData);

                }
            }

            // update transaction status
            DB::table('tbl_transaction_client')->where([['status', 1], ['mid', $marketId]])
                ->update(['status' => 2]);
            DB::table('tbl_transaction_parent')->where([['status', 1], ['mid', $marketId]])
                ->update(['status' => 2]);
            DB::table('tbl_transaction_admin')->where([['status', 1], ['mid', $marketId]])
                ->update(['status' => 2]);

            // update result
            DB::table('tbl_bet_pending_3')->where([['status', 1], ['mid', $marketId]])
                ->update(['result' => 'PENDING']);

            return true;

        }

        return false;
    }

    //user Expose Update
    public function userExposeUpdate($marketId){

        $userList = DB::table('tbl_bet_pending_3')
            ->where([ ['status',1],['mid',$marketId]])->distinct()->count('uid');

        if(!empty($userList)){
            foreach ($userList as $user ) {
                $userId = $user->uid; $expose = 0;

                $userExpose = DB::table('tbl_user_market_expose')->where([[ 'uid' => $userId , 'status' => 1]])->sum('expose');

                if($userExpose != null){
                    $expose = round($userExpose,2);
                }

                DB::table('tbl_user_info')->where([['uid',$userId]])->update(['expose',$expose]);

            }
        }
    }

    //user Summary Update
    public function userSummaryUpdate($summaryData){

        if( isset( $summaryData['uid'] ) ){

            $uid = $summaryData['uid'];
            $ownPl = isset($summaryData['ownPl']) ? $summaryData['ownPl'] : 0;
            $parentPl = isset($summaryData['ownPl']) ? $summaryData['parentPl'] : 0;

            $ownComm = isset($summaryData['ownComm']) ? $summaryData['ownComm'] : 0;
            $parentComm = isset($summaryData['parentComm']) ? $summaryData['parentComm'] : 0;

            $userSummary = DB::table('tbl_settlement_summary')
                ->where([['status',1],['uid',$uid]])->first();

            if( $userSummary != null ){

                if( isset( $summaryData['type'] ) && $summaryData['type'] == 'CREDIT' ){
                    $ownPl = $userSummary->ownPl-$ownPl;
                    $parentPl = $userSummary->parentPl-$parentPl;
                }else{
                    $ownPl = $userSummary->ownPl+$ownPl;
                    $parentPl = $userSummary->parentPl+$parentPl;
                }

                $ownComm = $userSummary->ownComm-$ownComm;
                $parentComm = $userSummary->parentComm-$parentComm;

                $updateArr = [ 'ownPl' => $ownPl, 'ownComm' => $ownComm,
                    'parentPl' => $parentPl, 'parentComm' => $parentComm ];

                DB::table('tbl_settlement_summary')->where([['status',1],['uid',$uid]])
                    ->update($updateArr);

            }else{

                $insertArr = [ 'uid' => $uid, 'ownPl' => 0, 'ownComm' => 0,
                    'parentPl' => 0, 'parentComm' => 0 ];

                DB::table('tbl_settlement_summary')->insert($insertArr);

            }

        }
    }
    */
}
