<?php

namespace App\Http\Controllers\Setting;
use App\Http\Controllers\Controller;
use App\Models\Rules;
use App\Models\Sport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class RulesController extends Controller
{
    /**
     * Manage
     */
    public function manage()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        $data = Rules::getList();

        if( $data != null ){
            $response = [ 'status' => 1, 'data' => $data ];
        }

        return response()->json($response, 200);
    }

    /**
     * Create
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'sport' => 'required|min:3',
            'market' => 'required|min:3',
            'rules' => 'required|min:10',
        ]);

        $response = Rules::doCreate($request->input());

        return response()->json($response, 200);
    }

    /**
     * Update
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'rules' => 'required|min:10',
        ]);

        $response = Rules::doUpdate($request->input());

        return response()->json($response, 200);
    }

    /**
     * Status
     */
    public function status(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'status' => 'required',
        ]);

        $response = Rules::doUpdate($request->input());

        return response()->json($response, 200);
    }

    /**
     * requiredData
     */
    public function requiredData()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];
        $data = [];
        $sportList = DB::table('tbl_sport')->select(['name'])->where([['status',1]])->get();
        $marketList = DB::table('tbl_market_type')->select(['name'])->where([['status',1]])->get();

        if( !empty($sportList) && !empty($marketList) ){
            $data = [ 'sport' => $sportList, 'market' => $marketList ];
        }

        if( $data != null ){
            $response = [ 'status' => 1, 'data' => $data ];
        }

        return response()->json($response, 200);
    }

}
