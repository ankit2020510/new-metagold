<?php

namespace App\Http\Controllers\Setting;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;


class ActionController extends Controller
{
    /**
     * Manage
     */
    public function manage()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $data = Setting::getList();

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }

    /**
     * Create
     */
    public function create(Request $request)
    {
        try{
            $this->validate($request, [
                'key' => 'required|min:5',
                'value' => 'required|min:1',
            ]);

            $response = Setting::doCreate($request->input());
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Update
     */
    public function update(Request $request)
    {
        try{

            $this->validate($request, [
                'id' => 'required',
                'value' => 'required|min:1',
            ]);

            $response = Setting::doUpdate($request->input());
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Status
     */
    public function status(Request $request)
    {
        try{
            $this->validate($request, [
                'id' => 'required',
                'status' => 'required',
            ]);

            $response = Setting::doUpdate($request->input());
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
