<?php

namespace App\Http\Controllers\GameOver;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class LiveGameController extends Controller
{

    /**
     * Do Game Over
     */
    public function doGameOver()
    {

        $resultData = DB::table('tbl_teenpatti_result')
            ->where([['status',1],['game_over',0]])->get();

        if( $resultData != null ){

            foreach ( $resultData as $rData ){

                $eventId = $rData->eid;
                $marketId = $rData->mid;
                $winner = $rData->winner;
                $roundId = $rData->round;
                $resultData = $rData->description;

                $betData = $rData->bet_data;
                $userData = $rData->user_data;

                $betvoid = $rData->betvoid;

                $checkBets = [];

                $gameData = DB::table('tbl_live_game')
                    ->select(['sportId','eventId','name','mType','is_block','min_stake','max_stake','max_profit_limit','suspend'])
                    ->where([['status',1],['eventId',$eventId]])->first();

                if( $gameData != null ){
                    $mType = $gameData->mType; $marketName = $gameData->name;
                }

                if( $betvoid != 1 ){

                    // Teen Patti
                    $checkBets = DB::table('tbl_bet_pending_teenpatti')->select(['id'])
                        ->where([ ['eid',$eventId],['mid',$marketId],['result','Pending'],['status',1] ])->first();

                    if( $checkBets != null ){
                        $this->gameOverResult( $eventId, $marketId, $betData );
                        $this->transactionResult( $roundId , $eventId , $marketId , $userData , $mType , $marketName );
                    }

                }else{

                    // Game Abandoned
                    DB::table('tbl_bet_pending_teenpatti')
                        ->where([ [ 'mid',$marketId ],['eid',$eventId],['result','PENDING'],['status',1] ])
                        ->update(['result'=>'CANCELED']);

                }

                if( DB::table('tbl_teenpatti_result')
                    ->where([ ['eid',$eventId],['mid',$marketId] ])->update(['game_over' => 1]) ){

                    // Update User Event Expose
                    DB::table('tbl_user_market_expose')
                        ->where([['eid',$eventId],['mid',$marketId ]])->update(['status' => 2]);

                    //Update User Expose
                    $this->userExposeUpdate($marketId);

                    $response = [
                        'status' => 1,
                        "success" => [
                            "message" => "Game Over successfully!"
                        ]
                    ];
                }else{
                    $response[ "error" ] = [
                        "message" => "Something wrong! event not updated!",
                    ];
                }

            }

        }else{
            $response = [
                'status' => 1,
                "success" => [
                    "message" => "Game Over successfully!"
                ]
            ];
        }

        echo json_encode($response);

    }

    //Game Over Result TeenPatti
    public function gameOverResult( $eventId , $marketId , $betData ){

        if( $betData != null ){

            $betData = json_decode($betData , 16);

            foreach ( $betData as $bets ){

                $uid = $bets['user_id'];

                foreach ( $bets['bets'] as $bet ){

                    $status = 'CANCELED';
                    $updateArr = ['result' => $status ];
                    $orderId = trim($bet['orderId']);

                    if( isset( $bet['status'] ) && $bet['status'] == 'LOST' ){
                        $status = 'LOSS';
                        $loss = (-1)*((int)$bet['downPl']);
                        $updateArr = ['result' => $status , 'loss' => $loss ];
                    }

                    if( isset( $bet['status'] ) && $bet['status'] == 'WON' ){
                        $status = 'WIN';
                        $win = (int)$bet['downPl'];
                        $updateArr = ['result' => $status , 'win' => $win ];
                    }

                    // Update User Event Expose
                    DB::table('tbl_bet_pending_teenpatti')
                        ->where([['eid',$eventId],['mid',$marketId],['orderId',$orderId],['result','PENDING']])
                        ->update($updateArr);

                }


            }

        }

    }

    //transaction Result MatchOdds
    public function transactionResult($roundId,$eventId,$marketId,$userData,$mType,$marketName){

        if( $userData != null ){

            $userData = json_decode($userData , 16);

            foreach ( $userData as $users ){
                $uid = $users['user_id'];
                $amount = $users['pl'];
                if( $amount != 0 ){
                    $this->updateTransactionHistory($uid,$roundId,$eventId,$marketId,$amount,$mType,$marketName);
                }
            }

        }

    }

    // Update Transaction History
    public function updateTransactionHistory($uid,$roundId,$eventId,$marketId,$amount,$mType,$marketName)
    {
        if( $amount > 0 ){ $type = 'CREDIT'; }else{ $amount = (-1)*$amount; $type = 'DEBIT'; }

        $summaryData = [];

        $cUser = DB::table('tbl_user')->select(['parentId'])->where([['id',$uid]])->first();
        $cUserInfo = DB::table('tbl_user_info')->select(['balance','pl_balance'])->where([['uid',$uid]])->first();

        $parentId = $cUser->parentId;
        $description = $this->setDescription($marketName,$roundId);
        $balance = $this->getCurrentBalance($uid,$amount,$type,$cUserInfo->balance,$cUserInfo->pl_balance,'CLIENT');

        //$where = [ ['clientId',$uid],['userId',$uid],['parentId',$parentId],['eid',$eventId],['mid',$marketId],['type',$type],
        //    ['amount',$amount],['status',1] ];

        //$checkData = DB::table('tbl_transaction_client')->select(['id'])->where($where)->first();

        //if( $checkData == null ){

        $resultArr = [
            'systemId' => 1,
            'clientId' => $uid,
            'userId' => $uid,
            'childId' => 0,
            'parentId' => $parentId,
            'sid' => 99,
            'eid' => $eventId,
            'mid' => $marketId,
            'round' => $roundId,
            'eType' => 0,
            'mType' => $mType,
            'type' => $type,
            'amount' => $amount,
            'p_amount' => 0,
            'c_amount' => 0,
            'balance' => $balance,
            'description' => $description,
            'remark' => 'Transactional Entry',
            'status' => 1,
        ];

        DB::table('tbl_transaction_client')->insert($resultArr);

        $summaryData['uid'] = $uid;
        $summaryData['ownPl'] = $amount;
        $summaryData['type'] = $type;

        $this->userSummaryUpdate($summaryData);

        //}

        if( $type == 'CREDIT' ){ $pType = 'DEBIT'; }else{ $pType = 'CREDIT'; }

        $parentUserData = DB::table('tbl_user_profit_loss as pl')
            ->select(['pl.userId as userId','pl.actual_profit_loss as apl','pl.profit_loss as gpl','ui.balance as balance','ui.pl_balance as pl_balance'])
            ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
            ->where([['pl.clientId',$uid],['pl.actual_profit_loss','!=',0]])->orderBy('pl.userId','desc')->get();

        $childId = $uid;

        foreach ( $parentUserData as $user ){

            $summaryData = [];

            $cUser = DB::table('tbl_user')->select(['parentId','role'])->where([['id',$user->userId]])->first();
            $parentId = $cUser->parentId;

            $transactionAmount = ( $amount*$user->apl )/100;
            $pTransactionAmount = ( $amount*(100-$user->gpl) )/100;
            $cTransactionAmount = ( $amount*($user->gpl-$user->apl) )/100;
            $currentBalance = $this->getCurrentBalance($user->userId,$transactionAmount,$pType,$user->balance,$user->pl_balance,'PARENT');

            //$where = [['clientId',$uid],['userId',$user->userId],['parentId',$parentId],['eid',$eventId],
            //    ['mid',$marketId],['type',$pType],['amount',$transactionAmount],['status',1]];

            //$checkData = DB::table('tbl_transaction_client')->select(['id'])->where($where)->first();

            //if( $checkData == null ) {

            $resultArr = [
                'systemId' => 1,
                'clientId' => $uid,
                'userId' => $user->userId,
                'childId' => $childId,
                'parentId' => $parentId,
                'sid' => 99,
                'eid' => $eventId,
                'mid' => $marketId,
                'round' => $roundId,
                'eType' => 0,
                'mType' => $mType,
                'type' => $pType,
                'amount' => $transactionAmount,
                'p_amount' => $pTransactionAmount,
                'c_amount' => $cTransactionAmount,
                'balance' => $currentBalance,
                'description' => $description,
                'remark' => 'Transactional Entry',
                'status' => 1,
            ];

            if( $cUser->role == 1 ){
                DB::table('tbl_transaction_admin')->insert($resultArr);
            }else{
                DB::table('tbl_transaction_parent')->insert($resultArr);
            }

            $summaryData['uid'] = $user->userId;
            $summaryData['type'] = $pType;
            $summaryData['ownPl'] = $transactionAmount;
            $summaryData['parentPl'] = $pTransactionAmount;

            $this->userSummaryUpdate($summaryData);

            //}

            $childId = $user->userId;

        }

    }

    // Function to get the Description
    public function setDescription($marketName,$roundId)
    {
        $description = $marketName. ' > Round #' .$roundId;
        return $description;
    }

    // get Current Balance
    public function getCurrentBalance($uid,$amount,$type,$balance,$plBalance,$role)
    {
        if( $type == 'CREDIT' ){
            $newplBalance = $plBalance+$amount;
        }else{
            $newplBalance = $plBalance-$amount;
        }

        if( DB::table('tbl_user_info')->where([['uid',$uid]])->update(['pl_balance',$newplBalance]) ){

            if( $role != 'CLIENT' ){
                return round( ( $newplBalance ),2);
            }else{
                return round( ( $balance + $newplBalance ),2);
            }

        }else{

            if( $role != 'CLIENT' ){
                return round( ( $plBalance ),2);
            }else{
                return round( ( $balance + $plBalance ),2);
            }
        }

    }

    //user Expose Update
    public function userExposeUpdate($marketId){

        $userList = DB::table('tbl_bet_pending_teenpatti')
            ->where([ ['status',1],['mid',$marketId]])->distinct()->count('uid');

        if(!empty($userList)){
            foreach ($userList as $user ) {
                $userId = $user->uid; $expose = 0;

                $userExpose = DB::table('tbl_user_market_expose')->where([[ 'uid' => $userId , 'status' => 1]])->sum('expose');

                if($userExpose != null){
                    $expose = round($userExpose,2);
                }

                DB::table('tbl_user_info')->where([['uid',$userId]])->update(['expose',$expose]);

            }
        }
    }

    //user Summary Update
    public function userSummaryUpdate($summaryData){

        if( isset( $summaryData['uid'] ) ){

            $uid = $summaryData['uid'];
            $ownPl = isset($summaryData['ownPl']) ? $summaryData['ownPl'] : 0;
            $parentPl = isset($summaryData['ownPl']) ? $summaryData['parentPl'] : 0;

            $ownComm = isset($summaryData['ownComm']) ? $summaryData['ownComm'] : 0;
            $parentComm = isset($summaryData['parentComm']) ? $summaryData['parentComm'] : 0;

            $userSummary = DB::table('tbl_settlement_summary')
                ->where([['status',1],['uid',$uid]])->first();

            if( $userSummary != null ){

                if( isset( $summaryData['type'] ) && $summaryData['type'] == 'CREDIT' ){
                    $ownPl = $ownPl+$userSummary->ownPl;
                    $parentPl = $parentPl+$userSummary->parentPl;
                }else{
                    $ownPl = $userSummary->ownPl-$ownPl;
                    $parentPl = $userSummary->parentPl-$parentPl;
                }

                $ownComm = $userSummary->ownComm+$ownComm;
                $parentComm = $userSummary->parentComm-$parentComm;

                $updateArr = [ 'ownPl' => $ownPl, 'ownComm' => $ownComm,
                    'parentPl' => $parentPl, 'parentComm' => $parentComm ];

                DB::table('tbl_settlement_summary')->where([['status',1],['uid',$uid]])
                    ->update($updateArr);

            }else{

                $insertArr = [ 'uid' => $uid, 'ownPl' => $ownPl, 'ownComm' => $ownComm,
                    'parentPl' => $parentPl, 'parentComm' => $parentComm ];

                DB::table('tbl_settlement_summary')->insert($insertArr);

            }

        }
    }


}
