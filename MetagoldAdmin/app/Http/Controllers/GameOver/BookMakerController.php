<?php

namespace App\Http\Controllers\GameOver;
use App\Http\Controllers\Controller;
use App\Models\CommonModel;
use Illuminate\Support\Facades\DB;


class BookMakerController extends Controller
{

    // Game Over Book Maker
    public function doGameOver(){

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
        $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

        if( isset( $requestData[ 'eventId' ] ) && ( $requestData[ 'eventId' ] != null )
            && isset( $requestData[ 'marketId' ] ) && ( $requestData[ 'marketId' ] != null )
            && isset( $requestData[ 'secId' ] ) && ( $requestData[ 'secId' ] != null ) ) {

            $sportId = $requestData['sportId'];
            $eventId = $requestData['eventId'];
            $marketId = $requestData['marketId'];
            $secId = $requestData['secId'];
            $winResult = $requestData['winResult'];

            $market = DB::table('tbl_bookmaker')
                ->where([['eid',$eventId],['marketId',$marketId],['game_over',0]])->first();

            if( $market != null ){

                // check if bet exist;
                $betTbl = 'tbl_bet_pending_1';
                $checkBets = DB::table($betTbl)
                    ->where([['eid',$eventId],['mid',$marketId],['result','PENDING']])->first();

                if( $checkBets != null ){
                    $mType = 'bookmaker';
                    CommonModel::gameOverResult($eventId,$marketId,$secId,$mType,$betTbl);
                    CommonModel::transactionResult($sportId,$eventId,$marketId,$mType,$betTbl);
                }

                if( DB::table('tbl_bookmaker')
                    ->where([['eid',$eventId],['marketId',$marketId]])
                    ->update(['game_over'=>1,'result'=>$winResult])){

                    //Update User Expose
                    if( CommonModel::userExposeUpdate($marketId,$betTbl) ){
                        $response = [
                            'status' => 1,
                            "success" => [
                                "message" => "Game Over Successfully!"
                            ]
                        ];
                    }else{
                        $response[ "error" ] = [
                            "message" => "Something wrong! Yet game over not done!"
                        ];
                    }


                }else{
                    $response[ "error" ] = [
                        "message" => "Something wrong! Yet game over not done!"
                    ];
                }

            }

        }

        return $response;

    }

    /*
    //Game Over Result
    public function gameOverResult($eventId,$marketId,$secId){

        // Win Update for Back
        DB::table('tbl_bet_pending_1')
            ->where([['mid',$marketId],['eid',$eventId],['secId',$secId],
                ['result','PENDING'],['bType','BACK'],['status',1],['is_match',1] ])
            ->update(['result'=>'WIN']);

        // Win Update for Lay
        DB::table('tbl_bet_pending_1')
            ->where([['mid',$marketId],['eid',$eventId],['secId','!=',$secId],
                ['result','PENDING'],['bType','LAY'],['status',1],['is_match',1] ])
            ->update(['result'=>'WIN']);

        // Loss Update
        DB::table('tbl_bet_pending_1')
            ->where([['mid',$marketId],['eid',$eventId],
                ['result','PENDING'],['status',1],['is_match',1] ])
            ->update(['result'=>'LOSS']);

        // Unmatched Bet canceled
        DB::table('tbl_bet_pending_1')
            ->where([['mid',$marketId],['eid',$eventId],
                ['result','PENDING'],['status',1],['is_match',0] ])
            ->update(['result'=>'CANCELED']);

    }

    //transaction Result MatchOdds
    public function transactionResult($sportId,$eventId,$marketId,$mType){

        $userList = DB::table('tbl_bet_pending_1')->select(['uid'])
            ->where([ ['mid',$marketId],['eid',$eventId],['status',1],['is_match',1] ])
            ->whereIn('bType',['WIN','LOSS'])
            ->groupBy('uid')->get();

        if( $userList != null ){

            foreach ( $userList as $user ){

                $uid = $user->uid; $profit = $loss = 0;

                $winAmount = DB::table('tbl_bet_pending_1')->select(['uid'])
                    ->where([ ['uid',$uid],['mid',$marketId],['eid',$eventId],['result','WIN'],['status',1],['is_match',1] ])
                    ->sum('win');

                $lossAmount = DB::table('tbl_bet_pending_1')->select(['uid'])
                    ->where([ ['uid',$uid],['mid',$marketId],['eid',$eventId],['result','LOSS'],['status',1],['is_match',1] ])
                    ->sum('loss');

                if( $winAmount != null ){
                    $profit = round($winAmount,2);
                }
                if( $lossAmount != null ){
                    $loss = round($lossAmount,2);
                }

                $amount = $profit-$loss;

                if( $amount != 0 ){
                    $this->updateTransactionHistory($sportId,$uid,$eventId,$marketId,$amount,$mType);
                }
            }

        }

    }

    // Update Transaction History
    public function updateTransactionHistory($sportId,$uid,$eventId,$marketId,$amount,$mType)
    {
        if( $amount > 0 ){ $type = 'CREDIT'; }else{ $amount = (-1)*$amount; $type = 'DEBIT'; }

        $summaryData = [];

        $cUser = DB::table('tbl_user')->select(['parentId'])->where([['id',$uid]])->first();
        $cUserInfo = DB::table('tbl_user_info')->select(['balance','pl_balance'])->where([['uid',$uid]])->first();

        $parentId = $cUser->parentId;
        $description = $this->setDescription($marketId);
        $balance = $this->getCurrentBalance($uid,$amount,$type,$cUserInfo->balance,$cUserInfo->pl_balance,'CLIENT');

        $resultArr = [
            'systemId' => 1,
            'clientId' => $uid,
            'userId' => $uid,
            'childId' => 0,
            'parentId' => $parentId,
            'sid' => $sportId,
            'eid' => $eventId,
            'mid' => $marketId,
            'eType' => 0,
            'mType' => $mType,
            'type' => $type,
            'amount' => $amount,
            'p_amount' => 0,
            'c_amount' => 0,
            'balance' => $balance,
            'description' => $description,
            'remark' => 'Transactional Entry',
            'status' => 1,
        ];

        DB::table('tbl_transaction_client')->insert($resultArr);

        $summaryData['uid'] = $uid;
        $summaryData['ownPl'] = $amount;
        $summaryData['type'] = $type;

        $this->userSummaryUpdate($summaryData);

        if( $type == 'CREDIT' ){ $pType = 'DEBIT'; }else{ $pType = 'CREDIT'; }

        $parentUserData = DB::table('tbl_user_profit_loss as pl')
            ->select(['pl.userId as userId','pl.actual_profit_loss as apl','pl.profit_loss as gpl','ui.balance as balance','ui.pl_balance as pl_balance'])
            ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
            ->where([['pl.clientId',$uid],['pl.actual_profit_loss','!=',0]])->orderBy('pl.userId','desc')->get();

        $childId = $uid;

        foreach ( $parentUserData as $user ){

            $summaryData = [];

            $cUser = DB::table('tbl_user')->select(['parentId','role'])->where([['id',$user->userId]])->first();
            $parentId = $cUser->parentId;

            $transactionAmount = ( $amount*$user->apl )/100;
            $pTransactionAmount = ( $amount*(100-$user->gpl) )/100;
            $cTransactionAmount = ( $amount*($user->gpl-$user->apl) )/100;
            $balance = $this->getCurrentBalance($user->userId,$transactionAmount,$pType,$user->balance,$user->pl_balance,'PARENT');

            $resultArr = [
                'systemId' => 1,
                'clientId' => $uid,
                'userId' => $user->userId,
                'childId' => $childId,
                'parentId' => $parentId,
                'sid' => $sportId,
                'eid' => $eventId,
                'mid' => $marketId,
                'eType' => 0,
                'mType' => $mType,
                'type' => $pType,
                'amount' => $transactionAmount,
                'p_amount' => $pTransactionAmount,
                'c_amount' => $cTransactionAmount,
                'balance' => $balance,
                'description' => $description,
                'remark' => 'Transactional Entry',
                'status' => 1,
            ];

            if( $cUser->role == 1 ){
                DB::table('tbl_transaction_admin')->insert($resultArr);
            }else{
                DB::table('tbl_transaction_parent')->insert($resultArr);
            }

            $summaryData['uid'] = $user->userId;
            $summaryData['type'] = $pType;
            $summaryData['ownPl'] = $transactionAmount;
            $summaryData['parentPl'] = $pTransactionAmount;

            $this->userSummaryUpdate($summaryData);

            $childId = $user->userId;

        }

    }

    // Function to get the Description
    public function setDescription($marketId)
    {
        $description = '';
        $betData = DB::table('tbl_bet_pending_1')->select(['sport','event','market','runner','mType'])
            ->where([ ['status',1],['mid',$marketId]])->first();

        if( $betData != null ){
            $description = $betData->sport. ' > ' .$betData->event. ' > '.$betData->mType. ' > '.$betData->market. ' > '.$betData->runner;
        }

        return $description;
    }

    // get Current Balance
    public function getCurrentBalance($uid,$amount,$type,$balance,$plBalance,$role)
    {
        if( $type == 'CREDIT' ){
            $newplBalance = $plBalance+$amount;
        }else{
            $newplBalance = $plBalance-$amount;
        }

        if( DB::table('tbl_user_info')->where([['uid',$uid]])->update(['pl_balance',$newplBalance]) ){

            if( $role != 'CLIENT' ){
                return round( ( $newplBalance ),2);
            }else{
                return round( ( $balance + $newplBalance ),2);
            }

        }else{

            if( $role != 'CLIENT' ){
                return round( ( $plBalance ),2);
            }else{
                return round( ( $balance + $plBalance ),2);
            }
        }

    }

    //user Expose Update
    public function userExposeUpdate($marketId){

        // Update User Market Expose
        if( DB::table('tbl_user_market_expose')
            ->where([['mid',$marketId]])->update(['status' => 2]) ){

            $userList = DB::table('tbl_bet_pending_1')
                ->where([ ['status',1],['mid',$marketId]])->distinct()->count('uid');

            if(!empty($userList)){
                foreach ($userList as $user ) {
                    $userId = $user->uid; $expose = 0;

                    $userExpose = DB::table('tbl_user_market_expose')->where([[ 'uid' => $userId , 'status' => 1]])->sum('expose');

                    if($userExpose != null){
                        $expose = round($userExpose,2);
                    }

                    DB::table('tbl_user_info')->where([['uid',$userId]])->update(['expose',$expose]);

                }
            }

        }
    }

    //user Summary Update
    public function userSummaryUpdate($summaryData){

        if( isset( $summaryData['uid'] ) ){

            $uid = $summaryData['uid'];
            $ownPl = isset($summaryData['ownPl']) ? $summaryData['ownPl'] : 0;
            $parentPl = isset($summaryData['ownPl']) ? $summaryData['parentPl'] : 0;

            $ownComm = isset($summaryData['ownComm']) ? $summaryData['ownComm'] : 0;
            $parentComm = isset($summaryData['parentComm']) ? $summaryData['parentComm'] : 0;

            $userSummary = DB::table('tbl_settlement_summary')
                ->where([['status',1],['uid',$uid]])->first();

            if( $userSummary != null ){

                if( isset( $summaryData['type'] ) && $summaryData['type'] == 'CREDIT' ){
                    $ownPl = $ownPl+$userSummary->ownPl;
                    $parentPl = $parentPl+$userSummary->parentPl;
                }else{
                    $ownPl = $userSummary->ownPl-$ownPl;
                    $parentPl = $userSummary->parentPl-$parentPl;
                }

                $ownComm = $userSummary->ownComm+$ownComm;
                $parentComm = $userSummary->parentComm-$parentComm;

                $updateArr = [ 'ownPl' => $ownPl, 'ownComm' => $ownComm,
                    'parentPl' => $parentPl, 'parentComm' => $parentComm ];

                DB::table('tbl_settlement_summary')->where([['status',1],['uid',$uid]])
                    ->update($updateArr);

            }else{

                $insertArr = [ 'uid' => $uid, 'ownPl' => $ownPl, 'ownComm' => $ownComm,
                    'parentPl' => $parentPl, 'parentComm' => $parentComm ];

                DB::table('tbl_settlement_summary')->insert($insertArr);

            }

        }
    }
    */
}
