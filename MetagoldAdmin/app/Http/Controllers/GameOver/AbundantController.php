<?php

namespace App\Http\Controllers\GameOver;
use App\Http\Controllers\Controller;
use App\Models\CommonModel;
use Illuminate\Support\Facades\DB;


class AbundantController extends Controller
{

    // Other Market
    public function otherMarket(){

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
        $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

        if( isset( $requestData[ 'eventId' ] ) && ( $requestData[ 'eventId' ] != null )
            && isset( $requestData[ 'marketId' ] ) && ( $requestData[ 'marketId' ] != null ) ) {

            $eventId = $requestData['eventId'];
            $marketId = $requestData['marketId'];
            $mType = $requestData['mType'];
            $winResult = 'Abundant';

            $betTbl = 'tbl_bet_pending_1';

            if( DB::table($betTbl)
                ->where([['eid',$eventId],['mid',$marketId],['result','PENDING'],['mType',$mType]])
                ->update(['result'=>'CANCELED']) ){

                // Update Market Result
                DB::table('tbl_other_markets')
                    ->where([['eid',$eventId],['mid',$marketId]])
                    ->update(['game_over'=>1,'result'=>$winResult]);

                // Update User Expose
                if( CommonModel::userExposeUpdate($marketId,$betTbl) ){
                    $response = [
                        'status' => 1,
                        "success" => [
                            "message" => "Game Over Successfully!"
                        ]
                    ];
                }else{
                    $response[ "error" ] = [
                        "message" => "Something wrong! Yet game over not done!"
                    ];
                }
            }else{
                $response[ "error" ] = [
                    "message" => "Something wrong! Yet game over not done!"
                ];
            }

        }

        return $response;

    }

    // Book Maker
    public function bookMaker(){

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
        $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

        if( isset( $requestData[ 'eventId' ] ) && ( $requestData[ 'eventId' ] != null )
            && isset( $requestData[ 'marketId' ] ) && ( $requestData[ 'marketId' ] != null ) ) {

            $eventId = $requestData['eventId'];
            $marketId = $requestData['marketId'];
            $mType = $requestData['mType'];
            $winResult = 'Abundant';

            $betTbl = 'tbl_bet_pending_1';

            if( DB::table($betTbl)
                ->where([['eid',$eventId],['mid',$marketId],['result','PENDING'],['mType',$mType]])
                ->update(['result'=>'CANCELED']) ){

                // Update Market Result
                DB::table('tbl_bookmaker')
                    ->where([['eid',$eventId],['mid',$marketId]])
                    ->update(['game_over'=>1,'result'=>$winResult]);

                // Update User Expose
                if( CommonModel::userExposeUpdate($marketId,$betTbl) ){
                    $response = [
                        "status" => 1,
                        "success" => [
                            "message" => "Game Over Successfully!"
                        ]
                    ];
                }else{
                    $response[ "error" ] = [
                        "message" => "Something wrong! Yet game over not done!"
                    ];
                }
            }else{
                $response[ "error" ] = [
                    "message" => "Something wrong! Yet game over not done!"
                ];
            }

        }

        return $response;

    }

    // Fancy
    public function fancy(){

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
        $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

        if( isset( $requestData[ 'eventId' ] ) && ( $requestData[ 'eventId' ] != null )
            && isset( $requestData[ 'marketId' ] ) && ( $requestData[ 'marketId' ] != null ) ) {

            $eventId = $requestData['eventId'];
            $marketId = $requestData['marketId'];
            $mType = $requestData['mType'];
            $winResult = 'Abundant';

            if( $mType == 'fancy' ){
                $tbl = 'tbl_fancy';
            }elseif ( $mType == 'fancy2' ){
                $tbl = 'tbl_fancy_2';
            }elseif ( $mType == 'fancy3' ){
                $tbl = 'tbl_fancy_3';
            }

            $betTbl = 'tbl_bet_pending_2';

            if( DB::table($betTbl)
                ->where([['eid',$eventId],['mid',$marketId],['result','PENDING'],['mType',$mType]])
                ->update(['result'=>'CANCELED']) ){

                // Update Market Result
                DB::table($tbl)->where([['eid',$eventId],['mid',$marketId]])
                    ->update(['game_over'=>1,'result'=>$winResult]);

                //Update User Expose
                if( CommonModel::userExposeUpdate($marketId,$betTbl) ){
                    $response = [
                        "status" => 1,
                        "success" => [
                            "message" => "Game Over Successfully!"
                        ]
                    ];
                }else{
                    $response[ "error" ] = [
                        "message" => "Something wrong! Yet game over not done!"
                    ];
                }
            }else{
                $response[ "error" ] = [
                    "message" => "Something wrong! Yet game over not done!"
                ];
            }

        }

        return $response;

    }

    // BallByBall And Khado
    public function ballbyballKhado(){

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
        $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

        if( isset( $requestData[ 'eventId' ] ) && ( $requestData[ 'eventId' ] != null )
            && isset( $requestData[ 'marketId' ] ) && ( $requestData[ 'marketId' ] != null ) ) {

            $eventId = $requestData['eventId'];
            $marketId = $requestData['marketId'];
            $mType = $requestData['mType'];
            $winResult = 'Abundant';

            if( $mType == 'khado' ){
                $tbl = 'tbl_khado_session';
            }else{
                $tbl = 'tbl_ball_session';
            }

            $betTbl = 'tbl_bet_pending_3';

            if( DB::table($betTbl)
                ->where([['eid',$eventId],['mid',$marketId],['result','PENDING'],['mType',$mType]])
                ->update(['result'=>'CANCELED']) ){

                // Update Market Result
                DB::table($tbl)
                    ->where([['eid',$eventId],['mid',$marketId]])
                    ->update(['game_over'=>1,'result'=>$winResult]);

                // Update User Expose
                if( CommonModel::userExposeUpdate($marketId,$betTbl) ){
                    $response = [
                        "status" => 1,
                        "success" => [
                            "message" => "Game Over Successfully!"
                        ]
                    ];
                }else{
                    $response[ "error" ] = [
                        "message" => "Something wrong! Yet game over not done!"
                    ];
                }
            }else{
                $response[ "error" ] = [
                    "message" => "Something wrong! Yet game over not done!"
                ];
            }

        }

        return $response;

    }

    // Jackpot And Casino
    public function jackpotCasino(){

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
        $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

        if( isset( $requestData[ 'eventId' ] ) && ( $requestData[ 'eventId' ] != null )
            && isset( $requestData[ 'marketId' ] ) && ( $requestData[ 'marketId' ] != null ) ) {

            $eventId = $requestData['eventId'];
            $marketId = $requestData['marketId'];
            $mType = $requestData['mType'];
            $winResult = 'Abundant';

            if( $mType == 'jackpot' ){
                $tbl = 'tbl_jackpot';
            }else{
                $tbl = 'tbl_casino';
            }

            $betTbl = 'tbl_bet_pending_4';

            if( DB::table($betTbl)
                ->where([['eid',$eventId],['mid',$marketId],['result','PENDING'],['mType',$mType]])
                ->update(['result'=>'CANCELED']) ){

                // Update Market Result
                DB::table($tbl)
                    ->where([['eid',$eventId],['mid',$marketId]])
                    ->update(['game_over'=>1,'result'=>$winResult]);

                // Update User Expose
                if( CommonModel::userExposeUpdate($marketId,$betTbl) ){
                    $response = [
                        "status" => 1,
                        "success" => [
                            "message" => "Game Over Successfully!"
                        ]
                    ];
                }else{
                    $response[ "error" ] = [
                        "message" => "Something wrong! Yet game over not done!"
                    ];
                }
            }else{
                $response[ "error" ] = [
                    "message" => "Something wrong! Yet game over not done!"
                ];
            }

        }

        return $response;

    }

    // Live Game
    public function liveGame(){

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
        $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

        if( isset( $requestData[ 'eventId' ] ) && ( $requestData[ 'eventId' ] != null )
            && isset( $requestData[ 'marketId' ] ) && ( $requestData[ 'marketId' ] != null ) ) {

            $eventId = $requestData['eventId'];
            $marketId = $requestData['marketId'];
            $mType = $requestData['mType'];
            $winResult = 'Abundant';

            $betTbl = 'tbl_bet_pending_teenpatti';

            if( DB::table($betTbl)
                ->where([['eid',$eventId],['mid',$marketId],['result','PENDING'],['mType',$mType]])
                ->update(['result'=>'CANCELED']) ){

                // Update Market Result
                DB::table('tbl_teenpatti_result')
                    ->where([['eid',$eventId],['mid',$marketId]])
                    ->update(['game_over'=>1,'result'=>$winResult]);

                // Update User Expose
                if( CommonModel::userExposeUpdate($marketId,$betTbl) ){
                    $response = [
                        "status" => 1,
                        "success" => [
                            "message" => "Game Over Successfully!"
                        ]
                    ];
                }else{
                    $response[ "error" ] = [
                        "message" => "Something wrong! Yet game over not done!"
                    ];
                }
            }else{
                $response[ "error" ] = [
                    "message" => "Something wrong! Yet game over not done!"
                ];
            }

        }

        return $response;

    }

    /*
    //user Expose Update
    public function userExposeUpdate($marketId,$betTbl){

        // Update User Market Expose
        if( DB::table('tbl_user_market_expose')
            ->where([['mid',$marketId]])->update(['status' => 2]) ) {

            $userList = DB::table($betTbl)
                ->where([['status', 1], ['mid', $marketId]])->distinct()->count('uid');

            if (!empty($userList)) {
                foreach ($userList as $user) {
                    $userId = $user->uid;
                    $expose = 0;

                    $userExpose = DB::table('tbl_user_market_expose')->where([['uid' => $userId, 'status' => 1]])->sum('expose');

                    if ($userExpose != null) {
                        $expose = round($userExpose, 2);
                    }

                    DB::table('tbl_user_info')->where([['uid', $userId]])->update(['expose', $expose]);

                }
            }
        }
    }
    */
}
