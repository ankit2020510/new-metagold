<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\OrderProfit;
use Illuminate\Support\Facades\Auth;
class AdminController extends Controller
{
    public function netProfit(){
        
        $orders = DB::table('tbl_order')
                    ->select('*')
                    ->where('status',1)->get();

    $sum = $goldBuy = $zincBuy = $silverBuy = $usdBuy = $gbpinrBuy = $gbpinr = $euroBuy = $copperBuy = $aluminiumBuy = $oilBuy = $gold = $silver = $zinc = $usd = $euro = $copper = $aluminium = $oil= 0;
    foreach ($orders as $key => $value) {

        $manualProfit[] = OrderProfit::get_net_profit($value->uid,$value->tradingSymbol,$value->instrumentToken,$value->name,$value->order_type,$value->trans_type,$value->lot,$value->buy_price,$value->sell_price,$value->price);

        //$total = OrderProfit::get_net_profit($value->uid,$value->tradingSymbol,$value->instrumentToken,$value->name,$value->order_type,$value->trans_type,$value->lot,$value->buy_price,$value->sell_price,$value->price);

          //$netProfit = $total['netProfit'];
          //$sum = $netProfit + $sum;
         // $name[] = $manualProfit['name'];
          //$type[] = $manualProfit['type'];
    }


    foreach ($manualProfit as $key => $val1) {

            if($val1['name'] == 'SILVER'){
              if ($val1['type'] == 'BUY') {
                  $silverBuy = $val1['netProfit'] + $silverBuy;
                  //$Arr[] = $silverBuy;
                }
                else{
                 $silver = $val1['netProfit'] + $silver;
                 //$Arr[] = $silver;
                }
            }

            if($val1['name'] == 'GOLD'){
               if ($val1['type'] == 'BUY') {
                 $goldBuy = $val1['netProfit'] + $goldBuy;
              
               }
               else{
                $gold = $val1['netProfit'] + $gold;
                //$Arr[] = $gold;
               }
            }

            if($val1['name'] == 'USDINR'){
              if ($val1['type'] == 'BUY') {
                  $usdBuy = $val1['netProfit'] + $usdBuy;
                }
                else{
                 $usd = $val1['netProfit'] + $usd; 
                }
            }
            
            if($val1['name'] == 'EURINR'){
               if ($val1['type'] == 'BUY') {
                 $euroBuy = $val1['netProfit'] + $euroBuy;
               }
               else{
                $euro = $val1['netProfit'] + $euro;
               }          
            }

            if($val1['name'] == 'GBPINR'){
              if ($val1['type'] == 'BUY') {
                  $gbpinrBuy = $val1['netProfit'] + $gbpinrBuy;
                }
                else{
                 $gbpinr = $val1['netProfit'] + $gbpinr; 
                }
            }
            
            if($val1['name'] == 'ALUMINIUM'){
               if ($val1['type'] == 'BUY') {
                 $aluminiumBuy = $val1['netProfit'] + $aluminiumBuy;
                 //$Arr[] = $aluminiumBuy;
               }
               else{
                $aluminium = $val1['netProfit'] + $aluminium;
               }          
            }

            if($val1['name'] == 'COPPER'){
              if ($val1['type'] == 'BUY') {
                  $copperBuy = $val1['netProfit'] + $copperBuy;
                }
                else{
                 $copper = $val1['netProfit'] + $copper;
                }
            }
            
            if($val1['name'] == 'CRUDEOIL'){
               if ($val1['type'] == 'BUY') {
                 $oilBuy = $val1['netProfit'] + $oilBuy;
               }
               else{
                $oil = $val1['netProfit'] + $oil;
               }          
            }

            if($val1['name'] == 'ZINC'){
              if ($val1['type'] == 'BUY') {
                  $zincBuy = $val1['netProfit'] + $zincBuy;
              }
              else{
                 $zinc = $val1['netProfit'] + $zinc; 
              }
          }
  }


  $profit[] = [
    "profit" => ['gold_buy'=>round($goldBuy),
                 'gold_sell'=>round($gold),
                 'silver_buy'=>round($silverBuy),
                 'silver_sell'=>round($silver),
                 'copper_buy'=>round($copperBuy),
                 'copper_sell'=>round($copper),
                 'crudeoil_buy'=>round($oilBuy),
                 'crudeoil_sell'=>round($oil),
                 'usdinr_buy'=>round($usdBuy),
                 'usdinr_sell'=>round($usd),
                 'aluminium_buy'=>round($aluminiumBuy),
                 'aluminium_sell'=>round($aluminium),
                 'gbpinr_buy'=>round($gbpinrBuy),
                 'gbpinr_sell'=>round($gbpinr),
                 'zinc_buy'=>round($zincBuy),
                 'zinc_sell'=>round($zinc),
                 'eurinr_buy'=>round($euroBuy),
                 'eurinr_sell'=>round($euro),
                 //'TOTAL'=> $sum
                ],
  ];
/*  echo $goldBuy."<br>";
  echo $gold."<br>";
  echo $usdBuy."<br>";*/
//echo "<pre>";
//print_r($manualProfit);
//print_r($profit);
//print_r($profit[0]['name']);
  


    $response = [ "status" => 1, "code" => 200, 'data' => $profit, "message" => "Successful"];
    return $response;
  }
}
