<?php

namespace App\Http\Controllers\History;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BetHistoryController extends Controller
{

    /**
     * Chip History List
     * @param null $uid
     * @return JsonResponse
     */
    public function list(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
             $uid = null;
            if( $uid == null ){
                $user = Auth::user(); $uid = $user->id;
            }else{
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }

            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            $type = 1; $searchDate = false; $start = $end = null;
            if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'type' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
                //print_r($requestData['type']);
                $type = $requestData[ 'type' ];
                $start = Carbon::parse($requestData[ 'start' ]);
                $end = Carbon::parse($requestData[ 'end' ]);
                $searchDate = true;
            }

            if( isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'type' ] ) ) {
                $type = $requestData[ 'type' ]; $now = Carbon::now();
                if( $requestData[ 'ftype' ] == 'week' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(7)->format('Y-m-d');
                }elseif ( $requestData[ 'ftype' ] == 'month' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(30)->format('Y-m-d');
                }else{
                    $end = $start = $now->format('Y-m-d');
                }

                $start = Carbon::parse($start); $end = Carbon::parse($end);
                $searchDate = true;
            }

            if( $user != null ){
                $userName = $user->name;
                $clientArr = [];

                if( $user->role != 1 && $user->role != 4){
                    $client = DB::table('tbl_user_child_data')->select(['clients'])
                        ->where([['uid',$uid]])->first();
                    if( $client != null ){
                        $clientArr = json_decode($client->clients);
                    }
                }
              
                if( $type == 2){
                    
                    $select = ['id','uid','name','price','buy_price','sell_price','instrumentToken','tradingSymbol','stopLoss','takeProfit','lot','order_type','trans_type','closeBuyPrice','closeSellPrice','updated_at','expiry','client','master','ip_address','status','profit'];
                    $query = DB::table('tbl_order_history')->select($select);
                    if( $user->role == 1 ){
                        $query->where([['status',5],['comm',1]]);
                    }elseif ( $user->role == 4 ){
                        $query->where([['uid',$uid],['status',5],['comm',1]]);
                    }else{
                        if( $clientArr != null ){
                            $query->where([['status',5],['comm',1]]);
                            $query->whereIn('uid',$clientArr);
                        }
                    }

                    if( $searchDate == true && $start != null && $end != null ){
                        $query->whereDate('updated_at','<=',$end->format('Y-m-d'))
                            ->whereDate('updated_at','>=',$start->format('Y-m-d'));
                    }

                    $list = $query->orderBy('updated_at', 'DESC')->get();

                    $i=0;
                    foreach ($list as $val) {
                            if($val->order_type == 'instant_execution'){
                          if($val->trans_type == 'BUY'){
                           $prc = $val->buy_price;
                           $description = $val->name.' > '.$val->lot.' > '.$val->trans_type.' AT '.$prc.' CLOSED AT '.$val->closeBuyPrice;
                         }else{
                           $prc = $val->sell_price;
                           $description = $val->name.' > '.$val->lot.' > '.$val->trans_type.' AT '.$prc.' CLOSED AT '.$val->closeSellPrice;
                         }
                       }elseif($val->order_type == 'buy_limit' || $val->order_type == 'sell_limit'){
                           if($val->trans_type == 'BUY'){
                             $prc = $val->price;
                             $description = $val->name.' > '.$val->lot.' > '.$val->trans_type.' AT '.$prc.' CLOSED AT '.$val->closeBuyPrice;
                         }else{
                              $prc = $val->price;
                             $description = $val->name.' > '.$val->lot.' > '.$val->trans_type.' AT '.$prc.' CLOSED AT '.$val->closeSellPrice;
                         }
                          
                       }
                      

                        $lists[] = $val;
                        $lists[$i]->description = $description;
                        $i++;
                    }

                    if( $list->isNotEmpty() ){
                        $response = [ 'status' => 1, 'data' => $lists, 'userName' => $userName ];
                    }else{ $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ]; }

                }elseif ( $type == 3){
                    
                    $select = ['id','uid','name','price','buy_price','sell_price','instrumentToken','tradingSymbol','stopLoss','takeProfit','lot','order_type','trans_type','closeBuyPrice','closeBuyPrice','updated_at','expiry','status','client','master','ip_address','profit'];
                    $query = DB::table('tbl_order')->select($select);
                    if( $user->role == 1 ){
                        $query->where([['status',1],['comm',0]]);
                    }elseif ( $user->role == 4 ){
                        $query->where([['uid',$uid],['status',1],['comm',0]]);
                    }else{
                        if( $clientArr != null ){
                            $query->where([['status',1],['comm',0]]);
                            $query->whereIn('uid',$clientArr);
                        }
                    }

                    if( $searchDate == true && $start != null && $end != null ){
                        $query->whereDate('updated_at','<=',$end->format('Y-m-d'))
                            ->whereDate('updated_at','>=',$start->format('Y-m-d'));
                    }

                    $list = $query->orderBy('updated_at', 'asc')->get();
                    $i=0;
                    foreach ($list as $val) {
                        if($val->order_type == 'instant_execution'){
                          if($val->trans_type == 'BUY'){
                           $prc = $val->buy_price;
                           $description = $val->name.' > '.$val->lot.' > '.$val->trans_type.' AT '.$prc;
                         }else{
                           $prc = $val->sell_price;
                           $description = $val->name.' > '.$val->lot.' > '.$val->trans_type.' AT '.$prc;
                         }
                       }elseif($val->order_type == 'buy_limit' || $val->order_type == 'sell_limit'){
                           if($val->trans_type == 'BUY'){
                             $prc = $val->price;
                             $description = $val->name.' > '.$val->lot.' > '.$val->trans_type.' AT '.$prc;
                         }else{
                              $prc = $val->price;
                             $description = $val->name.' > '.$val->lot.' > '.$val->trans_type.' AT '.$prc;
                         }
                          
                       }

                      

                        $lists[] = $val;
                        $lists[$i]->description = $description;
                        $i++;
                    }

                    if( $list->isNotEmpty() ){
                        $response = [ 'status' => 1, 'data' => $list, 'userName' => $userName ];
                    }else{ $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ]; }

                }elseif ( $type == 4){
                    
                    $select = ['id','uid','name','price','buy_price','sell_price','instrumentToken','tradingSymbol','stopLoss','takeProfit','lot','order_type','trans_type','closeBuyPrice','closeBuyPrice','updated_at','expiry','status','client','master','ip_address','profit'];
                    $query = DB::table('tbl_order')->select($select);
                    if( $user->role == 1 ){
                        $query->where('status',2);
                    }elseif ( $user->role == 4 ){
                        $query->where([['uid',$uid],['status',2]]);
                    }else{
                        if( $clientArr != null ){
                            $query->where('status',2);
                            $query->whereIn('uid',$clientArr);
                        }
                    }

                    if( $searchDate == true && $start != null && $end != null ){
                        $query->whereDate('updated_at','<=',$end->format('Y-m-d'))
                            ->whereDate('updated_at','>=',$start->format('Y-m-d'));
                    }

                    $list = $query->orderBy('updated_at', 'asc')->get();
                    $i=0;
                    foreach ($list as $val) {
                        if($val->order_type == 'instant_execution'){
                          if($val->trans_type == 'BUY'){
                           $prc = $val->buy_price;
                           $description = $val->name.' > '.$val->lot.' > '.$val->trans_type.' AT '.$prc;
                         }else{
                           $prc = $val->sell_price;
                           $description = $val->name.' > '.$val->lot.' > '.$val->trans_type.' AT '.$prc;
                         }
                       }elseif($val->order_type == 'buy_limit' || $val->order_type == 'sell_limit'){
                           if($val->trans_type == 'BUY'){
                             $prc = $val->price;
                             $description = $val->name.' > '.$val->lot.' > '.$val->trans_type.' AT '.$prc;
                         }else{
                              $prc = $val->price;
                             $description = $val->name.' > '.$val->lot.' > '.$val->trans_type.' AT '.$prc;
                         }
                          
                       }

                      

                        $lists[] = $val;
                        $lists[$i]->description = $description;
                        $i++;
                    }

                    if( $list->isNotEmpty() ){
                        $response = [ 'status' => 1, 'data' => $list, 'userName' => $userName ];
                    }else{ $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ]; }

                }elseif( $request->route('id') != '' || $request->route('id') != null){
                   
                    $select = ['id','uid','name','price','buy_price','sell_price','instrumentToken','tradingSymbol','stopLoss','takeProfit','lot','order_type','trans_type','closeBuyPrice','closeBuyPrice','updated_at','expiry','status','client','master','ip_address'];
                    $query = DB::table('tbl_order')->select($select);
                    if( $user->role == 1 ){
                        $query->where([['status',1],['comm',0],['tradingSymbol',$request->route('id')]]);
                    }elseif ( $user->role == 4 ){
                        $query->where([['uid',$uid],['status',1],['comm',0],['tradingSymbol',$request->route('id')]]);
                    }else{
                        if( $clientArr != null ){
                            $query->where([['status',1],['comm',0],['tradingSymbol',$request->route('id')]]);
                            $query->whereIn('uid',$clientArr);
                        }
                    }

                    if( $searchDate == true && $start != null && $end != null ){
                        $query->whereDate('updated_at','<=',$end->format('Y-m-d'))
                            ->whereDate('updated_at','>=',$start->format('Y-m-d'));
                    }

                    $list = $query->orderBy('updated_at', 'asc')->get();
                    
                    $i=0;
                    foreach ($list as $val) {
                        if($val->order_type == 'instant_execution'){
                         if($val->trans_type == 'BUY'){
                           $prc = $val->buy_price;
                         }else{
                           $prc = $val->sell_price;
                         }
                        }else{
                             $prc = $val->price;
                         }
                      

                        $lists[] = $val;
                        $lists[$i]->description = $val->name.'>'.$val->lot.'>'.$prc;
                    }

                    if( $list->isNotEmpty() ){
                        $response = [ 'status' => 1, 'data' => $lists, 'userName' => $userName ];
                    }else{ $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ]; }


                }else{

                    $data = [];
                    $select = ['id','price','size','rate','win','loss','client','master','bType','mType','result','description','ip_address','updated_on','status'];
                    for( $i=1;$i<=5;$i++ ){
                        if( $i == 5 ){
                            $tbl = 'tbl_bet_pending_teenpatti';
                        }else{
                            $tbl = 'tbl_bet_pending_'.$i;
                        }

                        $query = DB::table($tbl)->select($select);
                        if( $user->role == 1 ){
                            $query->where([['systemId',$user->systemId]]);
                        }elseif ( $user->role == 4 ){
                            $query->where([['uid',$uid],['systemId',$user->systemId]]);
                        }else{
                            if( $clientArr != null ){
                                $query->where([['systemId',$user->systemId]]);
                                $query->whereIn('uid',$clientArr);
                            }
                        }

                        if( $searchDate == true && $start != null && $end != null ){
                            $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                                ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                        }

                        $list = $query->orderBy('updated_on', 'asc')->limit(100)->get();

                        if( $list->isNotEmpty() ){
                            foreach ( $list as $item ){
                                $data[] = $item;
                            }
                        }

                    }

                    if( $data != null ){
                        $response = [ 'status' => 1, 'data' => $data , 'userName' => $userName ];
                    }else{ $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ]; }

                }

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
