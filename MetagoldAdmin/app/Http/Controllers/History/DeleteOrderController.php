<?php

namespace App\Http\Controllers\History;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use App\Helpers\LogActivity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\OrderDetail;
use App\Models\UserDetail;



class DeleteOrderController extends Controller
{

    /**
     * Chip History List
     * @param null $uid
     * @return JsonResponse
     */
    public function list(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];
        /*print_r($request->route('id'));
    die('delete');*/
        try{
            
            $orderId = $request->route('id');

            $status = ['status' => 2];
            
           $adminUpdate = DB::table('tbl_transaction_admin')->where('eid',$orderId)->update($status);
            $clientUpdate = DB::table('tbl_transaction_client')->where('eid',$orderId)->update($status);
            $parentUpdate = DB::table('tbl_transaction_parent')->where('eid',$orderId)->update($status);

            $order =OrderDetail::where([['id',$orderId],['status',1],['comm',0]])->select('uid','commission','orderMargin')->first();
            $uid = $order->uid;
            $commission = $order->commission;
            $expose = $order->orderMargin;

            $cUser = DB::table('tbl_user_profit_loss')->where('clientId',$uid)->select('userId','actual_profit_loss')->get();
            
            foreach ($cUser as  $user) {
                

                 $userId = $user->userId;
                 $actual_profit_loss = $user->actual_profit_loss;
                 if($actual_profit_loss != 0){ 
                 
                     $amount = ($commission * $actual_profit_loss)/100;
                     $updateArr = [
                        'pl_balance' => DB::raw('pl_balance - ' . round($amount,0))
                           ];
        
                     $order = UserDetail::where('uid',$userId)->update($updateArr);
                 }else{         
                     $updateArr = [
                           'pl_balance' => DB::raw('pl_balance + ' . round($commission,0)),
                           'expose' => DB::raw('expose - ' . round($expose,0))
                           ];
        
                     $order = UserDetail::where('uid',$userId)->update($updateArr);
                  }

            }

                 
                 $arr = [ 'status' => 2, 'margin'=> 1 ];
                 $order = OrderDetail::where([['id',$orderId],['uid',$uid]])->update($arr);
                $log = 'Order Deleted';
                LogActivity::addToLog($log);
                $response = ['status'=>1 , 'code'=> 200, 'message'=> 'Order deleted successfully'];
                return $response;
               

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
