<?php

namespace App\Http\Controllers\Complain;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class ComplainController extends Controller
{

  public function list(Request $request)
       {   
            
             $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];           

             $query = DB::table('tbl_complain')->select('*');
        
             if($request->type == 'RESOLVED'){
               $complainList = $query->where('status',0)->get();
             }elseif($request->type == 'FILED'){
                $complainList = $query->where('status',1)->get();
             }elseif($request->type == 'PROCESS'){
                $complainList = $query->where('status',2)->get();
             }else{
                $complainList = $query->whereIn('status',[0,1,2])->get();
             }
    
             if(!empty($complainList)){

               $response = [ "status" => 1 , "code" => 200 ,'data'=>$complainList, "message" => "Data Found !! " ];  
             }else{
                 $response = [ "status" => 0 , "code" => 200 ,'data'=>null, "message" => "Data Foundss !! " ];
             }

   return $response;

    }

public function manage(Request $request)
       {   
            
             $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];           
              
             if($request->id !='' && $request->uid !='' ){
              $remark = ' ';
                if($request->remark != ' '){
                  $remark = $request->remark;
                }

              $arr = ['status'=>$request->status, 'remark'=>$request->remark, 'updated_at'=>date('Y-m-d h:i:s')];

              $data = DB::table('tbl_complain')->where([['id',$request->id],['uid',$request->uid]])->update($arr);

               $log = 'Complain modified';
               LogActivity::addToLog($log);

               $response = [ "status" => 1 , "code" => 200 , "message" => "Complain has been modified !! " ];  
             }else{
                 $response = [ "status" => 0 , "code" => 200 ,'data'=>null, "message" => "Data Foundss !! " ];
             }

   return $response;

    }
   

 }























?>