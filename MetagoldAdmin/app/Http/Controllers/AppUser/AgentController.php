<?php

namespace App\Http\Controllers\AppUser;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;
use App\Helpers\LogActivity;

class AgentController extends Controller
{

    /**
     * Agent Manage List
     * Action - Get
     * Created at JULY 2020 by dream
     */
    public function manage()
    {
        try{
            $data = Agent::getList();

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Agent Create
     * Action - Post
     * Created at JULY2020 by dream
     */
    public function create(Request $request)
    {
        try{
            $response = Agent::create($request->input());

            $log = 'Agent created';
            LogActivity::addToLog($log);

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
