<?php

namespace App\Http\Controllers\AppUser;
use App\Http\Controllers\Controller;
use App\Models\CommonModel;
use App\Models\UserInfo;
use App\User;
use App\Helpers\LogActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ActionController extends Controller
{

    /**
     * User User Data
     * Action - Post
     * Created at APR 2020 by dream
     */
    public function updateUserData()
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];
        return $response;exit;
        $userList = DB::table('tbl_user')->where('role',4)
            ->select(['id','parentId'])->get();
        if( $userList->isNotEmpty() ){
            foreach ($userList as $user){
                $pUser = DB::table('tbl_user')->select('username')
                    ->where('id',$user->parentId)->first();
                if( $pUser != null ){
                    $pName = $pUser->username;
                    DB::table('tbl_user_info')->where('uid',$user->id)->update(['pName' => $pName]);
                }
            }
        }
    }
    /**
     * Clear User Data
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function clearUserData()
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];
        //return $response;exit;
        try {

            DB::table('tbl_order')->delete();
            DB::table('tbl_bet_pending_1')->delete();
            DB::table('tbl_bet_pending_2')->delete();
            DB::table('tbl_bet_pending_3')->delete();
            DB::table('tbl_bet_pending_4')->delete();
            DB::table('tbl_bet_pending_teenpatti')->delete();
            DB::table('tbl_bet_history')->delete();
            DB::table('tbl_bet_history_teenpatti')->delete();
            DB::table('tbl_book_bookmaker')->delete();
            DB::table('tbl_book_matchodd')->delete();
            DB::table('tbl_user_market_expose')->delete();
            DB::table('tbl_user_market_expose_2')->delete();
            DB::table('tbl_transaction_admin')->where([['eType','!=',1]])->delete();
            DB::table('tbl_transaction_client')->where([['eType','!=',1]])->delete();
            DB::table('tbl_transaction_parent')->where([['eType','!=',1]])->delete();
            DB::table('tbl_transaction_history')->where([['eType','!=',1]])->delete();
            //DB::table('tbl_teenpatti_result')->delete();
            DB::table('tbl_user_info')->where([['id','!=',0]])->update(['pl_balance'=>0,'expose'=>0]);
            DB::table('tbl_settlement_summary')->where([['id','!=',0]])
                ->update(['ownPl'=>0,'ownCash'=>0, 'ownComm'=>0,
                    'parentCash'=>0,'parentComm'=>0,'parentPl'=>0,'lastTransId'=>0]);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }
    /**
     * Check User Data
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function checkUserData()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try {
            $pUser = Auth::user(); // current user id
            $pUserInfo = UserInfo::where('uid',$pUser->id)->first();

            if( $pUser->role == 1 && $pUserInfo->pl == 0){
                $pl = 100;
                $commission = 100;
            }else{
                $pl = $pUserInfo->pl;
                $commission = $pUserInfo->commission;
            }

            $data = [
                'balance' => $pUserInfo->balance,
                'pl' => $pl,
                'username' => $pUser->name,
                'commission'=>$commission
            ];

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Check User Data Both
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function checkUserDataBoth($uid)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try {
            $cUser = User::where('id',$uid)->first();
            $pUser = User::where('id',$cUser->parentId)->first();
            $pUserInfo = UserInfo::where('uid',$cUser->parentId)->first();

            if( $pUser->role == 1 && $pUserInfo->pl == 0){
                $pl = 100;
            }else{
                $pl = $pUserInfo->pl;
            }

            $pData = [
                'balance' => $pUserInfo->balance,
                'pl' => $pl,
                'username' => $pUser->username,
            ];

            $cUserInfo = UserInfo::where('uid',$uid)->first();

            if( $cUser->role == 4 ){
                if( $cUserInfo->pl_balance < 0 ){
                    $available = $cUserInfo->balance+$cUserInfo->pl_balance-$cUserInfo->expose;
                }else{
                    $available = $cUserInfo->balance-$cUserInfo->expose;
                }
            }else{
                $available = $cUserInfo->balance;
            }

            $cData = [
                'balance' => $available,
                'pl' => $cUserInfo->pl,
                'username' => $cUser->username,
            ];

            $data = [ 'pData' => $pData , 'cData' => $cData ];

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * doDelete
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function doDelete($uid)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try {

            $user = DB::table('tbl_user')->where([['id',$uid],['status','!=',2]])->first();
            
            if($user->roleName == 'AG'){
                DB::table('tbl_user')->where([['id',$uid]])->update(['status' => 2]);
                DB::table('oauth_access_tokens')->where([['user_id',$uid]])->delete();
                $log = 'Agent deleted';
                LogActivity::addToLog($log);
              $response = [ 'status' => 1 , "success" => [ "message" => "Agent deleted successfully!" ] ];
              return $response;
              exit();
            }
            
            if( $user != null ){
                $cUser = Auth::user(); // current user id
                if( $cUser->id == $user->parentId ){
                    if( $user->role == 4 ){
                        $userInfo = UserInfo::where('uid',$uid)->first();
                        if( $userInfo->balance == '0' && $userInfo->pl_balance == '0' && $userInfo->expose == '0' ){
                            if( DB::table('tbl_user')->where([['id',$uid]])->update(['status' => 2]) ){
                                DB::table('oauth_access_tokens')->where([['user_id',$uid]])->delete();
                                $log = 'user deleted';
                                LogActivity::addToLog($log);
                                $response = [ 'status' => 1 , "success" => [ "message" => "User Deleted successfully!" ] ];
                            }else{
                                $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! User can not deleted !" ] ];
                            }
                        }else{
                            $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! P&L Or Balance OrExposer is pending !" ] ];
                        }
                    }elseif( $user->role == 5 ){
                        if( DB::table('tbl_user')->where([['id',$uid]])->update(['status' => 2]) ){
                            DB::table('oauth_access_tokens')->where([['user_id',$uid]])->delete();
                            $log = 'user deleted';
                            LogActivity::addToLog($log);
                            $response = [ 'status' => 1 , "success" => [ "message" => "User Deleted successfully!" ] ];
                        }else{
                            $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! User can not deleted !" ] ];
                        }
                    }else{
                        $childUser = DB::table('tbl_user')->where([['parentId',$uid],['status','!=',2]])->first();

                        if( $childUser != null ){
                            $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! Still Child User is Active !" ] ];
                        }else{
                            $userInfo = UserInfo::where('uid',$uid)->first();
                            if( $userInfo->pl_balance == '0' && $userInfo->balance == '0'){
                                if( DB::table('tbl_user')->where([['id',$uid]])->update(['status' => 2]) ){
                                    DB::table('oauth_access_tokens')->where([['user_id',$uid]])->delete();
                                    $log = 'Agent deleted';
                                    LogActivity::addToLog($log);
                                    $response = [ 'status' => 1 , "success" => [ "message" => "User deleted successfully!" ] ];
                                }else{
                                    $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! User can not deleted !" ] ];
                                }
                            }else{
                                $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! P&L Or Balance is pending !" ] ];
                            }
                        }


                    }

                }else{
                    $response = [ 'status' => 0 , "error" => [ "message" => "Something Wrong! Only parent can delete!" ] ];
                }
            }else{
                $response = [ 'status' => 1 , "success" => [ "message" => "This user already deleted !" ] ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Client Reset Password
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function resetPassword(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        try{

            $this->validate($request, [
                'uid' => 'required',
                'password' => 'required|min:6',
            ]);

            $data = $request->input();
            $user = User::find($data['uid']);
            $user->password = bcrypt( $data['password'] );

            if( $user->save() ){
                DB::table('oauth_access_tokens')
                    ->where([['user_id',$user->id]])->delete();
                $response = [
                    'status' => 1,
                    'success' => [
                        'message' => 'Reset Password successfully!'
                    ]
                ];
            }

            $log = 'Reset Password for'.$data['uid'];
                LogActivity::addToLog($log);
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Client Deposit Balance
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function depositBalance(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        try{

            $this->validate($request, [
                'uid' => 'required',
                'balance' => 'required',
            ]);

            $data = $request->input();
            $user = User::where('id',$data['uid'])->first();

            $parentId = $user->parentId;
            $amount = $data['balance'];
            $remark = $data['remark'];
            $uid = $data['uid'];
            $type = 'DEPOSIT';

            // child user
            $userInfo = UserInfo::where('uid',$data['uid'])->first();
            $userInfo->balance = ($userInfo->balance+$data['balance']);

            // parent user
            $pUser = UserInfo::where('uid',$parentId)->first();
            $pUser->balance = ($pUser->balance-$data['balance']);

            if( $pUser->balance >= 0 && $userInfo->save() && $pUser->save() ){

                CommonModel::updateChipTransaction($uid,$parentId,$amount,$remark,$type);

                $response = [
                    'status' => 1,
                    'success' => [
                        'message' => 'Deposit successfully!'
                    ]
                ];
            }

            $log = 'Deposited balance to '.$data['uid'];
                LogActivity::addToLog($log);
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Client Withdrawal Balance
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function withdrawalBalance(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        try{
            $this->validate($request, [
                'uid' => 'required',
                'balance' => 'required',
            ]);

            $data = $request->input();
            $user = User::where('id',$data['uid'])->first();

            $parentId = $user->parentId;
            $amount = $data['balance'];
            $remark = $data['remark'];
            $uid = $data['uid'];
            $type = 'WITHDRAWAL';

            // child user
            $userInfo = UserInfo::where('uid',$uid)->first();
            $userInfo->balance = ($userInfo->balance-$amount);

            if( $user->role == 4 ){
                if( $userInfo->pl_balance < 0 ){
                    $available = $userInfo->balance+$userInfo->pl_balance-$userInfo->expose;
                }else{
                    $available = $userInfo->balance-$userInfo->expose;
                }
            }else{
                $available = $userInfo->balance;
            }

            // parent user
            $pUser = UserInfo::where('uid',$parentId)->first();
            $pUser->balance = ($pUser->balance+$amount);

            if( $available >= 0 && $userInfo->save() && $pUser->save() ){
                CommonModel::updateChipTransaction($uid,$parentId,$amount,$remark,$type);
                $response = [
                    'status' => 1,
                    'success' => [
                        'message' => 'Withdrawal successfully!'
                    ]
                ];
            }
            $log = 'withdraw successfully for'.$data['uid'];
                LogActivity::addToLog($log);
            return response()->json($response);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
