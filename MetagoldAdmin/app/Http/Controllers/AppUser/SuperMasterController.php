<?php

namespace App\Http\Controllers\AppUser;

use App\Http\Controllers\Controller;
use App\Models\SuperMaster;
use App\Helpers\LogActivity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class SuperMasterController extends Controller
{

    /**
     * Super Master Manage List
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function manage()
    {
        try{
            $data = SuperMaster::getList();

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Super Master Reference List
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function reference($id)
    {
        try{
            $data = SuperMaster::getList($id);

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Super Master Create
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function create(Request $request)
    {
        try{

//            $this->validate($request, [
//                'name' => 'required|min:3',
//                'username' => 'required|unique:tbl_user',
//                'password' => 'required|min:6',
//                'pl' => 'required|min:1',
//            ]);

            $response = SuperMaster::create($request->input());

            $log = 'SuperMaster created';
            LogActivity::addToLog($log);
            
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
