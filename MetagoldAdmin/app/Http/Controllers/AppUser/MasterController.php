<?php

namespace App\Http\Controllers\AppUser;
use App\Http\Controllers\Controller;
use App\Models\Master;
use App\Helpers\LogActivity;
use Illuminate\Http\Request;


class MasterController extends Controller
{

    /**
     * Master Manage List
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function manage()
    {
        try{
            $data = Master::getList();

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Master Reference List
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function reference($id)
    {
        try{
            $data = Master::getList($id);
            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Master Create
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function create(Request $request)
    {
        try{
//            $this->validate($request, [
//                'name' => 'required|min:3',
//                'username' => 'required|unique:tbl_user',
//                'password' => 'required|min:6',
//                'pl' => 'required|min:1',
//            ]);

            $response = Master::create($request->input());

            $log = 'Master created';
            LogActivity::addToLog($log);

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
