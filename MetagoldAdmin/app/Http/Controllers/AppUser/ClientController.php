<?php

namespace App\Http\Controllers\AppUser;
use App\Http\Controllers\Controller;
use App\Models\Client;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;


class ClientController extends Controller
{

    /**
     * Client Manage List
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function manage()
    {
        try{
            $data = Client::getList();
            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Client Reference List
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function reference($id)
    {
        try{
            $data = Client::getList($id);

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Master Create
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function create(Request $request)
    {
        try{
//            $this->validate($request, [
//                'name' => 'required|min:3',
//                'username' => 'required|unique:tbl_user',
//                'password' => 'required|min:6',
//            ]);

            $response = Client::create($request->input());

            $log = 'Client created';
            LogActivity::addToLog($log);

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
