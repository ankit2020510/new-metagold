<?php

namespace App\Http\Controllers\OrderMargin;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\OrderMargin;
use Illuminate\Support\Facades\Redis;
class OrderMarginController extends Controller
{


   public function orderMargin(Request $request)
   { 

        $startTime = time();
        $endTime = time() + 60;
        while ($endTime > time()) {
            usleep(900000);
            //die('kkkkkllllllll');
            $client = DB::table('tbl_user_info')->select('uid','balance','pl_balance')
                               ->whereExists(function($query)
                                 {
                                   $query->select(DB::raw(1))
                                         ->from('tbl_order')
                                         ->whereRaw('tbl_order.uid = tbl_user_info.uid');

                                 })->get();

            foreach ($client as $val) {
                
                 $netProfit = 0;
                 $order = OrderMargin::getList($val->uid);
                     
                // print_r($order);  
                 foreach ($order as $val1) {
                    $netProfitMargin = $this->getOrderProfit($val1);
                    $netProfit+= $netProfitMargin['netProfit'];
                 }
                
                 $balanceCut = round( 0.95 * ( $val->balance + $val->pl_balance ));
                 if($netProfit < 0){
                      
                       $netProfit = ltrim($netProfit, '-');
                       
                       if($netProfit >= $balanceCut){
                              
                          foreach ($order as $value){

                            $netProfitMargin = $this->getOrderProfit($value);
                            $netProfitFinal = $netProfitMargin['netProfit'];
                            $depth = $this->getCurrentPrice($value->instrumentToken, $value->tradingSymbol);
                            $currentBuyPrice = $depth['currentBuy'];
                            $currentSellPrice = $depth['currentSell'];

                            $update = $this->updateOrder($value->uid,$value->instrumentToken,$netProfitFinal,$value->id,$currentBuyPrice,$currentSellPrice);

                          }

                      }
                  }
                //die('opopopo');
              //}//iff
            }
            
               
        }
         $response = ['status'=>1, 'code'=>200, 'message'=>'successfully updated !!!'];
     return $response;

    }


  
  public function getOrderProfit($val){

          $depth = $this->getCurrentPrice($val->instrumentToken,$val->tradingSymbol);
          $currentBuyPrice = $depth['currentBuy'];
          $currentSellPrice = $depth['currentSell'];

                if($val->order_type == 'instant_execution'){
                      if($val->trans_type == 'BUY'){
                           $profit = $currentSellPrice-$val->buy_price;
                           $netProfit = $profit*$val->lot;
                           $margin = $val->buy_price * $val->lot;
                           $margin = $margin * 2/100; 
                      }else{
                           $profit = $val->sell_price-$currentBuyPrice;
                           $netProfit = $profit*$val->lot;
                           $margin = $val->sell_price * $val->lot;
                           $margin = $margin * 2/100;
                       }//close checking of trans_type
                  }elseif($val->order_type == 'buy_limit'){
                      if($val->trans_type == 'BUY'){
                           $profit = $currentSellPrice-$val->price;
                           $netProfit = $profit*$val->lot;
                           $margin = $val->price * $val->lot;
                           $margin = $margin*2/100;
                      }//close checking of trans_type
                }else{
                     if($val->trans_type == 'SELL'){
                           $profit = $val->price-$currentBuyPrice;
                           $netProfit = $profit*$val->lot;
                           $margin = $val->price * $val->lot;
                           $margin = $margin*2/100;
                      }//close checking of trans_type
                }

          $response = ['margin'=>$margin, 'netProfit'=>$netProfit];
          return $response;
  }


  public function getCurrentPrice($instrumentToken, $tradingSymbol){

         
       $cache = Redis::connection();
       $key = 'Mcx_Market';
       $result=$cache->get($key);
       $arrData = json_decode($result);
       if(!empty($arrData)){
       foreach ($arrData as $key => $val) {
          
         if($key == $tradingSymbol && $val->instrument_token == $instrumentToken){
              $depth = $val->depth;
              $currentBuy = $depth->buy[0]->price;
              $currentSell = $depth->sell[0]->price;      
                         
          }
       }
          $depth = ['currentBuy'=>$currentBuy,'currentSell'=>$currentSell];

          return $depth;
        }else{

          return 0;
        }

 }

 public function updateOrder($uid,$instrumentToken,$profit,$orderId,$currentBuyPrice,$currentSellPrice){

       //$order = DB::table('tbl_user_info')->where([['uid',$orders->uid]])->update($pl);
      // $userPl = DB::table('tbl_user_info')->where('uid',$uid)->select('pl_balance')->first();
         
     /*  $pl = $userPl->pl_balance + $profit; 
       $pl_balance = ['pl_balance' => $pl] ;*/
        $pl_balance = [
            'pl_balance' => DB::raw('pl_balance + ' . round($profit))
        ];

       $status = ['status'=> 5, 'profit'=>$profit,'closeBuyPrice'=>$currentSellPrice,'closeSellPrice'=>$currentBuyPrice, 'balanceLow'=> 1];
       //print_r($uid); echo "<pre>"; print_r($userPl->pl_balance); die('ankit');
       $orderStatus = DB::table('tbl_order')->where([['id',$orderId],['uid',$uid]])->update($status);

      // $order = DB::table('tbl_user_info')->where('uid',$uid)->update($pl_balance);
        
      return 1;

  }


}