<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Models\CommonModel;
use App\Models\Sport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class BlockUnblockController extends Controller
{

    /**
     * Event
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function event($id)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $user = Auth::user(); $uid = Auth::id(); $eventId = $id;

            if( $user->role != 1 ){

                $userArr = CommonModel::userChildData($uid);

                $checkStatus = DB::table('tbl_user_event_status')->select(['uid'])
                    ->where([['uid',$uid],['eid',$eventId]])->first();

                if( $checkStatus != null ){
                    // do unblock
                    foreach ( $userArr as $usrId ){
                        DB::table('tbl_user_event_status')
                            ->where([['uid',$usrId],['eid',$eventId],['byuserId',$uid]])
                            ->delete();
                    }

                    $message = 'Unblock successfully!';

                }else{
                    // do block
                    foreach ( $userArr as $usrId ){
                        $userData = ['uid' => $usrId,'eid' => $eventId,'byuserId' => $uid ];
                        DB::table('tbl_user_event_status')->insert($userData);
                    }

                    $message = 'Block successfully!';

                }

                $response = [
                    'status' => 1 , "success" => [ "message" => $message ]
                ];

            }else{

                $event = DB::connection('mysql2')->table('tbl_event')
                    ->where([['eventId',$eventId]])->first();

                if( $event != null ){

                    if( $event->is_block == 1 ){
                        $isBlock = 0; $message = 'Unblock successfully!';
                    }else{
                        $isBlock = 1; $message = 'Block successfully!';
                    }

                    if( DB::connection('mysql2')->table('tbl_event')
                        ->where([['eventId',$eventId]])->update(['is_block' => $isBlock]) ){
                        CommonModel::redisEventList($eventId,'is_block');
                        $response = [
                            'status' => 1 , "success" => [ "message" => $message ]
                        ];
                    }

                }else{
                    $event = DB::table('tbl_live_game')
                        ->where([['eventId',$eventId]])->first();
                    if( $event != null ){
                        if( $event->is_block == 1 ){
                            $isBlock = 0; $message = 'Unblock successfully!';
                        }else{
                            $isBlock = 1; $message = 'Block successfully!';
                        }
                        if( DB::table('tbl_live_game')
                            ->where([['eventId',$eventId]])->update(['is_block' => $isBlock]) ){
                            $response = [
                                'status' => 1 , "success" => [ "message" => $message ]
                            ];
                        }

                    }
                }

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }

    /**
     * Sport
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function sport($id)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];
        try{

            $user = Auth::user(); $uid = Auth::id(); $sportId = $id;

            if( $user->role != 1 ){

                $userArr = CommonModel::userChildData($uid);

                $checkStatus = DB::table('tbl_user_sport_status')->select(['uid'])
                    ->where([['uid',$uid],['sid',$sportId]])->first();

                if( $checkStatus != null ){
                    // do unblock
                    foreach ( $userArr as $usrId ){
                        DB::table('tbl_user_sport_status')
                            ->where([['uid',$usrId],['sid',$sportId],['byuserId',$uid]])
                            ->delete();
                    }

                    $message = 'Unblock successfully!';

                }else{
                    // do block
                    foreach ( $userArr as $usrId ){
                        $userData = ['uid' => $usrId,'sid' => $sportId,'byuserId' => $uid ];
                        DB::table('tbl_user_sport_status')->insert($userData);
                    }

                    $message = 'Block successfully!';

                }

                $response = [
                    'status' => 1 , "success" => [ "message" => $message ]
                ];

            }else{

                $sport = Sport::where([['sportId',$sportId]])->first();

                if( $sport != null ){

                    if( $sport->is_block == 1 ){
                        $sport->is_block = 0;
                        $message = 'Block successfully!';
                    }else{
                        $sport->is_block = 1;
                        $message = 'Unblock successfully!';
                    }

                    if( $sport->save() ){
                        DB::connection('mysql2')->table('tbl_sport')
                            ->where([['sportId',$sportId]])->update(['is_block' => $sport->is_block ]);
                        $response = [
                            'status' => 1 , "success" => [ "message" => $message ]
                        ];
                    }

                }

            }
            $this->redisSportUpdate();
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    public function redisSportUpdate()
    {
        $redis = Redis::connection();
        $sportList = DB::connection('mysql2')->table('tbl_sport')->where('status',1)->get();
        if($sportList->isNotEmpty()) {
            $sportData = json_encode($sportList);
            $redis->set('SportList', $sportData);
            foreach ($sportList as $sport_List) {
                $sportId = $sport_List->sportId;
                $sportDT = json_encode($sport_List);
                $redis->set('Sport_' . $sportId, $sportDT);
            }
        }
    }
}
