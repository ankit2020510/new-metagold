<?php

namespace App\Http\Controllers\Dashboard;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class MarketBlockController extends Controller
{
	public function block(Request $request)
	{
		if(isset($request->type) && $request->type == 'all' ){

			if($request->active == 1){

               $updateStatus = ['is_block' => $request->active];
               DB::table('tbl_mcxmarket2')->where('is_block','=',0)->update($updateStatus);
               $message = "Block successfully";
             }else{

               $updateStatus = ['is_block' => $request->active];
               DB::table('tbl_mcxmarket2')->where('is_block','=',1)->update($updateStatus);
               $message = "UnBlock successfully";
             }

		}else{

			if($request->active == 1){

				$updateStatus = ['is_block' => $request->active]; 
				DB::table('tbl_mcxmarket2')->where('tradingSymbol',$request->type)->update($updateStatus);

				$message = "Block successfully";

			}else{


				$updateStatus = ['is_block' => $request->active]; 
				DB::table('tbl_mcxmarket2')->where('tradingSymbol',$request->type)->update($updateStatus);

				$message = "UnBlock successfully";

			}
		}

		$response = ['status'=>1,'code'=>200, 'message'=>$message];
		
		return $response;
		
	}

}
