<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Models\CommonModel;
use App\Models\Sport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use App\Http\Traits\CommissionTrait;


class ActionController extends Controller
{
    use CommissionTrait;
    /**
     * get User Data
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function getUserData()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( auth()->user() ){

                $user = auth()->user();
                if( $user->role == 6 ){
                    $user->id = 1;
                }

                $userData = DB::table('tbl_user_info')
                    ->select(['balance','pl_balance','pl_commission'])
                    ->where([['uid',$user->id]])->first();

                if( $userData != null ){
                    $data = [
                        'name' => $user->username,
                        'last_logged_on' => $user->last_logged_on,
                        'balance' => round($userData->balance),
                        'pl_balance' => round($userData->pl_balance),
                        'commission'=>round($userData->pl_commission),
                        'role' => $user->roleName,
                        'uid' => $user->id
                    ];

                    if( $user->roleName == 'ADMIN' || $user->roleName == 'SA' ){
                        $cUserSummaryData = DB::table('tbl_settlement_summary')
                            ->select(['parentPl','parentComm','parentCash'])
                            ->where([['pid',$user->id],['status',1],['systemId','!=',1]])->get();
                        $ownPl = 0;
                        if( $cUserSummaryData->isNotEmpty() ) {
                            foreach ($cUserSummaryData as $cUserSummary) {
                                $ownPl = $ownPl + round($cUserSummary->parentPl + $cUserSummary->parentComm, 2);
                            }
                        }
                        $data['w_pl_balance'] = $ownPl;
                    }

                }else{
                    $data = [
                        'name' => $user->name,
                        'last_logged_on' => $user->last_logged_on,
                        'role' => $user->roleName,
                        'uid' => $user->id
                    ];
                }

                $commentary = 'This is global commentary!';
                $commentaryData = DB::table('tbl_common_setting')
                    ->select(['value'])->where([['key_name','GLOBAL_COMMENTARY'],['status',1]])->first();

                if( $commentaryData != null ){
                    $commentary = $commentaryData->value;
                }

                $lastcommentary = DB::table('tbl_global_commentary')->select('commentary')
                    ->orderBy('id','desc')->limit(3)->get();
                $lastcomm = 'No last data!';
                if( $lastcommentary->isNotEmpty() ){
                    $lastcomm = '<ul class="p-2 mb-0">';
                    foreach ( $lastcommentary as $cdata ){
                        $lastcomm .= '<li>'.$cdata->commentary.'</li>';
                    }
                    $lastcomm .= '</ul>';
                }

                $data['commentary'] = $commentary;
                $data['lastcomm'] = $lastcomm;

                $response = [ 'status' => 1 , "data" => $data ];
            }

            //$marketProfit = $this->getMarketProfit();
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    ///add 0.00007 % commision 

       ///add 0.00007 % commision 

        public function getMarketProfit()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

         $uid = Auth::user()->id;

          $userData = DB::table('tbl_order')
                    ->select('*')
                    ->where([['status',1],['comm',0]])->get();
           $comm = 0;
          foreach($userData as $data){
                  
            if($data->name == 'GOLD'){
                 if($data->order_type == 'instant_execution'){
                    if($data->trans_type == 'SELL'){
                        if($data->lot == '100gm'){
                            $sell = $data->sell_price * 10;
                            $comm = $sell * 0.00007 ;

                        }elseif($data->lot == '1kg'){
                            $sell = $data->sell_price * 100;
                            $comm = $sell * 0.00007;
                        }else{
                            $sell = $data->sell_price * 300;
                            $comm = $sell * 0.00007;
                        }
                    }else{
                         if($data->lot == '100gm'){
                            $buy = $data->buy_price * 10;
                            $comm = $buy * 0.00007 ;

                        }elseif($data->lot == '1kg'){
                            $buy = $data->buy_price * 100;
                            $comm = $buy * 0.00007;
                        }else{
                            $buy = $data->buy_price * 300;
                            $comm = $buy * 0.00007;
                        }

                    }

                 }elseif($data->order_type == 'buy_limit' || $data->order_type == 'sell_limit' ){
                        
                       if($data->trans_type == 'SELL'){
                        if($data->lot == '100gm'){
                            $price = $data->price * 10;
                            $comm = $price * 0.00007 ;

                        }elseif($data->lot == '1kg'){
                            $price = $data->price * 100;
                            $comm = $price * 0.00007;
                        }else{
                            $price = $data->price * 300;
                            $comm = $price * 0.00007;
                        }
                    }else{
                         if($data->lot == '100gm'){
                            $price = $data->price * 10;
                            $comm = $price * 0.00007 ;

                        }elseif($data->lot == '1kg'){
                            $price = $data->price * 100;
                            $comm = $price * 0.00007;
                        }else{
                            $price = $data->price * 300;
                            $comm = $price * 0.00007;
                        }

                    }
                 }

            }elseif($data->name == 'SILVER'){

              if($data->order_type == 'instant_execution'){
                    if($data->trans_type == 'SELL'){
                        if($data->lot == '1kg'){
                            $sell = $data->sell_price;
                            $comm = $sell * 0.00007 ;

                        }elseif($data->lot == '5kg'){
                            $sell = $data->sell_price * 5;
                            $comm = $sell * 0.00007;
                        }else{
                            $sell = $data->sell_price * 30;
                            $comm = $sell * 0.00007;
                        }
                    }else{
                         if($data->lot == '1kg'){
                            $buy = $data->buy_price;
                            $comm = $buy * 0.00007 ;

                        }elseif($data->lot == '5kg'){
                            $buy = $data->buy_price * 5;
                            $comm = $buy * 0.00007;
                        }else{
                            $buy = $data->buy_price * 30;
                            $comm = $buy * 0.00007;
                        }

                    }

                 }elseif($data->order_type == 'buy_limit' || $data->order_type == 'sell_limit' ){
                        
                       if($data->trans_type == 'SELL'){
                        if($data->lot == '1kg'){
                            $price = $data->price ;
                            $comm = $price * 0.00007 ;

                        }elseif($data->lot == '5kg'){
                            $price = $data->price * 5;
                            $comm = $price * 0.00007;
                        }else{
                            $price = $data->price * 30;
                            $comm = $price * 0.00007;
                        }
                    }else{
                         if($data->lot == '1kg'){
                            $price = $data->price ;
                            $comm = $price * 0.00007 ;

                        }elseif($data->lot == '5kg'){
                            $price = $data->price * 5;
                            $comm = $price * 0.00007;
                        }else{
                            $price = $data->price * 30;
                            $comm = $price * 0.00007;
                        }

                    }
                 }

      


            }else{
               
              if($data->order_type == 'instant_execution'){
                    if($data->trans_type == 'SELL'){
                        if($data->lot == '1000'){
                            $sell = $data->sell_price * 1000;
                            $comm = $sell * 0.00007 ;

                        }elseif($data->lot == '10000'){
                            $sell = $data->sell_price * 10000;
                            $comm = $sell * 0.00007;
                        }else{
                            $sell = $data->sell_price * 50000;
                            $comm = $sell * 0.00007;
                        }
                    }else{
                         if($data->lot == '1000'){
                            $buy = $data->buy_price * 1000;
                            $comm = $buy * 0.00007 ;

                        }elseif($data->lot == '10000'){
                            $buy = $data->buy_price * 10000;
                            $comm = $buy * 0.00007;
                        }else{
                            $buy = $data->buy_price * 50000;
                            $comm = $buy * 0.00007;
                        }

                    }

                 }elseif($data->order_type == 'buy_limit' || $data->order_type == 'sell_limit' ){
                        
                       if($data->trans_type == 'SELL'){
                        if($data->lot == '1000'){
                            $price = $data->price * 1000 ;
                            $comm = $price * 0.00007 ;

                        }elseif($data->lot == '10000'){
                            $price = $data->price * 10000;
                            $comm = $price * 0.00007;
                        }else{
                            $price = $data->price * 50000;
                            $comm = $price * 0.00007;
                        }
                    }else{
                         if($data->lot == '1000'){
                            $price = $data->price * 1000 ;
                            $comm = $price * 0.00007 ;

                        }elseif($data->lot == '10000'){
                            $price = $data->price * 10000;
                            $comm = $price * 0.00007;
                        }else{
                            $price = $data->price * 50000;
                            $comm = $price * 0.00007;
                        }

                    }
                 }

            }
           
          $pl =  DB::table('tbl_user_info')->where('uid',$uid)->select('pl_balance')->first();
          $newPl = $pl->pl_balance + $comm;
          $arr =[ 'pl_balance' => $newPl];
          $commision =[ 'comm' => 1];
          
          $com =  DB::table('tbl_order')->where('id',$data->id)->update($commision);
          $order =  DB::table('tbl_user_info')->where('uid',$uid)->update($arr);

          }
          



    }

public function getDataList($type)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $user = Auth::user(); $client = [];

            $market =  DB::table('tbl_mcxmarket2')->where('status',1)->select('*')->get();
            /*$cache = Redis::Connection();
            $key = 'Mcx_Market';
            $result=$cache->get($key);
            $arrData = json_decode($result);
            print_r($arrData); die();*/
        
            foreach ($market as $val) {
                $depth = json_decode($val->depth);
                $buyPrice = $depth->buy[0]->price;
                $sellPrice = $depth->sell[0]->price;
              
                $arr[] = [
                  'name'=>$val->name,
                  'instrumentToken'=>$val->instrument_token,
                  'tradingSymbol'=>$val->tradingSymbol,
                  'buy_price' =>$buyPrice,
                  'sell_price'=>$sellPrice,
                   'expiry'=>$val->expiry,
                  'status'=>1
                ];
            }
            
        return $arr;

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


    /**
     * get Data List
     * Action - Get
     * Created at FEB 2020 by dream
     */
    /*public function getDataList($type)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $user = Auth::user(); $client = [];

            if( $user->roleName != 'ADMIN' ){
                $clientJson = DB::table('tbl_user_child_data')->select('clients')
                    ->where([['uid',$user->id]])->first();
                if( $clientJson != null && isset($clientJson->clients) ){
                    $client = json_decode($clientJson->clients);
                }
            }

            if( $user->role != 1 && $user->role != 6 ){
                $blockEvents = $this->getBlockEventIds($user->id);
                $blockSports = $this->getBlockSportIds($user->id);

                $sportList = DB::table('tbl_sport')->where([['status',1],['is_block',0]])
                    ->whereNotIn('sportId', $blockSports)
                    ->select(['sportId','name','sportId','slug','icon','img','is_block'])
                    ->orderBy('position','ASC')->get();
            }else{
                $blockEvents = [];
                $sportList = DB::table('tbl_sport')->where([['status',1]])
                    ->select(['sportId','name','sportId','slug','icon','img','is_block'])
                    ->orderBy('position','ASC')->get();
            }

            if( $sportList != null ){
                $sportData = [];
                foreach ( $sportList as $sport ){

                    if( $user->role != 1 && $user->role != 6 ){
                        $isBlock = $this->getBlockStatus($user->id,$sport->sportId,'sport');
                    }else{
                        $isBlock = $sport->is_block;
                    }

                    $sportData[] = [
                        'name' => $sport->name,
                        'sportId' => $sport->sportId,
                        'slug' => $sport->slug,
                        'icon' => $sport->icon,
                        'img' => $sport->img,
                        'count' => 0,
                        'isBlock' => $isBlock
                    ];
                }
                $response = [ 'status' => 1, 'data' => $sportData ];
            }

            if( $type != 'all' && $type != 'market' ){

                $sport = Sport::where([['slug',$type]])->select(['sportId'])->first();
                $response['sport'] = [];
                if( $sport != null ){
                    if( $sport->sportId != '99' ){

                        $redis = Redis::connection();
                        $eventArrJson = $redis->get("Event_List_".$sport->sportId);
                        $eventArr = json_decode($eventArrJson);

                        if( $eventArr != null && isset($eventArr->eventData) && !empty($eventArr->eventData) ){
                            $event = $eventArr->eventData;
                            $eventData = $oddsData = $matchOddMarket = $matchOddMarketJson = [];
                            foreach ( $event as $data ){
                                if( !in_array( $data->eventId,$blockEvents )
                                    && $data->game_over != 1 ){

                                    $matchOddMarketJson = $redis->get('Match_Odd_'.$data->eventId);
                                    $matchOddMarket = json_decode($matchOddMarketJson);

                                    if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )) {
                                        $oddsDataJson = $redis->get($matchOddMarket->marketId);
                                        $oddsData = json_decode($oddsDataJson);
                                    }

                                    if( $user->role != 1 && $user->role != 6 ){
                                        if( $data->is_block != 1 ) {
                                            $eventData[] = [
                                                'eventId' => $data->eventId,
                                                'name' => $data->name,
                                                'league' => $data->league,
                                                'time' => $data->time,
                                                'isBlock' => $this->getBlockStatus($user->id,$data->eventId,'event'),
                                                'type' => $data->type,
                                                'betCount' => $this->getBetCount($data->eventId,$client,$user->systemId),
                                                'oddsData' => $oddsData,
                                            ];
                                        }
                                    }else{
                                        $eventData[] = [
                                            'eventId' => $data->eventId,
                                            'name' => $data->name,
                                            'league' => $data->league,
                                            'time' => $data->time,
                                            'isBlock' => $data->is_block,
                                            'type' => $data->type,
                                            'betCount' => $this->getBetCount($data->eventId,$client,$user->systemId),
                                            'oddsData' => $oddsData,
                                        ];
                                    }


                                }
                            }

                            $response['event'] = $eventData;
                        }
                    }else{
                        $query = DB::table('tbl_live_game')->select(['eventId','name','is_block']);
                        $where = [['sportId',$sport->sportId]];
                        if( $user->role != 1 && $user->role != 6 ){
                            $where = [['sportId',$sport->sportId],['is_block',0]];
                            if( $blockEvents != null ){
                                $query->whereNotIn('eventId',$blockEvents);
                            }
                        }
                        $event = $query->where($where)->get();
                        if( $event != null ){
                            $eventData = [];
                            foreach ( $event as $data ){
                                if( !in_array( $data->eventId,$blockEvents ) ) {

                                    if( $user->role != 1 && $user->role != 6 ){
                                        if( $data->is_block != 1 ) {
                                            $eventData[] = [
                                                'eventId' => $data->eventId,
                                                'name' => $data->name,
                                                'isBlock' => $this->getBlockStatus($user->id,$data->eventId,'event'),
                                                'betCount' => $this->getBetCountTeenPatti($data->eventId,$client,$user->systemId),
                                            ];
                                        }
                                    }else{
                                        $eventData[] = [
                                            'eventId' => $data->eventId,
                                            'name' => $data->name,
                                            'isBlock' => $data->is_block,
                                            'betCount' => $this->getBetCountTeenPatti($data->eventId,$client,$user->systemId),
                                        ];
                                    }


                                }
                            }

                            $response['event'] = $eventData;
                        }
                    }

                }

            }else if( $type == 'market' ){

                $sport = Sport::whereIn('sportId',[1,2,4])->select(['sportId','name','slug'])->get();
                $sportData = [];
                foreach ( $sport as $spt ){
                    $redis = Redis::connection();
                    $eventArrJson = $redis->get("Event_List_".$spt->sportId);
                    $eventArr = json_decode($eventArrJson);
                    $eventData = [];
                    if( isset($eventArr->eventData) && $eventArr != null &&  !empty($eventArr->eventData) ){
                        $event = $eventArr->eventData;
                        foreach ( $event as $data ){
                            if( !in_array( $data->eventId,$blockEvents ) && $data->game_over == 0 ) {
                                $betCount = $this->getBetCount($data->eventId,$client,$user->systemId);
                                $runners = $this->runnerDataWithBook($user, $data->eventId); //$this->getMatchOddBook($user->id,$data->eventId);
                                if ($betCount > 0) {

                                    if( $user->role != 1 && $user->role != 6 ){
                                        if( $data->is_block != 1 ) {
                                            $eventData[] = [
                                                'eventId' => $data->eventId,
                                                'name' => $data->name,
                                                'league' => $data->league,
                                                'time' => $data->time,
                                                'isBlock' => $this->getBlockStatus($user->id,$data->eventId,'event'),
                                                'type' => $data->type,
                                                'betCount' => $betCount,
                                                'runners' => $runners
                                            ];
                                        }
                                    }else{
                                        $eventData[] = [
                                            'eventId' => $data->eventId,
                                            'name' => $data->name,
                                            'league' => $data->league,
                                            'time' => $data->time,
                                            'isBlock' => $data->is_block,
                                            'type' => $data->type,
                                            'betCount' => $betCount,
                                            'runners' => $runners
                                        ];
                                    }


                                }
                            }
                        }
                    }

                    if( $eventData != null ){
                        $sportData[] = [
                            'sportId' => $spt->sportId,
                            'name' => $spt->name,
                            'slug' => $spt->slug,
                            'event' => $eventData
                        ];
                    }

                }

                $response['sport'] = $sportData;

            }else{
                $response['event'] = [];
                $response['sport'] = [];
            }

            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
                $response['userData'] = $userData;
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }*/


    public function commissionAllinOne(){

        //$comm = $this->commissionAll();
        $startTime = time();
        $endTime = time() + 60;
        while ($endTime > time()) {
            usleep(900000);

        $userList = DB::table('tbl_order')->where([['status',5],['comm',0]])->get();
        //print_r($userList); die('oooooo');
        foreach ($userList as $list) {  
            $this->updateTransaction($list);
        }
      }
        
        print_r($userList); die('ppp');
    }

    /**
     * get Data List By System
     * Action - Post
     * Created at FEB 2020 by dream
     */
    /*public function getDataListBySystemOld($type)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $request = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

            if( isset( $request['systemId'] ) ){

                $systemId = $request['systemId']; $sportList = $system = [];

                $system = DB::table('tbl_system')
                    ->where([['operatorId',$systemId],['status',1]])->first();

                if( $system != null ){
                    $user = DB::table('tbl_user')
                        ->where([['id',$system->uid],['status',1]])->first();

                    $system = [
                        'systemName' => $system->systemname,
                        'uid' => $system->uid
                    ];

                }else{
                    return response()->json($response, 200);
                }

                if( $user != null ){

                    $blockEvents = [];
                    $sportList = DB::table('tbl_sport')->where([['status',1],['is_block',0]])
                        ->select(['sportId','name','sportId','slug','icon','img','is_block'])
                        ->orderBy('position','ASC')->get();
                }

                if( $sportList != null ){
                    $sportData = [];
                    foreach ( $sportList as $sport ){
                        $sportData[] = [
                            'name' => $sport->name,
                            'sportId' => $sport->sportId,
                            'slug' => $sport->slug,
                            'icon' => $sport->icon,
                            'img' => $sport->img,
                            'count' => 0,
                            'isBlock' => $sport->is_block
                        ];
                    }

                    $response = ['status' => 1,'data' => $sportData , 'system' => $system ];

                }

                if( $type != 'all' && $type != 'market' ){

                    $sport = Sport::where([['slug',$type]])->select(['sportId'])->first();
                    $response['sport'] = [];
                    if( $sport != null ){
                        if( $sport->sportId != '99' ){

                            $redis = Redis::connection();
                            $eventArrJson = $redis->get("Event_List_".$sport->sportId);
                            $eventArr = json_decode($eventArrJson);

                            if( $eventArr != null && isset($eventArr->eventData) && !empty($eventArr->eventData) ){
                                $event = $eventArr->eventData;
                                $eventData = $oddsData = [];
                                foreach ( $event as $data ){
                                    if( !in_array( $data->eventId,$blockEvents )
                                        && $data->game_over != 1 ){

                                        $matchOddMarketJson = $redis->get('Match_Odd_'.$data->eventId);
                                        $matchOddMarket = json_decode($matchOddMarketJson);

                                        if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )) {
                                            $oddsDataJson = $redis->get($matchOddMarket->marketId);
                                            $oddsData = json_decode($oddsDataJson);
                                        }

                                        if( $data->is_block != 1 ) {
                                            $eventData[] = [
                                                'eventId' => $data->eventId,
                                                'name' => $data->name,
                                                'league' => $data->league,
                                                'time' => $data->time,
                                                'isBlock' => $this->getBlockStatus($user->id,$data->eventId,'event'),
                                                'type' => $data->type,
                                                'betCount' => $this->getBetCount($data->eventId),
                                                'oddsData' => $oddsData,
                                            ];
                                        }

                                    }
                                }

                                $response['event'] = $eventData;
                            }
                        }else{
                            $query = DB::table('tbl_live_game')->select(['eventId','name','is_block']);
                            $where = [['sportId',$sport->sportId]];

                            $event = $query->where($where)->get();
                            if( $event != null ){
                                $eventData = [];
                                foreach ( $event as $data ){
                                    if( $data->is_block != 1 ) {
                                        $eventData[] = [
                                            'eventId' => $data->eventId,
                                            'name' => $data->name,
                                            'isBlock' => $this->getBlockStatus($user->id,$data->eventId,'event'),
                                            'betCount' => $this->getBetCount($data->eventId),
                                        ];
                                    }
                                }

                                $response['event'] = $eventData;
                            }
                        }

                    }

                }else if( $type == 'market' ){

                    $sport = Sport::whereIn('sportId',[1,2,4])->select(['sportId','name','slug'])->get();
                    $sportData = [];
                    foreach ( $sport as $spt ){
                        $redis = Redis::connection();
                        $eventArrJson = $redis->get("Event_List_".$spt->sportId);
                        $eventArr = json_decode($eventArrJson);
                        $eventData = [];
                        if( isset($eventArr->eventData) && $eventArr != null &&  !empty($eventArr->eventData) ){
                            $event = $eventArr->eventData;
                            foreach ( $event as $data ){
                                $betCount = $this->getBetCount($data->eventId);
                                $runners = $this->runnerDataWithBook($user, $data->eventId); //$this->getMatchOddBook($user->id,$data->eventId);
                                if ($betCount > 0) {
                                    if( $data->is_block != 1 ) {
                                        $eventData[] = [
                                            'eventId' => $data->eventId,
                                            'name' => $data->name,
                                            'league' => $data->league,
                                            'time' => $data->time,
                                            'isBlock' => $this->getBlockStatus($user->id,$data->eventId,'event'),
                                            'type' => $data->type,
                                            'betCount' => $betCount,
                                            'runners' => $runners
                                        ];
                                    }
                                }
                            }
                        }

                        $sportData[] = [
                            'sportId' => $spt->sportId,
                            'name' => $spt->name,
                            'slug' => $spt->slug,
                            'event' => $eventData
                        ];
                    }

                    $response['sport'] = $sportData;

                }else{
                    $response['event'] = [];
                    $response['sport'] = [];
                }

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }*/

    /**
     * get Data List By System
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function getDataListBySystem($type)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $request = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            $sportList = $system = $client = [];

            $user = Auth::user(); $uid = $user->id;

            $systemData = DB::table('tbl_system')->where([['status',1]])->get();

            if( $systemData->isNotEmpty() ){

                $system = [
                    'systemName' => 'White Label',
                    'uid' => $uid,
                ];

//                foreach ( $systemData as $data ){
//                    $system[] = [
//                        'systemName' => $data->systemname,
//                        'operatorId' => $data->operatorId,
//                        'uid' => $data->uid
//                    ];
//                }

            }else{
                return response()->json($response, 200);
            }

            if( $user->roleName != 'ADMIN' && $user->roleName != 'ADMIN2' ){
                $clientJson = DB::table('tbl_user_child_data')->select('clients')
                    ->where([['uid',$user->id]])->first();
                if( $clientJson != null && isset($clientJson->clients) ){
                    $client = json_decode($clientJson->clients);
                }
            }

            $blockEvents = [];
            $sportList = DB::table('tbl_sport')->where([['status',1],['is_block',0]])
                ->select(['sportId','name','sportId','slug','icon','img','is_block'])
                ->orderBy('position','ASC')->get();

            if( $sportList != null ){
                $sportData = [];
                foreach ( $sportList as $sport ){
                    $sportData[] = [
                        'name' => $sport->name,
                        'sportId' => $sport->sportId,
                        'slug' => $sport->slug,
                        'icon' => $sport->icon,
                        'img' => $sport->img,
                        'count' => 0,
                        'isBlock' => $sport->is_block
                    ];
                }

                $response = ['status' => 1,'data' => $sportData , 'system' => $system ];

            }

            if( $type != 'all' && $type != 'market' ){

                $sport = Sport::where([['slug',$type]])->select(['sportId'])->first();
                $response['sport'] = [];
                if( $sport != null ){
                    if( $sport->sportId != '99' ){

                        $redis = Redis::connection();
                        $eventArrJson = $redis->get("Event_List_".$sport->sportId);
                        $eventArr = json_decode($eventArrJson);

                        if( $eventArr != null && isset($eventArr->eventData) && !empty($eventArr->eventData) ){
                            $event = $eventArr->eventData;
                            $eventData = $oddsData = [];
                            foreach ( $event as $data ){
                                if( $data->game_over != 1 && $data->is_block != 1){

                                    $matchOddMarketJson = $redis->get('Match_Odd_'.$data->eventId);
                                    $matchOddMarket = json_decode($matchOddMarketJson);

                                    if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )) {
                                        $oddsDataJson = $redis->get($matchOddMarket->marketId);
                                        $oddsData = json_decode($oddsDataJson);
                                    }

                                    if( $data->is_block != 1 ) {
                                        $eventData[] = [
                                            'eventId' => $data->eventId,
                                            'name' => $data->name,
                                            'league' => $data->league,
                                            'time' => $data->time,
                                            'isBlock' => $data->is_block,
                                            'type' => $data->type,
                                            'betCount' => $this->getBetCountForSystem($data->eventId,$client),
                                            'oddsData' => $oddsData,
                                        ];
                                    }

                                }
                            }

                            $response['event'] = $eventData;
                        }
                    }else{
                        $query = DB::table('tbl_live_game')->select(['eventId','name','is_block']);
                        $where = [['sportId',$sport->sportId]];

                        $event = $query->where($where)->get();
                        if( $event != null ){
                            $eventData = [];
                            foreach ( $event as $data ){
                                if( $data->is_block != 1 ) {
                                    $eventData[] = [
                                        'eventId' => $data->eventId,
                                        'name' => $data->name,
                                        'isBlock' => $data->is_block,
                                        'betCount' => $this->getBetCountTeenPattiForSystem($data->eventId,$client),
                                    ];
                                }
                            }

                            $response['event'] = $eventData;
                        }
                    }

                }

            }else if( $type == 'market' ){

                $sport = Sport::whereIn('sportId',[1,2,4])->select(['sportId','name','slug'])->get();
                $sportData = [];
                foreach ( $sport as $spt ){
                    $redis = Redis::connection();
                    $eventArrJson = $redis->get("Event_List_".$spt->sportId);
                    $eventArr = json_decode($eventArrJson);
                    $eventData = [];
                    if( isset($eventArr->eventData) && $eventArr != null &&  !empty($eventArr->eventData) ){
                        $event = $eventArr->eventData;
                        foreach ( $event as $data ){
                            $betCount = $this->getBetCountForSystem($data->eventId,$client);
                            $runners = $this->runnerDataWithBookForSystem($uid,$data->eventId); //$this->getMatchOddBook($user->id,$data->eventId);
                            if ($betCount > 0) {
                                if( $data->is_block != 1 ) {
                                    $eventData[] = [
                                        'eventId' => $data->eventId,
                                        'name' => $data->name,
                                        'league' => $data->league,
                                        'time' => $data->time,
                                        'isBlock' => $data->is_block,
                                        'type' => $data->type,
                                        'betCount' => $betCount,
                                        'runners' => $runners
                                    ];
                                }
                            }
                        }
                    }
                    if( $eventData != null ) {
                        $sportData[] = [
                            'sportId' => $spt->sportId,
                            'name' => $spt->name,
                            'slug' => $spt->slug,
                            'event' => $eventData
                        ];
                    }
                }

                $response['sport'] = $sportData;

            }else{
                $response['event'] = [];
                $response['sport'] = [];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // get Block Status
    public function getBlockStatus($uid,$eventIdOrSportId,$type)
    {
        if( $type == 'event' ){
            $eventId = $eventIdOrSportId;
            $check = DB::table('tbl_user_event_status')->where([['eid',$eventId],['uid',$uid]])->first();
            if( $check != null ){
                return 1;
            }else{
                return 0;
            }
        }else{
            $sportId = $eventIdOrSportId;
            $check = DB::table('tbl_user_sport_status')->where([['sid',$sportId],['uid',$uid]])->first();
            if( $check != null ){
                return 1;
            }else{
                return 0;
            }
        }

    }

    // get Bet Count
    public function getBetCount($eventId,$client,$systemId)
    {
        $countArr = [];
        $p1 = DB::table('tbl_bet_pending_1')->where([['systemId',$systemId],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p1 = $p1->whereIn('uid',$client); } $p1 = $p1->count();
        $p2 = DB::table('tbl_bet_pending_2')->where([['systemId',$systemId],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p2 = $p2->whereIn('uid',$client); } $p2 = $p2->count();
        $p3 = DB::table('tbl_bet_pending_3')->where([['systemId',$systemId],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p3 = $p3->whereIn('uid',$client); } $p3 = $p3->count();
        $p4 = DB::table('tbl_bet_pending_4')->where([['systemId',$systemId],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p4 = $p4->whereIn('uid',$client); } $p4 = $p4->count();

        if( $p1 != null ){ $countArr[] = $p1; }
        if( $p2 != null ){ $countArr[] = $p2; }
        if( $p3 != null ){ $countArr[] = $p3; }
        if( $p4 != null ){ $countArr[] = $p4; }

        if( $countArr != null ){
            return array_sum($countArr);
        }else{
            return 0;
        }

    }

    // get Bet Count TeenPatti
    public function getBetCountTeenPatti($eventId,$client,$systemId)
    {
        $countArr = [];
        $p1 = DB::table('tbl_bet_pending_teenpatti')->where([['systemId',$systemId],['eid',$eventId],['status',1]]);
        if( $client != null ){ $p1 = $p1->whereIn('uid',$client); } $p1 = $p1->count();

        if( $p1 != null ){ $countArr[] = $p1; }

        if( $countArr != null ){
            return array_sum($countArr);
        }else{
            return 0;
        }

    }

    // get Bet Count For System
    public function getBetCountForSystem($eventId,$client)
    {
        $countArr = [];
        $p1 = DB::table('tbl_bet_pending_1')->where([['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p1 = $p1->whereIn('uid',$client); } $p1 = $p1->count();
        $p2 = DB::table('tbl_bet_pending_2')->where([['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p2 = $p2->whereIn('uid',$client); } $p2 = $p2->count();
        $p3 = DB::table('tbl_bet_pending_3')->where([['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p3 = $p3->whereIn('uid',$client); } $p3 = $p3->count();
        $p4 = DB::table('tbl_bet_pending_4')->where([['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p4 = $p4->whereIn('uid',$client); } $p4 = $p4->count();

        if( $p1 != null ){ $countArr[] = $p1; }
        if( $p2 != null ){ $countArr[] = $p2; }
        if( $p3 != null ){ $countArr[] = $p3; }
        if( $p4 != null ){ $countArr[] = $p4; }

        if( $countArr != null ){
            return array_sum($countArr);
        }else{
            return 0;
        }

    }

    // get Bet Count TeenPatti
    public function getBetCountTeenPattiForSystem($eventId,$client)
    {
        $countArr = [];
        $p1 = DB::table('tbl_bet_pending_teenpatti')->where([['systemId','!=',1],['eid',$eventId],['status',1]]);
            if( $client != null ){ $p1 = $p1->whereIn('uid',$client); } $p1 = $p1->count();

        if( $p1 != null ){ $countArr[] = $p1; }

        if( $countArr != null ){
            return array_sum($countArr);
        }else{
            return 0;
        }

    }

    // get MatchOdd Book
    public function getMatchOddBook($uid,$eventId)
    {
        $runnersArr = [];
        $redis = Redis::connection();
        $matchOddMarketJson = $redis->get('Match_Odd_'.$eventId);
        $matchOddMarket = json_decode($matchOddMarketJson);

        if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )) {
            $marketId = $matchOddMarket->marketId;
            $runnerData = json_decode($matchOddMarket->runners);
            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = $this->getProfitLossMatchOdd($uid,$marketId,$runner->secId);
                    $runnersArr[] = [
                        'secId' => $runner->secId,
                        'runnerName' => $runner->runner,
                        'profitLoss' => $profitLoss,
                    ];
                }
            }

        }

        return $runnersArr;

    }

    // runnerDataWithBook
    public function runnerDataWithBookForSystem($uid,$eventId)
    {
        $runnersArr = $finalTotalArr = $clientArr = [];
        $redis = Redis::connection();
        $matchOddMarketJson = $redis->get('Match_Odd_'.$eventId);
        $matchOddMarket = json_decode($matchOddMarketJson);

        if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )) {
            $marketId = $matchOddMarket->marketId;
            $runnerData = json_decode($matchOddMarket->runners);

            $query = DB::table('tbl_bet_pending_1')
                ->where([['mid',$marketId],['status',1],['systemId','!=',1]]);

//            if( $user->role != 1 ){
//                $clientJson = DB::table('tbl_user_child_data')->select('clients')
//                    ->where([['uid',$user->id]])->first();
//                $client = json_decode($clientJson->clients);
//                $query->whereIn('uid',$client);
//            }

            $clientArr = $query->distinct()->select('uid')->get();

            if( $clientArr != null ){
                $total = 0; $totalArr = [];
                foreach ( $clientArr as $c ){

                    $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                        ->where([['clientId',$c->uid],['parentId',$uid]])->first();

                    $bookData = DB::table('tbl_book_matchodd')->select(['book','secId'])
                        ->where([['mid',$marketId],['uid',$c->uid]])->get();

                    if( $bookData->isNotEmpty() ){

                        foreach ( $bookData as $book ){

                            $total = $book->book;
                            if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                                $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                                $totalArr[$book->secId][] = $total;
                            }else{
                                $totalArr[$book->secId][] = $total;
                            }

                        }

                    }

                }

                if( $totalArr != null ){
                    $finalTotalArr = [];
                    foreach ( $totalArr as $secId => $data ){
                        if( array_sum($data) != null && array_sum($data) ){
                            $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                        }
                    }

                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'secId' => $runner->secId,
                        'runnerName' => $runner->runner,
                        'profitLoss' => $profitLoss,
                    ];
                }
            }

        }

        return $runnersArr;

    }

    // runnerDataWithBook
    public function runnerDataWithBook($user,$eventId)
    {
        $runnersArr = $finalTotalArr = $clientArr = [];
        $redis = Redis::connection();
        $matchOddMarketJson = $redis->get('Match_Odd_'.$eventId);
        $matchOddMarket = json_decode($matchOddMarketJson);

        if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )) {
            $marketId = $matchOddMarket->marketId;
            $runnerData = json_decode($matchOddMarket->runners);

            $query = DB::table('tbl_bet_pending_1')
                ->where([['mid',$marketId],['status',1]]);

            if( $user->role != 1 && $user->role != 6 ){
                $clientJson = DB::table('tbl_user_child_data')->select('clients')
                    ->where([['uid',$user->id]])->first();
                $client = json_decode($clientJson->clients);
                $query->whereIn('uid',$client);
            }

            $clientArr = $query->distinct()->select('uid')->get();

            if( $clientArr != null ){
                $total = 0; $totalArr = [];
                foreach ( $clientArr as $c ){

                    $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                        ->where([['clientId',$c->uid],['userId',$user->id]])->first();

                    $bookData = DB::table('tbl_book_matchodd')->select(['book','secId'])
                        ->where([['mid',$marketId],['uid',$c->uid]])->get();

                    if( $bookData->isNotEmpty() ){

                        foreach ( $bookData as $book ){

                            $total = $book->book;
                            if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                                $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                                $totalArr[$book->secId][] = $total;
                            }else{
                                $totalArr[$book->secId][] = $total;
                            }

                        }

                    }

                }

                if( $totalArr != null ){
                    $finalTotalArr = [];
                    foreach ( $totalArr as $secId => $data ){
                        if( array_sum($data) != null && array_sum($data) ){
                            if( $user->role == 4 ){
                                $finalTotalArr[$secId] = round(array_sum($data),2);
                            }else{
                                $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                            }
                        }
                    }

                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'secId' => $runner->secId,
                        'runnerName' => $runner->runner,
                        'profitLoss' => $profitLoss,
                    ];
                }
            }

        }

        return $runnersArr;

    }

    // get Profit Loss for Match Odd
    public function getProfitLossMatchOdd($uid,$marketId,$secId)
    {
        if( $uid != null ){
            $user = DB::table('tbl_user')->select(['id','role'])
                ->where([['id',$uid],['status',1]])->first();
        }else{ $user = Auth::user();  }
        $clientArr = []; $book = 0;

        $query = DB::table('tbl_bet_pending_1')
            ->where([['mid',$marketId],['status',1]]);

        if( $user->role != 1 && $user->role != 6 ){
            $clientJson = DB::table('tbl_user_child_data')->select('clients')
                ->where([['uid',$user->id]])->first();
            $client = json_decode($clientJson->clients);
            $query->whereIn('uid',$client);
        }

        $clientArr = $query->distinct()->select('uid')->get();

        if( $clientArr != null ){
            $total = 0; $totalArr = [];
            foreach ( $clientArr as $c ){
                $book = DB::table('tbl_book_bookmaker')->select('book')
                    ->where([['mid',$marketId],['secId',$secId],['uid',$c->uid]])->first();
                //$book = DB::table('tbl_user_market_expose_2')->select('expose')
                //    ->where([['mid',$marketId],['secId',$secId],['uid',$c->uid],['mType','match_odd']])->first();

                if( $book != null && $book->book != 0 ){
                    $total = $book->book;
                    $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                        ->where([['clientId',$c->uid],['userId',$user->id]])->first();

                    if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                        $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                        $totalArr[] = $total;
                    }else{
                        $totalArr[] = $total;
                    }

                }

            }

            if( array_sum($totalArr) != null && array_sum($totalArr) ){

                if( $user->role == 4 ){
                    $book = round(array_sum($totalArr),0);
                }else{
                    $book = round((-1)*array_sum($totalArr),0);
                }

            }

        }

        return $book;

    }

    // get Block EventIds
    public function getBlockEventIds($uid)
    {
        $returnArr = [];
        $eventData = DB::table('tbl_user_event_status')->select('eid')
            ->where([['uid',$uid],['byuserId','!=',$uid]])->get();

        if( $eventData != null ){
            foreach ( $eventData as $data ){
                $returnArr[] = $data->eid;
            }
        }

        return $returnArr;

    }

    // get Block SportIds
    public function getBlockSportIds($uid)
    {
        $returnArr = [];
        $eventData = DB::table('tbl_user_sport_status')->select('sid')
            ->where([['uid',$uid],['byuserId','!=',$uid]])->get();

        if( $eventData != null ){
            foreach ( $eventData as $data ){
                $returnArr[] = $data->sid;
            }
        }

        return $returnArr;

    }

    /**
     * action User Book Data MatchOdd
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionUserBookDataMatchOdd(Request $request)
    {

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            if( isset( $request->marketId ) ){
                $titleData = $userData = $totalData = [];
                if( isset( $request->userId ) ){
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id',$uid],['status',1]])->first();
                }else{
                    $user = Auth::user(); $uid = $user->id;
                    if( $user->role == 6 ){ $uid = 1; }
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $redis = Redis::connection();
                $marketDataJson = $redis->get('Market_'.$marketId);
                $marketData = json_decode($marketDataJson);

                if( $marketData != null && isset($marketData->runners) ){
                    $runnerData = json_decode($marketData->runners);
                    $runners = [];
                    if( $runnerData != null ){
                        foreach ( $runnerData as $runner ){
                            $runners[] = [
                                'secId' => $runner->secId,
                                'name' => $runner->runner
                            ];
                        }
                    }

                    if( $runners != null ) {
                        $runnerName1 = $runnerName2 = $runnerName3 = '';
                        $titleData['0'] = 'User Name';
                        if (isset($runners[0]['name'])) {
                            $runnerName1 = $runners[0]['name'];
                            $titleData['1'] = $runnerName1;
                        }
                        if (isset($runners[1]['name'])) {
                            $runnerName2 = $runners[1]['name'];
                            $titleData['2'] = $runnerName2;
                        }
                        if (isset($runners[2]['name'])) {
                            $runnerName3 = $runners[2]['name'];
                            $titleData['3'] = $runnerName3;
                        }

                    }

                    $query = DB::table('tbl_bet_pending_1')
                        ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                    $clientArr = $query->distinct()->select('uid')->get();

                    if( $clientArr->isNotEmpty() ){

                        $AllClients = [];
                        foreach ( $clientArr as $c ){
                            $AllClients[] = $c->uid;
                        }

                        $AllClients = array_unique($AllClients);

                        $client=[];
                        $cdata = DB::table('tbl_user_profit_loss')->select(['userId'])
                            ->where([['parentId',$uid]])->whereIn('clientId',$AllClients)
                            ->distinct()->select('userId')->get();

                        if ($cdata != null) {
                            foreach ($cdata as $c) {
                                $client[] = (int)$c->userId;
                            }
                        }

                    }

                    if(!empty( $client )){
                        $userList = DB::table('tbl_user')->select(['id','parentId','username','role','systemId'])
                            ->whereIn('id',$client)->where([['status',1],['systemId',$systemId]])->get();

                        if( $userList->isNotEmpty() && $runners != null ){
                            $runner1 = $runner2 = $runner3 = 0;

                            $t1 = $t2 = $t3 = null;
                            $i = 0;
                            foreach ( $userList as $user ){

                                $userData[$i] = [
                                    'username' => $user->username,
                                    'id' => $user->id,
                                    'role' => $user->role
                                ];

                                if( isset( $runners[0]['secId'] ) ){
                                    $runner1 = $this->getProfitLossMatchOddForBook($user,$marketId,$runners[0]['secId']);
                                    $t1[] = round($runner1,2);
                                    $userData[$i]['runner1'] = round($runner1);
                                }
                                if( isset( $runners[1]['secId'] ) ){
                                    $runner2 = $this->getProfitLossMatchOddForBook($user,$marketId,$runners[1]['secId']);
                                    $t2[] = round($runner2,2);
                                    $userData[$i]['runner2'] = round($runner2);
                                }
                                if( isset( $runners[2]['secId'] ) ){
                                    $runner3 = $this->getProfitLossMatchOddForBook($user,$marketId,$runners[2]['secId']);
                                    $t3[] = round($runner3,2);
                                    $userData[$i]['runner3'] = round($runner3);
                                }

                                $i++;
                            }

                        }

                    }

                }

                $response = [ "status" => 1 , "data" => [ 'titleData' => $titleData , 'userData' => $userData , 'totalData' => $totalData ] ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data MatchOdd
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionSystemBookDataMatchOdd(Request $request)
    {

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            if( isset( $request->marketId ) ){
                $titleData = $userData = $totalData = [];
                $user = Auth::user(); $uid = $user->id;
                $marketId = $request->marketId;

                $redis = Redis::connection();
                $marketDataJson = $redis->get('Market_'.$marketId);
                $marketData = json_decode($marketDataJson);

                if( $marketData != null && isset($marketData->runners) ){
                    $runnerData = json_decode($marketData->runners);
                    $runners = [];
                    if( $runnerData != null ){
                        foreach ( $runnerData as $runner ){
                            $runners[] = [
                                'secId' => $runner->secId,
                                'name' => $runner->runner
                            ];
                        }
                    }

                    if( $runners != null ) {
                        $runnerName1 = $runnerName2 = $runnerName3 = '';
                        $titleData['0'] = 'User Name';
                        if (isset($runners[0]['name'])) {
                            $runnerName1 = $runners[0]['name'];
                            $titleData['1'] = $runnerName1;
                        }
                        if (isset($runners[1]['name'])) {
                            $runnerName2 = $runners[1]['name'];
                            $titleData['2'] = $runnerName2;
                        }
                        if (isset($runners[2]['name'])) {
                            $runnerName3 = $runners[2]['name'];
                            $titleData['3'] = $runnerName3;
                        }

                    }

                    $userList = DB::table('tbl_user')->select(['id','parentId','username','role','systemId'])
                        ->where([['parentId',$uid],['status',1],['systemId','!=',1],['role',1]])->get();

                    if( $userList->isNotEmpty() && $runners != null ){
                        $runner1 = $runner2 = $runner3 = 0;

                        $t1 = $t2 = $t3 = null;
                        $i = 0;
                        foreach ( $userList as $user ){

                            $userData[$i] = [
                                'username' => $user->username,
                                'id' => $user->id,
                                'role' => $user->role
                            ];

                            if( isset( $runners[0]['secId'] ) ){
                                $runner1 = $this->getProfitLossMatchOddForBook($user,$marketId,$runners[0]['secId']);
                                $t1[] = round($runner1,2);
                                $userData[$i]['runner1'] = round($runner1);
                            }
                            if( isset( $runners[1]['secId'] ) ){
                                $runner2 = $this->getProfitLossMatchOddForBook($user,$marketId,$runners[1]['secId']);
                                $t2[] = round($runner2,2);
                                $userData[$i]['runner2'] = round($runner2);
                            }
                            if( isset( $runners[2]['secId'] ) ){
                                $runner3 = $this->getProfitLossMatchOddForBook($user,$marketId,$runners[2]['secId']);
                                $t3[] = round($runner3,2);
                                $userData[$i]['runner3'] = round($runner3);
                            }

                            $i++;
                        }

                    }

                }

                $response = [ "status" => 1 , "data" => [ 'titleData' => $titleData , 'userData' => $userData , 'totalData' => $totalData ] ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // get Profit Loss for Match Odd
    public function getProfitLossMatchOddForBook($user,$marketId,$secId)
    {
        $role = $user->role;
        $parentId = $user->parentId;
        $userId  = $user->id;
        $systemId  = $user->systemId;

        $clientArr = []; $book = 0;

        if( $parentId == 0 ){ return $book; }

        $query = DB::table('tbl_bet_pending_1');

        if( ( $role != 1 && $role != 4 ) || ( $systemId != 1 && $role == 1 ) ){

            $query->where([['mid',$marketId],['status',1],['systemId',$systemId],['result','PENDING']]);

            $clientJson = DB::table('tbl_user_child_data')->select('clients')
                ->where([['uid',$userId]])->first();
            if( $clientJson != null && isset( $clientJson->clients )){
                $client = json_decode($clientJson->clients);
                $query->whereIn('uid',$client);
            }

            $clientArr = $query->distinct()->select('uid')->get();
            if( $clientArr->isNotEmpty() ){
                $total = 0; $totalArr = [];
                foreach ( $clientArr as $c ){

                    $bookData = DB::table('tbl_book_matchodd')->select('book')
                        ->where([['mid',$marketId],['secId',$secId],['uid',$c->uid]])->first();

                    if( $bookData != null && $bookData->book != 0 ){
                        $total = $bookData->book;

                        if( $systemId != 1 && $role == 1 ){
                            $where = [['clientId',$c->uid],['parentId',$parentId]];
                        }else{
                            $where = [['clientId',$c->uid],['userId',$parentId]];
                        }

                        $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                            ->where($where)->first();

                        if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){

                            $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                            $totalArr[] = $total;
                        }

                    }

                }

                if( $totalArr != null ){
                    $book = round((-1)*array_sum($totalArr),2);
                }

            }

        }else{
            $total = $book = 0;
            $clientArr = $query->where([['mid',$marketId],['status',1],['systemId',$systemId],['uid',$userId],['result','PENDING']])->first();
            if( $clientArr != null ){
                $bookData = DB::table('tbl_book_matchodd')->select('book')
                    ->where([['mid',$marketId],['secId',$secId],['uid',$userId]])->first();

                if( $bookData != null && $bookData->book != 0 ){
                    $total = $bookData->book;
                    $where = [['clientId',$userId],['userId',$parentId]];
                    $profitLoss = DB::table('tbl_user_profit_loss')
                        ->select(['actual_profit_loss'])->where($where)->first();

                    if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                        $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                    }

                    $book = round((-1)*$total,2);

                }
            }
        }

        return $book;

    }

    /**
     * action Bet Allowed
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function actionBetAllowed($eventId)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $cUser = Auth::user();
            if( $cUser != null && $cUser->role != 1){ return $response; }

            if($eventId != null){

                $conn = DB::connection('mysql2')->table('tbl_event');
                $where = [ ['eventId',$eventId] ];
                $event = $conn->select('bet_allowed')->where($where)->first();
                if( $event != null ){
                    if( $event->bet_allowed == 1 ){
                        $update = ['bet_allowed' => 0]; $msg = 'Bet Not Allowed Successfully!';
                    }else{
                        $update = ['bet_allowed' => 1]; $msg = 'Bet Allowed Successfully!';
                    }

                    if( $conn->where($where)->update($update) ){
                        CommonModel::redisEventList($eventId,'bet_allowed');
                        $response = [ 'status' => 1, 'success' => [ 'message' => $msg ] ];
                    }else{
                        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Something Wrong! Event Not Updated!!" ] ];
                    }

                }
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Bet Allowed Market
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function actionBetAllowedMarket(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $cUser = Auth::user();
            if( $cUser != null && $cUser->role != 1){ return $response; }

            if( isset( $request->mid ) && isset( $request->type )){

                $marketId = $request->mid;
                $type = $request->type;

                $tbl = 'tbl_'.$type;

                if( $tbl == 'tbl_khado_session' ){ $tbl = 'tbl_khado_sesstion'; }

                $conn = DB::connection('mysql2')->table($tbl);
                $where = [ ['marketId',$marketId] ];
                $market = $conn->select(['bet_allowed','eid'])->where($where)->first();
                if( $market != null ){
                    $eventId = $market->eid;
                    if( $market->bet_allowed == 1 ){
                        $update = ['bet_allowed' => 0]; $msg = 'Bet Not Allowed Successfully!';
                    }else{
                        $update = ['bet_allowed' => 1]; $msg = 'Bet Allowed Successfully!';
                    }

                    if( $conn->where($where)->update($update) ){
                        CommonModel::redisMarketUpdate($eventId, $marketId, $type);
                        $response = [ 'status' => 1, 'success' => [ 'message' => $msg ] ];
                    }else{
                        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Something Wrong! Market Not Updated!!" ] ];
                    }

                }
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action All Logout
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function actionAllLogout()
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            $cUser = Auth::user();
            if( $cUser != null && $cUser->roleName != 'ADMIN'){ return $response; }

            if( DB::table('oauth_access_tokens')
                ->where([['user_id','!=',$cUser->id]])->delete() ){
                DB::table('tbl_user')->where([['id','!=',$cUser->id]])->update(['is_login' => 0]);
                $response = [ 'status' => 1, 'success' => [ 'message' => 'All User Logout Successfully !' ] ];
            }else{
                $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Something Wrong! User Not Logout!!" ] ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action User Book Data BookMaker
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionUserBookDataBookMaker(Request $request)
    {

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            if( isset( $request->marketId ) ){
                $titleData = $userData = $totalData = [];
                if( isset( $request->userId ) ){
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
                }else{
                    $user = Auth::user(); $uid = $user->id;
                    if( $user->role == 6 ){ $uid = 1; }
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $redis = Redis::connection();
                $marketDataJson = $redis->get('Market_'.$marketId);
                $marketData = json_decode($marketDataJson);

                if( $marketData != null && isset($marketData->runners) ){
                    $runnerData = json_decode($marketData->runners);
                    $runners = [];
                    if( $runnerData != null ){
                        foreach ( $runnerData as $runner ){
                            $runners[] = [
                                'secId' => $runner->secId,
                                'name' => $runner->runner
                            ];
                        }
                    }

                    if( $runners != null ) {
                        $runnerName1 = $runnerName2 = $runnerName3 = '';
                        $titleData['0'] = 'User Name';
                        if (isset($runners[0]['name'])) {
                            $runnerName1 = $runners[0]['name'];
                            $titleData['1'] = $runnerName1;
                        }
                        if (isset($runners[1]['name'])) {
                            $runnerName2 = $runners[1]['name'];
                            $titleData['2'] = $runnerName2;
                        }
                        if (isset($runners[2]['name'])) {
                            $runnerName3 = $runners[2]['name'];
                            $titleData['3'] = $runnerName3;
                        }

                    }

                    $query = DB::table('tbl_bet_pending_1')
                        ->where([['systemId',$systemId],['mid',$marketId],['result','PENDING'],['status',1]]);

                    $clientArr = $query->distinct()->select('uid')->get();

                    if( $clientArr->isNotEmpty() ){

                        $AllClients = [];
                        foreach ( $clientArr as $c ){
                            $AllClients[] = $c->uid;
                        }

                        $AllClients = array_unique($AllClients);

                        $client = [];
                        $cdata = DB::table('tbl_user_profit_loss')->select(['userId'])
                            ->where([['parentId',$uid]])->whereIn('clientId',$AllClients)
                            ->distinct()->select('userId')->get();

                        if ($cdata != null) {
                            foreach ($cdata as $c) {
                                $client[] = (int)$c->userId;
                            }
                        }

                    }

                    if(!empty( $client )){
                        $userList = DB::table('tbl_user')->select(['id','parentId','username','role','systemId'])
                            ->whereIn('id',$client)->where([['status',1],['systemId',$systemId]])->get();

                        if( $userList->isNotEmpty() && $runners != null ){
                            $runner1 = $runner2 = $runner3 = 0;

                            $t1 = $t2 = $t3 = null;
                            $i = 0;
                            foreach ( $userList as $user ){

                                $userData[$i] = [
                                    'username' => $user->username,
                                    'id' => $user->id,
                                    'role' => $user->role
                                ];

                                if( isset( $runners[0]['secId'] ) ){
                                    $runner1 = $this->getProfitLossBookMakerForBook($user,$marketId,$runners[0]['secId']);
                                    $t1[] = round($runner1,2);
                                    $userData[$i]['runner1'] = round($runner1);
                                }
                                if( isset( $runners[1]['secId'] ) ){
                                    $runner2 = $this->getProfitLossBookMakerForBook($user,$marketId,$runners[1]['secId']);
                                    $t2[] = round($runner2,2);
                                    $userData[$i]['runner2'] = round($runner2);
                                }
                                if( isset( $runners[2]['secId'] ) ){

                                    $runner3 = $this->getProfitLossBookMakerForBook($user,$marketId,$runners[2]['secId']);
                                    $t3[] = round($runner3,2);
                                    $userData[$i]['runner3'] = round($runner3);
                                }

                                $i++;
                            }

                        }

                    }

                }

                $response = [ "status" => 1 , "data" => [ 'titleData' => $titleData , 'userData' => $userData , 'totalData' => $totalData ] ];

            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data BookMaker
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionSystemBookDataBookMaker(Request $request)
    {

        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            if( isset( $request->marketId ) ){
                $titleData = $userData = $totalData = [];
                $user = Auth::user();
                $uid = $user->id;
                $marketId = $request->marketId;

                $redis = Redis::connection();
                $marketDataJson = $redis->get('Market_'.$marketId);
                $marketData = json_decode($marketDataJson);

                if( $marketData != null && isset($marketData->runners) ){
                    $runnerData = json_decode($marketData->runners);
                    $runners = [];
                    if( $runnerData != null ){
                        foreach ( $runnerData as $runner ){
                            $runners[] = [
                                'secId' => $runner->secId,
                                'name' => $runner->runner
                            ];
                        }
                    }

                    if( $runners != null ) {
                        $runnerName1 = $runnerName2 = $runnerName3 = '';
                        $titleData['0'] = 'User Name';
                        if (isset($runners[0]['name'])) {
                            $runnerName1 = $runners[0]['name'];
                            $titleData['1'] = $runnerName1;
                        }
                        if (isset($runners[1]['name'])) {
                            $runnerName2 = $runners[1]['name'];
                            $titleData['2'] = $runnerName2;
                        }
                        if (isset($runners[2]['name'])) {
                            $runnerName3 = $runners[2]['name'];
                            $titleData['3'] = $runnerName3;
                        }

                    }

                    $userList = DB::table('tbl_user')->select(['id','parentId','username','role','systemId'])
                        ->where([['parentId',$uid],['status',1],['systemId','!=',1],['role',1]])->get();

                    if( $userList->isNotEmpty() && $runners != null ){
                        $runner1 = $runner2 = $runner3 = 0;

                        $t1 = $t2 = $t3 = null;
                        $i = 0;
                        foreach ( $userList as $user ){

                            $userData[$i] = [
                                'username' => $user->username,
                                'id' => $user->id,
                                'role' => $user->role
                            ];

                            if( isset( $runners[0]['secId'] ) ){
                                $runner1 = $this->getProfitLossBookMakerForBook($user,$marketId,$runners[0]['secId']);
                                $t1[] = round($runner1,2);
                                $userData[$i]['runner1'] = round($runner1);
                            }
                            if( isset( $runners[1]['secId'] ) ){
                                $runner2 = $this->getProfitLossBookMakerForBook($user,$marketId,$runners[1]['secId']);
                                $t2[] = round($runner2,2);
                                $userData[$i]['runner2'] = round($runner2);
                            }
                            if( isset( $runners[2]['secId'] ) ){

                                $runner3 = $this->getProfitLossBookMakerForBook($user,$marketId,$runners[2]['secId']);
                                $t3[] = round($runner3,2);
                                $userData[$i]['runner3'] = round($runner3);
                            }

                            $i++;
                        }

                    }

                }

                $response = [ "status" => 1 , "data" => [ 'titleData' => $titleData , 'userData' => $userData , 'totalData' => $totalData ] ];

            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // get Profit Loss for Book Maker
    public function getProfitLossBookMakerForBook($user,$marketId,$secId)
    {
        $role = $user->role;
        $parentId = $user->parentId;
        $userId  = $user->id;
        $systemId  = $user->systemId;

        $clientArr = []; $book = 0;

        if( $parentId == 0 ){ return $book; }

        $query = DB::table('tbl_bet_pending_1');

        if( ( $role != 1 && $role != 4 ) || ( $systemId != 1 && $role == 1 )  ){
            $query->where([['mid',$marketId],['status',1],['systemId',$systemId],['result','PENDING']]);
            $clientJson = DB::table('tbl_user_child_data')->select('clients')
                ->where([['uid',$userId]])->first();
            if( $clientJson != null && isset( $clientJson->clients )){
                $client = json_decode($clientJson->clients);
                $query->whereIn('uid',$client);
            }

            $clientArr = $query->distinct()->select('uid')->get();
            //echo json_encode($clientArr);
            if( $clientArr->isNotEmpty() ){
                $total = 0; $totalArr = [];
                foreach ( $clientArr as $c ){

                    $bookData = DB::table('tbl_book_bookmaker')->select('book')
                        ->where([['mid',$marketId],['secId',$secId],['uid',$c->uid]])->first();

                    if( $bookData != null && $bookData->book != 0 ){
                        $total = $bookData->book;

                        if( $systemId != 1 && $role == 1 ){
                            $where = [['clientId',$c->uid],['parentId',$parentId]];
                        }else{
                            $where = [['clientId',$c->uid],['userId',$parentId]];
                        }

                        $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                            ->where($where)->first();

                        if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                            $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                            $totalArr[] = $total;
                        }

                    }

                }

                if( $totalArr != null ){
                    $book = round((-1)*array_sum($totalArr),2);
                }

            }

        }else{
            $total = $book = 0;
            $clientArr = $query->where([['mid',$marketId],['status',1],['systemId',$systemId],['uid',$userId],['result','PENDING']])->first();
            if( $clientArr != null ){
                $bookData = DB::table('tbl_book_bookmaker')->select('book')
                    ->where([['mid',$marketId],['secId',$secId],['uid',$userId]])->first();

                if( $bookData != null && $bookData->book != 0 ){
                    $total = $bookData->book;

                    if( $systemId != 1 ){
                        $where = [['clientId',$userId],['parentId',$parentId]];
                    }else{
                        $where = [['clientId',$userId],['userId',$parentId]];
                    }

                    $profitLoss = DB::table('tbl_user_profit_loss')
                        ->select(['actual_profit_loss'])->where($where)->first();

                    if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                        $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                    }

                    $book = round((-1)*$total,2);

                }
            }
        }

        return $book;

    }

    /**
     * action Book Data Fancy
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionBookDataFancy(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            $bookDataFancy = [];
            if( isset( $request->marketId ) ) {
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user(); $uid = $user->id;
                    if( $user->role == 6 ){ $uid = 1; }
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = DB::table('tbl_bet_pending_2')
                    ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $c ){
                        $client[] = $c->uid;
                    }

                    $client = array_unique($client);

                    $bookDataFancy = $this->bookDataFancy($systemId,$marketId,$client,$user);

                    if( $bookDataFancy != null ){
                        $bookDataFancyNew = [];
                        $i = $start = $startPl = $end = 0;

                        foreach ( $bookDataFancy as $index => $d) {

                            if ($index == 0) {
                                $bookDataFancyNew[] = [ 'price' => $d['price'] . ' or less' , 'profitLoss' => $d['profitLoss'] ];
                            } else {
                                if ($startPl != $d['profitLoss']) {
                                    if ($end != 0) {

                                        if( $start == $end ){
                                            $priceVal = $start;
                                        }else{
                                            $priceVal = $start . ' - ' . $end;
                                        }

                                        $bookDataFancyNew[] = [ 'price' => $priceVal , 'profitLoss' => $startPl ];
                                    }

                                    $start = $d['price'];
                                    $end = $d['price'];

                                } else {
                                    $end = $d['price'];
                                }
                                if ($index == ( count($bookDataFancy) - 1)) {
                                    $bookDataFancyNew[] = [ 'price' => $start . ' or more' , 'profitLoss' => $startPl ];
                                }

                            }

                            $startPl = $d['profitLoss'];
                            $i++;

                        }
                        $bookDataFancy = $bookDataFancyNew;
                    }

                }
                $response = [ "status" => 1 , "data" => $bookDataFancy ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data Fancy
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionSystemBookDataFancy(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            $bookDataFancy = [];
            if( isset( $request->marketId ) ) {
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user();
                    $uid = $user->id;
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = DB::table('tbl_bet_pending_2')
                    ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $c ){
                        $client[] = $c->uid;
                    }

                    $client = array_unique($client);

                    $bookDataFancy = $this->bookDataFancy($systemId,$marketId,$client,$user);

                    if( $bookDataFancy != null ){
                        $bookDataFancyNew = [];
                        $i = $start = $startPl = $end = 0;

                        foreach ( $bookDataFancy as $index => $d) {

                            if ($index == 0) {
                                $bookDataFancyNew[] = [ 'price' => $d['price'] . ' or less' , 'profitLoss' => $d['profitLoss'] ];
                            } else {
                                if ($startPl != $d['profitLoss']) {
                                    if ($end != 0) {

                                        if( $start == $end ){
                                            $priceVal = $start;
                                        }else{
                                            $priceVal = $start . ' - ' . $end;
                                        }

                                        $bookDataFancyNew[] = [ 'price' => $priceVal , 'profitLoss' => $startPl ];
                                    }

                                    $start = $d['price'];
                                    $end = $d['price'];

                                } else {
                                    $end = $d['price'];
                                }
                                if ($index == ( count($bookDataFancy) - 1)) {
                                    $bookDataFancyNew[] = [ 'price' => $start . ' or more' , 'profitLoss' => $startPl ];
                                }

                            }

                            $startPl = $d['profitLoss'];
                            $i++;

                        }
                        $bookDataFancy = $bookDataFancyNew;
                    }

                }
                $response = [ "status" => 1 , "data" => $bookDataFancy ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // book Data Fancy
    public function bookDataFancy($systemId,$marketId,$client,$user){

        $dataReturn = []; $userId = $user->id; if( $user->role == 6 ){ $userId = 1; }
        $query = DB::table('tbl_bet_pending_2')
            ->where([['mid',$marketId],['status', 1],['systemId',$systemId],['result','PENDING']])->whereIn('uid',$client);
        $betList = $query->select('price')->get();

        if( $betList != null ){
            $min = $max = 0;
            foreach ($betList as $index => $bet) {
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1; $max = $max+1;
            $allClient = implode(',', $client);

            for( $i=$min; $i<=$max; $i++ ){

                $sqlQuery = "SELECT  SUM( pb.win*upl.actual_profit_loss )/100 as winVal FROM tbl_bet_pending_2 as pb
                           LEFT JOIN tbl_user_profit_loss as upl ON upl.clientId = pb.uid AND upl.userId='".$userId."' 
                            where pb.mid='".$marketId."' and pb.status=1 and bType='no' and pb.result='PENDING' and pb.systemId='".$systemId."' and pb.uid in ($allClient) and pb.price > ".(int)$i;
                $betList1 = DB::select($sqlQuery);
                if( !isset($betList1[0]->winVal) ){ $winVal1 = 0; }else{ $winVal1 = $betList1[0]->winVal; }

                $sqlQuery = "SELECT  SUM( pb.win*upl.actual_profit_loss )/100 as winVal FROM tbl_bet_pending_2 as pb
                           LEFT JOIN tbl_user_profit_loss as upl ON upl.clientId = pb.uid AND upl.userId='".$userId."' 
                            where pb.mid='".$marketId."' and pb.status=1 and bType='yes' and pb.result='PENDING' and pb.systemId='".$systemId."' and pb.uid in ($allClient) and pb.price <= ".(int)$i;
                $betList2 = DB::select($sqlQuery);
                if( !isset($betList2[0]->winVal) ){ $winVal2 = 0; }else{ $winVal2 = $betList2[0]->winVal; }

                $sqlQuery = "SELECT  SUM( pb.loss*upl.actual_profit_loss )/100 as lossVal FROM tbl_bet_pending_2 as pb
                           LEFT JOIN tbl_user_profit_loss as upl ON upl.clientId = pb.uid AND upl.userId='".$userId."' 
                            where pb.mid='".$marketId."' and pb.status=1 and bType='yes' and pb.result='PENDING' and pb.systemId='".$systemId."' and pb.uid in ($allClient) and pb.price > ".(int)$i;
                $betList3 = DB::select($sqlQuery);
                if( !isset($betList3[0]->lossVal) ){ $lossVal1 = 0; }else{ $lossVal1 = $betList3[0]->lossVal; }

                $sqlQuery = "SELECT  SUM( pb.loss*upl.actual_profit_loss )/100 as lossVal FROM tbl_bet_pending_2 as pb
                           LEFT JOIN tbl_user_profit_loss as upl ON upl.clientId = pb.uid AND upl.userId='".$userId."' 
                            where pb.mid='".$marketId."' and pb.status=1 and bType='no' and pb.result='PENDING' and pb.systemId='".$systemId."' and pb.uid in ($allClient) and pb.price <= ".(int)$i;
                $betList4 = DB::select($sqlQuery);
                if( !isset($betList4[0]->lossVal) ){ $lossVal2 = 0; }else{ $lossVal2 = $betList4[0]->lossVal; }

                $profit = ( $winVal1 + $winVal2 );
                $loss = ( $lossVal1 + $lossVal2 );

                $total = $loss-$profit;

                $dataReturn[] = [
                    'price' => $i,
                    'profitLoss' => round($total,0),
                ];
            }

        }

        return $dataReturn;
    }

    /**
     * action Book Data Fancy3
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionBookDataFancy3(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            $bookDataFancy3 = [];
            if( isset( $request->marketId ) ) {
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user();
                    $uid = $user->id;
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = DB::table('tbl_bet_pending_2')
                    ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $c ){
                        $client[] = $c->uid;
                    }

                    $client = array_unique($client);

                    $bookDataFancy3 = $this->bookDataFancy3($systemId,$marketId,$client,$user);

                }

                $response = [ "status" => 1 , "data" => $bookDataFancy3 ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data Fancy3
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionSystemBookDataFancy3(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            $bookDataFancy3 = [];
            if( isset( $request->marketId ) ) {
                $user = Auth::user();
                $marketId = $request->marketId;

                $query = DB::table('tbl_bet_pending_2')
                    ->where([['systemId','!=',1],['mid',$marketId],['status',1],['result','PENDING']]);

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $c ){
                        $client[] = $c->uid;
                    }

                    $client = array_unique($client);

                    $bookDataFancy3 = $this->systemBookDataFancy3($marketId,$client,$user);

                }

                $response = [ "status" => 1 , "data" => $bookDataFancy3 ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    public function bookDataFancy3($systemId,$marketId,$client,$user){

        $userId = $user->id; if( $user->role == 6 ){ $userId = 1; }

        $betList1 = DB::table('tbl_bet_pending_2 as pb')->select(DB::raw('SUM( ( pb.win*upl.actual_profit_loss )/100 ) as winVal'))
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['upl.userId',$userId],['pb.status',1],['bType','yes'],['result','PENDING'],['pb.systemId',$systemId]])
                    ->whereIn('pb.uid',$client)->get();

        if( !isset($betList1[0]->winVal) ){ $yesWinVal = 0; }else{ $yesWinVal = $betList1[0]->winVal; }

        $betList2 = DB::table('tbl_bet_pending_2 as pb')->select(DB::raw('SUM( ( pb.loss*upl.actual_profit_loss )/100 ) as lossVal'))
            ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
            ->where([['pb.mid',$marketId],['upl.userId',$userId],['pb.status',1],['bType','yes'],['result','PENDING'],['pb.systemId',$systemId]])
            ->whereIn('pb.uid',$client)->get();

        if( !isset($betList2[0]->lossVal) ){ $yesLossVal = 0; }else{ $yesLossVal = $betList2[0]->lossVal; }

        $betList3 = DB::table('tbl_bet_pending_2 as pb')->select(DB::raw('SUM( ( pb.win*upl.actual_profit_loss )/100 ) as winVal'))
            ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
            ->where([['pb.mid',$marketId],['upl.userId',$userId],['pb.status',1],['bType','no'],['result','PENDING'],['pb.systemId',$systemId]])
            ->whereIn('pb.uid',$client)->get();

        if( !isset($betList3[0]->winVal) ){ $noWinVal = 0; }else{ $noWinVal = $betList3[0]->winVal; }

        $betList4 = DB::table('tbl_bet_pending_2 as pb')->select(DB::raw('SUM( ( pb.loss*upl.actual_profit_loss )/100 ) as lossVal'))
            ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
            ->where([['pb.mid',$marketId],['upl.userId',$userId],['pb.status',1],['bType','no'],['result','PENDING'],['pb.systemId',$systemId]])
            ->whereIn('pb.uid',$client)->get();

        if( !isset($betList4[0]->lossVal) ){ $noLossVal = 0; }else{ $noLossVal = $betList4[0]->lossVal; }

        $yesPl = (-1)*( $yesWinVal - $noLossVal );
        $noPl = (-1)*( $noWinVal - $yesLossVal );

        $dataReturn = [
            'yesPl' => round($yesPl),
            'noPl' => round($noPl),
        ];

        return $dataReturn;
    }

    public function systemBookDataFancy3($marketId,$client,$user){

        $userId = $user->id;

        $betList1 = DB::table('tbl_bet_pending_2 as pb')->select(DB::raw('SUM( ( pb.win*upl.actual_profit_loss )/100 ) as winVal'))
            ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
            ->where([['pb.mid',$marketId],['upl.parentId',$userId],['pb.status',1],['bType','yes'],['result','PENDING']])
            ->whereIn('pb.uid',$client)->get();

        if( !isset($betList1[0]->winVal) ){ $yesWinVal = 0; }else{ $yesWinVal = $betList1[0]->winVal; }

        $betList2 = DB::table('tbl_bet_pending_2 as pb')->select(DB::raw('SUM( ( pb.loss*upl.actual_profit_loss )/100 ) as lossVal'))
            ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
            ->where([['pb.mid',$marketId],['upl.parentId',$userId],['pb.status',1],['bType','yes'],['result','PENDING']])
            ->whereIn('pb.uid',$client)->get();

        if( !isset($betList2[0]->lossVal) ){ $yesLossVal = 0; }else{ $yesLossVal = $betList2[0]->lossVal; }

        $betList3 = DB::table('tbl_bet_pending_2 as pb')->select(DB::raw('SUM( ( pb.win*upl.actual_profit_loss )/100 ) as winVal'))
            ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
            ->where([['pb.mid',$marketId],['upl.parentId',$userId],['pb.status',1],['bType','no'],['result','PENDING']])
            ->whereIn('pb.uid',$client)->get();

        if( !isset($betList3[0]->winVal) ){ $noWinVal = 0; }else{ $noWinVal = $betList3[0]->winVal; }

        $betList4 = DB::table('tbl_bet_pending_2 as pb')->select(DB::raw('SUM( ( pb.loss*upl.actual_profit_loss )/100 ) as lossVal'))
            ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
            ->where([['pb.mid',$marketId],['upl.parentId',$userId],['pb.status',1],['bType','no'],['result','PENDING']])
            ->whereIn('pb.uid',$client)->get();

        if( !isset($betList4[0]->lossVal) ){ $noLossVal = 0; }else{ $noLossVal = $betList4[0]->lossVal; }


        $yesPl = (-1)*( $yesWinVal - $noLossVal );
        $noPl = (-1)*( $noWinVal - $yesLossVal );

        $dataReturn = [
            'yesPl' => round($yesPl),
            'noPl' => round($noPl),
        ];

        return $dataReturn;
    }

    /**
     * action Book Data Ball Session
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionBookDataBallSession(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            $bookDataFancy = [];
            if( isset( $request->marketId ) ) {
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user();
                    $uid = $user->id;
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = DB::table('tbl_bet_pending_3')
                    ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $c ){
                        $client[] = $c->uid;
                    }

                    $client = array_unique($client);

                    $bookDataFancy = $this->bookDataBallSession($systemId,$marketId,$client,$user);

                    if( $bookDataFancy != null ){
                        $bookDataFancyNew = [];
                        $i = $start = $startPl = $end = 0;

                        foreach ( $bookDataFancy as $index => $d) {

                            if ($index == 0) {
                                $bookDataFancyNew[] = [ 'price' => $d['price'] . ' or less' , 'profitLoss' => $d['profitLoss'] ];
                            } else {
                                if ($startPl != $d['profitLoss']) {
                                    if ($end != 0) {

                                        if( $start == $end ){
                                            $priceVal = $start;
                                        }else{
                                            $priceVal = $start . ' - ' . $end;
                                        }

                                        $bookDataFancyNew[] = [ 'price' => $priceVal , 'profitLoss' => $startPl ];
                                    }

                                    $start = $d['price'];
                                    $end = $d['price'];

                                } else {
                                    $end = $d['price'];
                                }
                                if ($index == ( count($bookDataFancy) - 1)) {
                                    $bookDataFancyNew[] = [ 'price' => $start . ' or more' , 'profitLoss' => $startPl ];
                                }

                            }

                            $startPl = $d['profitLoss'];
                            $i++;

                        }
                        $bookDataFancy = $bookDataFancyNew;
                    }

                }

                $response = [ "status" => 1 , "data" => $bookDataFancy ];
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data Ball Session
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionSystemBookDataBallSession(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            $bookDataFancy = [];
            if( isset( $request->marketId ) ) {
                $user = Auth::user();
                $marketId = $request->marketId;

                $query = DB::table('tbl_bet_pending_3')
                    ->where([['systemId','!=',1],['mid',$marketId],['status',1],['result','PENDING']]);

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $c ){
                        $client[] = $c->uid;
                    }

                    $client = array_unique($client);

                    $bookDataFancy = $this->systemBookDataBallSession($marketId,$client,$user);

                    if( $bookDataFancy != null ){
                        $bookDataFancyNew = [];
                        $i = $start = $startPl = $end = 0;

                        foreach ( $bookDataFancy as $index => $d) {

                            if ($index == 0) {
                                $bookDataFancyNew[] = [ 'price' => $d['price'] . ' or less' , 'profitLoss' => $d['profitLoss'] ];
                            } else {
                                if ($startPl != $d['profitLoss']) {
                                    if ($end != 0) {

                                        if( $start == $end ){
                                            $priceVal = $start;
                                        }else{
                                            $priceVal = $start . ' - ' . $end;
                                        }

                                        $bookDataFancyNew[] = [ 'price' => $priceVal , 'profitLoss' => $startPl ];
                                    }

                                    $start = $d['price'];
                                    $end = $d['price'];

                                } else {
                                    $end = $d['price'];
                                }
                                if ($index == ( count($bookDataFancy) - 1)) {
                                    $bookDataFancyNew[] = [ 'price' => $start . ' or more' , 'profitLoss' => $startPl ];
                                }

                            }

                            $startPl = $d['profitLoss'];
                            $i++;

                        }
                        $bookDataFancy = $bookDataFancyNew;
                    }

                }

                $response = [ "status" => 1 , "data" => $bookDataFancy ];
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Book Data Khado Session
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionBookDataKhadoSession(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            $bookDataFancy = [];
            if( isset( $request->marketId ) ) {
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id','role','systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user();
                    $uid = $user->id;
                }

                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = DB::table('tbl_bet_pending_3')
                    ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $c ){
                        $client[] = $c->uid;
                    }

                    $client = array_unique($client);
                    $bookDataFancy = $this->BookDataKhadoSession($systemId,$marketId,$client,$user);

                }

                $response = [ "status" => 1 , "data" => $bookDataFancy ];
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data Khado Session
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionSystemBookDataKhadoSession(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            $bookDataFancy = [];
            if( isset( $request->marketId ) ) {
                $user = Auth::user();
                $marketId = $request->marketId;

                $query = DB::table('tbl_bet_pending_3')
                    ->where([['systemId','!=',1],['mid',$marketId],['status',1],['result','PENDING']]);

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ){

                    $client = [];
                    foreach ( $clientArr as $c ){
                        $client[] = $c->uid;
                    }

                    $client = array_unique($client);

                    $bookDataFancy = $this->systemBookDataKhadoSession($marketId,$client,$user);

                    if( $bookDataFancy != null ){
                        $bookDataFancyNew = [];
                        $i = $start = $startPl = $end = 0;

                        foreach ( $bookDataFancy as $index => $d) {

                            if ($index == 0) {
                                $bookDataFancyNew[] = [ 'price' => $d['price'] . ' or less' , 'profitLoss' => $d['profitLoss'] ];
                            } else {
                                if ($startPl != $d['profitLoss']) {
                                    if ($end != 0) {

                                        if( $start == $end ){
                                            $priceVal = $start;
                                        }else{
                                            $priceVal = $start . ' - ' . $end;
                                        }

                                        $bookDataFancyNew[] = [ 'price' => $priceVal , 'profitLoss' => $startPl ];
                                    }

                                    $start = $d['price'];
                                    $end = $d['price'];

                                } else {
                                    $end = $d['price'];
                                }
                                if ($index == ( count($bookDataFancy) - 1)) {
                                    $bookDataFancyNew[] = [ 'price' => $start . ' or more' , 'profitLoss' => $startPl ];
                                }

                            }

                            $startPl = $d['profitLoss'];
                            $i++;

                        }
                        $bookDataFancy = $bookDataFancyNew;
                    }

                }

                $response = [ "status" => 1 , "data" => $bookDataFancy ];
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


    // system Book Data BallSession
    public function systemBookDataBallSession($marketId,$client,$user){

        $dataReturn = []; $userId = $user->id;
        $query = DB::table('tbl_bet_pending_3')
            ->where([['mid',$marketId],['status', 1],['systemId','!=',1],['result','PENDING']])->whereIn('uid',$client);
        $betList = $query->select('price')->get();

        if( $betList != null ){
            $min = $max = 0;
            foreach ($betList as $index => $bet) {
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1; $max = $max+1;

            for( $i=$min; $i<=$max; $i++ ){

                $betList1 = DB::table('tbl_bet_pending_3 as pb')->select(DB::raw('SUM( ( pb.win*upl.actual_profit_loss )/100 ) as noWinVal'))
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['upl.parentId',$userId],['pb.status',1],['bType','no'],['result','PENDING'],['pb.systemId','!=',1],['pb.price','>',(int)$i]])
                    ->whereIn('pb.uid',$client)->get();

                if( !isset($betList1[0]->noWinVal) ){ $noWinVal = 0; }else{ $noWinVal = $betList1[0]->noWinVal; }

                $betList2 = DB::table('tbl_bet_pending_3 as pb')->select(DB::raw('SUM( ( pb.loss*upl.actual_profit_loss )/100 ) as noLossVal'))
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['upl.parentId',$userId],['pb.status',1],['bType','no'],['result','PENDING'],['pb.systemId','!=',1],['pb.price','<=',(int)$i]])
                    ->whereIn('pb.uid',$client)->get();

                if( !isset($betList2[0]->noLossVal) ){ $noLossVal = 0; }else{ $noLossVal = $betList2[0]->noLossVal; }

                $betList3 = DB::table('tbl_bet_pending_3 as pb')->select(DB::raw('SUM( ( pb.win*upl.actual_profit_loss )/100 ) as yesWinVal'))
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['upl.parentId',$userId],['pb.status',1],['bType','yes'],['result','PENDING'],['pb.systemId','!=',1],['pb.price','<=',(int)$i]])
                    ->whereIn('pb.uid',$client)->get();

                if( !isset($betList3[0]->yesWinVal) ){ $yesWinVal = 0; }else{ $yesWinVal = $betList3[0]->yesWinVal; }

                $betList4 = DB::table('tbl_bet_pending_3 as pb')->select(DB::raw('SUM( ( pb.loss*upl.actual_profit_loss )/100 ) as yesLossVal'))
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['upl.parentId',$userId],['pb.status',1],['bType','yes'],['result','PENDING'],['pb.systemId','!=',1],['pb.price','>',(int)$i]])
                    ->whereIn('pb.uid',$client)->get();

                if( !isset($betList4[0]->yesLossVal) ){ $yesLossVal = 0; }else{ $yesLossVal = $betList4[0]->yesLossVal; }

                $profit = ( $yesWinVal+$noWinVal );
                $loss = ( $yesLossVal+$noLossVal );

                $total = $loss-$profit;

                $dataReturn[] = [
                    'price' => $i,
                    'profitLoss' => round($total,0),
                ];
            }

        }

        return $dataReturn;
    }

    // book Data BallSession
    public function bookDataBallSession($systemId,$marketId,$client,$user){

        $dataReturn = []; $userId = $user->id; if( $user->role == 6 ){ $userId = 1; }
        $query = DB::table('tbl_bet_pending_3')
            ->where([['mid',$marketId],['status', 1],['systemId',$systemId],['result','PENDING']])->whereIn('uid',$client);
        $betList = $query->select('price')->get();

        if( $betList != null ){
            $min = $max = 0;
            foreach ($betList as $index => $bet) {
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1; $max = $max+1;

            for( $i=$min; $i<=$max; $i++ ){

                $betList1 = DB::table('tbl_bet_pending_3 as pb')->select(DB::raw('SUM( ( pb.win*upl.actual_profit_loss )/100 ) as noWinVal'))
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['upl.userId',$userId],['pb.status',1],['bType','no'],['result','PENDING'],['pb.systemId',$systemId],['pb.price','>',(int)$i]])
                    ->whereIn('pb.uid',$client)->get();

                if( !isset($betList1[0]->noWinVal) ){ $noWinVal = 0; }else{ $noWinVal = $betList1[0]->noWinVal; }

                $betList2 = DB::table('tbl_bet_pending_3 as pb')->select(DB::raw('SUM( ( pb.loss*upl.actual_profit_loss )/100 ) as noLossVal'))
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['upl.userId',$userId],['pb.status',1],['bType','no'],['result','PENDING'],['pb.systemId',$systemId],['pb.price','<=',(int)$i]])
                    ->whereIn('pb.uid',$client)->get();

                if( !isset($betList2[0]->noLossVal) ){ $noLossVal = 0; }else{ $noLossVal = $betList2[0]->noLossVal; }

                $betList3 = DB::table('tbl_bet_pending_3 as pb')->select(DB::raw('SUM( ( pb.win*upl.actual_profit_loss )/100 ) as yesWinVal'))
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['upl.userId',$userId],['pb.status',1],['bType','yes'],['result','PENDING'],['pb.systemId',$systemId],['pb.price','<=',(int)$i]])
                    ->whereIn('pb.uid',$client)->get();

                if( !isset($betList3[0]->yesWinVal) ){ $yesWinVal = 0; }else{ $yesWinVal = $betList3[0]->yesWinVal; }

                $betList4 = DB::table('tbl_bet_pending_3 as pb')->select(DB::raw('SUM( ( pb.loss*upl.actual_profit_loss )/100 ) as yesLossVal'))
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['upl.userId',$userId],['pb.status',1],['bType','yes'],['result','PENDING'],['pb.systemId',$systemId],['pb.price','>',(int)$i]])
                    ->whereIn('pb.uid',$client)->get();

                if( !isset($betList4[0]->yesLossVal) ){ $yesLossVal = 0; }else{ $yesLossVal = $betList4[0]->yesLossVal; }

                $profit = ( $yesWinVal+$noWinVal );
                $loss = ( $yesLossVal+$noLossVal );

                $total = $loss-$profit;

                $dataReturn[] = [
                    'price' => $i,
                    'profitLoss' => round($total,0),
                ];
            }

        }

        return $dataReturn;
    }


    // Book Data KhadoSession
    public function BookDataKhadoSession($systemId,$marketId,$client,$user){

        $dataReturn = []; $userId = $user->id;
        if( $user->role == 6 ){ $userId = 1; }

        $betList = DB::table('tbl_bet_pending_3')
            ->select('bType', 'price', 'win', 'loss','mid','uid','diff')
            ->where([['result', 'PENDING'],['mid', $marketId] , ['status', 1],['systemId',$systemId]])
            ->get();

        if( $betList != null ){

            $min = 0; $max = 0;

            foreach ($betList as $index => $bet) {
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->diff;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->diff)
                    $max = $bet->diff;
            }

            $min = $min-1; $max = $max+1;
            $client = implode(', ', $client);
           // print_r($client);
            for($i=$min;$i<=$max;$i++){
                $currentVal=(int)$i;

                $sqlQuery = "SELECT  SUM( pb.win*upl.actual_profit_loss )/100 as winVal FROM tbl_bet_pending_3 as pb
                                       LEFT JOIN tbl_user_profit_loss as upl ON upl.clientId = pb.uid AND upl.userId=$userId
                                        where (pb.diff > $currentVal and pb.price <= $currentVal) and pb.mid='" . $marketId .  "' and pb.status=1 and pb.result='PENDING'  and pb.systemId=$systemId and pb.uid in ($client)";
                $betList1 = DB::select($sqlQuery);
                if (!isset($betList1[0]->winVal)) {
                    $winVal = 0;
                } else {
                    $winVal = $betList1[0]->winVal;
                }

                $sqlQuery = "SELECT  SUM( pb.loss*upl.actual_profit_loss )/100 as lossVal FROM tbl_bet_pending_3 as pb
                                       LEFT JOIN tbl_user_profit_loss as upl ON upl.clientId = pb.uid AND upl.userId=$userId
                                        where (diff <= $currentVal OR price > $currentVal) and pb.mid='" . $marketId . "' and pb.status=1 and pb.result='PENDING' and pb.systemId=$systemId and pb.uid in ($client)";
                $betList11 = DB::select($sqlQuery);
                if (!isset($betList11[0]->lossVal)) {
                    $lossVal = 0;
                } else {
                    $lossVal = $betList11[0]->lossVal;
                }

                $total = $lossVal-$winVal;

                $dataReturn[] = [
                    'price' => $i,
                    'profitLoss' => round($total,0),
                    'loss'=>$lossVal,
                    'profit'=>$winVal
                ];
            }

        }

        return $dataReturn;

    }

    // system Book Data KhadoSession
    public function systemBookDataKhadoSession($marketId,$client,$user){

        $dataReturn = []; $userId = $user->id;

        $betList = DB::table('tbl_bet_pending_3')
            ->select('bType', 'price', 'win', 'loss','mid','uid','diff')
            ->where([['result', 'PENDING'],['mid', $marketId] , ['status', 1],['systemId','!=',1]])
            ->get();

        if( $betList != null ){

            $min = 0; $max = 0;

            foreach ($betList as $index => $bet) {
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->diff;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->diff)
                    $max = $bet->diff;
            }

            $min = $min-1; $max = $max+1;
            $client = implode(', ', $client);
            for($i=$min;$i<=$max;$i++){

                $currentVal=(int)$i;
                /* $betList1 = DB::table('tbl_bet_pending_3 as pb')->select(DB::raw('SUM( ( pb.win*upl.actual_profit_loss )/100 ) as winVal'))
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['upl.parentId',$userId],['pb.status',1],['result','PENDING'],['pb.systemId','!=',1],['pb.price','<=',(int)$i],['pb.diff','>',(int)$i]])
                    ->whereIn('pb.uid',$client)->get();

                if( !isset($betList1[0]->winVal) ){ $winVal = 0; }else{ $winVal = $betList1[0]->winVal; }

                $betList2 = DB::table('tbl_bet_pending_3 as pb')->select(DB::raw('SUM( ( pb.win*upl.actual_profit_loss )/100 ) as lossVal'))
                    ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                    ->where([['pb.mid',$marketId],['upl.parentId',$userId],['pb.status',1],['result','PENDING'],['pb.systemId','!=',1],['pb.price','>',(int)$i],['pb.diff','<=',(int)$i]])
                    ->whereIn('pb.uid',$client)->get();

                if( !isset($betList2[0]->lossVal) ){ $lossVal = 0; }else{ $lossVal = $betList2[0]->lossVal; }
 */

                $sqlQuery = "SELECT  SUM( pb.win*upl.actual_profit_loss )/100 as winVal FROM tbl_bet_pending_3 as pb
                                       LEFT JOIN tbl_user_profit_loss as upl ON upl.clientId = pb.uid AND upl.userId='".$userId."' 
                                        where (pb.diff > $currentVal and pb.price <= $currentVal) and pb.mid='" . $marketId . "' and pb.status=1 and pb.result='PENDING'  and pb.systemId !=1 and pb.uid in ($client)";
                $betList1 = DB::select($sqlQuery);
                if (!isset($betList1[0]->winVal)) {
                    $winVal = 0;
                } else {
                    $winVal = $betList1[0]->winVal;
                }

                $sqlQuery = "SELECT  SUM( pb.loss*upl.actual_profit_loss )/100 as lossVal FROM tbl_bet_pending_3 as pb
                                       LEFT JOIN tbl_user_profit_loss as upl ON upl.clientId = pb.uid AND upl.userId='".$userId."' 
                                        where (diff <= $currentVal OR price > $currentVal) and pb.mid='" . $marketId . "' and pb.status=1 and pb.result='PENDING' and pb.systemId !=1 and pb.uid in ($client)";
                $betList11 = DB::select($sqlQuery);
                if (!isset($betList11[0]->lossVal)) {
                    $lossVal = 0;
                } else {
                    $lossVal = $betList11[0]->lossVal;
                }
                $total = $winVal-$lossVal;

                $dataReturn[] = [
                    'price' => $i,
                    'profitLoss' => round($total,0),
                    'loss'=>$lossVal,
                    'profit'=>$winVal
                ];
            }

        }

        return $dataReturn;

    }

    /**
     * action Book Data Casino
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionBookDataCasino(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $bookData = [];
            if( isset( $request->marketId ) ) {
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->select(['id', 'role', 'systemId'])->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user();
                    $uid = $user->id; if( $user->role == 6 ){ $uid = 1; }
                }
                $marketId = $request->marketId;
                $systemId = $user->systemId;

                $query = DB::table('tbl_bet_pending_4')
                    ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ) {

                    $client = [];
                    foreach ($clientArr as $c) {
                        $client[] = $c->uid;
                    }

                    $client = array_unique($client);

                    for( $i=0; $i<=9; $i++ ){
                        $profitLoss = $this->profitLossCasino($uid,$systemId,$marketId,$client,$i);
                        $profitLoss = (-1)*$profitLoss;
                        $bookData[] = [
                            'number' => $i,
                            'profitLoss' => round($profitLoss,0),
                        ];

                    }

                    $response = ["status" => 1, "data" => $bookData];
                }

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action System Book Data Casino
     * Action - Post
     * Created at FEB 2020 by dream
     */
    public function actionSystemBookDataCasino(Request $request)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{
            $bookData = [];
            if( isset( $request->marketId ) ) {
                $user = Auth::user();
                $uid = $user->id;
                $marketId = $request->marketId;

                $query = DB::table('tbl_bet_pending_4')
                    ->where([['systemId','!=',1],['mid',$marketId],['status',1],['result','PENDING']]);

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr->isNotEmpty() ) {

                    $client = [];
                    foreach ($clientArr as $c) {
                        $client[] = $c->uid;
                    }

                    $client = array_unique($client);

                    for( $i=0; $i<=9; $i++ ){
                        $profitLoss = $this->systemProfitLossCasino($uid,$marketId,$client,$i);
                        $profitLoss = (-1)*$profitLoss;
                        $bookData[] = [
                            'number' => $i,
                            'profitLoss' => round($profitLoss,0),
                        ];

                    }

                    $response = ["status" => 1, "data" => $bookData];
                }

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // profitLossCasino
    public function profitLossCasino($uid,$systemId,$marketId,$client,$secId){

        $win = $loss = $total = 0; $totalArr = [];
        if( $client != null ){

            $betData = DB::table('tbl_bet_pending_4')->select(['uid','win','loss','secId'])
                ->where([['result','PENDING'],['status',1],['mid',$marketId],['systemId',$systemId]])
                ->whereIn('uid',$client)->get();

            if( $betData->isNotEmpty() ){
                $newUser = [];
                foreach ( $betData as $bets ){
                    if( !isset($newUser[$bets->uid]['total']) ){
                        $newUser[$bets->uid]['total'] = 0;
                    }

                    if( $bets->secId == $secId ){
                        $newUser[$bets->uid]['total'] = ( $newUser[$bets->uid]['total'] + $bets->win );
                    }else{
                        $newUser[$bets->uid]['total'] = ( $newUser[$bets->uid]['total'] - $bets->loss );
                    }
                }

                if( $newUser != null ){
                    $totalArr = [];
                    foreach ( $newUser as $usr=>$data ){
                        $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                            ->where([['clientId',$usr],['userId',$uid]])->first();

                        if( $data['total'] != 0 && $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                            $totalArr[] = ($data['total']*(float)$profitLoss->actual_profit_loss)/100;
                        }
                    }
                }

                if( $totalArr != null && array_sum($totalArr) != 0 ){
                    return round( array_sum($totalArr) );
                }
            }

        }

        return $total;

    }

    // profitLossCasino
    public function systemProfitLossCasino($uid,$marketId,$client,$secId){

        $win = $loss = $total = 0; $totalArr = [];
        if( $client != null ){

            $betData = DB::table('tbl_bet_pending_4')->select(['uid','win','loss','secId'])
                ->where([['result','PENDING'],['status',1],['mid',$marketId],['systemId','!=',1]])
                ->whereIn('uid',$client)->get();

            if( $betData->isNotEmpty() ){
                $newUser = [];
                foreach ( $betData as $bets ){
                    if( !isset($newUser[$bets->uid]['total']) ){
                        $newUser[$bets->uid]['total'] = 0;
                    }

                    if( $bets->secId == $secId ){
                        $newUser[$bets->uid]['total'] = ( $newUser[$bets->uid]['total'] + $bets->win );
                    }else{
                        $newUser[$bets->uid]['total'] = ( $newUser[$bets->uid]['total'] - $bets->loss );
                    }
                }

                if( $newUser != null ){
                    $totalArr = [];
                    foreach ( $newUser as $usr=>$data ){
                        $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                            ->where([['clientId',$usr],['parentId',$uid]])->first();

                        if( $data['total'] != 0 && $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                            $totalArr[] = ($data['total']*(float)$profitLoss->actual_profit_loss)/100;
                        }
                    }
                }

                if( $totalArr != null && array_sum($totalArr) != 0 ){
                    return round( array_sum($totalArr) );
                }
            }

        }

        return $total;

    }

    /**
     * action Bet Delete
     * Action - Get
     * Created at FEB 2020 by dream
     */
    public function actionBetDelete($betId)
    {
        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];

        try{

            $cUser = Auth::user();
            if( $cUser != null && $cUser->role != 1){ return $response; }

            if($betId != null){

                if( $betId >= 10000000 && $betId < 20000000 ){
                    $tbl = 'tbl_bet_pending_1';
                }elseif ( $betId >= 20000000 && $betId < 30000000 ){
                    $tbl = 'tbl_bet_pending_2';
                }elseif ( $betId >= 30000000 && $betId < 40000000 ){
                    $tbl = 'tbl_bet_pending_3';
                }elseif ( $betId >= 40000000 && $betId < 50000000 ){
                    $tbl = 'tbl_bet_pending_4';
                }else{
                    return $response;
                }

                $betData = DB::table($tbl)->where([ ['id',$betId],['status',1],['result','Pending'] ])->first();

                if( $betData != null ){

                    if( DB::table($tbl)->where([ ['id',$betId],['status',1],['result','Pending'] ])->update(['status' => 2]) ){

                        $uid = $betData->uid;
                        $marketId = $betData->mid;

                        $oldMarketExpose = $newMarketExpose = $pendingMarketExpose = $exposeBalance = $newMarketProfit = 0;
                        $userExpose = DB::table('tbl_user_market_expose')
                            ->where([['uid',$uid],['status',1],['mid','!=',$marketId]])->sum("expose");

                        if(!empty($userExpose)){
                            if( $userExpose < 0 ){
                                $oldMarketExpose = (-1)*($userExpose);
                            }else{
                                $oldMarketExpose = $userExpose;
                            }
                        }

                        $getNewExpose = $this->checkExposeBalance($betData);

                        if(!empty($getNewExpose)) {

                            $newMarketExpose = $getNewExpose['expose'];
                            $newMarketProfit = $getNewExpose['profit'];

                            if( $newMarketExpose < 0 ){
                                $newMarketExpose = (-1)*$newMarketExpose;
                                $exposeBalance = $oldMarketExpose+$newMarketExpose;
                            }else{
                                $exposeBalance = $oldMarketExpose+$newMarketExpose;
                            }

                            $updated_on = date('Y-m-d H:i:s');

                            if( DB::table('tbl_user_info')->where('uid',$uid)->update(['expose' => $exposeBalance,'updated_on' => $updated_on]) ){

                                DB::table('tbl_user_market_expose')->where([['uid',$uid],['status',1],['mid',$marketId]])
                                    ->update(['expose' => $newMarketExpose , 'profit' => $newMarketProfit]);

                                $response = [ 'status' => 1, 'success' => [ 'message' => 'Bet Deleted Successfully!' ] ];
                            }else{
                                $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Something Wrong! Event Not Updated!!" ] ];
                            }

                        }else{
                            $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Something Wrong! Event Not Updated!!" ] ];
                        }

                    }else{
                        $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Something Wrong! Event Not Updated!!" ] ];
                    }

                }else{
                    $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Something Wrong! Event Not Updated!!" ] ];
                }

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // checkExposeBalance
    public function checkExposeBalance($betData)
    {
        $uid = $betData->uid;
        $mType = $betData->mType;
        $marketId = $betData->mid;
        $eventId = $betData->eid;
        $resultArray = $dataExpose = $maxBal['expose'] = $maxBal['plus'] = $balPlus = $balPlus1 = $balExpose = $balExpose2 = $profitLossNew = [];

        $minExpose = $maxProfit = 0;

        if( $mType == 'match_odd' ) {
            if (!empty($marketId)) {
                $profitLossData = $this->getProfitLossMatchOddOnZeroFinal($betData);

                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if ($profitLossData != null) {
                    $maxProfit = max($profitLossData);
                }
            }
        }

        if( $mType == 'bookmaker' ) {
            if (!empty($marketId)) {
                $profitLossData = $this->getProfitLossBookmakerOnZeroFinal($betData);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if ($profitLossData != null) {
                    $maxProfit = max($profitLossData);
                }
            }
        }

        if( $mType == 'fancy2' || $mType == 'fancy') {
            if (!empty($marketId)) {
                $profitLossData = $this->getProfitLossFancyOnZeroFinal($uid,$marketId);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if ($profitLossData != null) {
                    $maxProfit = max($profitLossData);
                }
            }
        }

        if( $mType == 'fancy3' ){
            if( !empty( $marketId) ) {
                $profitLossData = $this->getProfitLossSingleFancyOnZeroFinal($uid,$marketId);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if( $profitLossData != null ){
                    $maxProfit = max($profitLossData);
                }
            }
        }


        if( $mType == 'ballbyball' ) {
            if (!empty($marketId)) {
                $profitLossData = $this->getProfitLossBallByBallFinal($uid,$marketId);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if ($profitLossData != null) {
                    $maxProfit = max($profitLossData);
                }
            }
        }

        if( $mType == 'khado' ) {
            if( !empty( $marketId) ) {
                $profitLossData = $this->getProfitLossKhadoFinal($uid,$marketId);
                if ($profitLossData != null) {
                    $minExpose = min($profitLossData);
                }

                if( $profitLossData != null ){
                    $maxProfit = max($profitLossData);
                }
            }
        }


        if( $mType == 'cricket_casino' ){
            if( !empty( $marketId) ) {
                $balExpose = $balPlus = [];
                for($n=0;$n<10;$n++){
                    $profitLoss = $this->getCasinoProfitLossFinal($uid,$eventId,$marketId,$n);
                    if( $profitLoss < 0 ){
                        $balExpose[] = $profitLoss;
                    }else{
                        $balPlus[] = $profitLoss;
                    }
                }

                if( $balExpose != null ){
                    $minExpose = min($balExpose);
                }
                if( $balPlus != null ){
                    $maxProfit = max($balPlus);
                }
            }
        }

        if( $mType == 'jackpot' ){
            if( !empty( $marketId) ) {
                $profitLossData = $this->getProfitLossJackpotOnZeroFinal($uid,$marketId);

                if( isset($profitLossData['balExpose']) && $profitLossData['balExpose'] != null ){
                    $minExpose = min($balExpose);
                }

                if( isset($profitLossData['balPlus']) && $profitLossData['balPlus'] != null ){
                    $maxProfit = max($balPlus);
                }

            }

        }

        $resultArray = [ 'expose' => $minExpose,'profit' => $maxProfit ];

        return $resultArray;

    }

    // get ProfitLoss MatchOdd OnZeroFinal
    public function getProfitLossMatchOddOnZeroFinal($betData)
    {

        $returnArr = [];
        $redis = Redis::connection();
        $matchOddJson = $redis->get("Market_" . $betData->mid);
        $matchOdd = json_decode($matchOddJson);

        if( $matchOdd != null && isset( $matchOdd->runners )){

            $eventId = $matchOdd->eid;
            $marketId = $matchOdd->mid;

            $runners = json_decode($matchOdd->runners);

            if( $runners != null ){
                $i = 0; $bookDataArr = [];
                foreach ( $runners as $runner ){

                    $bookDataArr[$i] = [
                        'uid' => $betData->uid,
                        'eid' => $eventId,
                        'mid' => $marketId,
                    ];

                    if( $betData->bType == 'lay' ){

                        if( $runner->secId == $betData->secId ){
                            $book = ( $betData->rate-1 )*$betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( $book , 2);
                        }else{
                            $book = $betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( (-1)*$book, 2);
                        }

                    }else{

                        if( $runner->secId == $betData->secId ){
                            $book = ( $betData->rate-1 )*$betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( (-1)*$book, 2);
                        }else{
                            $book = $betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( $book , 2);
                        }

                    }

                    $i++;
                }

                if( $bookDataArr != null ){

                    $book = DB::table('tbl_book_matchodd')->select(['book'])
                        ->where([['uid',$betData->uid],['eid',$eventId],['mid',$marketId]])->first();

                    if( $book != null ){

                        foreach ( $bookDataArr as $bookData ){
                            $where = [['uid',$betData->uid],['eid',$eventId],['mid',$marketId],['secId',$bookData['secId']]];
                            $bookCheck = DB::table('tbl_book_matchodd')->select(['book'])->where($where)->first();
                            if( $bookCheck != null ){
                                $newBook = round( ($bookCheck->book+$bookData['book']) , 2 );
                                DB::table('tbl_book_matchodd')->where($where)->update(['book'=>$newBook]);
                            }else{
                                DB::table('tbl_book_matchodd')->insert($bookData);
                            }
                        }

                    }else{
                        DB::table('tbl_book_matchodd')->insert($bookDataArr);
                    }


                }


                $bookDataNew = DB::table('tbl_book_matchodd')->select(['book'])
                    ->where([['uid',$betData->uid],['eid',$eventId],['mid',$marketId]])->get();
                if( $bookDataNew->isNotEmpty() ){
                    foreach ( $bookDataNew as $bookNew ){
                        $returnArr[] = $bookNew->book;
                    }
                }

            }
        }

        return $returnArr;

    }

    // get ProfitLoss Bookmaker OnZeroFinal
    public function getProfitLossBookmakerOnZeroFinal($betData)
    {

        $returnArr = [];
        $redis = Redis::connection();
        $bookMakerJson = $redis->get("Market_" . $betData->mid);
        $bookMaker = json_decode($bookMakerJson);

        if( $bookMaker != null && isset( $bookMaker->runners )){

            $eventId = $bookMaker->eid;
            $marketId = $bookMaker->mid;

            $runners = json_decode($bookMaker->runners);

            if( $runners != null ){
                $i = 0; $bookDataArr = [];
                foreach ( $runners as $runner ){

                    $bookDataArr[$i] = [
                        'uid' => $betData->uid,
                        'eid' => $eventId,
                        'mid' => $marketId,
                    ];

                    if( $betData->bType == 'lay' ){

                        if( $runner->secId == $betData->secId ){
                            $book = ( $betData->rate*$betData->size )/100;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( $book , 2);
                        }else{
                            $book = $betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( (-1)*$book, 2);
                        }

                    }else{

                        if( $runner->secId == $betData->secId ){
                            $book = ( $betData->rate*$betData->size )/100;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( (-1)*$book, 2);
                        }else{
                            $book = $betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( $book , 2);
                        }

                    }

                    $i++;
                }

                if( $bookDataArr != null ){

                    $book = DB::table('tbl_book_bookmaker')->select(['book'])
                        ->where([['uid',$betData->uid],['eid',$eventId],['mid',$marketId]])->first();

                    if( $book != null ){

                        foreach ( $bookDataArr as $bookData ){
                            $where = [['uid',$betData->uid],['eid',$eventId],['mid',$marketId],['secId',$bookData['secId']]];
                            $bookCheck = DB::table('tbl_book_bookmaker')->select(['book'])->where($where)->first();
                            if( $bookCheck != null ){
                                $newBook = round( ($bookCheck->book+$bookData['book']) , 2 );
                                DB::table('tbl_book_bookmaker')->where($where)->update(['book'=>$newBook]);
                            }else{
                                DB::table('tbl_book_bookmaker')->insert($bookData);
                            }
                        }

                    }else{
                        DB::table('tbl_book_bookmaker')->insert($bookDataArr);
                    }

                }

                $bookDataNew = DB::table('tbl_book_bookmaker')->select(['book'])
                    ->where([['uid',$betData->uid],['eid',$eventId],['mid',$marketId]])->get();
                if( $bookDataNew->isNotEmpty() ){
                    foreach ( $bookDataNew as $bookNew ){
                        $returnArr[] = $bookNew->book;
                    }
                }

            }
        }

        return $returnArr;

    }

    // get Profit Loss Jackpot OnZeroFinal
    public function getProfitLossJackpotOnZeroFinal($userId,$marketId)
    {

        $marketList = DB::table('tbl_bet_pending_4')->select('secId','mid','eid','win','loss')
            ->where([['uid',$userId],['mid',$marketId],['result','PENDING']])
            ->whereIn('status',[1,5])
            ->orderBy('created_on' ,'DESC')
            ->get();

        $balExpose = $balPlus = $betmarkrtID = [];
        if( $marketList != null ){
            $betarray = [];

            foreach ( $marketList as $market ){
                $betarray[] = $market;
                $betsecID[] = $market->secId;
            }

            if(!empty( $betsecID ) ){

                // $gameid = 1;
                $cache = Redis::connection();
                $listData = $cache->get("Jackpot_type");
                $listData = json_decode($listData);

                foreach ($listData as $value) {
                    $gameid = $value->id;

                    $jackpotMarket = $cache->get("Jackpot_" . $gameid . "_" . $marketId);
                    $jackpotMarket = json_decode($jackpotMarket);

                    $total = $expose = $count = $totalLoss1 = 0;
                    $final_bet_array = $expose_array = $jackpot_array = [];

                    if(!empty($jackpotMarket)){
                        foreach ($jackpotMarket as $key => $jackpotarray ) {

                            $rate = $jackpotarray->rate;
                            $secId = $jackpotarray->secId;

                            $total = $totalWin = $totalLoss = 0;

                            foreach ($betarray as $key => $betarrays) {
                                if($secId!= $betarrays->secId){
                                    $totalLoss= $totalLoss+$betarrays->loss;
                                    $totalLoss1=$totalLoss;
                                }
                            }

                            $total1 = 0;
                            foreach ($betarray as $key => $betarrays) {
                                if($secId== $betarrays->secId){
                                    $totalWin= $totalWin+$betarrays->win;
                                    $total1 = $totalWin-$totalLoss;
                                }
                            }

                            $total = $totalWin-$totalLoss;

                            if( $total < 0 ){
                                $balExpose[] = $total;
                            }else{
                                $balPlus[] = $total;
                            }

                        }
                    }
                }
            }
        }

        return [ 'balExpose' => $balExpose, 'balPlus' => $balPlus ];

    }

    // get Profit Loss FancyOnZeroFinal
    public function getProfitLossFancyOnZeroFinal($userId,$marketId)
    {

        $betList = DB::table('tbl_bet_pending_2')->select(['bType','price','win','loss'])
            ->where([['uid',$userId],['result','Pending'],['mid',$marketId]])
            ->whereIn('status',[1,5])->get();
        $result = $newbetresult = [];

        if( $betList != null ){
            $result = $betresult = [];
            $min = $max = 0;

            foreach ($betList as $index => $bet) {
                $betresult[] = [ 'price' => $bet->price, 'bType' => $bet->bType, 'loss' => $bet->loss, 'win' => $bet->win ];
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1;
            $max = $max+1;
            $win = $loss=0;
            $count = $min;
            $totalbetcount = count($betresult);
            foreach ($betresult as $key => $value) {
                $val = $value['price']- $count;
                $minval = $value['price'] -$min;
                $maxval = $max-$value['price'];
                $bType = $value['bType'];
                $loss = $value['loss'];
                $newresult = [];
                $top = $bottom = $profitcount = $losscount = 0;

                for( $i= 0; $i < $minval; $i++){
                    if($bType == 'no'){
                        $top = $top+$value['win'];
                        $profitcount++;
                        $newresult[] = ['count' => $count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win']];
                    }else{
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                        $newresult[] = ['count'=>$count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose'=> (-1)*$value['loss']];
                    }
                    $count++;
                }

                for( $i= 0; $i <= $maxval; $i++){
                    if($bType == 'no'){
                        $newresult[] = ['count' => $count,'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => (-1)*$value['loss']];
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                    }else{
                        $top = $top + $value['win'];
                        $profitcount++;
                        $newresult[] = ['count' => $count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win']];
                    }

                    $count++;
                }
                $result[] = ['count' => $value['price'], 'bType' => $value['bType'], 'profit'=>$top, 'loss'=>$bottom, 'profitcount' => $profitcount, 'losscount' => $losscount, 'newarray' => $newresult];
            }

            $newbetarray = $newbetresult = [];
            $totalmaxcount = $max-$min;
            if( $totalmaxcount > 0 ){
                for( $i = 0; $i < $totalmaxcount; $i++ ){
                    $newbetarray1 = []; $finalexpose = 0;
                    for( $x = 0; $x < $totalbetcount; $x++ ){
                        $expose = $result[$x]['newarray'][$i]['expose'];
                        $finalexpose = $finalexpose+$expose;

                        $newbetarray1[] = [ 'bet_price' => $result[$x]['count'], 'bType' => $result[$x]['bType'], 'expose' => $expose ];
                    }
                    $newbetresult[] = $finalexpose;
                    $newbetarray[] = [ 'exposearray' => $newbetarray1, 'finalexpose' => $finalexpose ];
                }
            }

            return $newbetresult;
        }
    }

    // get Profit Loss BallByBallFinal
    public function getProfitLossBallByBallFinal($userId,$marketId)
    {

        $betList = DB::table('tbl_bet_pending_3')->select(['bType','price','win','loss'])
            ->where([['uid',$userId],['result','Pending'],['mid',$marketId]])
            ->whereIn('status',[1,5])->get();

        $result = $newbetresult = [];
        if( $betList != null ){
            $result =  $betresult = [];
            $min = $max = 0;

            foreach ($betList as $index => $bet) {
                $betresult[] = array('price'=>$bet->price,'bType'=>$bet->bType,'loss'=>$bet->loss,'win'=>$bet->win);
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1; $max = $max+1; $win = $loss = 0; $count = $min;
            $totalbetcount = count($betresult);
            foreach ($betresult as $key => $value) {
                $val = $value['price']- $count;
                $minval = $value['price'] -$min;
                $maxval = $max-$value['price'];
                $bType = $value['bType'];
                $loss = $value['loss'];
                $newresult = [];
                $top = $bottom = $profitcount = $losscount = 0;

                for( $i= 0; $i < $minval; $i++){
                    if($bType == 'no'){
                        $top = $top+$value['win'];
                        $profitcount++;
                        $newresult[] = ['count' => $count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win']];
                    }else{
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                        $newresult[] = ['count'=>$count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose'=> (-1)*$value['loss']];
                    }
                    $count++;
                }

                for( $i= 0; $i <= $maxval; $i++){
                    if($bType == 'no'){
                        $newresult[] = ['count' => $count,'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => (-1)*$value['loss']];
                        $bottom = $bottom + $value['loss'];
                        $losscount++;
                    }else{
                        $top = $top + $value['win'];
                        $profitcount++;
                        $newresult[] = ['count' => $count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win']];
                    }

                    $count++;
                }
                $result[] = ['count' => $value['price'], 'bType' => $value['bType'], 'profit'=>$top, 'loss'=>$bottom, 'profitcount' => $profitcount, 'losscount' => $losscount, 'newarray' => $newresult];
            }

            $newbetarray = $newbetresult = [];
            $totalmaxcount = $max-$min;
            if( $totalmaxcount > 0 ){
                for( $i = 0; $i < $totalmaxcount; $i++ ){
                    $newbetarray1 = []; $finalexpose = 0;
                    for( $x = 0; $x < $totalbetcount; $x++ ){
                        $expose = $result[$x]['newarray'][$i]['expose'];
                        $finalexpose = $finalexpose+$expose;

                        $newbetarray1[] = ['bet_price' => $result[$x]['count'], 'bType' => $result[$x]['bType'], 'expose' => $expose];
                    }
                    $newbetresult[] = $finalexpose;
                    $newbetarray[] = ['exposearray' => $newbetarray1, 'finalexpose' => $finalexpose];
                }
            }

        }

        return $newbetresult;
    }

    // get Profit Loss KhadoFinal
    public function getProfitLossKhadoFinal($userId,$marketId)
    {

        $betList = DB::table('tbl_bet_pending_3')->select(['bType','price','win','loss'])
            ->where([['uid',$userId],['result','Pending'],['mid',$marketId]])
            ->whereIn('status',[1,5])->get();

        $result = $newbetresult = [];
        if( $betList != null ){
            $result = $betresult = []; $min = $max = 0;

            foreach ($betList as $index => $bet) {
                $betresult[] = [ 'price' => $bet->price, 'bType' => $bet->bType, 'loss' => $bet->loss, 'win' => $bet->win ];
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price)
                    $min = $bet->price;
                if ($max < $bet->price)
                    $max = $bet->price;
            }

            $min = $min-1; $max = $max+1; $win = $loss = 0; $count = $min;

            $totalbetcount = count($betresult);
            foreach ($betresult as $key => $value) {
                $val = $value['price']- $count;
                $minval = $value['price'] -$min;
                $maxval = $max-$value['price'];
                $bType = $value['bType'];
                $loss = $value['loss'];
                $newresult = [];
                $top = $bottom = $profitcount = $losscount = 0;

                for( $i= 0; $i < $minval; $i++){
                    $newresult[] = [ 'count'=>$count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose'=> (-1)*$value['loss'] ];
                    $count++;
                }

                for( $i= 0; $i <= $maxval; $i++){
                    $newresult[] = [ 'count' => $count, 'price' => $value['price'], 'bType' => $value['bType'], 'totalbetcount' => $totalbetcount, 'expose' => $value['win'] ];
                    $count++;
                }
                $result[] = ['count' => $value['price'], 'bType' => $value['bType'], 'profit'=>$top, 'loss'=>$bottom, 'profitcount' => $profitcount, 'losscount' => $losscount, 'newarray' => $newresult];
            }

            $newbetarray = $newbetresult = [];
            $totalmaxcount = $max-$min;
            if( $totalmaxcount > 0 ){
                for( $i = 0; $i < $totalmaxcount; $i++ ){
                    $newbetarray1 = []; $finalexpose = 0;
                    for( $x = 0; $x < $totalbetcount; $x++ ){
                        $expose = $result[$x]['newarray'][$i]['expose'];
                        $finalexpose = $finalexpose+$expose;

                        $newbetarray1[] = ['bet_price' => $result[$x]['count'], 'bType' => $result[$x]['bType'], 'expose' => $expose];
                    }
                    $newbetresult[] = $finalexpose;
                    $newbetarray[] = ['exposearray' => $newbetarray1, 'finalexpose' => $finalexpose];
                }
            }

        }

        return $newbetresult;

    }

    // get Profit Loss Single FancyOnZeroFinal
    public function getProfitLossSingleFancyOnZeroFinal($userId,$marketId)
    {
        $yesWin = $yesLoss = $noWin = $noLoss = 0;

        $where = [['result', 'Pending'], ['bType', 'yes'], ['uid', $userId], ['mid', $marketId]];

        $betList = DB::table('tbl_bet_pending_2')->where($where)->whereIn('status',[1,5]);
        $yesWinSum = $betList->sum("win");
        if( !empty($yesWinSum) ){ $yesWin = $yesWinSum; }

        $yesLossSum = $betList->sum("loss");
        if( !empty($yesLossSum) ){ $yesLoss = $yesLossSum; }

        $where = [['result', 'Pending'], ['bType', 'no'], ['uid', $userId], ['mid', $marketId]];

        $betList = DB::table('tbl_bet_pending_2')->where($where)->whereIn('status',[1,5]);
        $noWinSum = $betList->sum("win");
        if( !empty($noWinSum) ){ $noWin = $noWinSum; }

        $noLossSum = $betList->sum("loss");
        if( !empty($noLossSum) ){ $noLoss = $noLossSum; }

        $yesProfitLoss = round($yesWin - $noLoss);
        $noProfitLoss = round($noWin - $yesLoss) ;

        $newResult[] = $yesProfitLoss;
        $newResult[] = $noProfitLoss;

        return $newResult;

    }


    // get Casino Profit Loss On Bet
    public function getCasinoProfitLossFinal($userId,$eventId,$marketId,$selectionId)
    {
        $total = $totalWin = $totalLoss = 0;
        $query = DB::table('tbl_bet_pending_4')->whereIn('status',[1,5]);

        // IF RUNNER WIN
        $where = [['uid', $userId],['mid', $marketId],['eid',$eventId],['secId',$selectionId]];
        $betWinSum = $query->where($where)->sum("win");
        if( !empty( $betWinList ) ){ $totalWin = $betWinSum; }

        // IF RUNNER LOSS
        $where = [['uid', $userId],['mid', $marketId],['eid',$eventId],['secId','!=',$selectionId]];
        $betLossSum = $query->where($where)->sum("loss");
        if( !empty( $betLossList ) ){ $totalWin = $betLossSum; }

        $total = $totalWin+$totalLoss;

        return $total;

    }

}
