<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Sport;
use Illuminate\Support\Facades\Auth;


class ListController extends Controller
{

    /**
     * Sport
     */
    public function sport()
    {

        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        $user = Auth::user();

        if( $user->role != 1 ){
            $sportList = Sport::where([['status',1],['is_block',0]])->get();
            if( $sportList != null ){
                $response = [ 'status' => 1, 'data' => $sportList ];
            }
        }

        return response()->json($response, 200);
    }


    /**
     * Event
     */
    public function event($id)
    {

        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        $user = Auth::user(); $sportId = $id;

        if( $user->role != 1 ){
            $sportList = Event::where([['status',1],['is_block',0],['sportId',$sportId]])->get();
            if( $sportList != null ){
                $response = [ 'status' => 1, 'data' => $sportList ];
            }
        }

        return response()->json($response, 200);
    }
}
