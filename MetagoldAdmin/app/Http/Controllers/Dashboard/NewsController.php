<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class NewsController extends Controller
{
    /**
     * get news and add
     * Action - Get
     * Created at JUNE 2020 by metagold
     */
    public function create(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];
           
        try{
           
           $data = [
              'title'=>$request->title,
              'information'=>$request->information
           ];

            if(DB::table('tbl_news')->insert($data)){

                $details = DB::table('tbl_news')->where('status',1)->get();
                $arr=0;
                if(!empty($details)){
                    $arr = $details;
                }

                $log = 'News added';
                LogActivity::addToLog($log);

                $response = ['status'=>1, 'code'=>200, 'data'=>$arr, 'message'=>'successfully added !!'];
                return $response;
            }

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


    public function delete(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];
           
        try{
           
           if(isset($request->id)){

                $data = [ 'status'=>0 ];
                if(DB::table('tbl_news')->where('id',$request->id)->update($data)){

                    $log = 'News deleted';
                    LogActivity::addToLog($log);
                   $response = ['status'=>1, 'code'=>200,'message'=>'Deleted successfully !!'];
                    return $response;
                }
            }

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


    public function list(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];
           
        try{

                $data = DB::table('tbl_news')->where('status',1)->get();
                if(!empty($data)){
                   $response = ['status'=>1, 'code'=>200, 'data'=>$data, 'message'=>'Data found !!'];
                    return $response;
                }else{
                    $response = ['status'=>1, 'code'=>201, 'data'=>null, 'message'=>'Data not found !!'];
                    return $response;
                }

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
