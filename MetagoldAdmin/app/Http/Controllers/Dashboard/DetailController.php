<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class DetailController extends Controller
{

    private $marketIdsArr = [];

    // getBlockEventIds
    public function getBlockEventIds($uid)
    {
        $returnArr = [];
        $eventData = DB::table('tbl_user_event_status')->select('eid')
            ->where([['uid',$uid],['byuserId','!=',$uid]])->get();

        if( $eventData != null ){
            foreach ( $eventData as $data ){
                $returnArr[] = $data->eid;
            }
        }

        return $returnArr;

    }

    // getBlockSportIds
    public function getBlockSportIds($uid)
    {
        $returnArr = [];
        $eventData = DB::table('tbl_user_sport_status')->select('sid')
            ->where([['uid',$uid],['byuserId','!=',$uid]])->get();

        if( $eventData != null ){
            foreach ( $eventData as $data ){
                $returnArr[] = $data->sid;
            }
        }

        return $returnArr;

    }

    /**
     * action Data Cricket
     */
    public function actionDataCricket($eventId)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( isset($_GET['uid']) ){
                $uid = $_GET['uid'];
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user();
                $uid = null;
            }

            $userData = [];
            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
            }

            $redis = Redis::connection();
            $eventDataJson = $redis->get('Event_'.$eventId);
            $eventData = json_decode($eventDataJson);

            if( $eventData != null && $eventData->game_over != 1){
                $title = $eventData->name;
                $sportId = $eventData->sportId;
                $betAllowed = $eventData->bet_allowed;

                if( $user->role != 1 && $user->role != 6 ){
                    $blockSports = $this->getBlockSportIds($user->id);
                    if( in_array( $sportId, $blockSports ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This sport is blocked for you !' ] ];
                        return $response;
                    }
                    $blockEvents = $this->getBlockEventIds($user->id);
                    if( in_array( $eventId, $blockEvents ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This event is blocked for you !' ] ];
                        return $response;
                    }

                }

                $otherMarket = $this->getOtherMarket($uid,$eventData,$eventId,'cricket');
                $bookMaker = $this->getBookMaker($uid,$sportId,$eventId);
                $virtualCricket = $this->getVirtualCricket($uid,$sportId,$eventId);
                $fancyMarket = $this->getFancyMarket($sportId,$eventId,'cricket');
                $fancy2Market = $this->getFancy2Market($sportId,$eventId,'cricket');
                $fancy3Market = $this->getFancy3Market($sportId,$eventId,'cricket');
                $khadoSession = $this->getKhadoSession($sportId,$eventId,'cricket');
                $ballSession = $this->getBallSession($sportId,$eventId,'cricket');

                $eventArr = [
                    'title' => $title,
                    'sport' => 'Cricket',
                    'eventId' => $eventId,
                    'sportId' => $sportId,
                    'betAllowed' => $betAllowed,
                    'tvAllowed' => 1,
                    'otherMarket' => $otherMarket,
                    'bookMaker' => $bookMaker,
                    'virtualCricket' => $virtualCricket,
                    'fancyMarket' => $fancyMarket,
                    'fancy2Market' => $fancy2Market,
                    'fancy3Market' => $fancy3Market,
                    'khadoSession' => $khadoSession,
                    'ballSession' => $ballSession,
                ];

                $response = [ "status" => 1, "data" => [ "items" => $eventArr, 'userData' => $userData , "marketIdsArr" => $this->marketIdsArr ] ];

            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Data Football
     */
    public function actionDataFootball($eventId)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( isset($_GET['uid']) ){
                $uid = $_GET['uid'];
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user();
                $uid = null;
            }
            $userData = [];
            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
            }

            $redis = Redis::connection();
            $eventDataJson = $redis->get('Event_'.$eventId);
            $eventData = json_decode($eventDataJson);

            if( $eventData != null && $eventData->game_over != 1 ){

                $title = $eventData->name;
                $sportId = $eventData->sportId;
                $betAllowed = $eventData->bet_allowed;

                if( $user->role != 1 && $user->role != 6 ){
                    $blockSports = $this->getBlockSportIds($user->id);
                    if( in_array( $sportId, $blockSports ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This sport is blocked for you !' ] ];
                        return $response;
                    }
                    $blockEvents = $this->getBlockEventIds($user->id);
                    if( in_array( $eventId, $blockEvents ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This event is blocked for you !' ] ];
                        return $response;
                    }

                }

                $otherMarket = $this->getOtherMarket($uid,$eventData,$eventId,'football');
                $bookMaker = $this->getBookMaker($uid,$sportId,$eventId);
                $fancy2Market = $this->getFancy2Market($sportId,$eventId,'football');
                $fancy3Market = $this->getFancy3Market($sportId,$eventId,'football');

                $eventArr = [
                    'title' => $title,
                    'sport' => 'Football',
                    'eventId' => $eventId,
                    'sportId' => $sportId,
                    'betAllowed' => $betAllowed,
                    'tvAllowed' => 1,
                    'otherMarket' => $otherMarket,
                    'bookMaker' => $bookMaker,
                    'fancy2Market' => $fancy2Market,
                    'fancy3Market' => $fancy3Market,
                ];

                $response = [ "status" => 1, "data" => [ "items" => $eventArr, 'userData' => $userData , "marketIdsArr" => $this->marketIdsArr ] ];

            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Data Tennis
     */
    public function actionDataTennis($eventId)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( isset($_GET['uid']) ){
                $uid = $_GET['uid'];
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user();
                $uid = null;
            }
            $userData = [];
            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
            }

            $redis = Redis::connection();
            $eventDataJson = $redis->get('Event_'.$eventId);
            $eventData = json_decode($eventDataJson);

            if( $eventData != null && $eventData->game_over != 1 ){
                $title = $eventData->name;
                $sportId = $eventData->sportId;
                $betAllowed = $eventData->bet_allowed;

                if( $user->role != 1 && $user->role != 6){
                    $blockSports = $this->getBlockSportIds($user->id);
                    if( in_array( $sportId, $blockSports ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This sport is blocked for you !' ] ];
                        return $response;
                    }
                    $blockEvents = $this->getBlockEventIds($user->id);
                    if( in_array( $eventId, $blockEvents ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This event is blocked for you !' ] ];
                        return $response;
                    }

                }

                $otherMarket = $this->getOtherMarket($uid,$eventData,$eventId,'tennis');
                $bookMaker = $this->getBookMaker($uid,$sportId,$eventId);
                $fancy2Market = $this->getFancy2Market($sportId,$eventId,'tennis');
                $fancy3Market = $this->getFancy3Market($sportId,$eventId,'tennis');

                $eventArr = [
                    'title' => $title,
                    'sport' => 'Tennis',
                    'eventId' => $eventId,
                    'sportId' => $sportId,
                    'betAllowed' => $betAllowed,
                    'tvAllowed' => 1,
                    'otherMarket' => $otherMarket,
                    'bookMaker' => $bookMaker,
                    'fancy2Market' => $fancy2Market,
                    'fancy3Market' => $fancy3Market,
                ];

                $response = [ "status" => 1, "data" => [ "items" => $eventArr, 'userData' => $userData , "marketIdsArr" => $this->marketIdsArr ] ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


    /**
     * action Data Election
     */
    public function actionDataElection($eventId)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( isset($_GET['uid']) ){
                $uid = $_GET['uid'];
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user();
                $uid = null;
            }
            $userData = [];
            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
            }

            $redis = Redis::connection();
            $eventDataJson = $redis->get('Event_'.$eventId);
            $eventData = json_decode($eventDataJson);

            if( $eventData != null && $eventData->game_over != 1 ){
                $title = $eventData->name;
                $sportId = $eventData->sportId;
                $betAllowed = $eventData->bet_allowed;

                if( $user->role != 1 && $user->role != 6 ){
                    $blockSports = $this->getBlockSportIds($user->id);
                    if( in_array( $sportId, $blockSports ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This sport is blocked for you !' ] ];
                        return $response;
                    }
                    $blockEvents = $this->getBlockEventIds($user->id);
                    if( in_array( $eventId, $blockEvents ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This event is blocked for you !' ] ];
                        return $response;
                    }

                }

                $bookMaker = $this->getBookMaker($uid,$sportId,$eventId);
                $fancy2Market = $this->getFancy2Market($sportId,$eventId,'election');
                $fancy3Market = $this->getFancy3Market($sportId,$eventId,'election');

                $eventArr = [
                    'title' => $title,
                    'sport' => 'Election',
                    'eventId' => $eventId,
                    'sportId' => $sportId,
                    'betAllowed' => $betAllowed,
                    'tvAllowed' => 0,
                    'bookMaker' => $bookMaker,
                    'fancy2Market' => $fancy2Market,
                    'fancy3Market' => $fancy3Market,
                ];

                $response = [ "status" => 1, "data" => [ "items" => $eventArr, 'userData' => $userData , "marketIdsArr" => $this->marketIdsArr ] ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Data LiveGames
     */
    public function actionDataLiveGames($eventId)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( isset($_GET['uid']) ){
                $uid = $_GET['uid'];
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user();
                $uid = null;
            }
            $userData = [];
            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
            }

            $eventData = DB::table('tbl_live_game')->where([['eventId',$eventId],['status',1]])->first();

            if( $eventData != null ){
                $title = $eventData->name;
                $sportId = $eventData->sportId;
                $betAllowed = $eventData->bet_allowed;

                if( $user->role != 1 && $user->role != 6 ){
                    $blockSports = $this->getBlockSportIds($user->id);
                    if( in_array( $sportId, $blockSports ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This sport is blocked for you !' ] ];
                        return $response;
                    }
                    $blockEvents = $this->getBlockEventIds($user->id);
                    if( in_array( $eventId, $blockEvents ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This event is blocked for you !' ] ];
                        return $response;
                    }

                }

                $eventArr = [
                    'title' => $title,
                    'sport' => 'Live Games',
                    'eventId' => $eventId,
                    'sportId' => $sportId,
                    'betAllowed' => $betAllowed,
                    'tvAllowed' => 0
                ];

                $response = [ "status" => 1, "data" => [ "items" => $eventArr, 'userData' => $userData , "marketIdsArr" => $this->marketIdsArr ] ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Data Jackpot
     */
    public function actionDataJackpot($eventId)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( isset($_GET['uid']) ){
                $uid = $_GET['uid'];
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user();
                $uid = null;
            }
            $userData = [];
            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
            }

            $redis = Redis::connection();
            $eventDataJson = $redis->get('Event_'.$eventId);
            $eventData = json_decode($eventDataJson);

            if( $eventData != null && $eventData->game_over != 1){
                $title = $eventData->name;
                $sportId = $eventData->sportId;
                $betAllowed = $eventData->bet_allowed;

                if( $user->role != 1 && $user->role != 6 ){
                    $blockSports = $this->getBlockSportIds($user->id);
                    if( in_array( $sportId, $blockSports ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This sport is blocked for you !' ] ];
                        return $response;
                    }
                    $blockEvents = $this->getBlockEventIds($user->id);
                    if( in_array( $eventId, $blockEvents ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This event is blocked for you !' ] ];
                        return $response;
                    }

                }

                $fancy2Market = $this->getFancy2Market($sportId,$eventId,'jackpot');
                $fancy3Market = $this->getFancy3Market($sportId,$eventId,'jackpot');
                $khadoSession = $this->getKhadoSession($sportId,$eventId,'jackpot');
                $jackpotData = $this->getJackpotData($uid,$sportId,$eventId);

                $eventArr = [
                    'title' => $title,
                    'sport' => 'Jackpot',
                    'eventId' => $eventId,
                    'sportId' => $sportId,
                    'betAllowed' => $betAllowed,
                    'tvAllowed' => 0,
                    'fancy2Market' => $fancy2Market,
                    'fancy3Market' => $fancy3Market,
                    'khadoSession' => $khadoSession,
                    'jackpotData' => $jackpotData,
                ];

                $response = [ "status" => 1, "data" => [ "items" => $eventArr, 'userData' => $userData , "marketIdsArr" => $this->marketIdsArr ] ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Data Jackpot
     */
    public function actionJackpotDetails(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $marketArr=null;
            if( isset( $request->type ) && isset( $request->mid ) && isset( $request->eid )){
                $marketName = $gameType = 'Jackpot';
                $gType = $request->type; $marketId = $request->mid; $eventId = $request->eid;
                if (isset($request->userId)) {
                    $uid = $request->userId;
                    $user = DB::table('tbl_user')->where([['id', $uid], ['status', 1]])->first();
                } else {
                    $user = Auth::user(); $uid = $user->id;
                    if( $user->role == 6 ){ $uid = 1; }
                }
                $systemId = $user->systemId;
                $redis = Redis::connection();
                $eventDataJson = $redis->get("Event_".$eventId);
                $eventData = json_decode($eventDataJson);
                if ($eventData != null) {
                    $eventName = $eventData->name;
                    $sportId = $eventData->sportId;

                    $jackpotMarketJson = $redis->get("Jackpot_market_".$gType."_". $eventId."_".$marketId);
                    $jackpotMarket = json_decode($jackpotMarketJson);

                    if( $jackpotMarket != null ){
                        $marketName = $jackpotMarket->title;

                        $gameTypeDataJson = $redis->get("Jackpot_type");
                        $gameTypeData = json_decode($gameTypeDataJson);
                        foreach ($gameTypeData as $type) {
                            if ( $gType == $type->id ) {
                                $gameType = $type->title;
                            }
                        }

                    }

                    $query = DB::table('tbl_bet_pending_4')
                        ->where([['systemId',$systemId],['mid',$marketId],['status',1],['result','PENDING']]);

                    if( $user->role != 1 && $user->role != 6){
                        $clientJson = DB::table('tbl_user_child_data')->select('clients')
                            ->where([['uid',$user->id]])->first();
                        if( $clientJson != null && isset( $clientJson->clients ) ){
                            $client = json_decode($clientJson->clients);
                            $query->whereIn('uid',$client);
                        }
                    }

                    $clientArr = $query->distinct()->select('uid')->get();

                    if( $clientArr->isNotEmpty() ) {
                        $client = [];
                        foreach ($clientArr as $c) {
                            $client[] = $c->uid;
                        }

                        $client = array_unique($client);
                        $result = $newbetresult = [];

                        $result = [];
                        $betresult = [];
                        $min = 0;
                        $max = 0;
                        $totalLoss=0;  $actualprofitloss=0; $betArray=[];
                        $betList = DB::table('tbl_bet_pending_4 as pb')->select('pb.win','pb.loss','pb.uid','pb.secId','upl.actual_profit_loss')
                            ->leftjoin('tbl_user_profit_loss as upl', 'upl.clientId', '=', 'pb.uid')
                            ->where([['pb.mid',$marketId],['upl.userId',$uid],['pb.status',1],['result','PENDING'],['pb.systemId',$systemId]])//,['pb.systemId',$systemId]
                            ->whereIn('pb.uid',$client)->get();
                        foreach ($betList as $index => $bet) {
                            $totalLoss= $totalLoss+$bet->loss;
                            $actualprofitloss=       $bet->actual_profit_loss;
                            $betArray[]=array('sec_id'=>$bet->secId,'user_id'=>$bet->uid,'loss'=>$bet->loss,'win'=>$bet->win,'actual_profit_loss'=>$bet->actual_profit_loss);

                        }


                        //echo "test"; echo "<pre>";  print_r($betArray); exit;
                        $actual_profit_loss=0;
                        $jackpotDataJson =  $redis->get('Jackpot_' . $gType . '_' . $marketId);
                        $jackpotData =  json_decode($jackpotDataJson);
                        $runners = [];
                        if(!empty($jackpotData)) {
                            foreach ( $jackpotData as $jackpot ) {
                                $profitLoss = 0;
                                $totalWin=0;   $totalLoss2=0;
                                $usertotalLoss=0;
                                foreach ($betArray as $bet) {
                                    if($bet['sec_id']==$jackpot->secId){
                                        $totalWin=  $totalWin+  $bet['win'];
                                        $usertotalLoss=  $usertotalLoss+  $bet['loss'];
                                        $actual_profit_loss=  $bet['actual_profit_loss'];
                                    }
                                }
                                $profitLoss= 0;
                                if($totalWin>0) {
                                    $userLoss=$totalLoss-$usertotalLoss;
                                    $finalexpose= $userLoss-$totalWin;
                                    $finalexpose=$finalexpose * $actual_profit_loss / 100;
                                    $profitLoss=  $finalexpose;
                                }else{
                                    $finalexpose=$totalLoss *  $actualprofitloss / 100;
                                    $profitLoss= $finalexpose;
                                }




                                $runners[] = [
                                    'sportId' => $sportId,
                                    'event_id' => $eventId,
                                    'market_id' => $marketId,
                                    'sec_id' => $jackpot->secId,
                                    'col1' => $jackpot->col_1,
                                    'col2' => $jackpot->col_2,
                                    'col3' => $jackpot->col_3,
                                    'col4' => $jackpot->col_4,
                                    'slug' => 'jackpot',
                                    'rate' => $jackpot->rate,
                                    'suspended' => $jackpot->suspended,
                                    'profitLoss' => $profitLoss,
                                ];

                            }

                        }

                        $marketArr = [
                            'sport' => 'Jackpot',
                            'eventName' => $eventName,
                            'marketName' => $marketName,
                            'gameType' => $gameType,
                            'runners' => $runners,
                        ];

                    }else{
                        $jackpotDataJson =  $redis->get('Jackpot_' . $gType . '_' . $marketId);
                        $jackpotData =  json_decode($jackpotDataJson);
                        $runners = [];
                        if(!empty($jackpotData)) {
                            foreach ( $jackpotData as $jackpot ) {
                                $profitLoss = 0;
                                $runners[] = [
                                    'sportId' => $sportId,
                                    'event_id' => $eventId,
                                    'market_id' => $marketId,
                                    'sec_id' => $jackpot->secId,
                                    'col1' => $jackpot->col_1,
                                    'col2' => $jackpot->col_2,
                                    'col3' => $jackpot->col_3,
                                    'col4' => $jackpot->col_4,
                                    'slug' => 'jackpot',
                                    'rate' => $jackpot->rate,
                                    'suspended' => $jackpot->suspended,
                                    'profitLoss' => $profitLoss,
                                ];
                            }
                        }

                        $marketArr = [
                            'sport' => 'Jackpot',
                            'eventName' => $eventName,
                            'marketName' => $marketName,
                            'gameType' => $gameType,
                            'runners' => $runners,
                        ];
                    }


                    $betList = []; $betCount = 0;
                    $query = DB::table('tbl_bet_pending_4')
                        ->select(['id','runner','client','master','price','size','rate','created_on','ip_address','bType'])
                        ->where([['mid',$marketId],['status',1]]);
                    $betCount = $query->count();
                    if( $betCount > 0 ){
                        $betList = $query->orderBy('id','desc')->limit(10)->get();
                    }

                    $betList = [ 'list' => $betList, 'count' => $betCount ];

                    $response = [ "status" => 1, "data" => [ "items" => $marketArr, 'betList' => $betList ] ];

                    if( $user != null ){
                        $userData = [
                            'name' => $user->username,
                            'role' => $user->roleName,
                        ];
                        $response['userData'] = $userData;
                    }

                }
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


    public function actionJackpotDetailsOld(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( isset( $request->type ) && isset( $request->mid ) && isset( $request->eid )){
                $marketName = $gameType = 'Jackpot';
                $gType = $request->type; $marketId = $request->mid; $eventId = $request->eid;

                $redis = Redis::connection();
                $eventDataJson = $redis->get("Event_".$eventId);
                $eventData = json_decode($eventDataJson);
                if ($eventData != null) {
                    $eventName = $eventData->name;
                    $sportId = $eventData->sportId;

                    $jackpotMarketJson = $redis->get("Jackpot_market_".$gType."_". $eventId."_".$marketId);
                    $jackpotMarket = json_decode($jackpotMarketJson);

                    if( $jackpotMarket != null ){
                        $marketName = $jackpotMarket->title;

                        $gameTypeDataJson = $redis->get("Jackpot_type");
                        $gameTypeData = json_decode($gameTypeDataJson);
                        foreach ($gameTypeData as $type) {
                            if ( $gType == $type->id ) {
                                $gameType = $type->title;
                            }
                        }

                    }

                    $jackpotDataJson =  $redis->get('Jackpot_' . $gType . '_' . $marketId);
                    $jackpotData =  json_decode($jackpotDataJson);
                    $runners = [];
                    if(!empty($jackpotData)) {
                        foreach ( $jackpotData as $jackpot ) {
                            $profitLoss = 0;
                            $runners[] = [
                                'sportId' => $sportId,
                                'event_id' => $eventId,
                                'market_id' => $marketId,
                                'sec_id' => $jackpot->secId,
                                'col1' => $jackpot->col_1,
                                'col2' => $jackpot->col_2,
                                'col3' => $jackpot->col_3,
                                'col4' => $jackpot->col_4,
                                'slug' => 'jackpot',
                                'rate' => $jackpot->rate,
                                'suspended' => $jackpot->suspended,
                                'profitLoss' => $profitLoss,
                            ];

                        }

                    }

                    $marketArr = [
                        'sport' => 'Jackpot',
                        'eventName' => $eventName,
                        'marketName' => $marketName,
                        'gameType' => $gameType,
                        'runners' => $runners,
                    ];

                    $betList = []; $betCount = 0;
                    $query = DB::table('tbl_bet_pending_4')
                        ->select(['id','runner','client','master','price','size','rate','created_on','ip_address','bType'])
                        ->where([['mid',$marketId],['status',1]]);
                    $betCount = $query->count();
                    if( $betCount > 0 ){
                        $betList = $query->orderBy('id','desc')->limit(10)->get();
                    }

                    $betList = [ 'list' => $betList, 'count' => $betCount ];

                    $response = [ "status" => 1, "data" => [ "items" => $marketArr, 'betList' => $betList ] ];

                }
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


    /**
     * action Data Casino
     */
    public function actionDataCricketCasino($eventId)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( isset($_GET['uid']) ){
                $uid = $_GET['uid'];
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user();
                $uid = null;
            }
            $userData = [];
            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
            }

            $redis = Redis::connection();
            $eventDataJson = $redis->get('Event_'.$eventId);
            $eventData = json_decode($eventDataJson);

            if( $eventData != null && $eventData->game_over != 1){
                $title = $eventData->name;
                $sportId = $eventData->sportId;
                $betAllowed = $eventData->bet_allowed;

                if( $user->role != 1 && $user->role != 6 ){
                    $blockSports = $this->getBlockSportIds($user->id);
                    if( in_array( $sportId, $blockSports ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This sport is blocked for you !' ] ];
                        return $response;
                    }
                    $blockEvents = $this->getBlockEventIds($user->id);
                    if( in_array( $eventId, $blockEvents ) ){
                        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! This event is blocked for you !' ] ];
                        return $response;
                    }

                }

                $cricketCasino = $this->getCricketCasino($uid,$sportId,$eventId);

                $eventArr = [
                    'title' => $title,
                    'sport' => 'Cricket Casino',
                    'eventId' => $eventId,
                    'sportId' => $sportId,
                    'betAllowed' => $betAllowed,
                    'tvAllowed' => 0,
                    'cricketCasino' => $cricketCasino,
                ];

                $response = [ "status" => 1, "data" => [ "items" => $eventArr , 'userData' => $userData ] ];

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Bet List
     */
    public function actionBetList($eventId)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( isset($_GET['uid']) ){
                $uid = $_GET['uid'];
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user();
                $uid = $user->id;
            }
            $client = [];
            if( $user->role != 1 && $user->role != 4 && $user->role != 6 ){
                $clientJson = DB::table('tbl_user_child_data')->select('clients')
                    ->where([['uid',$user->id]])->first();
                if( $clientJson != null && isset($clientJson->clients) ){
                    $client = json_decode($clientJson->clients);
                }
            }

            $checkGame = DB::table('tbl_live_game')->where([['eventId',$eventId]])->first();

            if( $checkGame != null ){
                $betList5 = $marketIds = []; $betCount5 = 0;
                if( $user->role == 4 ){
                    $where = [['eid',$eventId],['status',1],['uid',$user->id],['systemId',$user->systemId]];
                }else{
                    $where = [['eid',$eventId],['status',1],['systemId',$user->systemId]];
                }
                $query5 = DB::table('tbl_bet_pending_teenpatti')
                    ->select(['id','runner','client','master','price','size','rate','win','loss','created_on','ip_address','bType'])
                    ->where($where);
                if( $client != null ){ $query5->whereIn('uid',$client); }
                $betCount5 = $query5->count();
                if( $betCount5 > 0 ){
                    $betList5 = $query5->orderBy('id','desc')->limit(10)->get();
                }

                $data = [
                    'betList5' => [ 'list' => $betList5, 'count' => $betCount5 ],
                ];

            }else{

                $betList1 = $betList2 = $betList3 = $betList4 = [];
                $betCount1 = $betCount2 = $betCount3 = $betCount4 = 0;

                if( $user->role == 4 ){
                    $where = [['eid',$eventId],['status',1],['uid',$user->id],['systemId',$user->systemId]];
                }else{
                    $where = [['eid',$eventId],['status',1],['systemId',$user->systemId]];
                }

                $query1 = DB::table('tbl_bet_pending_1')
                    ->select(['id','runner','client','master','price','size','rate','win','loss','created_on','ip_address','bType'])
                    ->where($where);
                if( $client != null ){ $query1->whereIn('uid',$client); }

                $betCount1 = $query1->count();
                if( $betCount1 > 0 ){
                    $betList1 = $query1->orderBy('id','desc')->limit(10)->get();
                }

                $query2 = DB::table('tbl_bet_pending_2')
                    ->select(['id','runner','client','master','price','size','rate','win','loss','created_on','ip_address','bType'])
                    ->where($where);
                if( $client != null ){ $query2->whereIn('uid',$client); }
                $betCount2 = $query2->count();
                if( $betCount2 > 0 ){
                    $betList2 = $query2->orderBy('id','desc')->limit(10)->get();
                }

                $query3 = DB::table('tbl_bet_pending_3')
                    ->select(['id','runner','client','master','price','size','rate','win','loss','created_on','ip_address','bType'])
                    ->where($where);
                if( $client != null ){ $query3->whereIn('uid',$client); }
                $betCount3 = $query3->count();
                if( $betCount3 > 0 ){
                    $betList3 = $query3->orderBy('id','desc')->limit(10)->get();
                }

                $query4 = DB::table('tbl_bet_pending_4')
                    ->select(['id','runner','client','master','price','size','rate','win','loss','created_on','ip_address','bType'])
                    ->where($where);
                if( $client != null ){ $query4->whereIn('uid',$client); }
                $betCount4 = $query4->count();
                if( $betCount4 > 0 ){
                    $betList4 = $query4->orderBy('id','desc')->limit(10)->get();
                }

                $data = [
                    'betList1' => [ 'list' => $betList1, 'count' => $betCount1 ],
                    'betList2' => [ 'list' => $betList2, 'count' => $betCount2 ],
                    'betList3' => [ 'list' => $betList3, 'count' => $betCount3 ],
                    'betList4' => [ 'list' => $betList4, 'count' => $betCount4 ],
                ];

                $marketsOnBet = DB::table('tbl_user_market_expose')
                    ->where([['status',1],['sid','!=',99]])->distinct()->select('mid')->get();

                $marketIds = [];

                if( !empty($marketsOnBet) ){
                    foreach ( $marketsOnBet as $key => $market ){
                        $marketIds[] = $market->mid;
                    }
                }

            }

            $response = [ "status" => 1, "data" => $data, 'marketIds' => $marketIds ];

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action Bet List
     */
    public function actionMarketBetList(Request $request)
    {

        try{

            $user = Auth::user();

            if( isset($request->uid) && $request->uid != null ){
                $uid = $request->uid;
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $uid = $user->id;
            }

            $userData = [];
            if( $user != null ){
                $userData = [
                    'name' => $user->username,
                    'role' => $user->roleName,
                ];
            }

            $client = [];
            if( $user->role != 1 && $user->role != 4 && $user->role != 6 ){
                $clientJson = DB::table('tbl_user_child_data')->select('clients')
                    ->where([['uid',$user->id]])->first();
                if( $clientJson != null && isset($clientJson->clients) ){
                    $client = json_decode($clientJson->clients);
                }
            }

            if( isset($request->mtype) && isset($request->eid)){
                $betList = []; $betCount = 0;
                $mType = $request->mtype;
                $systemId = $user->systemId;

                if( $mType == 'live-games' ){
                    $tbl = 'tbl_bet_pending_teenpatti';
                }elseif ( $mType == 'other' || $mType == 'bookmaker' || $mType == 'virtual-cricket'){
                    $tbl = 'tbl_bet_pending_1';
                }elseif (  $mType == 'fancy' || $mType == 'fancy2' || $mType == 'fancy3'  ){
                    $tbl = 'tbl_bet_pending_2';
                }elseif (  $mType == 'ball' || $mType == 'khado' ){
                    $tbl = 'tbl_bet_pending_3';
                }else{ $tbl = 'tbl_bet_pending_4'; }

                if( isset($request->mid) && $request->mid != null){
                    $marketId = $request->mid;
                    $eventId = $request->eid;
                    if( $user->role == 4 ){
                        $where = [['mid',$marketId],['eid',$eventId],['status',1],['systemId',$systemId],['uid',$user->id]];
                    }else{
                        $where = [['mid',$marketId],['eid',$eventId],['status',1],['systemId',$systemId]];
                    }
                }else{
                    $eventId = $request->eid;
                    if( $user->role == 4 ){
                        $where = [['status',1],['eid',$eventId],['systemId',$systemId],['uid',$user->id]];
                    }else{
                        $where = [['status',1],['eid',$eventId],['systemId',$systemId]];
                    }
                }

                if( isset($request->client) && $request->client != null ){
                    array_push($where, ['client', 'like', '%' . $request->client . '%']);
                }
                if( isset($request->runner) && $request->runner != null ){
                    array_push($where, ['runner', 'like', '%' . $request->runner . '%']);
                }

                $query = DB::table($tbl)
                    ->select(['id','runner','client','master','price','size','rate','win','loss','created_on','ip_address','bType','mType','result'])
                    ->where($where);

                if( $client != null ){ $query->whereIn('uid',$client); }

                if( $mType == 'fancy' || $mType == 'fancy2' || $mType == 'fancy3' ){
                    $mTypeArr = [];$mTypeArr[] = $mType;
                    $query->whereIn('mType',$mTypeArr);
                }

                if( $mType == 'ball' || $mType == 'khado' ){
                    $mTypeArr = [];
                    if( $mType == 'ball' ){
                        $mType = 'ballbyball';
                    }
                    $mTypeArr[] = $mType;
                    $query->whereIn('mType',$mTypeArr);
                }

                $betCount = $query->count();
                if( $betCount > 0 ){
                    $betList = $query->orderBy('id','desc')->get();
                }

                $response = [ "status" => 1, 'userData' => $userData,"data" => [ 'list' => $betList, 'count' => $betCount ] ];

            }else{

                $betList = []; $betCount = 0; $systemId = $user->systemId;
                $tbl = 'tbl_bet_history';

                if( isset($request->mid) && $request->mid != null){
                    $marketId = $request->mid;
                    if( $user->role == 4 ){
                        $where = [['mid',$marketId],['status',1],['systemId',$systemId],['uid',$user->id]];
                    }else{
                        $where = [['mid',$marketId],['status',1],['systemId',$systemId]];
                    }
                }else{
                    if( $user->role == 4 ){
                        $where = [['status',1],['systemId',$systemId],['uid',$user->id]];
                    }else{
                        $where = [['status',1],['systemId',$systemId]];
                    }
                }

                if( isset($request->client) && $request->client != null ){
                    array_push($where, ['client', 'like', '%' . $request->client . '%']);
                }
                if( isset($request->runner) && $request->runner != null ){
                    array_push($where, ['runner', 'like', '%' . $request->runner . '%']);
                }

                $query = DB::table($tbl)
                    ->select(['id','betId','runner','client','master','price','size','rate','win','loss','created_on','ip_address','bType','mType','result','description'])
                    ->where($where);

                if( $client != null ){ $query->whereIn('uid',$client); }

                $betCount = $query->count();
                if( $betCount > 0 ){
                    $betList = $query->orderBy('id','desc')->get();
                }else{
                    $tbl = 'tbl_bet_history_teenpatti';
                    $query = DB::table($tbl)
                        ->select(['id as betId','runner','client','master','price','size','rate','win','loss','created_on','ip_address','bType','mType','result','description'])
                        ->where($where);

                    if( $client != null ){ $query->whereIn('uid',$client); }

                    $betCount = $query->count();
                    if( $betCount > 0 ){
                        $betList = $query->orderBy('id','desc')->get();
                    }

                }

                $response = [ "status" => 1,'userData' => $userData, "data" => [ 'list' => $betList, 'count' => $betCount ] ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // get Other Market
    public function getOtherMarket($uid,$eventData,$eventId,$slug)
    {
        $otherMarketArr = $runnersArr = [];
        $redis = Redis::connection();
        $matchOddMarketJson = $redis->get('Match_Odd_'.$eventId);
        $matchOddMarket = json_decode($matchOddMarketJson);

        if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )
            && isset( $matchOddMarket->game_over ) && $matchOddMarket->game_over != 1 ) {

            $marketId = $matchOddMarket->marketId;
            $suspended = $matchOddMarket->suspended;
            $ballRunning = $matchOddMarket->ball_running;
            $betAllowed = $matchOddMarket->bet_allowed;
            $time = $eventData->time;
            $sportId = $eventData->sportId;

            $runnersArr = $this->matchOddsRunnerDataWithBook($uid,$slug,$sportId,$eventId,$matchOddMarket);

            $otherMarketArr['MatchOdd'] = [
                'sportId' => $sportId,
                'slug' => $slug,
                'mType' => 'match_odd',
                'marketId' => $marketId,
                'eventId' => $eventId,
                'suspended' => $suspended,
                'ballRunning' => $ballRunning,
                'bet_allowed' => $betAllowed,
                'time' => $time,
                'marketName' => 'Match Odds',
                'matched' => '',
                'runners' => $runnersArr,
            ];

        }

        $completedMatchMarketJson = $redis->get('Completed_Match_'.$eventId);
        $completedMatchMarket = json_decode($completedMatchMarketJson);

        if (!empty( $completedMatchMarket ) && isset( $completedMatchMarket->marketId )
            && isset( $completedMatchMarket->game_over ) && $completedMatchMarket->game_over != 1 ) {

            $marketId = $completedMatchMarket->marketId;
            $suspended = $completedMatchMarket->suspended;
            $ballRunning = $completedMatchMarket->ball_running;
            $betAllowed = $completedMatchMarket->bet_allowed;
            $time = $eventData->time;
            $sportId = $eventData->sportId;

            $runnersArr = $this->completedMatchRunnerDataWithBook($uid,$slug,$sportId,$eventId,$completedMatchMarket);

            $otherMarketArr['CompletedMatch'] = [
                'sportId' => $sportId,
                'slug' => $slug,
                'mType' => 'completed_match',
                'marketId' => $marketId,
                'eventId' => $eventId,
                'suspended' => $suspended,
                'ballRunning' => $ballRunning,
                'bet_allowed' => $betAllowed,
                'time' => $time,
                'marketName' => 'Completed Match',
                'matched' => '',
                'runners' => $runnersArr,
            ];

        }

        $tiedMatchMarketJson = $redis->get('Tied_Match_'.$eventId);
        $tiedMatchMarket = json_decode($tiedMatchMarketJson);

        if (!empty( $tiedMatchMarket ) && isset( $tiedMatchMarket->marketId )
            && isset( $tiedMatchMarket->game_over ) && $tiedMatchMarket->game_over != 1 ) {

            $marketId = $tiedMatchMarket->marketId;
            $suspended = $tiedMatchMarket->suspended;
            $ballRunning = $tiedMatchMarket->ball_running;
            $betAllowed = $tiedMatchMarket->bet_allowed;
            $time = $eventData->time;
            $sportId = $eventData->sportId;

            $runnersArr = $this->tiedMatchRunnerDataWithBook($uid,$slug,$sportId,$eventId,$tiedMatchMarket);

            $otherMarketArr['TiedMatch'] = [
                'sportId' => $sportId,
                'slug' => $slug,
                'mType' => 'tied_match',
                'marketId' => $marketId,
                'eventId' => $eventId,
                'suspended' => $suspended,
                'ballRunning' => $ballRunning,
                'bet_allowed' => $betAllowed,
                'time' => $time,
                'marketName' => 'Tied Match',
                'matched' => '',
                'runners' => $runnersArr,
            ];

        }

        return $otherMarketArr;

    }

    // Match Odds Runner Data With Book
    public function matchOddsRunnerDataWithBook($uid,$slug,$sportId,$eventId,$matchOddMarket)
    {
        if( $uid != null ){
            $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
        }else{ $user = Auth::user(); if( $user->role == 6 ){ $user->id = 1; } }

        $runnersArr = $finalTotalArr = $clientArr = [];

        if (!empty( $matchOddMarket ) && isset( $matchOddMarket->marketId )) {

            $marketId = $matchOddMarket->marketId;
            $suspended = $matchOddMarket->suspended;
            $ballRunning = $matchOddMarket->ball_running;
            $runnerData = json_decode($matchOddMarket->runners);

            if( $user->role != 4){

                $query = DB::table('tbl_bet_pending_1')
                    ->where([['mid',$marketId],['status',1],['systemId',$user->systemId]]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    $client = json_decode($clientJson->clients);
                    $query->whereIn('uid',$client);
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr != null ){
                    $total = 0; $totalArr = [];
                    foreach ( $clientArr as $c ){
                        $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                            ->where([['clientId',$c->uid],['userId',$user->id]])->first();

                        $bookData = DB::table('tbl_book_matchodd')->select(['book','secId'])
                            ->where([['mid',$marketId],['uid',$c->uid]])->get();

                        if( $bookData->isNotEmpty() ){
                            foreach ( $bookData as $book ){
                                $total = $book->book;
                                if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                                    $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                                    $totalArr[$book->secId][] = $total;
                                }else{
                                    $totalArr[$book->secId][] = 0;
                                }
                            }
                        }
                    }

                    if( $totalArr != null ){
                        $finalTotalArr = [];
                        foreach ( $totalArr as $secId => $data ){
                            if( array_sum($data) != null && array_sum($data) ){
                                $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                            }
                        }

                    }

                }

            }elseif( $user->role == 4 ){
                $bookData = DB::table('tbl_book_matchodd')->select(['book','secId'])
                    ->where([['mid',$marketId],['uid',$user->id]])->get();

                if( $bookData->isNotEmpty() ){
                    foreach ( $bookData as $book ){
                        $total = $book->book;
                        $finalTotalArr[$book->secId] = round($total,2);
                    }
                }
            }



            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'slug' => $slug,
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'suspended' => $suspended,
                        'ballRunning' => $ballRunning,
                        'secId' => $runner->secId,
                        'runnerName' => $runner->runner,
                        'profit_loss' => $profitLoss,
                        'mType' => 'match_odd',
                        'backPrice' => '-',
                        'backSize' => '',
                        'layPrice' => '-',
                        'laySize' => ''
                    ];
                }
            }

            $this->marketIdsArr[] = $marketId;

        }

        return $runnersArr;

    }

    // Completed Match Runner Data With Book
    public function completedMatchRunnerDataWithBook($uid,$slug,$sportId,$eventId,$completedMatchMarket)
    {
        if( $uid != null ){
            $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
        }else{ $user = Auth::user(); if( $user->role == 6 ){ $user->id = 1; } }

        $runnersArr = $finalTotalArr = $clientArr = [];

        if (!empty( $completedMatchMarket ) && isset( $completedMatchMarket->marketId )) {

            $marketId = $completedMatchMarket->marketId;
            $suspended = $completedMatchMarket->suspended;
            $ballRunning = $completedMatchMarket->ball_running;
            $runnerData = json_decode($completedMatchMarket->runners);

            if( $user->role != 4){

                $query = DB::table('tbl_bet_pending_1')
                    ->where([['mid',$marketId],['status',1],['systemId',$user->systemId]]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    $client = json_decode($clientJson->clients);
                    $query->whereIn('uid',$client);
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr != null ){
                    $total = 0; $totalArr = [];
                    foreach ( $clientArr as $c ){
                        $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                            ->where([['clientId',$c->uid],['userId',$user->id]])->first();

                        $bookData = DB::table('tbl_book_matchodd')->select(['book','secId'])
                            ->where([['mid',$marketId],['uid',$c->uid]])->get();

                        if( $bookData->isNotEmpty() ){
                            foreach ( $bookData as $book ){
                                $total = $book->book;
                                if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                                    $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                                    $totalArr[$book->secId][] = $total;
                                }else{
                                    $totalArr[$book->secId][] = 0;
                                }
                            }
                        }
                    }

                    if( $totalArr != null ){
                        $finalTotalArr = [];
                        foreach ( $totalArr as $secId => $data ){
                            if( array_sum($data) != null && array_sum($data) ){
                                $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                            }
                        }

                    }

                }

            }elseif( $user->role == 4 ){
                $bookData = DB::table('tbl_book_matchodd')->select(['book','secId'])
                    ->where([['mid',$marketId],['uid',$user->id]])->get();

                if( $bookData->isNotEmpty() ){
                    foreach ( $bookData as $book ){
                        $total = $book->book;
                        $finalTotalArr[$book->secId] = round($total,2);
                    }
                }
            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'slug' => $slug,
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'suspended' => $suspended,
                        'ballRunning' => $ballRunning,
                        'secId' => $runner->secId,
                        'runnerName' => $runner->runner,
                        'profit_loss' => $profitLoss,
                        'mType' => 'completed_match',
                        'backPrice' => '-',
                        'backSize' => '',
                        'layPrice' => '-',
                        'laySize' => ''
                    ];
                }
            }

            $this->marketIdsArr[] = $marketId;

        }

        return $runnersArr;

    }

    // Tied Match Runner Data With Book
    public function tiedMatchRunnerDataWithBook($uid,$slug,$sportId,$eventId,$tiedMatchMarket)
    {
        if( $uid != null ){
            $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
        }else{ $user = Auth::user(); if( $user->role == 6 ){ $user->id = 1; } }

        $runnersArr = $finalTotalArr = $clientArr = [];

        if (!empty( $tiedMatchMarket ) && isset( $tiedMatchMarket->marketId )) {

            $marketId = $tiedMatchMarket->marketId;
            $suspended = $tiedMatchMarket->suspended;
            $ballRunning = $tiedMatchMarket->ball_running;
            $runnerData = json_decode($tiedMatchMarket->runners);

            if( $user->role != 4){

                $query = DB::table('tbl_bet_pending_1')
                    ->where([['mid',$marketId],['status',1],['systemId',$user->systemId]]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    $client = json_decode($clientJson->clients);
                    $query->whereIn('uid',$client);
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr != null ){
                    $total = 0; $totalArr = [];
                    foreach ( $clientArr as $c ){
                        $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                            ->where([['clientId',$c->uid],['userId',$user->id]])->first();

                        $bookData = DB::table('tbl_book_matchodd')->select(['book','secId'])
                            ->where([['mid',$marketId],['uid',$c->uid]])->get();

                        if( $bookData->isNotEmpty() ){
                            foreach ( $bookData as $book ){
                                $total = $book->book;
                                if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                                    $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                                    $totalArr[$book->secId][] = $total;
                                }else{
                                    $totalArr[$book->secId][] = 0;
                                }
                            }
                        }
                    }

                    if( $totalArr != null ){
                        $finalTotalArr = [];
                        foreach ( $totalArr as $secId => $data ){
                            if( array_sum($data) != null && array_sum($data) ){
                                $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                            }
                        }

                    }

                }

            }elseif( $user->role == 4 ){
                $bookData = DB::table('tbl_book_matchodd')->select(['book','secId'])
                    ->where([['mid',$marketId],['uid',$user->id]])->get();

                if( $bookData->isNotEmpty() ){
                    foreach ( $bookData as $book ){
                        $total = $book->book;
                        $finalTotalArr[$book->secId] = round($total,2);
                    }
                }
            }



            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'slug' => $slug,
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'suspended' => $suspended,
                        'ballRunning' => $ballRunning,
                        'secId' => $runner->secId,
                        'runnerName' => $runner->runner,
                        'profit_loss' => $profitLoss,
                        'mType' => 'tied_match',
                        'backPrice' => '-',
                        'backSize' => '',
                        'layPrice' => '-',
                        'laySize' => ''
                    ];
                }
            }

            $this->marketIdsArr[] = $marketId;

        }

        return $runnersArr;

    }

    // get Book Maker
    public function getBookMaker($uid,$sportId,$eventId)
    {
        $items = [];
        $redis = Redis::connection();
        $bookMakerJson = $redis->get("Bookmaker_" . $eventId);
        $bookMaker = json_decode($bookMakerJson);

        if ($bookMaker != null) {
            foreach ( $bookMaker as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1){
                    $runners = $this->bookmakerRunnerDataWithBook($uid,$sportId,$eventId,$data);

                    $items[] = [
                        'marketName' => $data->title,
                        'sportId' => $sportId,
                        'market_id' => $data->marketId,
                        'event_id' => $eventId,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'info' => $data->info,
                        'runners' => $runners,
                    ];
                    $this->marketIdsArr[] =  $data->marketId;
                }

            }

        }

        return $items;
    }

    // Bookmaker Runner Data With Book
    public function bookmakerRunnerDataWithBook($uid,$sportId,$eventId,$bookmakerData)
    {
        if( $uid != null ){
            $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
        }else{
            $user = Auth::user();
            if( $user->role == 6 ){ $user->id = 1; }
        }

        $runnersArr = $finalTotalArr = $clientArr = [];

        if (!empty( $bookmakerData ) && isset( $bookmakerData->marketId )) {
            $marketId = $bookmakerData->marketId;
            $suspended = $bookmakerData->suspended;
            $ballRunning = $bookmakerData->ball_running;
            $runnerData = json_decode($bookmakerData->runners);
            $systemId = $user->systemId;

            if( $user->role != 4){

                $query = DB::table('tbl_bet_pending_1')
                    ->where([['mid',$marketId],['status',1],['systemId',$systemId]]);

                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr != null ){
                    $total = 0; $totalArr = [];
                    foreach ( $clientArr as $c ){

                        $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                            ->where([['clientId',$c->uid],['userId',$user->id]])->first();

                        $bookData = DB::table('tbl_book_bookmaker')->select(['book','secId'])
                            ->where([['mid',$marketId],['uid',$c->uid],['status',1]])->get();

                        if( $bookData->isNotEmpty() ){
                            foreach ( $bookData as $book ){
                                $total = $book->book;
                                if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                                    $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                                    $totalArr[$book->secId][] = $total;
                                }else{
                                    $totalArr[$book->secId][] = 0;
                                }

                            }

                        }

                    }

                    if( $totalArr != null ){
                        $finalTotalArr = [];
                        foreach ( $totalArr as $secId => $data ){
                            if( array_sum($data) != null && array_sum($data) ){
                                $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                            }
                        }

                    }

                }

            }elseif( $user->role == 4 ){
                $bookData = DB::table('tbl_book_bookmaker')->select(['book','secId'])
                    ->where([['mid',$marketId],['uid',$user->id]])->get();
                if( $bookData->isNotEmpty() ){
                    foreach ( $bookData as $book ){
                        $total = $book->book;
                        $finalTotalArr[$book->secId] = round($total,2);
                    }

                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'secId' => $runner->secId,
                        'runner' => $runner->runner,
                        'mType' => 'bookmaker',
                        'suspended' => $suspended,
                        'ballRunning' => $ballRunning,
                        'profitLoss' => $profitLoss,
                        'backPrice' => 0,
                        'backSize' => '',
                        'layPrice' => 0,
                        'laySize' => ''
                    ];

                }
            }

        }

        return $runnersArr;

    }

    // get Virtual Cricket
    public function getVirtualCricket($uid,$sportId,$eventId)
    {
        $items = [];
        $redis = Redis::connection();
        $virtualCricketJson = $redis->get("Virtual_" . $eventId);
        $virtualCricket = json_decode($virtualCricketJson);

        if ( $virtualCricket != null ) {
            foreach ( $virtualCricket as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1){
                    $runners = $this->virtualCricketRunnerDataWithBook($uid,$sportId,$eventId,$data);

                    $items[] = [
                        'marketName' => $data->title,
                        'sportId' => $sportId,
                        'market_id' => $data->marketId,
                        'event_id' => $eventId,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'info' => $data->info,
                        'runners' => $runners,
                    ];
                    $this->marketIdsArr[] =  $data->marketId;
                }

            }

        }

        return $items;
    }

    // Virtual Cricket Runner Data With Book
    public function virtualCricketRunnerDataWithBook($uid,$sportId,$eventId,$virtualCricketData)
    {
        if( $uid != null ){
            $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
        }else{
            $user = Auth::user();
            if( $user->role == 6 ){ $user->id = 1; }
        }

        $runnersArr = $finalTotalArr = $clientArr = [];

        if (!empty( $virtualCricketData ) && isset( $virtualCricketData->marketId )) {
            $marketId = $virtualCricketData->marketId;
            $suspended = $virtualCricketData->suspended;
            $ballRunning = $virtualCricketData->ball_running;
            $runnerData = json_decode($virtualCricketData->runners);
            $systemId = $user->systemId;
            if( $user->role != 4){
                $query = DB::table('tbl_bet_pending_1')
                    ->where([['mid',$marketId],['status',1],['systemId',$systemId]]);
                if( $user->role != 1 && $user->role != 6 ){
                    $clientJson = DB::table('tbl_user_child_data')->select('clients')
                        ->where([['uid',$user->id]])->first();
                    if( $clientJson != null && isset( $clientJson->clients ) ){
                        $client = json_decode($clientJson->clients);
                        $query->whereIn('uid',$client);
                    }
                }

                $clientArr = $query->distinct()->select('uid')->get();

                if( $clientArr != null ){
                    $total = 0; $totalArr = [];
                    foreach ( $clientArr as $c ){
                        $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                            ->where([['clientId',$c->uid],['userId',$user->id]])->first();
                        $bookData = DB::table('tbl_book_bookmaker')->select(['book','secId'])
                            ->where([['mid',$marketId],['uid',$c->uid],['status',1]])->get();

                        if( $bookData->isNotEmpty() ){
                            foreach ( $bookData as $book ){
                                $total = $book->book;
                                if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                                    $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                                    $totalArr[$book->secId][] = $total;
                                }else{
                                    $totalArr[$book->secId][] = 0;
                                }

                            }

                        }

                    }

                    if( $totalArr != null ){
                        $finalTotalArr = [];
                        foreach ( $totalArr as $secId => $data ){
                            if( array_sum($data) != null && array_sum($data) ){
                                $finalTotalArr[$secId] = round((-1)*array_sum($data),2);
                            }
                        }

                    }

                }

            }elseif( $user->role == 4 ){
                $bookData = DB::table('tbl_book_bookmaker')->select(['book','secId'])
                    ->where([['mid',$marketId],['uid',$user->id]])->get();
                if( $bookData->isNotEmpty() ){
                    foreach ( $bookData as $book ){
                        $total = $book->book;
                        $finalTotalArr[$book->secId] = round($total,2);
                    }

                }

            }

            if( $runnerData != null ){
                foreach ($runnerData as $runner) {
                    $profitLoss = 0;
                    if( $finalTotalArr != null && isset( $finalTotalArr[$runner->secId] ) ){
                        $profitLoss = $finalTotalArr[$runner->secId];
                    }

                    $runnersArr[] = [
                        'sportId' => $sportId,
                        'marketId' => $marketId,
                        'eventId' => $eventId,
                        'secId' => $runner->secId,
                        'runner' => $runner->runner,
                        'mType' => 'virtual_cricket',
                        'suspended' => $suspended,
                        'ballRunning' => $ballRunning,
                        'profitLoss' => $profitLoss,
                        'backPrice' => 0,
                        'backSize' => '',
                        'layPrice' => 0,
                        'laySize' => ''
                    ];

                }
            }

        }

        return $runnersArr;

    }

    // get Fancy Market
    public function getFancyMarket($sportId,$eventId,$slug)
    {
        $items = [];
        $redis = Redis::connection();
        $fancyMarketJson = $redis->get("Fancy_" . $eventId);
        $fancyMarket = json_decode($fancyMarketJson);

        if ( $fancyMarket != null ) {
            foreach ( $fancyMarket as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1) {
                    $items[] = [
                        'marketId' => $data->marketId,
                        'eventId' => $eventId,
                        'title' => $data->title,
                        'info' => $data->info,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'mType' => 'fancy',
                        'sportId' => $sportId,
                        'slug' => $slug,
                        'isBook' => 0,
                        'no' => 0,
                        'noRate' => 0,
                        'yes' => 0,
                        'yesRate' => 0,
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }

            }

        }

        return $items;

    }

    // get Fancy2 Market
    public function getFancy2Market($sportId,$eventId,$slug)
    {
        $items = [];
        $redis = Redis::connection();
        $fancyMarketJson = $redis->get("Fancy_2_" . $eventId);
        $fancyMarket = json_decode($fancyMarketJson);

        if ( $fancyMarket != null ) {

            foreach ( $fancyMarket as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                    $items[] = [
                        'id' => $data->id,
                        'marketId' => $data->marketId,
                        'eventId' => $eventId,
                        'title' => $data->title,
                        'info' => $data->info,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'mType' => 'fancy2',
                        'sportId' => $sportId,
                        'slug' => $slug,
                        'isBook' => 0,
                        'no' => 0,
                        'noRate' => 0,
                        'yes' => 0,
                        'yesRate' => 0,
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }

            }

        }

        return $items;

    }

    // get Fancy3 Market
    public function getFancy3Market($sportId,$eventId,$slug)
    {
        $items = [];
        $redis = Redis::connection();
        $fancyMarketJson = $redis->get("Fancy_3_" . $eventId);
        $fancyMarket = json_decode($fancyMarketJson);

        if ( $fancyMarket != null ) {
            foreach ( $fancyMarket as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                    $items[] = [
                        'id' => $data->id,
                        'marketId' => $data->marketId,
                        'eventId' => $eventId,
                        'title' => $data->title,
                        'info' => $data->info,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'mType' => 'fancy3',
                        'sportId' => $sportId,
                        'slug' => $slug,
                        'isBook' => 0,
                        'no' => 0,
                        'noRate' => 0,
                        'yes' => 0,
                        'yesRate' => 0,
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }

            }

        }

        return $items;

    }

    // get Khado Session
    public function getKhadoSession($sportId,$eventId,$slug)
    {
        $items = [];
        $redis = Redis::connection();
        $khadoSessionJson = $redis->get("Khado_" . $eventId);
        $khadoSession = json_decode($khadoSessionJson);

        if ( $khadoSession != null ) {

            foreach ( $khadoSession as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                    $items[] = [
                        'id' => $data->id,
                        'marketId' => $data->marketId,
                        'eventId' => $eventId,
                        'title' => $data->title,
                        'info' => $data->info,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'mType' => 'khado',
                        'sportId' => $sportId,
                        'slug' => $slug,
                        'isBook' => 0,
                        'no' => 0,
                        'noRate' => 0,
                        'yes' => 0,
                        'yesRate' => 0,
                        'diff' => $data->diff,
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }

            }

        }
        return $items;
    }

    // get Ball Session
    public function getBallSession($sportId,$eventId,$slug)
    {
        $items = [];
        $redis = Redis::connection();
        $ballSessionJson = $redis->get("Ballbyball_" . $eventId);
        $ballSession = json_decode($ballSessionJson);

        if ( $ballSession != null ) {

            foreach ( $ballSession as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                    $items[] = [
                        'id' => $data->id,
                        'marketId' => $data->marketId,
                        'eventId' => $eventId,
                        'title' => $data->title,
                        'info' => $data->info,
                        'suspended' => $data->suspended,
                        'ballRunning' => $data->ball_running,
                        'bet_allowed' => $data->bet_allowed,
                        'mType' => 'ballbyball',
                        'sportId' => $sportId,
                        'slug' => $slug,
                        'isBook' => 0,
                        'no' => 0,
                        'noRate' => 0,
                        'yes' => 0,
                        'yesRate' => 0,
                        'ball' => $data->ball,
                        'onEdit' => $data->editOn
                    ];

                    $this->marketIdsArr[] = $data->marketId;
                }

            }

        }
        return $items;
    }

    // get Jackpot Data
    public function getJackpotData($uid,$sportId,$eventId)
    {
        $items = [];
        $redis = Redis::connection();

        $jackpotGameTypeJson = $redis->get("Jackpot_type");
        $jackpotGameType = json_decode($jackpotGameTypeJson);

        foreach ( $jackpotGameType as $type ) {

            $jackpotDataJson = $redis->get("Jackpot_data_".$type->id."_". $eventId);
            $jackpotData = json_decode($jackpotDataJson);

            if( $jackpotData != null ){
                $listData = [];
                foreach ( $jackpotData as $data ) {
                    if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1 ) {
                        $listData[] = [
                            'marketId' => $data->marketId,
                            'title' => $data->title,
                            'suspended' => $data->suspended,
                            'ballRunning' => $data->ball_running,
                            'bet_allowed' => $data->bet_allowed,
                        ];
                    }
                }

                if( isset( $type->status ) && $type->status == 1 ) {
                    $items[] = [
                        'eventId' => $eventId,
                        'title' => $type->title,
                        'sportId' => $sportId,
                        'typeId' => $type->id,
                        'listData' => $listData
                    ];
                }
            }

        }

        return $items;

    }

    // get Cricket Casino
    public function getCricketCasino($uid,$sportId,$eventId)
    {
        $items = [];
        $redis = Redis::connection();
        $casinoDataJson = $redis->get("Casino_". $eventId);
        $casinoData = json_decode($casinoDataJson);

        if( $casinoData != null ){
            foreach ( $casinoData as $data ) {
                if( isset( $data->game_over ) && $data->game_over != 1 && $data->status == 1) {
                    $items[] = [
                        'id' => $data->id,
                        'sportId' => $sportId,
                        'mType' => 'cricket_casino',
                        'eventId' => $eventId,
                        'marketId' => $data->marketId,
                        'title' => $data->title,
                        'is_book' => 0,
                        'rate' => $data->rate,
                        'bet_allowed' => $data->bet_allowed,
                    ];
                }
            }
        }

        return $items;

    }

    // get Profit Loss for Book Maker
    public function getProfitLossBookMaker($uid,$marketId,$secId)
    {
        if( $uid != null ){
            $user = DB::table('tbl_user')->select(['id','role'])
                ->where([['id',$uid],['status',1]])->first();
        }else{ $user = Auth::user();  }
        $clientArr = []; $book = 0;

        $query = DB::table('tbl_bet_pending_1')->where([['mid',$marketId],['status',1]]);

        if( $user->role != 1 && $user->role != 6 ){
            $clientJson = DB::table('tbl_user_child_data')->select('clients')
                ->where([['uid',$user->id]])->first();
            $client = json_decode($clientJson->clients);
            $query->whereIn('uid',$client);
        }

        $clientArr = $query->distinct()->select('uid')->get();

        if( $clientArr != null ){
            $total = 0; $totalArr = [];
            foreach ( $clientArr as $c ){

                $book = DB::table('tbl_book_bookmaker')->select('book')
                   ->where([['uid',$c->uid],['mid',$marketId],['secId',$secId]])->first();

               // $book = DB::table('tbl_user_market_expose_2')->select('expose')
                 //  ->where([['mid',$marketId],['secId',$secId],['uid',$c->uid],['mType','bookmaker']])->first();

                if( !empty($book) && $book->book != 0){
                 $total = $book->book;
                    $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                        ->where([['clientId',$c->uid],['userId',$user->id]])->first();

                    if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                        $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                        $totalArr[] = $total;
                    }else{
                        $totalArr[] = $total;
                    }

                }

            }

            if( array_sum($totalArr) != null && array_sum($totalArr) ){

                if( $user->role == 4 ){
                    $book = round(array_sum($totalArr),0);
                }else{
                    $book = round((-1)*array_sum($totalArr),0);
                }

            }

        }

        return $book;

    }

    // get Profit Loss for Match Odd
    public function getProfitLossMatchOdd($uid,$marketId,$secId)
    {
        if( $uid != null ){
            $user = DB::table('tbl_user')->select(['id','role'])
                ->where([['id',$uid],['status',1]])->first();
        }else{ $user = Auth::user();  }
        $clientArr = []; $book = 0;

        $query = DB::table('tbl_bet_pending_1')
            ->where([['mid',$marketId],['status',1]]);

        if( $user->role != 1 && $user->role != 6 ){
            $clientJson = DB::table('tbl_user_child_data')->select('clients')
                ->where([['uid',$user->id]])->first();
            $client = json_decode($clientJson->clients);
            $query->whereIn('uid',$client);
        }

        $clientArr = $query->distinct()->select('uid')->get();

        if( $clientArr != null ){
            $total = 0; $totalArr = [];
            foreach ( $clientArr as $c ){
                $book = DB::table('tbl_book_matchodd')->select('book')
                    ->where([['mid',$marketId],['secId',$secId],['uid',$c->uid]])->first();
                //$book = DB::table('tbl_user_market_expose_2')->select('expose')
                //    ->where([['mid',$marketId],['secId',$secId],['uid',$c->uid],['mType','match_odd']])->first();

                if( $book != null && $book->book != 0 ){
                    $total = $book->book;
                    $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                        ->where([['clientId',$c->uid],['userId',$user->id]])->first();

                    if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                        $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                        $totalArr[] = $total;
                    }else{
                        $totalArr[] = 0;
                    }

                }

            }

            if( array_sum($totalArr) != null && array_sum($totalArr) ){

                if( $user->role == 4 ){
                    $book = round(array_sum($totalArr),0);
                }else{
                    $book = round((-1)*array_sum($totalArr),0);
                }

            }

        }

        return $book;

    }

    // get Profit Loss Cricket Casino
    public function getProfitLossCricketCasino($uid,$marketId,$secId)
    {
        if( $uid != null ){
            $user = DB::table('tbl_user')->select(['id','role'])
                ->where([['id',$uid],['status',1]])->first();
        }else{ $user = Auth::user();  }
        $clientArr = []; $book = 0;

        $query = DB::table('tbl_bet_pending_4')
            ->where([['mid',$marketId],['secId',$secId],['status',1]]);

        if( $user->role != 1 && $user->role != 6 ){
            $clientJson = DB::table('tbl_user_child_data')->select('clients')
                ->where([['uid',$user->id]])->first();
            $client = json_decode($clientJson->clients);
            $query->whereIn('uid',$client);
        }

        $clientArr = $query->distinct()->select('uid')->get();

        if( $clientArr != null ){
            $total = 0; $totalArr = [];
            foreach ( $clientArr as $c ){
                $book = DB::table('tbl_book_bookmaker')->select('book')
                    ->where([['mid',$marketId],['secId',$secId],['uid',$c->uid]])->first();
                //$book = DB::table('tbl_user_market_expose_2')->select('expose')
                //    ->where([['mid',$marketId],['secId',$secId],['uid',$c->uid],['mType','match_odd']])->first();

                if( $book != null && $book->book != 0 ){
                    $total = $book->book;
                    $profitLoss = DB::table('tbl_user_profit_loss')->select(['actual_profit_loss'])
                        ->where([['clientId',$c->uid],['userId',$user->id]])->first();

                    if( $profitLoss != null && (float)$profitLoss->actual_profit_loss > 0 ){
                        $total = ($total*(float)$profitLoss->actual_profit_loss)/100;
                        $totalArr[] = $total;
                    }else{
                        $totalArr[] = $total;
                    }

                }

            }

            if( array_sum($totalArr) != null && array_sum($totalArr) ){

                if( $user->role == 4 ){
                    $book = round(array_sum($totalArr),0);
                }else{
                    $book = round((-1)*array_sum($totalArr),0);
                }

            }

        }

        return $book;

    }

    // setBookData
    public function setBookData($uid,$betData)
    {
        $redis = Redis::connection();
        $bookMakerJson = $redis->get("Market_" . $betData->mid);
        $bookMaker = json_decode($bookMakerJson);

        if( $bookMaker != null && isset( $bookMaker->runners )){

            $eventId = $bookMaker->eid;
            $marketId = $bookMaker->mid;

            $runners = json_decode($bookMaker->runners);

            if( $runners != null ){
                $i = 0; $bookDataArr = [];
                foreach ( $runners as $runner ){

                    $bookDataArr[$i] = [
                        'uid' => $betData->uid,
                        'eid' => $eventId,
                        'mid' => $marketId,
                    ];

                    if( $betData->bType == 'lay' ){

                        if( $runner->secId == $betData->secId ){
                            $book = ( $betData->rate*$betData->size )/100;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( (-1)*$book , 2);
                        }else{
                            $book = $betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( $book, 2);
                        }

                    }else{

                        if( $runner->secId == $betData->secId ){
                            $book = ( $betData->rate*$betData->size )/100;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( $book, 2);
                        }else{
                            $book = $betData->size;
                            $bookDataArr[$i]['secId'] = $runner->secId;
                            $bookDataArr[$i]['book'] = round( (-1)*$book , 2);
                        }

                    }


                    $i++;
                }

                if( $bookDataArr != null ){

                    $book = DB::table('tbl_book_bookmaker')->select(['book'])
                        ->where([['uid',$uid],['eid',$eventId],['mid',$marketId]])->first();

                    if( $book != null ){

                        foreach ( $bookDataArr as $bookData ){
                            $where = [['uid',$uid],['eid',$eventId],['mid',$marketId],['secId',$bookData['secId']]];
                            $bookCheck = DB::table('tbl_book_bookmaker')->select(['book'])->where($where)->first();
                            if( $bookCheck != null ){
                                $newBook = round( ($bookCheck->book+$bookData['book']) , 2 );
                                DB::table('tbl_book_bookmaker')->where($bookDataArr)->update(['book'=>$newBook]);
                            }else{
                                DB::table('tbl_book_bookmaker')->insert($bookData);
                            }
                        }

                    }else{
                        DB::table('tbl_book_bookmaker')->insert($bookDataArr);
                    }


                }


            }
        }

    }
}
