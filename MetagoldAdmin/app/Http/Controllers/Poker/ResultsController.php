<?php

namespace App\Http\Controllers\Poker;
use App\Http\Controllers\Controller;
use App\Models\TeenpattiResult;
use Illuminate\Support\Facades\DB;


class ResultsController extends Controller
{

    /**
     * Action Game Result
     */
    public function actionGameResult()
    {

        $data = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

        if( isset($data) && isset( $data['result'] ) && isset($data['result'][0]) && isset( $data['runners'] )
            && isset( $data['roundId'] ) ){

            $userArr = $userIds = $betArr = [];
            $roundId = $data['roundId'];
            $result = $data['result'][0];

            $eventId = $result['gameId'];
            $marketId = $result['marketId'];
            $winner = $result['winnerId'];

            $betvoid = false;

            if( isset( $data['betvoid'] ) && $data['betvoid'] != false ){
                $betvoid = true;
            }

            $resultCheck = DB::table('tbl_teenpatti_result')->select(['user_data'])
                ->where([['market_id',$marketId],['event_id',$eventId]])->first();

            if( $resultCheck != null ){
                // do nothing
            }else{

                foreach ( $data['result'] as $res ){

                    $userArr[] = [
                        'event_id' => $eventId,
                        'market_id' => $marketId,
                        'user_id' => $res['userId'],
                        'pl' => $res['downpl']
                    ];

                    $betArr[] = [
                        'user_id' => $res['userId'],
                        'bets' => $res['orders']
                    ];

                    $userIds[] = $this->updatedUserResultData($res['userId'],$marketId,$res['downpl']);
                }

                $description = 'Teen Patti ';

                if( isset($data['market']) ){

                    $marketData = $data['market'];

                    $description = '';
                    foreach ( $marketData['marketRunner'] as $marketRunner ){
                        $cards = ' , ';
                        if( isset( $marketRunner['cards'] ) && $marketRunner['cards'] != null ){
                            $cards = json_encode($marketRunner['cards']);
                            $cards = '( '.$cards.' )';
                        }

                        $description .= $marketRunner['name'].' : '.$marketRunner['status'].$cards;
                    }

                    if( isset( $marketData['indexCard'] ) && $marketData['indexCard'] != null ){
                        $indexCard = json_encode($marketData['indexCard']);
                        $description .= ' IndexCard( '.$indexCard.' )';
                    }

                }

                $model = new TeenpattiResult();

                $model->eid = $eventId;
                $model->mid = $marketId;
                $model->round = $roundId;
                $model->game_over = 0;
                $model->winner = $winner;
                $model->description = $description;
                $model->result = json_encode($data['runners']);
                $model->user_data = json_encode($userArr);
                $model->bet_data = json_encode($betArr);
                $model->result_data = json_encode($data);
                $model->status = 1;
                $model->betvoid = $betvoid == true ? 1 : 0;

                if( $model->save() ){
                    $response = [
                        "Error" => 0,
                        "result" => $userIds,
                        "message" => "user pl updated!",//"Result Saved Successfully...!",
                    ];
                }else{
                    $response = [
                        "Error"=> 1,
                        "result" => null,
                        "message"=> "Something Wrong!!!",
                    ];
                }

                return json_encode($response);

            }


        }

    }

    //updated User Result Data
    public function updatedUserResultData($uid,$marketId,$pl)
    {
        $wallet = $newExpose = $newPl = 0;

        $user = DB::table('tbl_user_info')->select(['balance','expose','pl_balance'])
            ->where([['uid',$uid]])->first();

        if( $user != null ){

            if( $marketId != false ){
                $newPl = round($user->pl_balance)+round($pl);

                $expose = DB::table('tbl_user_market_expose')->select(['expose'])
                    ->where([['uid',$uid],['market_id',$marketId]])->first();

                if( $expose != null ){
                    $newExpose = round($user->expose)-round($expose->expose);
                }
                $wallet = round($user->balance)+round($newPl)-round($newExpose);
            }else{
                $wallet = round($user->balance)+round($user->pl_balance)-round($user->expose);
            }

        }

        return ['wallet' => $wallet , 'exposure' => 0 , 'userId' => $uid ];


    }

}
