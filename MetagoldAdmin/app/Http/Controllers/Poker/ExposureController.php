<?php

namespace App\Http\Controllers\Poker;
use App\Http\Controllers\Controller;
use App\Models\Teenpatti;
use Illuminate\Support\Facades\DB;


class ExposureController extends Controller
{

    /**
     * Action Bet Place
     */
    public function actionBetPlace()
    {

        try {
            $response = [ "status" => 1, "message" => "Something wrong! Bet Not Saved!"];
            $requestData = json_decode(file_get_contents('php://input'), JSON_FORCE_OBJECT);
            // $this->saveResponse($requestData,'auth');

            $setting = DB::table('tbl_common_setting')->select(['value'])
                ->where([['key','LIVE_GAME_STATUS'],['status',1]])->first();

            if( $setting != null && trim($setting->value) != 'TRUE' ){
                $response['errorDescription'] = "This sport is block by parent!";
                return json_encode($response);
            }

            if( isset($requestData) && isset($requestData['token']) && isset($requestData['betInfo']) ){

                $token = $requestData['token']; $betInfo = $requestData['betInfo'];
                $calculateExpose = isset( $requestData['calculateExposure'] ) ? $requestData['calculateExposure'] : 0;
                if( $calculateExpose < 0 ){
                    $calculateExpose = (-1)*round( $calculateExpose );
                }

                $authCheck = DB::table('oauth_access_tokens')->select(['user_id'])->where([['id',$token]])->first();

                if( $authCheck != null ) {
                    $uid = $authCheck->user_id; $pId = 1;
                    $gameId = $betInfo['gameId'];

                    $userStatusCheck = DB::table('tbl_user_block_status')->select(['type'])->where([['uid',$uid]])->first();
                    if( $userStatusCheck != null ){
                        $message = 'You are blocked by parent! Plz contact administrator!!';
                        if( $userStatusCheck->type != 1 ){
                            $message = 'Your bet is locked by parent! Plz contact administrator!!';
                        }
                        $response = [ "status" => 3, "message" => $message ];
                        return json_encode($response);exit;
                    }

                    $user = DB::table('tbl_user as u')
                        ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                        ->select(['u.systemId','parentId','name','username','balance','pl_balance','expose','role','pName'])
                        ->where([['is_login',1],['role',4],['u.id',$uid]])->first();

                    if( $user != null ){

                        $parentId = $user->parentId;
                        $systemId = $user->systemId;
                        $mainBalance = round($user->balance);
                        $plBalance = round($user->pl_balance);
                        $exposeBalance = round($user->expose);
                        $balance = round($mainBalance + $plBalance - $exposeBalance);

                        $gameData = DB::table('tbl_live_game')
                            ->select(['sportId','eventId','name','mType','is_block','min_stake','max_stake','max_profit_limit','suspend'])
                            ->where([['status',1],['is_block',0],['eventId',$gameId]])->first();

                        if( $gameData != null ){

                            if ($gameData != null && $gameData->suspended != 0) {
                                $response = [
                                    "status" => 3,
                                    "message" => "Suspended ! Bet Not Allowed!",
                                ];

                                return json_encode($response);

                            } elseif ($gameData != null && ((int)$gameData->min_stack > (int)$betInfo['reqStake'])) {

                                $response = [
                                    "status" => 3,
                                    "message" => "Min Stack is " . $gameData->min_stack . "! Bet Not Allowed!",
                                ];

                                return json_encode($response);

                            } elseif ($gameData != null && ((int)$gameData->max_stack < (int)$betInfo['reqStake'])) {

                                $response = [
                                    "status" => 3,
                                    "message" => "Max Stack is " . $gameData->max_stack . "! Bet Not Allowed!",
                                ];

                                return json_encode($response);
                            }


                            $checkEventStatus = DB::table('tbl_user_event_status')->select(['uid'])
                                ->where([['uid',$parentId],['eid',$gameId]])->first();

                            if( $checkEventStatus != null ){
                                $message = 'This event is block by parent!';
                                $response = [ "status" => 3, "message" => $message ];
                                return json_encode($response);exit;
                            }

                            $checkSportStatus = DB::table('tbl_user_sport_status')->select(['uid'])
                                ->where([['uid',$parentId],['sid',99]])->first();

                            if( $checkSportStatus != null ){
                                $message = 'This sport is block by parent!';
                                $response = [ "status" => 3, "message" => $message ];
                                return json_encode($response);exit;
                            }

                            $userAvailableBalance = $this->checkAvailableBalance($uid,$betInfo['marketId'],$calculateExpose);

                            $exposeBalance = $userAvailableBalance['expose'];

                            if (($userAvailableBalance['expose'] > $userAvailableBalance['balance'])) {
                                $response = [
                                    "status" => 3,
                                    "message" => "Insufficient fund !! Bet Not Allowed!",
                                ];
                                return json_encode($response);
                            }


                            $model = new Teenpatti();

                            $model->systemId = $systemId;
                            $model->orderId = isset( $betInfo['orderId'] ) ? $betInfo['orderId'] : 123;
                            $model->uid = $uid;
                            $model->sid = 99;
                            $model->eid = $gameId;
                            $model->mid = $betInfo['marketId'];
                            $model->round = $betInfo['roundId'];
                            $model->secId = $betInfo['runnerId'];

                            $model->price = $betInfo['requestedOdds'];
                            $model->size = $betInfo['reqStake'];
                            $model->rate = $betInfo['requestedOdds'];
                            $model->win = isset( $betInfo['pnl'] ) ? $betInfo['pnl'] : 0;
                            $model->loss = isset( $betInfo['reqStake'] ) ? $betInfo['reqStake'] : 0;

                            $model->client = $user->username;
                            $model->master = $user->pName;

                            $model->runner = $betInfo['runnerName'];
                            $model->market = $gameData->name.' - '.ucfirst( isset( $requestData['marketType'] ) ? $requestData['marketType'] : '' );
                            $model->event = $gameData->name;
                            $model->sport = 'Live Game';

                            $model->bType = $betInfo['isBack'] == 1 ? 'BACK' : 'LAY';
                            $model->mType = $gameData->mType;


                            $model->result = 'PENDING';
                            $model->description = $gameData->name . ' > ' . $betInfo['runnerName'];
                            $model->is_match = 1;

                            $model->ip_address = $this->getClientIp();
                            $model->status = 1;

                            if ($model->save()) {

                                $balance = round($mainBalance + $plBalance - $exposeBalance);
                                $response = [
                                    "status" => 0,
                                    "message" => "Exposure insert Successfully...!",
                                    "wallet" => $balance,
                                ];
                                return json_encode($response);

                            }


                        }else{
                            $response['message'] = "Wrong data! Bet Not Allowed!";
                            return json_encode($response);
                        }


                    }



                }


            }else{
                return json_encode($response);
            }


        } catch ( \Throwable $t) {
            $response['errorDescription'] = "Something Wrong! Server Error 500 !!";
            return json_encode($response);
        }

    }

    //checkAvailableBalance
    public function checkAvailableBalance($uid,$marketId,$calculateExpose)
    {
        $user = DB::table('tbl_user_info')->select(['balance','expose','pl_balance'])
            ->where([['uid',$uid]])->first();

        if ($user != null) {
            $balance = round( (int)$user->balance );
            $expose = round( (int)$user->expose );
            $profitLoss = round( (int)$user->pl_balance );

            if( $profitLoss < 0 ){
                $profitLoss = (-1)*$profitLoss;
                $mywallet = ( $balance - $profitLoss );
            }else{
                $mywallet = ( $balance + $profitLoss );
            }

            //Old Expose
            $userExpose = DB::table('tbl_user_market_expose')
                ->where([['uid',$uid],['status',1],[ 'market_id','!=',$marketId ]])->sum(['expose']);

            if ( isset( $userExpose ) && $userExpose != null && $userExpose != '') {
                $balExpose = round( $userExpose );
            }else{
                $balExpose = 0;
            }

            $newExpose = (int)$balExpose + (int)$calculateExpose;
            return $data = [ 'balance' => $mywallet, 'expose' => $newExpose , 'calExp' => $calculateExpose ];

        }
        return $data = [ 'balance' => 0, 'expose' => 0 , 'calExp' => 0 ];

    }

    /**
     * update User Expose
     */
    public function updateUserExpose( $uid, $eid, $mid, $mType, $expose )
    {
        $userExpose = DB::table('tbl_user_market_expose')->select(['id'])
            ->where([['uid',$uid],['eid',$eid],['mid',$mid],['status',1]])->first();

        if( $userExpose != null ){
            $updateData = [
                'expose' => $expose,
            ];

            DB::table('tbl_user_market_expose')
                ->where([['uid',$uid],['eid',$eid],['mid',$mid],['status',1]])->update($updateData);

        }else{
            $addData = [
                'uid' => $uid,
                'eid' => $eid,
                'mid' => $mid,
                'mType' => $mType,
                'expose' => $expose,
                'profit' => 0,
                'status' => 1,
            ];

            DB::table('tbl_user_market_expose')->insert($addData);
        }

    }

    /**
     * update User Profit
     */
    public function updateUserProfit( $uid, $eid, $mid, $mType, $profit )
    {
        $userExpose = DB::table('tbl_user_market_expose')->select(['id'])
            ->where([['uid',$uid],['eid',$eid],['mid',$mid],['status',1]])->first();

        if( $userExpose != null ){
            $updateData = [
                'profit' => $profit,
            ];

            DB::table('tbl_user_market_expose')
                ->where([['uid',$uid],['eid',$eid],['mid',$mid],['status',1]])->update($updateData);

        }else{
            $addData = [
                'uid' => $uid,
                'eid' => $eid,
                'mid' => $mid,
                'mType' => $mType,
                'expose' => 0,
                'profit' => $profit,
                'status' => 1,
            ];

            DB::table('tbl_user_market_expose')->insert($addData);
        }

    }


    /**
     * saveResponse
     */
    public function saveResponse($data,$type)
    {
        $filePath = '/var/www/html/pokerlog/'.$type.'.txt';

        $fp = fopen($filePath, 'a');
        fwrite($fp, json_encode($data));
        fclose($fp);
    }

    /**
     * getClientIp
     */
    public function getClientIp()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}
