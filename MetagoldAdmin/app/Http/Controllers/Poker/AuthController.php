<?php

namespace App\Http\Controllers\Poker;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class AuthController extends Controller
{

    /**
     * Action Login
     */
    public function actionLogin()
    {

        try {
            $response = [ "errorCode" => 1, "errorDescription" => "Something Wrong!" ];
            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            // $this->saveResponse($requestData,'auth');
            $setting = DB::table('tbl_common_setting')->select(['value'])
                ->where([['key','LIVE_GAME_STATUS'],['status',1]])->first();

            if( $setting != null && trim($setting->value) != 'TRUE' ){
                $response['errorDescription'] = "This sport is block by parent!";
                return json_encode($response);
            }

            if( isset($requestData) && isset($requestData['token']) && isset($requestData['operatorId']) ){

                $token = $requestData['token'];
                $authCheck = DB::table('oauth_access_tokens')->select(['user_id'])->where([['id',$token]])->first();

                if( $authCheck != null ){
                    $uid = $authCheck->user_id;

                    $userStatusCheck = DB::table('tbl_user_block_status')->select(['type'])
                        ->where([['uid',$uid],['type',1]])->first();
                    if( $userStatusCheck != null ){
                        $message = 'You are blocked by parent! Plz contact administrator!!';
                        $response = [ "status" => 3, "message" => $message ];
                        return json_encode($response);exit;
                    }

                    $user = DB::table('tbl_user as u')
                        ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                        ->select(['name','username','balance','pl_balance','expose','role'])
                        ->where([['is_login',1 ],['u.id' => $uid ]])->first();

                    if( $user != null ){
                        $balance = 0;
                        if( $user->role == 4 ){
                            $balance = round(($user->balance)+($user->pl_balance)-($user->expose));
                        }

                        $responseData = [
                            "operatorId" => 9093,
                            "userId" => $uid,
                            "username" => $user->username,
                            "playerTokenAtLaunch" => $token,
                            "token" => $token,
                            "balance" => $balance,
                            "currency" => "INR",
                            "language" => "en",
                            "timestamp" => time(),
                            "clientIP" => ["1"],
                            "VIP" => "3",
                            "errorCode" => 0,
                            "errorDescription" => "ok"
                        ];

                        return json_encode($responseData);

                    }else{
                        $response['errorDescription'] = "User Not Found!";
                        return json_encode($response);
                    }

                }else{
                    return json_encode($response);
                }

            }else{
                return json_encode($response);
            }

        } catch ( \Throwable $t) {
            $response['errorDescription'] = "Something Wrong! Server Error 500 !!";
            return json_encode($response);
        }

    }

    /**
     * saveResponse
     */
    public function saveResponse($data,$type)
    {
        $filePath = '/var/www/html/pokerlog/'.$type.'.txt';

        $fp = fopen($filePath, 'a');
        fwrite($fp, json_encode($data));
        fclose($fp);
    }

}
