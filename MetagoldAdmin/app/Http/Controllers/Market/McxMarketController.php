<?php

namespace App\Http\Controllers\Market;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class McxMarketController extends Controller
{
  public function mcxMarket(Request $request)
       {   
          $market = DB::table('tbl_mcxmarket2')->select('*')->whereIn('status',[1,0])->get();

          $blockCount=[];
          $i=0;
          foreach ($market as $key => $val) {
             
          	  $depth = json_decode($val->depth);
          	  $buy_price = $depth->buy[0]->price;
          	  $sell_price = $depth->sell[0]->price;
          	  $depthPrice = ['buy'=>$buy_price,'sell'=>$sell_price];
              $blockCount[] = $val->is_block;
           $arr[] =[
               'name'=>$val->name,
               'buy_quantity'=>$val->buy_quantity,
               'depth'=>$depthPrice,
               'expiry'=>$val->expiry,
               'instrument_token'=>$val->instrument_token,
               'last_price'=>$val->last_price,
               'last_quantity'=>$val->last_quantity,
               'last_trade_time'=>$val->last_trade_time,
               'lower_circuit_limit'=>$val->lower_circuit_limit,
               'net_change'=>$val->net_change,
               'ohlc'=>json_decode($val->ohlc),
               'oi'=>$val->oi,
               'oi_day_high'=>$val->oi_day_high,
               'oi_day_low'=>$val->oi_day_low,
               'sell_quantity'=>$val->sell_quantity,
               'timestamp'=>$val->timestamp,
               'tradingSymbol'=>$val->tradingSymbol,
               'upper_circuit_limit'=>$val->upper_circuit_limit,
               'average_price'=>$val->average_price,
               'is_block'=>$val->is_block

            ];
          $i++;
          }
          if(in_array(0, $blockCount)){
            $is_all_block = 0;
          }else{
            $is_all_block = 1;
          }
          
        
$response = ['status'=>1, 'code'=>200, 'message'=>'Data found', 'data'=>['list'=>$arr,'is_block_all'=>$is_all_block]];

        return $response;



       }

}
