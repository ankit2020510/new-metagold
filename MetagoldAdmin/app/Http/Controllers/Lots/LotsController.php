<?php

namespace App\Http\Controllers\Lots;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\PlaceOrder;

class LotsController extends Controller
{


       public function lot(Request $request)
       {   
     
           $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!!" ];
     	   
           $lots = DB::table('tbl_lots')->where('status',1)->get();
           if(!empty($lots)){
            // print_r($arr);
            foreach ($lots as  $val) {

                $arr[] = [
                    'id'=>$val->id,
                    'key_name'=>$val->key_name,
                    'min'=>$val->min,
                    'max'=>$val->max,
                    'commission'=>$val->commission,
                    'unit'=>$val->unit,
                    'status'=>$val->status,
                    'buttonsConfig'=>json_decode($val->buttonConfig)
                ];
            }
              $response = ['status' => 1, 'code' => 200, 'data' =>$arr, 'message' => 'data found!!'];
           }else{
             $response = ['status' => 1, 'code' => 200, 'data' =>null, 'message' => 'data found!!'];
           } 
           return $response;
       }

       public function create(Request $request)
       {   
     
           $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!!" ];
           $key_name=[];

          // print_r($request->items); die('djfkjdkjkdgj');
           $lots = DB::table('tbl_lots')->where('status',1)->get();
           if(!empty($lots)){
             foreach ($lots as $val) {
                  $key_name[]=$val->key_name;
             }  
           }
           
           if(isset($request->key_name)){
             if(!in_array(strtoupper($request->key_name), $key_name)){             
            // print_r($arr);
                $data = [
                   'key_name'=>strtoupper($request->key_name),
                   'min'=>$request->min,
                   'max'=>$request->max,
                   'commission'=>$request->commission,
                   'unit'=>$request->unit,
                   'buttonConfig'=>json_encode($request->buttonsConfig)
                 ];

              $lots = DB::table('tbl_lots')->insert($data);

              $log = 'Market lots addded';
              LogActivity::addToLog($log);
              $response = ['status' => 1, 'code' => 200, 'message' => 'Market details added successfully!!'];
           }else{

              $update = [
                   'min'=>$request->min,
                   'max'=>$request->max,
                   'commission'=>$request->commission,
                   'unit'=>$request->unit,
                   'buttonConfig'=>json_encode($request->buttonsConfig)
                 ];
              $lots = DB::table('tbl_lots')->where([['key_name',$request->key_name],['status',1]])->update($update);
               $log = 'Market lots modified';
              LogActivity::addToLog($log);
             $response = ['status' => 1, 'code' => 200, 'message' => 'Details updated successfully!!'];
           } 

         }
           return $response;
       }

       public function edit(Request $request)
       {   
     
           $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!!" ];
          
           $list = DB::table('tbl_lots')->where([['status',1],['key_name',strtoupper($request->key_name)]])->first();
           if(!empty($list)){

              $response = ['status' => 1, 'code' => 200, 'data'=>$list, 'message' => 'Data Found!!'];
           } 

       
           return $response;
       }

       public function delete(Request $request)
       {   
     
           $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!!" ];
           if(isset($request->key_name)){

               $status = ['status'=>2];
               $lots = DB::table('tbl_lots')->where('key_name',strtoupper($request->key_name))->update($status); 

                $log = 'Market lots deleted';
              LogActivity::addToLog($log);

               $response = ['status' => 1, 'code' => 200, 'message' => 'Deleted successfully!!'];
           }
           
          
           return $response;
       }

      public function addLog(Request $request){

        $arr = ['title' => 'Log Activity' ,'icon'=>'log.svg', 'url'=>'/logactivity','main_menu_id'=>12,'position'=>19,'status'=>1];
          $stmt = 'ALTER TABLE tbl_user_info
ADD netere int NOT NULL DEFAULT(0)';
          //$data = DB::table('tbl_main_menu')->where('title','Log Activity')->first();
        /* if(empty($data)){
            DB::table('tbl_main_menu')->insert($arr);
            $res = "update";
          }else{
            $res = "exist";
          }*/


         // print_r(get_browser($request->header('User-Agent'), true));  
          die('lklkkk');

        
        return $res;
      }

     


 }























