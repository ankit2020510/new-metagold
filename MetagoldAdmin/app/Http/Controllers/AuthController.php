<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\LogActivity;


class AuthController extends Controller
{
    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = [
            'username' => $request->username,
            'password' => $request->password,
        ];

        if (auth()->attempt($credentials)) {

            $token = auth()->user()->createToken('TutsForWeb')->accessToken;
            $userData = auth()->user();

            $checkBlock = DB::table('tbl_user_block_status')->where([['uid',$userData->id],['type',1]])->first();

            if( $userData->status != 1 ){

                $message = 'This user is not active !!';

                if( $userData->status == 2 ){
                    $message = 'This user is already deleted !!';
                }

                $this->logout();
                $response = [
                    'status' => 0,
                    'error' => [ 'message' => $message ]
                ];

                return response()->json($response);

            }

            if( $checkBlock != null || !in_array($userData->role,[1,2,3,5,6,8]) || $userData->systemId != $request->systemId ){

                $this->logout();
                $message = 'Invalid User For This System !!';
                if( $checkBlock != null ){
                    $message = 'This user is blocked by parent!!';
                }

                $response = [
                    'status' => 0,
                    'error' => [ 'message' => $message ]
                ];

                return response()->json($response);

            }

            $details = [
                'token' => $token,
                'systemId' => $userData->systemId,
                'auth_id' => $userData->id,
                'name' => $userData->name,
                'username' => $userData->username,
                'role' => $userData->roleName,
                'last_logged_on' => $userData->last_logged_on,
                'is_password_updated' => $userData->is_password_updated
            ];

            DB::table('tbl_user')->where([['id',$userData->id]])->update(['is_login' => 1]);

            $response = [
                'status' => 1,
                'data' => $details,
                'success' => [
                    'message' => 'Logged In successfully!'
                ]
            ];

            $message = 'Login Activity | User: ['.$userData->id.' | '.$userData->username.'] | ipAddress: '.$this->getClientIp();
            $this->activityLog($message);
            $log = 'User Logged In';
            LogActivity::addToLog($log);

            return response()->json($response, 200);


        } else {

            $response = [
                'status' => 0,
                'error' => [ 'message' => 'Incorrect username or password !' ]
            ];

            return response()->json($response);
        }

    }

    /**
     * Handles change pass Request
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function changePassword(Request $request)
    {
        if( strlen($request->password2) >= 8
            && $request->password1 == $request->password2
            && $request->password != $request->password2 ){

            $uppercase = preg_match('@[A-Z]@', $request->password2);
            $number    = preg_match('@[0-9]@', $request->password2);

            if( !$uppercase || !$number ) {
                $response = [
                    'status' => 0,
                    'error' => [ 'message' => 'Password must like this format. example : Xyz$@123' ]
                ];
                return response()->json($response);
            }

            $credentials = [
                'username' => $request->username,
                'password' => $request->password,
            ];

            if (auth()->attempt($credentials)) {
                $userData = auth()->user();
                $user = User::find($userData->id);
                if( $user != null ){
                    $user->password = bcrypt( $request->password2 );
                    $user->is_password_updated = 1;
                    if( $user->save() ){
                        $response = [
                            'status' => 1,
                            'success' => [
                                'message' => 'Change Password successfully!'
                            ]
                        ];
                    }
                }
                $log = 'Password changed';
                LogActivity::addToLog($log);
                return response()->json($response, 200);

            } else {
                $response = [
                    'status' => 0,
                    'error' => [ 'message' => 'Something wrong! Please try again !' ]
                ];
                return response()->json($response);
            }

        }else{
            $message = 'Both password did not match !!';
            if( strlen($request->password2) < 8){
                $message = 'password length must be minimum 8 char !!';
            }
            if( $request->password == $request->password2 ){
                $message = 'old password and new password must be different !!';
            }
            $response = [
                'status' => 0,
                'error' => [ 'message' => $message ]
            ];

            return response()->json($response);

        }

    }

    /**
     * Handles Logout Request
     *
     * @return JsonResponse
     */
    public function logout()
    {
        $id = auth()->id();
        DB::table('tbl_user')->where('id',$id)->update(['is_login' => 0]);
        $log = 'Logout successfully';
                LogActivity::addToLog($log);
        return response()->json(['status' => 1, 'success' => ["message" => "Successfully logged out!"]]);
    }

    /**
     * Returns Authenticated User Details
     *
     * @return JsonResponse
     */
    public function details()
    {
        return response()->json(['user' => auth()->user()], 200);
    }
}
