<?php

namespace App\Http\Controllers\Transaction;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ProfitLossController extends Controller
{

    /**
     * Profit Loss List
     */
    public function list($uid = null)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( $uid == null ){
                $user = Auth::user();
            }else{
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }

            if( $user != null ){
                $userName = $user->username;
                $sportList = DB::table('tbl_sport')
                    ->select(['name','sportId'])->where([['status',1]])
                    ->orderBy('position', 'ASC')->get();
                if( $sportList != null ){
                    $data = []; $totalPl = 0;
                    foreach ( $sportList as $sport ){

                        if( $sport->sportId != 99 ){
                            $profitLoss = $this->getProfitLossBySport($user, $sport->sportId);
                            $data[] = [
                                'name' => $sport->name,
                                'sportId' => $sport->sportId,
                                'profitLoss' => $profitLoss,
                            ];
                            $totalPl = $totalPl+$profitLoss;
                        }

                    }

                    $response = [ 'status' => 1, 'data' => $data, 'userName' => $userName,'total' => [ 'pl' => $totalPl ] ];
                }else{
                    $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                }
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * listEventDetail
     */
    public function listEventDetail($uid = null)
    {

        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

            if( !isset( $requestData[ 'sid' ] ) ) {
                return $response;
            }

            $sportId = $requestData[ 'sid' ]; $searchDate = false; $start = $end = null;

            if( $uid == null ){
                $user = Auth::user(); $uid = $user->id;
                if( $user->role == 6 ){ $uid = 1; }
            }else{
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }

            if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
                $start = Carbon::parse($requestData[ 'start' ]);
                $end = Carbon::parse($requestData[ 'end' ]);

                $searchDate = true;
            }

            if( isset( $requestData[ 'ftype' ] ) ) {
                $now = Carbon::now();
                if( $requestData[ 'ftype' ] == 'week' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(7)->format('Y-m-d');
                }elseif ( $requestData[ 'ftype' ] == 'month' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(30)->format('Y-m-d');
                }else{
                    $end = $start = $now->format('Y-m-d');
                }

                $start = Carbon::parse($start); $end = Carbon::parse($end);
                $searchDate = true;
            }

            if( $user != null ){

                $userName = $user->username;

                if( $user->role == 1 || $user->role == 6 ){
                    $tbl = 'tbl_transaction_admin';
                }elseif ( $user->role == 4 ){
                    $tbl = 'tbl_transaction_client';
                }else{
                    $tbl = 'tbl_transaction_parent';
                }

                $query = DB::table($tbl)
                    ->select(['eid','description','balance','updated_on','amount','type','eType','mType','result'])
                    ->where([['userId',$uid],['sid',$sportId],['status',1],['systemId',$user->systemId]])
                    ->whereIn('eType',[0,3]);

                if( $searchDate == true && $start != null && $end != null ){
                    $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                        ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                }

                $listArr = $query->orderBy('updated_on', 'DESC')->limit(100)->get();

                if( $listArr != null ){

                    $eidArr = $transArr = [];
                    foreach ( $listArr as $list ){
                        $amount = $amount1 = 0;
                        if( $list->eType == 0 ){
                            $amount = $list->amount;
                            if( $list->type != 'CREDIT' ){
                                $amount = (-1)*$list->amount;
                            }
                        }else{
                            $amount1 = $list->amount;
                            if( $list->type != 'CREDIT' ){
                                $amount1 = (-1)*$list->amount;
                            }
                        }

                        if( !in_array( $list->eid , $eidArr ) ){
                            $eidArr[] = $list->eid;
                            $description = 'Not Found!';
                            if( $list->description != null ){
                                $descData = explode( '>',$list->description );
                                if( $descData != null ){
                                    $description = implode(' > ',[trim($descData[0]),trim($descData[1])]);
                                }
                            }

                            $transArr[$list->eid] = [
                                'description' => $description,
                                'time' => $list->updated_on,
                                'pl' => $amount,
                                'comm' => $amount1,
                                'eid' => $list->eid,
                            ];
                            if( $list->mType == 'match_odd' ){
                                $transArr[$list->eid]['result'] = $list->result;
                            }
                        }else{
                            if( $list->mType == 'match_odd' ){
                                $transArr[$list->eid]['result'] = $list->result;
                            }
                            $transArr[$list->eid]['comm'] = round( ( $transArr[$list->eid]['comm']+$amount1 ) , 2 );
                            $transArr[$list->eid]['pl'] = round( ( $transArr[$list->eid]['pl']+$amount ) , 2 );
                            $transArr[$list->eid]['time'] = $list->updated_on;
                        }

                    }
                    $dataArr = []; $totalPl = $totalComm = 0;
                    if( $transArr != null ){
                        foreach ( $transArr as $eid => $data ){
                            $dataArr[] = $data;
                            $totalPl = $totalPl+$data['pl'];
                            $totalComm = $totalComm+$data['comm'];
                        }
                    }

                    $response = [ 'status' => 1, 'data' => $dataArr, 'userName' => $userName ,'total' => [ 'pl' => round($totalPl,2) , 'comm' => round($totalComm,2) ] ];

                }else{
                    $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                }

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * listEventDetail
     */
    public function listMarketDetail($uid = null)
    {

        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

            if( !isset( $requestData[ 'eid' ] ) ) {
                return $response;
            }

            $eventId = $requestData[ 'eid' ]; $searchDate = false; $start = $end = null;

            if( $uid == null ){
                $user = Auth::user(); $uid = $user->id;
                if( $user->role == 6 ){ $uid = 1; }
            }else{
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }

            $eType = 0; $searchDate = false; $start = $end = null;
            if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
                $start = Carbon::parse($requestData[ 'start' ]);
                $end = Carbon::parse($requestData[ 'end' ]);

                $searchDate = true;
            }

            if( isset( $requestData[ 'ftype' ] ) ) {
                $now = Carbon::now();
                if( $requestData[ 'ftype' ] == 'week' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(7)->format('Y-m-d');
                }elseif ( $requestData[ 'ftype' ] == 'month' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(30)->format('Y-m-d');
                }else{
                    $end = $start = $now->format('Y-m-d');
                }

                $start = Carbon::parse($start); $end = Carbon::parse($end);
                $searchDate = true;
            }

            if( $user != null ){
                $userName = $user->username;
                if( $user->role == 1 || $user->role == 6 ){
                    $tbl = 'tbl_transaction_admin';
                }elseif ( $user->role == 4 ){
                    $tbl = 'tbl_transaction_client';
                }else{
                    $tbl = 'tbl_transaction_parent';
                }

                $query = DB::table($tbl)
                    ->select(['mid','description','balance','remark','updated_on','amount','type','eType','mType','result'])
                    ->where([['userId',$uid],['status',1],['eid',$eventId],['systemId',$user->systemId]])
                    ->whereIn('eType',[0,3]);

                if( $searchDate == true && $start != null && $end != null ){
                    $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                        ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                }

                $listArr = $query->orderBy('updated_on', 'DESC')->limit(100)->get();

                if( $listArr != null ){

                    if( $user->role == 4 ){

                        $transArr = []; $totalPl = $totalComm = 0;
                        foreach ( $listArr as $list ) {
                            $amount = $amount1 = 0;
                            if( $list->eType == 0 ){
                                $amount = $list->amount;
                                if( $list->type != 'CREDIT' ){
                                    $amount = (-1)*$list->amount;
                                }
                                $totalPl = $totalPl+$amount;
                            }else{
                                $amount1 = $list->amount;
                                if( $list->type != 'CREDIT' ){
                                    $amount1 = (-1)*$list->amount;
                                }
                                $totalComm = $totalComm+$amount1;
                            }

                            $description = 'Not Found!';
                            if( $list->description != null ){
                                $descData = explode( '>',$list->description );
                                if( $descData != null ){

                                    if( $list->mType == 'match_odd' || $list->mType == 'completed_match' || $list->mType == 'tied_match'){
                                        $description = implode(' > ',[trim($descData[0]),trim($descData[1]),trim($descData[3])]);
                                    }else{
                                        $description = implode(' > ',[trim($descData[0]),trim($descData[1]),trim($descData[2]),trim($descData[3])]);
                                    }

                                }
                            }

                            $transArr[] = [
                                'description' => $description,
                                'time' => $list->updated_on,
                                'result' => $list->result,
                                'pl' => $amount,
                                'comm' => $amount1,
                                'mid' => $list->mid,
                            ];
                        }

                        $response = [ 'status' => 1, 'data' => $transArr , 'userName' => $userName ,'total' => [ 'pl' => $totalPl , 'comm' => $totalComm ]];

                    }else{
                        $midArr = $transArr = [];
                        foreach ( $listArr as $list ){

                            $amount = $amount1 = 0;
                            if( $list->eType == 0 ){
                                $amount = $list->amount;
                                if( $list->type != 'CREDIT' ){
                                    $amount = (-1)*$list->amount;
                                }
                            }else{
                                $amount1 = $list->amount;
                                if( $list->type != 'CREDIT' ){
                                    $amount1 = (-1)*$list->amount;
                                }
                            }

                            if( !in_array( $list->mid , $midArr ) ){

                                $description = $list->description;
//                                if( $list->description != null ){
//                                    $descData = explode( '>',$list->description );
//                                    if( $descData != null ){
//                                        $description = implode(' > ',[trim($descData[0]),trim($descData[1]),trim($descData[2]),trim($descData[3])]);
//                                    }
//                                }

                                $midArr[] = $list->mid;
                                $transArr[$list->mid] = [
                                    'description' => $description,
                                    'time' => $list->updated_on,
                                    'result' => $list->result,
                                    'pl' => $amount,
                                    'comm' => $amount1,
                                    'mid' => $list->mid,
                                ];

                            }else{
                                $transArr[$list->mid]['comm'] = round( ( $transArr[$list->mid]['comm']+$amount1 ) , 2 );
                                $transArr[$list->mid]['pl'] = round( ( $transArr[$list->mid]['pl']+$amount ) , 2 );
                                $transArr[$list->mid]['balance'] = $list->balance;
                                $transArr[$list->mid]['time'] = $list->updated_on;
                            }

                        }
                        $dataArr = []; $totalPl = $totalComm = 0;
                        if( $transArr != null ){
                            foreach ( $transArr as $mid => $data ){
                                $dataArr[] = $data;
                                $totalPl = $totalPl+$data['pl'];
                                $totalComm = $totalComm+$data['comm'];
                            }
                        }

                        $response = [ 'status' => 1, 'data' => $dataArr, 'userName' => $userName, 'total' => [ 'pl' => round($totalPl,2) , 'comm' => round($totalComm,2) ] ];
                    }

                }else{
                    $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                }

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // getProfitLossBySport
    public function getProfitLossBySport($user, $sportId)
    {
        $total = 0;
        $uid = $user->id; if( $user->role == 6 ){ $uid = 1; }

        if( $user->role == 1 || $user->role == 6){
            $tbl = 'tbl_transaction_admin';
        }elseif ( $user->role == 4 ){
            $tbl = 'tbl_transaction_client';
        }else{
            $tbl = 'tbl_transaction_parent';
        }

        $query = DB::table($tbl)
            ->select(['amount','type'])
            ->where([['userId',$uid],['status',1],['eType',0],['sid',$sportId],['systemId',$user->systemId]]);

        $listArr = $query->orderBy('updated_on', 'DESC')->get();

        foreach ( $listArr as $list ){

            $amount = $list->amount;
            if ($list->type != 'CREDIT') {
                $amount = (-1) * $list->amount;
            }

            $total = $total+$amount;

        }

        if( $total != 0 ){ return round( $total , 2 ); }else{ return $total;}

    }

    /**
     * Teen patti Profit Loss List
     */
    public function listTeenPatti($uid = null)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            if( $uid == null ){
                $user = Auth::user();
            }else{
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }

            if( $user != null ){
                $userName = $user->username;
                $liveGameList = DB::table('tbl_live_game')
                    ->select(['name','sportId','eventId'])->where([['status',1],['sportId',99]])
                    ->orderBy('id', 'ASC')->get();
                if( $liveGameList != null ){
                    $data = []; $totalPl = 0;
                    foreach ( $liveGameList as $list ){

                        $profitLoss = $this->getProfitLossByEvent($user, $list->eventId);

                        $data[] = [
                            'name' => $list->name,
                            'sportId' => $list->sportId,
                            'eventId' => $list->eventId,
                            'profitLoss' => $profitLoss,
                        ];
                        $totalPl = $totalPl+$profitLoss;
                    }
                    $response = [ 'status' => 1, 'data' => $data, 'userName' => $userName ,'total' => [ 'pl' => $totalPl ] ];
                }
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }

    // getProfitLossByEvent
    public function getProfitLossByEvent($user, $eventId)
    {
        $total = 0;
        $uid = $user->id; if( $user->role == 6 ){ $uid = 1; }

        if( $user->role == 1 || $user->role == 6 ){
            $tbl = 'tbl_transaction_admin';
        }elseif ( $user->role == 4 ){
            $tbl = 'tbl_transaction_client';
        }else{
            $tbl = 'tbl_transaction_parent';
        }

        $query = DB::table($tbl)
            ->select(['amount','type'])
            ->where([['userId',$uid],['status',1],['eType',0],['eid',$eventId],['systemId',$user->systemId]]);

        $listArr = $query->orderBy('updated_on', 'DESC')->get();

        foreach ( $listArr as $list ){

            $amount = $list->amount;
            if ($list->type != 'CREDIT') {
                $amount = (-1) * $list->amount;
            }

            $total = $total+$amount;

        }

        if( $total != 0 ){ return round( $total , 2 ); }else{ return $total;}

    }

}
