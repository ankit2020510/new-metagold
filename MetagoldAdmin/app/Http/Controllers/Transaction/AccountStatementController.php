<?php

namespace App\Http\Controllers\Transaction;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use http\Env\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class AccountStatementController extends Controller
{

    /**
     * action list
     */
    public function list($uid = null)
    {

        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{
            
            if( $uid == null ){

                $user = Auth::user(); $uid = $user->id;
                if( $user->role == 6 ){ $uid = 1; }
            }else{

                $user = DB::table('tbl_user')
                    ->select(['username','name','role','systemId'])->where([['id',$uid],['status',1]])->first();
            }

            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            $eType = 0; $searchDate = false; $start = $end = null;
            if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'type' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
                if( $requestData[ 'type' ] == 'settlement' ){
                    $eType = 0;
                }
                $start = Carbon::parse($requestData[ 'start' ]);
                $end = Carbon::parse($requestData[ 'end' ]);

                $searchDate = true;
            }

            if( isset( $requestData[ 'ftype' ] ) ) {
                if( $requestData[ 'type' ] == 'settlement' ){
                    $eType = 2;
                }
                $now = Carbon::now();
                if( $requestData[ 'ftype' ] == 'week' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(7)->format('Y-m-d');
                }elseif ( $requestData[ 'ftype' ] == 'month' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(30)->format('Y-m-d');
                }else{
                    $end = $start = $now->format('Y-m-d');
                }

                $start = Carbon::parse($start); $end = Carbon::parse($end);
                $searchDate = true;
            }

          
            if( $user != null ){

                $userName = $user->username;
                
                if( $user->role == 1 || $user->role == 6 ){
                    $tbl = 'tbl_transaction_admin';
                    $where = [['userId',$uid],['status',1],['eType',$eType],['systemId',$user->systemId]];
                }elseif ( $user->role == 4 ){
                    $tbl = 'tbl_transaction_client';
                    $where = [['userId',$uid],['status',1],['systemId',$user->systemId]];
                    if( isset($requestData[ 'type' ]) && $requestData[ 'type' ] == 'settlement' ){
                        $where = [['userId',$uid],['status',1],['eType',$eType],['systemId',$user->systemId]];
                    }
                }else{
                    $tbl = 'tbl_transaction_parent';
                    $where = [['userId',$uid],['status',1],['eType',$eType],['systemId',$user->systemId]];
                }

                $query = DB::table($tbl)
                    ->select(['mid','description','balance','remark','updated_on','amount','type','childId','clientId'])
                    ->where($where);

                if( $searchDate == true && $start != null && $end != null ){
                    $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                        ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                }

                if( $searchDate == true){ $limit = 500; }else{ $limit = 500; }
                $listArr = $query->orderBy('id','desc')->get();
               //print_r($user->role); echo "<pre>"; print_r($listArr); die('ppppppp');
                if( $listArr != null ){

                    if( $eType == 2 ){

                        $transArr = [];
                        foreach ( $listArr as $list ) {

                           $user = DB::table('tbl_user')
                                   ->select('username')->where([['id',$list->clientId],['status',1]])->first();
                           $username = $user->username;
                            $amount = $list->amount;
                            if ($list->type != 'CREDIT') {
                                $amount = (-1) * $list->amount;
                            }
                            $transArr[] = [
                                'description' => $list->description,
                                'balance' => round($list->balance),
                                'remark' => $list->remark,
                                'clientId' =>$list->clientId,//childId,
                                'updated_on' => $list->updated_on,
                                'clientName'=>$username,
                                'amount' => round($amount)
                            ];
                        }

                        $response = [ 'status' => 1, 'data' => $transArr ];

                    }elseif( $eType == 0 && $user->role == 4 ){

                        $transArr = [];
                        foreach ( $listArr as $list ) {

                           $user = DB::table('tbl_user')
                                   ->select('username')->where([['id',$list->clientId],['status',1]])->first();
                           $username = $user->username;

                            $amount = $list->amount;
                            if ($list->type != 'CREDIT') {
                                $amount = (-1) * $list->amount;
                            }
                            $transArr[] = [
                                'description' => $list->description,
                                'balance' => round($list->balance),
                                'remark' => $list->remark,
                                'clientId' =>$list->clientId,//childId,
                                'updated_on' => $list->updated_on,
                                'clientName'=>$username,
                                'amount' => round($amount)
                            ];
                        }

                        $response = [ 'status' => 1, 'data' => $transArr ];

                    }elseif( $eType == 0 && $user->role != 4 ){
                        $midArr = $transArr = [];

                        foreach ( $listArr as $list ){

                            $user = DB::table('tbl_user')
                                   ->select('username')->where([['id',$list->clientId],['status',1]])->first();
                           $username = $user->username;

                            $amount = $list->amount;
                            if( $list->type != 'CREDIT' ){
                                $amount = (-1)*$list->amount;
                            }

                            $transArr[] = [
                                    'description' => $list->description,
                                    'balance' => round($list->balance),
                                    'remark' => $list->remark,
                                    'clientId' =>$list->childId,
                                    'updated_on' => $list->updated_on,
                                    'clientName'=>$username,
                                    'amount' => round($amount)
                                ];

                            /*if( !in_array( $list->mid , $midArr ) ){
                                $midArr[] = $list->mid;

                                $transArr[$list->mid] = [
                                    'description' => $list->description,
                                    'balance' => $list->balance,
                                    'remark' => $list->remark,
                                    'childId' =>$list->childId,
                                    'updated_on' => $list->updated_on,
                                    'amount' => $amount,
                                ];

                            }else{

                                $transArr[$list->mid]['amount'] = round( ( $transArr[$list->mid]['amount']+$amount ) , 2 );
                                

                            }*/

                        }
                        //print_r($transArr); die('dsddsd');
                        $dataArr = [];
                        if( $transArr != null ){
                            foreach ( $transArr as $mid => $data ){
                                $dataArr[] = $data;
                            }
                        }

                        $response = [ 'status' => 1, 'data' => $dataArr, 'userName' => $userName ];
                    }else{
                        $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                    }

                }else{
                    $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                }

            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


}
