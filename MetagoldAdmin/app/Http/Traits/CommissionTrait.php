<?php
namespace App\Http\Traits;
use DB;

trait CommissionTrait {
    public function updateTransaction($list) {

       
       $amount = $list->profit;
       
       if( $amount > 0 ){ $type = 'CREDIT'; }else{ $amount = (-1)*$amount; $type = 'DEBIT'; }

       $summaryData = [];

       $uid = $list->uid;
       $id= $list->id;
       $cUser = DB::table('tbl_user')->select(['parentId','systemId'])->where([['id',$uid]])->first();
       $cUserInfo = DB::table('tbl_user_info')->select(['balance','pl_balance'])->where([['uid',$uid]])->first();

        $parentId = $cUser->parentId;
        $systemId = $cUser->systemId;
        $balance = $this->getCurrentBalanceClientNew($uid,$amount,$type,$cUserInfo->balance,$cUserInfo->pl_balance,'CLIENT');
        
         if($list->order_type == 'instant_execution'){
                         if($list->trans_type == 'BUY'){
                           $prc = $list->buy_price;
                           $description = $list->name.' > '.$list->lot.' > '.$list->trans_type.' AT '.$prc.' CLOSED AT '.$list->closeBuyPrice;
                         }else{
                           $prc = $list->sell_price;
                           $description = $list->name.' > '.$list->lot.' > '.$list->trans_type.' AT '.$prc.' CLOSED AT '.$list->closeSellPrice;
                         }
                       }elseif($list->order_type == 'buy_limit' || $list->order_type == 'sell_limit'){
                           if($list->trans_type == 'BUY'){
                             $prc = $list->price;
                             $description = $list->name.' > '.$list->lot.' > '.$list->trans_type.' AT '.$prc.' CLOSED AT '.$list->closeBuyPrice;
                         }else{
                              $prc = $list->price;
                             $description = $list->name.' > '.$list->lot.' > '.$list->trans_type.' AT '.$prc.' CLOSED AT '.$list->closeSellPrice;
                         }
                          
                       }


        $resultArr = [
            'systemId' => $systemId,
            'clientId' => $uid,
            'userId' => $uid,
            'childId' => 0,
            'parentId' => $parentId,
            'sid' => 0,
            'eid' => $id,
            'mid' => 0,
            'eType' => 0,
            'mType' => 0,
            'trans_type'=>$list->trans_type,
            'order_type'=>$list->order_type,
            'closeBuyPrice'=>$list->closeBuyPrice,
            'closeSellPrice'=>$list->closeSellPrice,
            'stopLoss'=>$list->stopLoss,
            'takeProfit'=>$list->takeProfit,
            'price'=>$list->price,
            'lot'=>$list->lot,
            'tradingSymbol'=>$list->tradingSymbol,
            'name'=>$list->name,
            'type' => $type,
            'amount' => round($amount,0),
            'p_amount' => 0,
            'c_amount' => 0,
            'balance' => $balance,
            'description' => $description,
            'remark' => 'Transactional Entry',
            'status' => 1,
            'created_on'=>date('Y-m-d h:i:s'),
            'updated_on'=>date('Y-m-d h:i:s'),
        ];

        DB::table('tbl_transaction_client')->insert($resultArr);
      
         $type = $type;
            $ownPl = $amount;
            $parentPl = 0;
            $parentComm = 0;
            $ownComm = 0;
       
     
         $this->userSummaryUpdateNew($uid,$ownPl,$parentPl,$type,$ownComm,$parentComm);

        if( $type == 'CREDIT' ){ $pType = 'DEBIT'; }else{ $pType = 'CREDIT'; }

        $parentUserData = DB::table('tbl_user_profit_loss as pl')
            ->select(['pl.userId as userId','pl.actual_profit_loss as apl','pl.profit_loss as gpl','ui.balance as balance','ui.pl_balance as pl_balance'])
            ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
            ->where([['pl.clientId',$uid],['pl.actual_profit_loss','!=',0]])->orderBy('pl.userId','desc')->get();

        $childId = $uid;
      
        $summaryData = [];

        foreach ( $parentUserData as $user ){

            $cUser = DB::table('tbl_user')->select(['parentId','role'])->where([['id',$user->userId]])->first();


           
            $parentId = $cUser->parentId;
            
            $transactionAmount = ( $amount*$user->apl )/100;
            $pTransactionAmount = ( $amount*(100-$user->gpl) )/100;
            $cTransactionAmount = ( $amount*($user->gpl-$user->apl) )/100;
            $balance = $this->getCurrentBalance($user->userId,$transactionAmount,$pType,$user->balance,$user->pl_balance,'PARENT');
              


            $resultArr = [
                'systemId' => $systemId,
                'clientId' => $uid,
                'userId' => $user->userId,
                'childId' => $childId,
                'parentId' => $parentId,
                'sid' => 0,
                'eid' => $id,
                'mid' => 0,
                'eType' => 0,
                'mType' => 0,
                'trans_type'=>$list->trans_type,
                'order_type'=>$list->order_type,
                'lot'=>$list->lot,
                'buyPrice' => $list->buy_price,
                'sellPrice'=>$list->sell_price,
                'closeBuyPrice'=>$list->closeBuyPrice,
                'closeSellPrice'=>$list->closeSellPrice,
                'stopLoss'=>$list->stopLoss,
                'takeProfit'=>$list->takeProfit,
                'price'=>$list->price,
                'tradingSymbol'=>$list->tradingSymbol,
                'name'=>$list->name,
                'type' => $pType,
                'amount' => round($transactionAmount,0),
                'p_amount' => round($pTransactionAmount,0),
                'c_amount' => round($cTransactionAmount,0),
                'balance' => $balance,
                'description' => $description,
                'remark' => 'Transactional Entry',
                'status' => 1,
                'created_on'=>date('Y-m-d h:i:s'),
               'updated_on'=>date('Y-m-d h:i:s'),
            ];

            if( $cUser->role == 1 ){
                DB::table('tbl_transaction_admin')->insert($resultArr);
            }else{
                DB::table('tbl_transaction_parent')->insert($resultArr);
            }

          

             $uid = $user->userId;
            $type = $pType;
            $ownPl = $transactionAmount;
            $parentPl = $pTransactionAmount;
            $parentComm = 0;
            $ownComm = 0;

            //$this->userSummaryUpdate($summaryData,null);
            $this->userSummaryUpdateNew($uid,$ownPl,$parentPl,$type,$ownComm,$parentComm);
            $childId = $user->userId;

            $SummaryData1 = DB::table('tbl_settlement_summary')->select('ownPl','ownComm')
                ->where( 'uid', $user->userId)->first();
            if(!empty($SummaryData1)){
                $updated_on = date('Y-m-d H:i:s');
                $ubalance=0;
                $ubalance= $SummaryData1->ownPl+ $SummaryData1->ownComm;
                DB::table('tbl_user_info')->where([['uid',$user->userId]])
                    ->update(['pl_balance' => $ubalance,'updated_on'=>$updated_on]);
            }

        }

          $updateStatus = ['comm'=>1];
         DB::table('tbl_order')->where('id',$id)->update($updateStatus);
      
        die('commision');
        
    }


//user Summary Update New
    public static function userSummaryUpdateNew($uid,$ownPl,$parentPl,$type,$ownComm,$parentComm){
        
        $userSummary = DB::table('tbl_settlement_summary')
            ->where([['status',1],['uid',$uid]])->first();
        if( $userSummary != null ){

            $updatedOn = date('Y-m-d H:i:s');
            if( isset( $type ) && $type != 'CREDIT' ){
                $ownPl = (-1)*round($ownPl,2);
                $parentPl = (-1)*round($parentPl,2);
                $ownComm = (-1)*round($ownComm,2);
                $parentComm = (-1)*round($parentComm,2);
            }

            $updateArr = [
                'ownPl' => DB::raw('ownPl + ' . $ownPl),
                'ownComm' => DB::raw('ownComm + ' . $ownComm),
                'parentPl' => DB::raw('parentPl + ' . $parentPl),
                'parentComm' => DB::raw('parentComm + ' . $parentComm),
                'updated_on' => $updatedOn
            ];

            if( DB::table('tbl_settlement_summary')
                ->where([['status',1],['uid',$uid]])->update($updateArr) ){
                $updateArr1 = [ 'uid' => $uid,'updated_on' => $updatedOn,
                    'ownPl' => $ownPl, 'parentPl' => $parentPl,
                    'ownComm' => $ownComm, 'parentComm' => $parentComm];

                //$array = array('function' => 'settelement', 'Query' => $updateArr1);
                //$this->activityLog(json_encode($array));
            }else{
                $userSummary = DB::table('tbl_settlement_summary')
                    ->where([['status',1],['uid',$uid]])->first();
                $updatedOn = date('Y-m-d H:i:s');
                $betTimeDiff = (strtotime($updatedOn)-strtotime($userSummary->updated_on) );
                if($betTimeDiff == 0) {
                    usleep(1000);
                    $userSummary = DB::table('tbl_settlement_summary')
                        ->where([['status',1],['uid',$uid]])->first();
                }
                $updateArr = [
                    'ownPl' => $userSummary->ownPl+$ownPl,
                    'ownComm' => $userSummary->ownComm+$ownComm,
                    'parentPl' => $userSummary->parentPl+$parentPl,
                    'parentComm' => $userSummary->parentComm+$parentComm,
                    'updated_on' => date('Y-m-d H:i:s')
                ];
                if( DB::table('tbl_settlement_summary')
                    ->where([['status',1],['uid',$uid]])->update($updateArr) ){
                    $updateArr1 = [ 'uid' => $uid,'updated_on'=>$updatedOn,
                        'ownPl' => $ownPl, 'parentPl'=>$parentPl,
                        'ownComm' => $ownComm, 'parentComm' => $parentComm];

                    //$array = array('function' => 'settelement', 'Query' => $updateArr1);
                    //$this->activityLog(json_encode($array));
                }
            }

        }else{
            $updatedOn = date('Y-m-d H:i:s');
            if( isset( $type ) && $type != 'CREDIT' ){
                $ownPl = (-1)*round($ownPl,2);
                $parentPl = (-1)*round($parentPl,2);
                $ownComm = (-1)*round($ownComm,2);
                $parentComm = (-1)*round($parentComm,2);
            }

            $insertArr = [  'uid' => $uid, 'updated_on' => $updatedOn,
                'ownPl' => $ownPl, 'ownComm' => $ownComm,
                'parentPl' => $parentPl,'parentComm' => $parentComm ];

            DB::table('tbl_settlement_summary')->insert($insertArr);

        }
    }
     /*//user Summary Update
    public function userSummaryUpdate($summaryData, $role){

        //print_r($summaryData); die('kgjkjfkgjf');

        if( isset( $summaryData['uid'] ) ){

            $uid = $summaryData['uid'];
            $ownPl = isset($summaryData['ownPl']) ? $summaryData['ownPl'] : 0;
            if($role == 'CLIENT'){
               $parentPl = $summaryData['ownPl']; 
            }else{
               $parentPl = $summaryData['parentPl'];    
            }
            //$parentPl = isset($summaryData['ownPl']) ? $summaryData['parentPl'] : 0;

            $ownComm = isset($summaryData['ownComm']) ? $summaryData['ownComm'] : 0;
            $parentComm = isset($summaryData['parentComm']) ? $summaryData['parentComm'] : 0;

            $userSummary = DB::table('tbl_settlement_summary')
                ->where([['status',1],['uid',$uid]])->first();

            if( $userSummary != null ){

                if( isset( $summaryData['type'] ) && $summaryData['type'] == 'CREDIT' ){
                    $ownPl = $ownPl+$userSummary->ownPl;
                    $parentPl = $parentPl+$userSummary->parentPl;
                }else{
                    $ownPl = $userSummary->ownPl-$ownPl;
                    $parentPl = $userSummary->parentPl-$parentPl;
                }

                $ownComm = $userSummary->ownComm+$ownComm;
                $parentComm = $userSummary->parentComm+$parentComm;

                $updateArr = [ 'ownPl' => $ownPl, 'ownComm' => $ownComm,
                    'parentPl' => $parentPl, 'parentComm' => $parentComm ];

                DB::table('tbl_settlement_summary')->where([['status',1],['uid',$uid]])
                    ->update($updateArr);

            }else{

                $insertArr = [ 'uid' => $uid, 'ownPl' => $ownPl, 'ownComm' => $ownComm,
                    'parentPl' => $parentPl, 'parentComm' => $parentComm ];

                DB::table('tbl_settlement_summary')->insert($insertArr);

            }

        }

    }*/

// get Current Balance New
    public static function getCurrentBalanceClientNew($uid,$amount,$type,$balance,$plBalance,$role)
    {
        //$conn = DB::connection('mysql2');
        if( $type != 'CREDIT' ){ $amount = (-1)*$amount; }
        $updated_on = date('Y-m-d H:i:s');
        $updateArr = [
            'pl_balance' => DB::raw('pl_balance + ' . $amount),
            'updated_on' => $updated_on
        ];

        if( DB::table('tbl_user_info')->where('uid', $uid)->update($updateArr) ){
            $cUserInfo = DB::table('tbl_user_info')
                ->select('balance','pl_balance','updated_on')->where('uid',$uid)->first();
            $balance = $cUserInfo->balance;
            $plBalance = $cUserInfo->pl_balance;

            if( $role != 'CLIENT' ){
                return round($balance ,0);
            }else{
                return round( ( $balance + $plBalance ),0);
            }
        }else{
            $updated_on = date('Y-m-d H:i:s');
            $cUserInfo = DB::table('tbl_user_info')
                ->select(['balance','pl_balance','updated_on'])->where([['uid',$uid]])->first();
            $betTimeDiff = (strtotime($updated_on)-strtotime($cUserInfo->updated_on) );
            if($betTimeDiff == 0) {
                usleep(1000);
                $cUserInfo = DB::table('tbl_user_info')
                    ->select(['balance','pl_balance','updated_on'])->where([['uid',$uid]])->first();
            }

            $balance = $cUserInfo->balance;
            $newPlBalance = $cUserInfo->pl_balance;

            $updateArr = [
                'pl_balance' => round($newPlBalance,0),
                'expose'  => $margin,
                'updated_on' => $updated_on
            ];

            if( DB::table('tbl_user_info')
                ->where([['uid', $uid]])->update($updateArr) ){
                if( $role != 'CLIENT' ){
                    return round($balance ,0);
                }else{
                    return round( ( $balance + $newPlBalance ),0);
                }
            }
        }
    }


     // get Current Balance
   /* public function getCurrentBalance($uid,$amount,$type,$balance,$plBalance,$role)
    {
       
        if( $type == 'CREDIT' ){
            $newplBalance = $plBalance+$amount;
        }else{
            $newplBalance = $plBalance-$amount;
        }

        $arr = ['pl_balance'=>$newplBalance];
        if( DB::table('tbl_user_info')->where('uid',$uid)->update($arr) ){

            if( $role != 'CLIENT' ){
                return round( ( $newplBalance ),2);
            }else{
                return round( ( $balance + $newplBalance),2);
            }

        }else{

            if( $role != 'CLIENT' ){
                return round( ( $plBalance ),2);
            }else{
                return round( ( $balance +$newplBalance ),2);
            }
        }

    }*/
    // get Current Balance admin/Super master and master
    public static function getCurrentBalance($uid,$amount,$type,$balance,$plBalance,$role)
    {
        if( $type != 'CREDIT' ){ $amount = (-1)*$amount; }
        

          $updateArr = [
            'pl_balance' => DB::raw('pl_balance + ' . round($amount))
        ];

        DB::table('tbl_user_info')->where('uid', $uid)->update($updateArr);
        $cUserInfo = DB::table('tbl_user_info')->select(['balance','pl_balance'])->where([['uid',$uid]])->first();

        $balance = $cUserInfo->balance;
        $plBalance = $cUserInfo->pl_balance;
        if( $role != 'CLIENT' ){
            return round($plBalance,0);
        }else{
            if($plBalance > 0){
                return round( ( $balance + $plBalance ),0);
            }else{
                return round( ( $balance - $plBalance ),0);
            }

        }
    }

}