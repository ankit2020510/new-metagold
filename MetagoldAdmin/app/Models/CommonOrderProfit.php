<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Redis;

class CommonOrderProfit extends Model
{
   public $timestamps=false;
    protected $table = 'tbl_order';

    public static function getProfit($val,$currentBuyPrice,$currentSellPrice)
    {              
            
                if($val->order_type == 'instant_execution'){
                      if($val->trans_type == 'BUY'){
                           $profit = $currentSellPrice-$val->buy_price;
                           $netProfit = $profit*$val->lot;
                      }else{
                           $profit = $val->sell_price-$currentBuyPrice;
                           $netProfit = $profit*$val->lot;
                       }//close checking of trans_type
                  }elseif($val->order_type == 'buy_limit'){
                      if($val->trans_type == 'BUY'){
                           $profit = $currentSellPrice-$val->buy_price;
                           $netProfit = $profit*$val->lot;
                      }//close checking of trans_type
                }else{
                     if($val->trans_type == 'SELL'){
                           $profit = $val->sell_price-$currentBuyPrice;
                           $netProfit = $profit*$val->lot;
                      }//close checking of trans_type
                }


   return $netProfit;
    } 

}