<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Setting extends Model
{
    public $timestamps = false;

    protected $table = 'tbl_common_setting';

    public static function getList()
    {
        $query = DB::table('tbl_common_setting')
            ->select(['id','key_name','value','status'])
            ->whereIn('status',[0,1])
            ->orderBy('id', 'DESC');

        $list = $query->get();

        return $list;
    }

    public static function doCreate($data)
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong!']];

        $cUser = Auth::user();

        if( $cUser != null && $cUser->role == 1){

            $keyName = strtoupper(str_replace(' ','_',trim($data['key'])));

            $checkUnique = Setting::where([['key_name',$keyName]])->first();

            if( $checkUnique != null ){
                $response = ['status' => 0, 'error' => ['message' => 'This key is already used!']];
                return $response;
            }

            $setting = new Setting();

            $setting->key_name = $keyName;
            $setting->value = trim($data['value']);

            if ($setting->save()) {

                if( $setting->key_name == 'GLOBAL_COMMENTARY' ){
                    DB::table('tbl_global_commentary')->insert(['commentary' => trim($data['value'])]);
                }

                $response = [
                    'status' => 1,
                    'success' => [
                        'message' => 'Created successfully!'
                    ]
                ];
            }

        }

        return $response;
    }

    public static function doUpdate($data)
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong!']];

        $cUser = Auth::user();

        if( $cUser != null && $cUser->role == 1){

            $setting = Setting::where([['id',$data['id']]])->first();

            if( $setting != null ){
                if( isset($data['status']) ){
                    $setting->status = trim($data['status']);
                    if( $data['status'] == 1 ){
                        $message = 'Active successfully!';
                    }else if( $data['status'] == 0 ){
                        $message = 'InActive successfully!';
                    }else{
                        $message = 'Delete successfully!';
                    }
                }else{
                    $setting->value = trim($data['value']);
                    $message = 'Updated successfully!';
                }

                if ($setting->save()) {

                 /*   if( !isset($data['status']) && $setting->key_name == 'GLOBAL_COMMENTARY' ){
                        $updat = ['commentary' => trim($data['value']),'id'=>$data['id']];
                        DB::table('tbl_global_commentary')->insert($updat);
                    }*/

                    $response = [
                        'status' => 1,
                        'success' => [
                            'message' => $message
                        ]
                    ];
                }
            }

        }

        return $response;
    }

}
