<?php

namespace App\Models;

use App\Models\System\Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MainMenu extends Model
{
    public static function getList()
    {
        $user = Auth::user();

        $query = DB::table('tbl_main_menu')->select(['id','title','icon','url','class']);

        if( $user->roleName == 'ADMIN2' || $user->roleName == 'SM1'
            || $user->roleName == 'SM2' || $user->roleName == 'M1'){
            $query = $query->where([['status',1],['main_menu_id',0]])
                ->whereNotIn('id',['28']);
        }

        if( $user->roleName == 'SV' ){
            $query = $query->where([['id',1],['status',1],['main_menu_id',0]]);
        }

        if( $user->roleName == 'ADMIN' || $user->roleName == 'SA' ){
            $query = $query->where('status',1)->whereIn('main_menu_id',[0,12]);
        }
 
        $mainMenu = $query->orderBy('position', 'ASC')->get();
        $menuArr = [];
        if( $mainMenu != null ){
            $menuArr = $childArr = [];
            foreach ( $mainMenu as $data ){
                $childArr = [];
                $query2 = DB::table('tbl_main_menu')->select(['id','title','icon','url','class'])
                    ->where([['status',1],['main_menu_id',$data->id]]);
                if ( $user->roleName == 'SA' ){
                    $query2 = $query2->whereNotIn('id',['32','33']);
                }else if ( $user->roleName == 'SM1' ){
                    $query2 = $query2->whereNotIn('id',['29','30','31','32','33','36','37']);

                }else if ( $user->roleName == 'SM2' ){
                    $query2 = $query2->whereNotIn('id',['21','29','30','31','32','33','36','37']);
                }else if ( $user->roleName == 'M1' ) {
                    $query2 = $query2->whereNotIn('id',['21','22','29','30','31','32','33','36','37']);
                }else if ( $user->roleName == 'ADMIN2' ) {
                    $query2 = $query2->whereNotIn('id',['29','30','31','32','33']);
                }else{
                    // do nothing
                }

                $childMenu = $query2->orderBy('position', 'ASC')->get();
            //    print_r($childMenu); die('lll');
                if( $childMenu != null ){
                    foreach ( $childMenu as $data1 ){
                        $childArr[] = [
                            'id' => $data1->id,
                            'title' => $data1->title,
                            'icon' => $data1->icon,
                            'url' => $data1->url,
                            'class' => $data1->class,
                        ];
                    }
                }

                $menuArr[] = [
                    'id' => $data->id,
                    'title' => $data->title,
                    'icon' => $data->icon,
                    'url' => $data->url,
                    'child' => !empty($childArr) ? $childArr : false,
                    'class' => $data->class
                ];


            }

        }

        return $menuArr;
    }


}



