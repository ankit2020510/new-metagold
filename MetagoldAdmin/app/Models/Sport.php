<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sport extends Model
{
    public $timestamps = false;

    protected $table = 'tbl_sport';

}
