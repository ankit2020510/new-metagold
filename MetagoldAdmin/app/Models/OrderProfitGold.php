<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Redis;

class OrderProfitGold extends Model
{
   public $timestamps=false;
    protected $table = 'tbl_order';

    public static function getProfit($val,$currentBuyPrice,$currentSellPrice)
    {
    	if($val->order_type = 'instant_execution'){
                              if($val->trans_type == 'BUY'){
                                if($val->lot == '100gm'){
                                   $profit = $currentBuyPrice-$val->buy_price;
                                   $netProfit = $profit*10;
                                }elseif($val->lot== '1kg'){
                                   $profit = $currentBuyPrice-$val->buy_price;
                                   $netProfit = $profit*100;
                                }else{
                                   $profit = $currentBuyPrice-$val->buy_price;
                                   $netProfit = $profit*300;
                                 }//close checking of lot
                              }else{
                                 if($val->lot == '100gm'){
                                   $profit = $val->sell_price-$currentSellPrice;
                                   $netProfit = $profit*10;
                                 }elseif($val->lot== '1kg'){
                                   $profit = $val->sell_price-$currentSellPrice;
                                   $netProfit = $profit*100;
                                 }else{
                                   $profit = $val->sell_price-$currentSellPrice;
                                   $netProfit = $profit*300;
                                 }//close checking of lot

                               }//close checking of trans_type
                        }elseif($val->order_type == 'buy_limit'){
                               if($val->trans_type == 'BUY'){
                                 if($val->lot == '100gm'){
                                   $profit = $currentBuyPrice-$val->price;
                                   $netProfit = $profit*10;
                                 }elseif($val->lot== '1kg'){
                                   $profit = $currentBuyPrice-$val->price;
                                   $netProfit = $profit*100;
                                 }else{
                                   $profit = $currentBuyPrice-$val->price;
                                   $netProfit = $profit*300;
                                 }//close checking of lot
                              }//close checking of trans_type

                       }else{
                           if($val->trans_type == 'SELL'){
                                   if($val->lot == '100gm'){
                                     $profit = $val->price-$currentSellPrice;
                                     $netProfit = $profit*10;
                                   }elseif($val->lot== '1kg'){
                                     $profit = $val->price-$currentSellPrice;
                                     $netProfit = $profit*100;
                                   }else{
                                     $profit = $val->price-$currentSellPrice;
                                     $netProfit = $profit*300;
                                   }//close checking of lot
                                }//close checking of trans_type
                       }//close of checking order_type
      return $netProfit;
    } 

}