<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class LogActivity extends Model
{
   public $timestamps=false;
   protected $table = 'tbl_log_activity';
   
   protected $fillable = [
        'description', 'url', 'method', 'ip', 'browser', 'userId','created_at','clientName'
    ]; 

    
}