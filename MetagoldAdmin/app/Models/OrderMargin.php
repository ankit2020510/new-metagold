<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;


class OrderMargin extends Model
{
   public $timestamps=false;
    protected $table = 'tbl_order';

    public static function getList($uid)
    {
        $query = DB::table('tbl_order')
            ->select('*')
            ->where([['status',1],['comm',0],['uid',$uid]]);
            

        $list = $query->get();

        return $list;
    }

}



