<?php

namespace App\Models;

use App\Models\System\Session;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Agent extends Model
{
    // list
    public static function getList()
    {
        $user = Auth::user();
        $list = [];
        if( $user != null ){
            $systemId = $user->systemId;
            $query = DB::table('tbl_user')->select(['id','name','username','status','parentId'])
                ->where([['role',8], ['systemId',$systemId]])
                ->whereIn('status',[0,1]);

            $userData = $query->orderBy('id', 'ASC')->get();
                   

            if( $userData->isNotEmpty() ){
                foreach ( $userData as $data ){
                     $isBlock = 1;
                     $status1 = DB::table('tbl_user_block_status')->select(['uid'])
                        ->where([['uid', $data->id], ['type', 1]])->first();
                 
                    if ($status1 != null) {
                        $isBlock = 0;
                    }
                    $list[] = [
                        'id' => $data->id,
                        'name' => $data->name,
                        'username' => $data->username,
                        'status' => $data->status,
                        'isBlock'=>$isBlock
                    ];
                }
            }

        }

        return $list;
    }

    // create
    public static function create($data)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        $pUser = Auth::user();
       
        if( $pUser->role != 1 ){ return $response; exit; }
        $parentId = $pUser->id; // current user id

        if( strlen($data['name']) < 3 || strlen($data['username']) < 3 || strlen($data['password']) < 8 ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The given data was invalid !' ] ];
            return $response;
        }

        $checkUser = DB::table('tbl_user')->where([['username',trim($data['username'])]])->first();

        if( $checkUser != null ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The username has already exists!' ] ];
            return $response;
        }

        $user = new User();
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->systemId = $pUser->systemId;
        $user->parentId = $parentId;
        $user->role = 8;
        $user->roleName = 'AG';
        $user->password = bcrypt( $data['password'] );

        if( $user->save() ){
            $response = [
                'status' => 1,
                'success' => [
                    'message' => 'Created successfully!'
                ]
            ];
        }

        return $response;
    }

}



