<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Redis;

class OrderProfitSilver extends Model
{
   public $timestamps=false;
    protected $table = 'tbl_order';

    public static function getProfit($val,$currentBuyPrice,$currentSellPrice)
    {
    	if($val->order_type == 'instant_execution'){
                      if($val->trans_type == 'BUY'){
                         if($val->lot == '1kg'){
                           $profit = $currentBuyPrice-$val->buy_price;
                           $netProfit = $profit;
                         }elseif($val->lot== '5kg'){
                           $profit = $currentBuyPrice-$val->buy_price;
                           $netProfit = $profit*5;
                         }else{
                           $profit = $currentBuyPrice-$val->buy_price;
                           $netProfit = $profit*30;
                         }//close checking of lot
                      }else{
                         if($val->lot == '1kg'){
                           $profit = $val->sell_price-$currentSellPrice;
                           $netProfit = $profit;
                         }elseif($val->lot== '5kg'){
                           $profit = $val->sell_price-$currentSellPrice;
                           $netProfit = $profit*5;
                         }else{
                           $profit = $val->sell_price-$currentSellPrice;
                           $netProfit = $profit*30;
                         }//close checking of lot

                       }//close checking of trans_type
              }elseif($val->order_type == 'buy_limit'){
                       if($val->trans_type == 'BUY'){
                         if($val->lot == '1kg'){
                           $profit = $currentBuyPrice-$val->price;
                           $netProfit = $profit;
                         }elseif($val->lot== '5kg'){
                           $profit = $currentBuyPrice-$val->price;
                           $netProfit = $profit*5;
                         }else{
                           $profit = $currentBuyPrice-$val->price;
                           $netProfit = $profit*30;
                         }//close checking of lot
                      }//close checking of trans_type

              }else{
                 if($val->trans_type == 'SELL'){
                         if($val->lot == '1kg'){
                           $profit = $val->price-$currentSellPrice;
                           $netProfit = $profit;
                         }elseif($val->lot== '5kg'){
                           $profit = $val->price-$currentSellPrice;
                           $netProfit = $profit*5;
                         }else{
                           $profit = $val->price-$currentSellPrice;
                           $netProfit = $profit*30;
                         }//close checking of lot
                      }//close checking of trans_type
              }//close of checking order_type
      return $netProfit;
    } 

}