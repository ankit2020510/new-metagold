<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Redis;

class OrderProfitUsd extends Model
{
   public $timestamps=false;
    protected $table = 'tbl_order';

    public static function getProfit($val,$currentBuyPrice,$currentSellPrice)
    {
    	if($val->order_type == 'instant_execution'){
                      if($val->trans_type == 'BUY'){
                         if($val->lot == '1k'){
                           $profit = $currentBuyPrice-$val->buy_price;
                           $netProfit = $profit*1000;
                         }elseif($val->lot== '10k'){
                           $profit = $currentBuyPrice-$val->buy_price;
                           $netProfit = $profit*10000;
                         }else{
                           $profit = $currentBuyPrice-$val->buy_price;
                           $netProfit = $profit*50000;
                         }//close checking of lot
                      }else{
                         if($val->lot == '1k'){
                           $profit = $val->sell_price-$currentSellPrice;
                           $netProfit = $profit*1000;
                         }elseif($val->lot== '10k'){
                           $profit = $val->sell_price-$currentSellPrice;
                           $netProfit = $profit*10000;
                         }else{
                           $profit = $val->sell_price-$currentSellPrice;
                           $netProfit = $profit*50000;
                         }//close checking of lot

                       }//close checking of trans_type
              }elseif($val->order_type == 'buy_limit'){
                       if($val->trans_type == 'BUY'){
                         if($val->lot == '1k'){
                           $profit = $currentBuyPrice-$val->price;
                           $netProfit = $profit*1000;
                         }elseif($val->lot== '10k'){
                           $profit = $currentBuyPrice-$val->price;
                           $netProfit = $profit*10000;
                         }else{
                           $profit = $currentBuyPrice-$val->price;
                           $netProfit = $profit*50000;
                         }//close checking of lot
                      }//close checking of trans_type

              }else{
                 if($val->trans_type == 'SELL'){
                         if($val->lot == '1k'){
                           $profit = $val->price-$currentSellPrice;
                           $netProfit = $profit*1000;
                         }elseif($val->lot== '10k'){
                           $profit = $val->price-$currentSellPrice;
                           $netProfit = $profit*10000;
                         }else{
                           $profit = $val->price-$currentSellPrice;
                           $netProfit = $profit*50000;
                         }//close checking of lot
                      }//close checking of trans_type
              }//close of checking order_type
     return $netProfit;
    } 

}