<?php

namespace App\Models;

use App\Models\System\Session;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SuperMaster extends Model
{
    // list
    public static function getList($rid = false)
    {
        if( $rid != false ){
            $user = DB::table('tbl_user')->where([['id',$rid],['status',1]])->first();
        }else{
            $user = Auth::user();
        }
        $list = [];
        if( $user != null ){

            $systemId = $user->systemId;

            $query = DB::table('tbl_user as u')
                ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                ->select(['u.id as id','u.name','u.username','u.is_login','pl','balance','pl_balance','pl_commission','commission','expose','pName','remark'])
                ->where([['u.status',1], ['u.role',2], ['u.systemId',$systemId]]);

            if( $rid != false ){
                $query->where([['u.parentId',$rid],['u.status',1], ['u.role',2], ['u.systemId',$systemId]]);
            }else{
                if( $user->role == 1 || $user->roleName == 'ADMIN' || $user->role == 6){
                    $query->where([['u.status',1], ['u.role',2], ['u.systemId',$systemId]]);
                }else{
                    $uid = $user->id; // current user id
                    $query->where([['u.parentId',$uid],['u.status',1], ['u.role',2], ['u.systemId',$systemId]]);
                }
            }

            $userData = $query->orderBy('u.id', 'ASC')->get();

            if( $userData->isNotEmpty() ){
                foreach ( $userData as $data ){
                    $isLock = $isBlock = 1;
                    $status1 = DB::table('tbl_user_block_status')->select(['uid'])
                        ->where([['uid',$data->id],['type',1]])->first();
                    if( $status1 != null ){
                        $isBlock = 0;
                    }

                    $status2 = DB::table('tbl_user_block_status')->select(['uid'])
                        ->where([['uid',$data->id],['type',2]])->first();
                    if( $status2 != null ){
                        $isLock = 0;
                    }

                    $smCount = CommonModel::getChildCount($data->id,2);
                    $mCount = CommonModel::getChildCount($data->id,3);
                    $cCount = CommonModel::getChildCount($data->id,4);

                    $list[] = [
                        'id' => $data->id,
                        'name' => $data->name,
                        'username' => $data->username,
                        'is_login' => $data->is_login,
                        'pl' => $data->pl,
                        'balance' => $data->balance,
                        'pl_balance' => $data->pl_balance,
                        'expose' => $data->expose,
                        'pName' => $data->pName,
                        'remark' => $data->remark,
                        'isLock' => $isLock,
                        'isBlock' => $isBlock,
                        'smCount' => $smCount,
                        'mCount' => $mCount,
                        'cCount' => $cCount,
                        'commission'=>$data->commission,
                        'commission_balance'=>$data->pl_commission,
                    ];
                }
            }

        }

        return $list;
    }

    // create
    public static function create($data)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        $pUser = Auth::user();
        $parentId = $pUser->id; // current user id

        if( $pUser->role == 2 ){ $roleName = 'SM2'; }else{ $roleName = 'SM1'; }

        if( strlen($data['name']) < 3 || strlen($data['username']) < 3 || strlen($data['password']) < 8 ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The given data was invalid !' ] ];
            return $response;
        }

        if( $data['pl'] < 1 ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'Entered pl is must be more them 0!' ] ];
            return $response;
        }
        if (preg_match('/[^a-zA-Z\d]/', $data['pl']) || preg_match("/[a-zA-Z]/i", $data['pl'])){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'PL should be in integer or number !!' ] ];
            return $response;
        }
        if (preg_match('/[^a-zA-Z\d]/', $data['balance']) || preg_match("/[a-zA-Z]/i", $data['balance'])){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'Balance should be in integer or number !!' ] ];
            return $response;
        }
        if( $data['balance'] < 0 ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'Entered chip balance is must be more them 0!' ] ];
            return $response;
        }

        $checkUser = DB::table('tbl_user')->where([['username',trim($data['username'])]])->first();

        if( $checkUser != null ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The username has already exists!' ] ];
            return $response;
        }else{
            $user = new User();
            $user->name = $data['name'];
            $user->username = $data['username'];
            $user->systemId = $pUser->systemId;
            $user->parentId = $parentId;
            $user->role = 2;
            $user->roleName = $roleName;
            $user->password = bcrypt( $data['password'] );

            if( $user->save() ){

                $userInfo = [
                    'systemId' => $user->systemId,
                    'uid' => $user->id,
                    'pName' => $pUser->username,
                    'pl' => $data['pl'],
                    'commission'=>$data['commission'],
                    'balance' => $data['balance'],
                    'pl_balance' => 0,
                    'expose' => 0,
                    'remark' => $data['remark']
                ];

                $settlementSummary = [
                    'uid' => $user->id,
                    'pid' => $user->parentId,
                    'systemId' => $user->systemId,
                    'role' => $user->roleName,
                    'name' => $user->name.' [ '.$user->username.' ]'
                ];

                $pUserInfo = UserInfo::where('uid',$parentId)->first();
                $pUserInfo->balance = ($pUserInfo->balance-$data['balance']);

                if( $pUserInfo->balance < 0 ){
                    $response = [ 'status' => 0, 'error' => [ 'message' => 'Entered chip balance is more them parent user\'s balance ! So this user can\'t create!' ] ];
                    DB::table('tbl_user')->delete($user->id);
                    return $response;
                }

                if( ($pUserInfo->pl-$data['pl']) < 1 && $pUserInfo->pl > 0 ){
                    $response = [ 'status' => 0, 'error' => [ 'message' => 'Entered pl is more them parent user\'s pl ! So this user can\'t create!' ] ];
                    DB::table('tbl_user')->delete($user->id);
                    return $response;
                }
                if( ($pUserInfo->commission-$data['commission']) < 1 && $pUserInfo->commission > 0 ){
                    $response = [ 'status' => 0, 'error' => [ 'message' => 'Entered commission is more then parent user\'s commission ! So this user can\'t create!' ] ];
                    DB::table('tbl_user')->delete($user->id);
                    return $response;
                }

                if( DB::table('tbl_user_info')->insert($userInfo)
                    && DB::table('tbl_settlement_summary')->insert($settlementSummary)){

                    if( $pUserInfo->save() ){

                        if( $pUser->role != 1 ){
                            CommonModel::updateUserChildData($user->id,$parentId,2);
                        }

                        CommonModel::updateChipTransaction($user->id,$parentId,$data['balance'],$data['remark'],'DEPOSIT');

                        $response = [
                            'status' => 1,
                            'success' => [
                                'message' => 'Created successfully!'
                            ]
                        ];
                    }else{
                        DB::table('tbl_user_info')->where('uid',$user->id)->delete();
                    }

                }else{
                    DB::table('tbl_user')->delete($user->id);
                }

            }

            return $response;
        }


    }

}



