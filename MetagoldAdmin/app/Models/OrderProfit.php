<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Auth;

class OrderProfit extends Model
{
   public $timestamps=false;
    protected $table = 'tbl_order';

    public static function getProfit($tradingSymbol,$instrumentToken,$name,$order_type,$trans_type,$lot,$buy_price,$sell_price,$price)
    {
    	$depth = self::getCurrentPrice($tradingSymbol,$instrumentToken);

          $currentBuyPrice = $depth['currentBuy'];
          $currentSellPrice = $depth['currentSell'];
  
                if($order_type == 'instant_execution'){
                      if($trans_type == 'BUY'){
                           $profit = $currentBuyPrice-$buy_price;
                           $netProfit = $profit*$lot;
                      }else{
                           $profit = $sell_price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                       }//close checking of trans_type
                  }elseif($order_type == 'buy_limit'){
                      if($trans_type == 'BUY'){
                           $profit = $currentBuyPrice-$price;
                           $netProfit = $profit*$lot;
                      }
                }else{
                     if($trans_type == 'SELL'){
                           $profit = $price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                      }//close checking of trans_type
                }

           $response = ['currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>$netProfit];

   return $response;

 } 

    public static function getCurrentPrice($tradingSymbol,$instrumentToken){
       
       $cache = Redis::connection();
       $key = 'Mcx_Market';
       $result=$cache->get($key);
       $arrData = json_decode($result);
       if(!empty($arrData)){
       foreach ($arrData as $key => $val) {
          
       	 if($key == $tradingSymbol && $val->instrument_token == $instrumentToken){
              $depth = $val->depth;
              $currentBuy = $depth->buy[0]->price;
              $currentSell = $depth->sell[0]->price;      
      	        	       
          }
       }
          $depth = ['currentBuy'=>$currentBuy,'currentSell'=>$currentSell];

       	  return $depth;
       	}else{

       		return 0;
       	}
       
    }

/*Admin purpose */

public static function get_net_profit($uid,$tradingSymbol,$instrumentToken,$name,$order_type,$trans_type,$lot,$buy_price,$sell_price,$price)
    {
      
      $depth = self::getCurrentPrice1($tradingSymbol,$instrumentToken);

          $currentBuyPrice = $depth['currentBuy'];
          $currentSellPrice = $depth['currentSell'];

          $netProfit = $net = 0;
          $response = 0;
          
     $parentUserData = DB::table('tbl_user_profit_loss as pl')
          ->select(['pl.userId as userId','pl.actual_profit_loss as apl'])
          ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
          ->where([['pl.clientId',$uid],['pl.actual_profit_loss','!=',0]])->orderBy('pl.userId','desc')->get();
    
    $id = Auth::user()->id;
    
    foreach ($parentUserData as $val) {
      
      if ($val->userId == $id) {     //here use Auth id and check
        
          if($order_type == 'instant_execution'){
                      if($trans_type == 'BUY'){
                           $profit = $currentBuyPrice-$buy_price;
                           $netProfit = $profit*$lot;
                           $net = ($netProfit * $val->apl)/100;
                      }else{
                           $profit = $sell_price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $net = ($netProfit * $val->apl)/100;
                       }//close checking of trans_type
                  }elseif($order_type == 'buy_limit'){
                      if($trans_type == 'BUY'){
                           $profit = $currentBuyPrice-$price;
                           $netProfit = $profit*$lot;
                           $net = ($netProfit * $val->apl)/100;
                      }
                }else{
                     if($trans_type == 'SELL'){
                           $profit = $price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $net = ($netProfit * $val->apl)/100;
                      }//close checking of trans_type
                }
    }}

    $response = ['currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>$net,'name'=>$name,'type'=>$trans_type];
    return $response;
               /*if($order_type == 'instant_execution'){
                      if($trans_type == 'BUY'){
                        if($name == 'GOLD'){
                          $profit = $currentBuyPrice-$buy_price;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'USDINR'){
                          $profit = $currentBuyPrice-$buy_price;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'GBPINR'){
                          $profit = $currentBuyPrice-$buy_price;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'COPPER'){
                          $profit = $currentBuyPrice-$buy_price;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'ZINC'){
                          $profit = $currentBuyPrice-$buy_price;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'CRUDEOIL'){
                          $profit = $currentBuyPrice-$buy_price;
                          $netProfit = $profit*$lot;
                          $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                          return $response;
                        }
                        if($name == 'ALUMINIUM'){
                          $profit = $currentBuyPrice-$buy_price;
                          $netProfit = $profit*$lot;
                          $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                          return $response;
                        }
                        if($name == 'EURINR'){
                          $profit = $currentBuyPrice-$buy_price;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'SILVER'){
                          $profit = $currentBuyPrice-$buy_price;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'BUY','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                           
                      }else{
                        if($name == 'GOLD'){
                           $profit = $sell_price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                         }
                        if($name == 'USDINR'){
                           $profit = $sell_price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                         }
                        if($name == 'GBPINR'){
                           $profit = $sell_price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                         }
                        if($name == 'COPPER'){
                           $profit = $sell_price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                         }
                        if($name == 'ZINC'){
                           $profit = $sell_price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                         }
                        if($name == 'CRUDEOIL'){
                           $profit = $sell_price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                         }
                        if($name == 'ALUMINIUM'){
                           $profit = $sell_price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                         }
                        if($name == 'SILVER'){
                           $profit = $sell_price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                         }
                         if($name == 'EURINR'){
                           $profit = $sell_price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                         }
                       }//close checking of trans_type
                  } 
                  elseif($order_type == 'buy_limit'){
                      if($trans_type == 'BUY'){
                        if($name == 'GOLD'){
                           $profit = $currentBuyPrice-$price;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                         }
                         if($name == 'USDINR'){
                          $profit = $currentBuyPrice-$price;
                          $netProfit = $profit*$lot;
                          $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'GBPINR'){
                          $profit = $currentBuyPrice-$price;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'COPPER'){
                          $profit = $currentBuyPrice-$price;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'ZINC'){
                          $profit = $currentBuyPrice-$price;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'CRUDEOIL'){
                         $profit = $currentBuyPrice-$price;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'ALUMINIUM'){
                          $profit = $currentBuyPrice-$price;
                           $netProfit = $profit*$lot;
                             $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                             return $response;
                        }
                        if($name == 'EURINR'){
                          $profit = $currentBuyPrice-$price;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'BUY','currentBuyPrice'=>$currentBuyPrice,'currentSellPrice'=>$currentSellPrice,'netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'SILVER'){
                          $profit = $currentBuyPrice-$price;
                          $netProfit = $profit*$lot;
                          $response = ['key'=>'BUY','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                          return $response;
                        }
                      }
                }
                else
                {
                     if($trans_type == 'SELL'){
                        if($name == 'GOLD'){
                           $profit = $price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'CRUDEOIL'){
                           $profit = $price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'COPPER'){
                           $profit = $price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'USDINR'){
                           $profit = $price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'ZINC'){
                           $profit = $price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'ALUMINIUM'){
                           $profit = $price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'GBPINR'){
                           $profit = $price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'EURINR'){
                           $profit = $price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        if($name == 'SILVER'){
                           $profit = $price-$currentSellPrice;
                           $netProfit = $profit*$lot;
                           $response = ['key'=>'SELL','netProfit'=>($netProfit * $val->apl)/100,'message'=>"$name"];
                           return $response;
                        }
                        //close checking of trans_type
                      
                }

           }*/
        // }}//foreach
 } 

    public static function getCurrentPrice1($tradingSymbol,$instrumentToken){
       
       $cache = Redis::connection();
       $key = 'Mcx_Market';
       $result=$cache->get($key);
       //$result = Cache::get($key);
       $arrData = json_decode($result);
     
     /*  print_r($arrData);
       die('shh');*/

       if(!empty($arrData)){
        $currentBuy = 0;
        $currentSell = 0;  
       foreach ($arrData as $key => $val) {
        //print_r($val);
         if($key == $tradingSymbol && $val->instrument_token == $instrumentToken){
              $depth = $val->depth;
               $currentBuy = $depth->buy[0]->price;
               $currentSell = $depth->sell[0]->price;              

          }
       }
          $depth = ['currentBuy'=>$currentBuy,'currentSell'=>$currentSell];

          return $depth;
        }else{

          return 0;
        }
       
    }

}