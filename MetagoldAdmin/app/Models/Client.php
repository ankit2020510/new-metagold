<?php

namespace App\Models;

use App\Models\System\Session;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Client extends Model
{
    public static function getList($rid = false)
    {
        if( $rid != false ){
            $user = DB::table('tbl_user')->where([['id',$rid],['status',1]])->first();
        }else{
            $user = Auth::user();
        }
        $list = [];
        if( $user != null ) {
            $systemId = $user->systemId;
            $query = DB::table('tbl_user as u')
                ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                ->select(['u.id as id', 'u.name', 'u.username', 'u.is_login', 'pl', 'balance', 'pl_balance', 'expose', 'mc', 'pName', 'remark'])
                ->where([['u.status', 1], ['u.role', 4], ['u.systemId', $systemId]]);

            if ($rid != false) {
                $query->where([['u.parentId', $rid], ['u.status', 1], ['u.role', 4], ['u.systemId', $systemId]]);
            } else {
                if( $user->role == 1 || $user->roleName == 'ADMIN' || $user->role == 6){
                    $query->where([['u.status', 1], ['u.role', 4], ['u.systemId', $systemId]]);
                } else {
                    $uid = $user->id; // current user id
                    $query->where([['u.parentId', $uid], ['u.status', 1], ['u.role', 4], ['u.systemId', $systemId]]);
                }
            }

            $userData = $query->orderBy('u.id', 'ASC')->get();
            $list = [];
            if ($userData->isNotEmpty()) {
                foreach ($userData as $data) {

                    $isLock = $isBlock = 1;
                    $status1 = DB::table('tbl_user_block_status')->select(['uid'])
                        ->where([['uid', $data->id], ['type', 1]])->first();
                    if ($status1 != null) {
                        $isBlock = 0;
                    }

                    $status2 = DB::table('tbl_user_block_status')->select(['uid'])
                        ->where([['uid', $data->id], ['type', 2]])->first();
                    if ($status2 != null) {
                        $isLock = 0;
                    }

                    $list[] = [
                        'id' => $data->id,
                        'name' => $data->name,
                        'username' => $data->username,
                        'is_login' => $data->is_login,
                        'pl_balance' => $data->pl_balance,
                        'balance' => $data->balance,
                        'mc' => $data->mc,
                        'available' => ($data->balance - $data->expose + $data->pl_balance),
                        'expose' => $data->expose,
                        'pName' => $data->pName,
                        'remark' => $data->remark,
                        'isLock' => $isLock,
                        'isBlock' => $isBlock,
                        'isActive' => self::userAct($data->id, $data->is_login)
                    ];
                }
            }
        }

        return $list;
    }

    public static function userAct($uid,$isLogin)
    {
        //logout 3,login 2,active 1
        $act = 3;

        if( $isLogin == 1 ){
            $act = 2; $t = time() - 60*60;

            $bet1 = DB::table('tbl_bet_pending_1')->select('created_on')
                ->where([ ['uid',$uid] ])->orderBy('id','desc')->first();
            if( $bet1 != null && strtotime($bet1->created_on) >= $t ){
                $act = 1;
            }else{
                $bet2 = DB::table('tbl_bet_pending_2')->select('created_on')
                    ->where([ ['uid',$uid] ])->orderBy('id','desc')->first();
                if( $bet2 != null && strtotime($bet2->created_on) >= $t ){
                    $act = 1;
                }else{
                    $bet3 = DB::table('tbl_bet_pending_3')->select('created_on')
                        ->where([ ['uid',$uid] ])->orderBy('id','desc')->first();
                    if( $bet3 != null && strtotime($bet3->created_on) >= $t ){
                        $act = 1;
                    }else{
                        $bet4 = DB::table('tbl_bet_pending_4')->select('created_on')
                            ->where([ ['uid',$uid] ])->orderBy('id','desc')->first();
                        if( $bet4 != null && strtotime($bet4->created_on) >= $t ){
                            $act = 1;
                        }
                    }
                }
            }
        }

        return $act;
    }

    public static function create($data)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        $pUser = Auth::user();

        if( $pUser->roleName == 'ADMIN' ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Admin can not create client !' ] ];
            return $response; exit;
        }

        if( strlen($data['name']) < 3 || strlen($data['username']) < 3 || strlen($data['password']) < 8 ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The given data was invalid !' ] ];
            return $response;
        }

        if (preg_match('/[^a-zA-Z\d]/', $data['balance']) || preg_match("/[a-zA-Z]/i", $data['balance'])){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'Balance should be in integer or number !!' ] ];
            return $response;
        } 

        if( $data['balance'] < 0 ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'Entered chip balance is must be more them 0!' ] ];
            return $response;
        }

        $checkUser = DB::table('tbl_user')->where([['username',trim($data['username'])]])->first();

        if( $checkUser != null ){
            $response = [ 'status' => 0, 'error' => [ 'message' => 'The username has already exists!' ] ];
            return $response;
        }

        $parentId = $pUser->id; // current user id

        $user = new User();

        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->systemId = $pUser->systemId;
        $user->parentId = $parentId;
        $user->role = 4;
        $user->roleName = 'C';
        $user->password = bcrypt( $data['password'] );

        if( $user->save() ){

            $setting = DB::table('tbl_common_setting')->select(['value'])
                ->where([['status',1],['key_name','DEFAULT_STACK_OPTION']])->first();
            if( $setting != null ){
                $betOption = trim($setting->value);
            }else{
                $betOption = '500,1000,5000,20000,50000';
            }

            $setting = DB::table('tbl_common_setting')->select(['value'])
                ->where([['status',1],['key_name','MATCH_ODD_COMM']])->first();
            if( $setting != null ){
                $mc = trim($setting->value);
            }else{
                $mc = 1;
            }

            $userInfo = [
                'systemId' => $user->systemId,
                'uid' => $user->id,
                'pName' => $pUser->username,
                'pl' => 0,
                'balance' => $data['balance'],
                'mc' => $mc,
                'pl_balance' => 0,
                'expose' => 0,
                'bet_option' => $betOption,
                'remark' => $data['remark']
            ];

            $userLimits = [
                'systemId' => $user->systemId,
                'uid' => $user->id
            ];

            $settlementSummary = [
                'uid' => $user->id,
                'pid' => $user->parentId,
                'systemId' => $user->systemId,
                'role' => $user->roleName,
                'name' => $user->name.' [ '.$user->username.' ]'
            ];

            $pUserInfo = UserInfo::where('uid',$parentId)->first();
            $pUserInfo->balance = ($pUserInfo->balance-$data['balance']);

            if( $pUserInfo->balance < 0 ){
                $response = [ 'status' => 0, 'error' => [ 'message' => 'Entered chip balance is more them parent user\'s balance ! So this user can\'t create!' ] ];
                DB::table('tbl_user')->delete($user->id);
                return $response;
            }

            if( DB::table('tbl_user_info')->insert($userInfo)
                && DB::table('tbl_user_limits')->insert($userLimits)
                && DB::table('tbl_settlement_summary')->insert($settlementSummary) ){

                if( $pUserInfo->save() ){

                    $pUser = Auth::user();
                    if( $pUser->role != 1 ){
                        CommonModel::updateUserChildData($user->id,$parentId,4);
                    }

                    self::updateProfitLossLevel($user);

                    CommonModel::updateChipTransaction($user->id,$parentId,$data['balance'],$data['remark'],'DEPOSIT');

                    $response = [
                        'status' => 1,
                        'success' => [
                            'message' => 'Created successfully!'
                        ]
                    ];
                }else{
                    DB::table('tbl_user_info')->where('uid',$user->id)->delete();
                    DB::table('tbl_user_limits')->where('uid',$user->id)->delete();
                }

            }else{
                DB::table('tbl_user')->delete($user->id);
            }

        }

        return $response;
    }

    // reset password
    /*public static function resetPassword($data)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        $user = User::find($data['uid']);
        $user->password = bcrypt( $data['password'] );

        if( $user->save() ){
            $response = [
                'status' => 1,
                'success' => [
                    'message' => 'Reset Password successfully!'
                ]
            ];
        }

        return $response;
    }*/

    // deposit balance
    /*public static function depositBalance($data)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        $parentId = Auth::id(); // current user id
        $amount = $data['balance'];
        $remark = $data['remark'];
        $uid = $data['uid'];
        $type = 'DEPOSIT';

        // child user
        $user = UserInfo::where('uid',$data['uid'])->first();
        $user->balance = ($user->balance+$data['balance']);

        // parent user
        $pUser = UserInfo::where('uid',$parentId)->first();
        $pUser->balance = ($pUser->balance-$data['balance']);

        if( $user->save() && $pUser->save() ){

            CommonModel::updateChipTransaction($uid,$parentId,$amount,$remark,$type);

            $response = [
                'status' => 1,
                'success' => [
                    'message' => 'Deposit successfully!'
                ]
            ];
        }

        return $response;
    }*/

    // withdrawal balance
    /*public static function withdrawalBalance($data)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        $parentId = Auth::id(); // current user id
        $amount = $data['balance'];
        $remark = $data['remark'];
        $uid = $data['uid'];
        $type = 'WITHDRAWAL';

        // child user
        $user = UserInfo::where('uid',$uid)->first();
        $user->balance = ($user->balance-$amount);

        // parent user
        $pUser = UserInfo::where('uid',$parentId)->first();
        $pUser->balance = ($pUser->balance+$amount);

        if( $user->save() && $pUser->save() ){

            CommonModel::updateChipTransaction($uid,$parentId,$amount,$remark,$type);

            $response = [
                'status' => 1,
                'success' => [
                    'message' => 'Withdrawal successfully!'
                ]
            ];
        }

        return $response;
    }*/

    // Add Profit Loss Level
    public static function updateProfitLossLevel($user){

        $userProfitLossArr = [];

        $systemId = $user->systemId;
        $uid = $user->id;

        $userProfitLoss = [
            'systemId' => $systemId,
            'clientId' => $uid,
            'userId' => $uid,
            'parentId' => $user->parentId,
            'level' => 0,
            'profit_loss' => 0,
            'actual_profit_loss' => 0,
            'commission' => 0,
            'actual_commission'=>0,
        ];

        array_push($userProfitLossArr, $userProfitLoss);

        $parentUser1 = DB::table('tbl_user as u')
            ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
            ->select(['u.id as id','parentId','pl','role','commission'])
            ->where([['u.status',1],['u.id',$user->parentId],['u.systemId',$systemId]])->first();

        if( $parentUser1 != null ){
            $profit_loss1 = $parentUser1->pl;
            $actual_profit_loss1 = $parentUser1->pl;
            $commission1 = $parentUser1->commission;
            $actual_commission1 = $parentUser1->commission;
            if( $parentUser1->role == 1 && $parentUser1->pl == 0 ){
                $profit_loss1 = 0;
                $actual_profit_loss1 = 100;
                $commission1 = 0;
                $actual_commission1 = 100;
            }

            $userProfitLoss1 = [
                'systemId' => $systemId,
                'clientId' => $uid,
                'userId' => $parentUser1->id,
                'parentId' => $parentUser1->parentId,
                'level' => 1,
                'profit_loss' => $profit_loss1,
                'actual_profit_loss' => $actual_profit_loss1,
                'commission'=>$commission1,
                'actual_commission'=>$actual_commission1,
            ];

            array_push($userProfitLossArr, $userProfitLoss1);

            if( $parentUser1->role != 1 ){

                $parentUser2 = DB::table('tbl_user as u')
                    ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                    ->select(['u.id as id','parentId','pl','role','commission'])
                    ->where([['u.status',1],['u.id',$parentUser1->parentId],['u.systemId',$systemId]])->first();

                if( $parentUser2 != null ){

                    $profit_loss2 = $parentUser2->pl;
                    $actual_profit_loss2 = $parentUser2->pl-$profit_loss1;
                    $commission2 = $parentUser2->commission;
                    $actual_commission2 = $parentUser2->commission-$commission1;
                    if( $parentUser2->role == 1 && $parentUser2->pl == 0 ){
                        $profit_loss2 = 0;
                        $actual_profit_loss2 = 100-$profit_loss1;
                        $commission2 = 0;
                        $actual_commission2 = 100-$commission1;
                    }

                    $userProfitLoss2 = [
                        'systemId' => $systemId,
                        'clientId' => $uid,
                        'userId' => $parentUser2->id,
                        'parentId' => $parentUser2->parentId,
                        'level' => 2,
                        'profit_loss' => $profit_loss2,
                        'actual_profit_loss' => $actual_profit_loss2,
                        'commission'=>$commission2,
                        'actual_commission'=>$actual_commission2,
                    ];

                    array_push($userProfitLossArr, $userProfitLoss2);

                    if( $parentUser2->role != 1 ){

                        $parentUser3 = DB::table('tbl_user as u')
                            ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                            ->select(['u.id as id','parentId','pl','role','commission'])
                            ->where([['u.status',1],['u.id',$parentUser2->parentId],['u.systemId',$systemId]])->first();

                        if( $parentUser3 != null ){

                            $profit_loss3 = $parentUser3->pl;
                            $actual_profit_loss3 = $parentUser3->pl-$profit_loss2;          
                            $commission3 = $parentUser3->commission;
                            $actual_commission3 = $parentUser3->commission-$commission2;
                            if( $parentUser3->role == 1 && $parentUser3->pl == 0 ){
                                $profit_loss3 = 0;
                                $actual_profit_loss3 = 100-$profit_loss2;
                                $commission3 = 0;
                                $actual_commission3 = 100-$commission2;
                            }

                            $userProfitLoss3 = [
                                'systemId' => $systemId,
                                'clientId' => $uid,
                                'userId' => $parentUser3->id,
                                'parentId' => $parentUser3->parentId,
                                'level' => 3,
                                'profit_loss' => $profit_loss3,
                                'actual_profit_loss' => $actual_profit_loss3,
                                'commission'=>$commission3,
                                'actual_commission'=>$actual_commission3,
                            ];

                            array_push($userProfitLossArr, $userProfitLoss3);

                            if( $parentUser3->role != 1 ){

                                $parentUser4 = DB::table('tbl_user as u')
                                    ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                                    ->select(['u.id as id','parentId','pl','role','commission'])
                                    ->where([['u.status',1],['u.id',$parentUser3->parentId],['u.systemId',$systemId]])->first();

                                if( $parentUser4 != null ){

                                    $profit_loss4 = $parentUser4->pl;
                                    $actual_profit_loss4 = $parentUser4->pl-$profit_loss3;
                                    $commission4 = $parentUser4->commission;
                                    $actual_commission4 = $parentUser4->commission-$commission3;
                                    if( $parentUser4->role == 1 && $parentUser4->pl == 0 ){
                                        $profit_loss4 = 0;
                                        $actual_profit_loss4 = 100-$profit_loss3;
                                        $commission4 = 0;
                                        $actual_commission4 = 100-$commission3;
                                    }

                                    $userProfitLoss4 = [
                                        'systemId' => $systemId,
                                        'clientId' => $uid,
                                        'userId' => $parentUser4->id,
                                        'parentId' => $parentUser4->parentId,
                                        'level' => 4,
                                        'profit_loss' => $profit_loss4,
                                        'actual_profit_loss' => $actual_profit_loss4,
                                        'commission'=>$commission4,
                                        'actual_commission'=>$actual_commission4,
                                    ];

                                    array_push($userProfitLossArr, $userProfitLoss4);

                                    if( $parentUser4->role != 1 ){

                                        $parentUser5 = DB::table('tbl_user as u')
                                            ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                                            ->select(['u.id as id','parentId','pl','role','commission'])
                                            ->where([['u.status',1],['u.id',$parentUser4->parentId],['u.systemId',$systemId]])->first();

                                        if( $parentUser5 != null ){

                                            $profit_loss5 = $parentUser5->pl;
                                            $actual_profit_loss5 = $parentUser5->pl-$profit_loss4;
                                            $commission5 = $parentUser5->commission;
                                            $actual_commission5 = $parentUser5->commission-$commission4;
                                            if( $parentUser5->role == 1 && $parentUser5->pl == 0 ){
                                                $profit_loss5 = 0;
                                                $actual_profit_loss5 = 100-$profit_loss4;
                                                   $commission5 = 0;
                                                $actual_commission5 = 100-$commission4;
                                            }

                                            $userProfitLoss5 = [
                                                'systemId' => $systemId,
                                                'clientId' => $uid,
                                                'userId' => $parentUser5->id,
                                                'parentId' => $parentUser5->parentId,
                                                'level' => 5,
                                                'profit_loss' => $profit_loss5,
                                                'actual_profit_loss' => $actual_profit_loss5,
                                                'commission'=>$commission5,
                                                'actual_commission'=>$actual_commission5
                                            ];

                                            array_push($userProfitLossArr, $userProfitLoss5);

                                        }

                                    }

                                }

                            }

                        }

                    }


                }

            }

        }

        if( DB::table('tbl_user_profit_loss')->insert($userProfitLossArr) ){
            return true;
        }else{
            return false;
        }

    }

}



