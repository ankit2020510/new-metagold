<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLimits extends Model
{
    public $timestamps = false;

    protected $table = 'tbl_user_limits';

    public static function doLimitUpdate($data)
    {
        $response = ['status' => 0, 'error' => ['message' => 'Something Wrong!']];

        $userLimit = UserLimits::where([['uid',$data['uid']]])->first();

        if( $userLimit != null ){
            $userLimit->min_stack = trim($data['min_stack']);
            $userLimit->max_stack = trim($data['max_stack']);
            $userLimit->max_profit_limit = trim($data['max_profit_limit']);
            $userLimit->max_expose_limit = trim($data['max_expose_limit']);
            $userLimit->bet_delay = trim($data['bet_delay']);

            if ($userLimit->save()) {
                $response = [
                    'status' => 1,
                    'success' => [
                        'message' => 'Updated successfully!'
                    ]
                ];
            }

        }

        return $response;
    }

}
