<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teenpatti extends Model
{
    public $timestamps = false;

    protected $table = 'tbl_bet_pending_teenpatti';

}
