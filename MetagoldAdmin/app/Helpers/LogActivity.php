<?php


namespace App\Helpers;
use Request;
use Illuminate\Support\Facades\Auth;
use App\Models\LogActivity as LogActivityModel;


class LogActivity
{


    public static function addToLog($subject)
    {
    	 //print_r($subject); die('log activity');
        $browser = Self::ExactBrowserName();
    	$log = [];
    	$log['description'] = $subject;
    	$log['url'] = Request::fullUrl();
    	$log['method'] = Request::method();
    	$log['ip'] = Self::get_client_ip();//Request::ip();
    	$log['browser'] = $browser;
    	$log['userId'] = Auth::user()->id;
      $log['clientName'] = Auth::user()->name;
      $log['created_at'] = date('Y-m-d h:i:s');
    	LogActivityModel::create($log);
    }

    // Function to get the client IP address
public static function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}


    public static function ExactBrowserName()
      {

      $ExactBrowserNameUA=$_SERVER['HTTP_USER_AGENT'];

      if (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "opr/")) {
          // OPERA
          $ExactBrowserNameBR="Opera";
      } elseIf (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "edg/")) {
          // CHROME
          $ExactBrowserNameBR="Edge";
      }elseIf (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "chrome/")) {
          // CHROME
          $ExactBrowserNameBR="Chrome";
      } elseIf (strpos(strtolower($ExactBrowserNameUA), "msie")) {
          // INTERNET EXPLORER
          $ExactBrowserNameBR="Internet Explorer";
      } elseIf (strpos(strtolower($ExactBrowserNameUA), "firefox/")) {
          // FIREFOX
          $ExactBrowserNameBR="Firefox";
      } elseIf (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "opr/")==false and strpos(strtolower($ExactBrowserNameUA), "chrome/")==false) {
          // SAFARI
          $ExactBrowserNameBR="Safari";
      } else {
          // OUT OF DATA
          $ExactBrowserNameBR="OUT OF DATA";
      };

      return $ExactBrowserNameBR;
    }



}