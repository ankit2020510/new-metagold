<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Test Call
// Route::get('app-user/action/update-user-data', 'AppUser\ActionController@updateUserData');
 Route::get('app-user/action/clear-user-data', 'AppUser\ActionController@clearUserData');
  Route::get('setting', 'Setting\SettingController@detail');
  
  Route::get('create-field', 'Lots\LotsController@addLog');
  Route::get('lot/list', 'Lots\LotsController@lot');
  
// Route::get('settlement/check/{uid}', 'Summary\SettlementController@checkSettlement');
// Route::get('settlement/check-comm/{uid}', 'Summary\SettlementController@checkSettlementComm');
// Route::get('settlement/check-cash/{uid}', 'Summary\SettlementController@checkSettlementCash');
  
Route::get('get-commission', 'Dashboard\ActionController@commissionAllinOne');
Route::get('cron-order-processing', 'SquareOff\SquareOffController@cronOrderProcessing'); 
Route::get('order-margin', 'OrderMargin\OrderMarginController@orderMargin'); 
Route::get('remove-expose', 'Expose\RemoveExposeController@expose');
Route::get('update-expose', 'Expose\UpdateExposeController@update');


Route::get('user-migration', 'AppUser\UserCronJobController@create');
Route::get('user-balance-update', 'AppUser\UserCronJobController@balanceUpdate');
Route::get('user-balance-update-all', 'AppUser\UserCronJobController@balanceUpdateAll');

//Poker
Route::post('poker/auth', 'Poker\AuthController@actionLogin');
Route::post('poker/exposure', 'Poker\ExposureController@actionBetPlace');
Route::post('poker/results', 'Poker\ResultsController@actionGameResult');

Route::post('game-over/live-game', 'GameOver\LiveGameController@doGameOver');
Route::post('game-over/abundant/live-game', 'GameOver\AbundantController@liveGame');
Route::post('game-recall/live-game', 'GameRecall\LiveGameController@doRecall');
Route::get('operator-detail', 'AppUser\ManageSystemController@detail');

//Login Or Auth
Route::post('login', 'AuthController@login');
Route::post('change-password', 'AuthController@changePassword');

//All Api After Auth
Route::middleware('auth:api')->group(function () {

    //Logout
    Route::get('logout', 'AuthController@logout');
   
    //Menu List
    Route::get('menu-list', 'Dashboard\MainMenuController@list');

    Route::post('create/lot', 'Lots\LotsController@create');
    Route::post('edit/lot', 'Lots\LotsController@edit');
    Route::post('delete/lot', 'Lots\LotsController@delete');
    Route::get('news-list', 'Dashboard\NewsController@list');
   Route::post('delete-news', 'Dashboard\NewsController@delete');
   Route::post('create-news', 'Dashboard\NewsController@create');


   Route::post('complain-list', 'Complain\ComplainController@list');
Route::post('complain/manage', 'Complain\ComplainController@manage');
    //Dashboard
    Route::get('log/list', 'Dashboard\LogListController@logList');
    Route::get('dashboard/get-user-data', 'Dashboard\ActionController@getUserData');
    Route::get('dashboard/data-list/{type}', 'Dashboard\ActionController@getDataList');

    Route::get('get-market-profit', 'Dashboard\ActionController@getMarketProfit');
  
  Route::get('mcx-market', 'Market\McxMarketController@mcxMarket');
  Route::post('market-block', 'Dashboard\MarketBlockController@block');
  Route::post('order-processing', 'SquareOff\SquareOffController@orderProcessing'); 
  //Route::get('cron-order-processing', 'SquareOff\SquareOffController@cronOrderProcessing'); 

    Route::post('dashboard/data-list-by-system/{type}', 'Dashboard\ActionController@getDataListBySystem');

    Route::get('dashboard/event/bet-allowed/{id}', 'Dashboard\ActionController@actionBetAllowed');
    Route::post('dashboard/market/bet-allowed', 'Dashboard\ActionController@actionBetAllowedMarket');

    Route::get('dashboard/event/bet-delete/{id}', 'Dashboard\ActionController@actionBetDelete');
    Route::get('dashboard/all-user-logout', 'Dashboard\ActionController@actionAllLogout');

    Route::get('dashboard/list', 'Dashboard\ListController@sport');
    Route::get('dashboard/list/{id}', 'Dashboard\ListController@event');
    Route::get('dashboard/block-unblock/event/{id}', 'Dashboard\BlockUnblockController@event');
    Route::get('dashboard/block-unblock/sport/{id}', 'Dashboard\BlockUnblockController@sport');

    Route::get('dashboard/event-details/cricket/{id}', 'Dashboard\DetailController@actionDataCricket');
    Route::get('dashboard/event-details/tennis/{id}', 'Dashboard\DetailController@actionDataTennis');
    Route::get('dashboard/event-details/football/{id}', 'Dashboard\DetailController@actionDataFootball');
    Route::get('dashboard/event-details/election/{id}', 'Dashboard\DetailController@actionDataElection');
    Route::get('dashboard/event-details/live-games/{id}', 'Dashboard\DetailController@actionDataLiveGames');

    Route::get('dashboard/event-details/jackpot/{id}', 'Dashboard\DetailController@actionDataJackpot');
    Route::get('dashboard/event-details/cricket-casino/{id}', 'Dashboard\DetailController@actionDataCricketCasino');

    Route::post('dashboard/jackpot-details', 'Dashboard\DetailController@actionJackpotDetails');

    Route::get('dashboard/event-bet-list/{id}', 'Dashboard\DetailController@actionBetList');
    Route::post('dashboard/market-bet-list', 'Dashboard\DetailController@actionMarketBetList');

    Route::post('dashboard/match-odd/user-book-data', 'Dashboard\ActionController@actionUserBookDataMatchOdd');
    Route::post('dashboard/book-maker/user-book-data', 'Dashboard\ActionController@actionUserBookDataBookMaker');

    Route::post('dashboard/fancy/user-book-data', 'Dashboard\ActionController@actionBookDataFancy');
    Route::post('dashboard/fancy3/user-book-data', 'Dashboard\ActionController@actionBookDataFancy3');
    Route::post('dashboard/ball-session/user-book-data', 'Dashboard\ActionController@actionBookDataBallSession');
    Route::post('dashboard/khado-session/user-book-data', 'Dashboard\ActionController@actionBookDataKhadoSession');
    Route::post('dashboard/casino/user-book-data', 'Dashboard\ActionController@actionBookDataCasino');

    // System Dashboard
    Route::get('dashboard/system-event-details/cricket/{id}', 'Dashboard\SystemDetailController@actionDataCricket');
    Route::get('dashboard/system-event-details/tennis/{id}', 'Dashboard\SystemDetailController@actionDataTennis');
    Route::get('dashboard/system-event-details/football/{id}', 'Dashboard\SystemDetailController@actionDataFootball');
    Route::get('dashboard/system-event-details/election/{id}', 'Dashboard\SystemDetailController@actionDataElection');
    Route::get('dashboard/system-event-details/live-games/{id}', 'Dashboard\SystemDetailController@actionDataLiveGames');

    Route::get('dashboard/system-event-details/jackpot/{id}', 'Dashboard\SystemDetailController@actionDataJackpot');
    Route::get('dashboard/system-event-details/cricket-casino/{id}', 'Dashboard\SystemDetailController@actionDataCricketCasino');

    Route::post('dashboard/system-jackpot-details', 'Dashboard\SystemDetailController@actionJackpotDetails');

    Route::get('dashboard/system-event-bet-list/{id}', 'Dashboard\SystemDetailController@actionBetList');

    Route::post('dashboard/system-market-bet-list', 'Dashboard\SystemDetailController@actionMarketBetList');

    Route::post('dashboard/match-odd/system-user-book-data', 'Dashboard\ActionController@actionSystemBookDataMatchOdd');
    Route::post('dashboard/book-maker/system-user-book-data', 'Dashboard\ActionController@actionSystemBookDataBookMaker');

    Route::post('dashboard/fancy/system-user-book-data', 'Dashboard\ActionController@actionSystemBookDataFancy');
    Route::post('dashboard/fancy3/system-user-book-data', 'Dashboard\ActionController@actionSystemBookDataFancy3');
    Route::post('dashboard/ball-session/system-user-book-data', 'Dashboard\ActionController@actionSystemBookDataBallSession');
    Route::post('dashboard/khado-session/system-user-book-data', 'Dashboard\ActionController@actionSystemBookDataKhadoSession');
    Route::post('dashboard/casino/system-user-book-data', 'Dashboard\ActionController@actionSystemBookDataCasino');



    //Manage System
    Route::get('app-user/manage-system/manage', 'AppUser\ManageSystemController@manage');
    Route::post('app-user/manage-system/create', 'AppUser\ManageSystemController@create');

    //App User Super Admin
    Route::get('app-user/super-admin/manage', 'AppUser\SuperAdminController@manage');
    Route::post('app-user/super-admin/create', 'AppUser\SuperAdminController@create');

    //App User Super Master
    Route::get('app-user/super-master/manage', 'AppUser\SuperMasterController@manage');
    Route::get('app-user/super-master/reference/{id}', 'AppUser\SuperMasterController@reference');
    Route::post('app-user/super-master/create', 'AppUser\SuperMasterController@create');
  
    //App User Supervisor
    Route::get('app-user/agent/manage', 'AppUser\AgentController@manage');
    Route::post('app-user/agent/create', 'AppUser\AgentController@create');

    //App User Supervisor
    Route::get('app-user/supervisor/manage', 'AppUser\SupervisorController@manage');
    Route::post('app-user/supervisor/create', 'AppUser\SupervisorController@create');

    //App User Master
    Route::get('app-user/master/manage', 'AppUser\MasterController@manage');
    Route::get('app-user/master/reference/{id}', 'AppUser\MasterController@reference');
    Route::post('app-user/master/create', 'AppUser\MasterController@create');

    //App User Client
    Route::get('app-user/client/manage', 'AppUser\ClientController@manage');
    Route::get('app-user/client/reference/{id}', 'AppUser\ClientController@reference');
    Route::post('app-user/client/create', 'AppUser\ClientController@create');

    //App User Common Action
    Route::get('app-user/action/check-user-data', 'AppUser\ActionController@checkUserData');
    Route::get('app-user/action/check-user-data-both/{id}', 'AppUser\ActionController@checkUserDataBoth');
    Route::post('app-user/action/reset-password', 'AppUser\ActionController@resetPassword');
    Route::post('app-user/action/deposit-balance', 'AppUser\ActionController@depositBalance');
    Route::post('app-user/action/withdrawal-balance', 'AppUser\ActionController@withdrawalBalance');

    Route::get('app-user/action/delete/{id}', 'AppUser\ActionController@doDelete');

    //App User Setting
    Route::get('app-user/setting/limit/{id}', 'AppUser\SettingController@limit');
    Route::post('app-user/setting/limit-update', 'AppUser\SettingController@limitUpdate');

    Route::get('app-user/setting/limit-setting/{id}', 'AppUser\SettingController@limitSetting');
    Route::post('app-user/setting/limit-setting-update', 'AppUser\SettingController@limitSettingUpdate');

    Route::get('app-user/setting/block-unblock/{id}', 'AppUser\SettingController@userBlockUnblock');
    Route::get('app-user/setting/lock-unlock/{id}', 'AppUser\SettingController@userLockUnlock');

    //History
    Route::post('history/chip-history', 'History\ChipHistoryController@list');
    Route::post('history/chip-history/{id}', 'History\ChipHistoryController@list');

    Route::get('history/account-history', 'History\AccountHistoryController@list');
    Route::get('history/account-history/{id}', 'History\AccountHistoryController@list');

    Route::post('history/bet-history', 'History\BetHistoryController@list');
    Route::post('history/bet-history/{id}', 'History\BetHistoryController@list');

    Route::post('history/pending-order', 'History\PendingOrdersController@list');
    Route::post('history/pending-order/{id}', 'History\PendingOrdersController@list');

    Route::post('current-order/{id}', 'History\CurrentOrdersController@list');

    Route::post('delete-order/{id}', 'History\DeleteOrderController@list');

    //Account Statement
    Route::post('transaction/account-statement', 'Transaction\AccountStatementController@list');
    Route::post('transaction/account-statement/{id}', 'Transaction\AccountStatementController@list');

    //Profit Loss
    Route::get('transaction/profit-loss', 'Transaction\ProfitLossController@list');
    Route::get('transaction/profit-loss/{id}', 'Transaction\ProfitLossController@list');

    Route::post('transaction/profit-loss/by-event', 'Transaction\ProfitLossController@listEventDetail');
    Route::post('transaction/profit-loss/by-event/{id}', 'Transaction\ProfitLossController@listEventDetail');

    Route::post('transaction/profit-loss/by-market', 'Transaction\ProfitLossController@listMarketDetail');
    Route::post('transaction/profit-loss/by-market/{id}', 'Transaction\ProfitLossController@listMarketDetail');

    Route::get('transaction/teenpatti-profit-loss', 'Transaction\ProfitLossController@listTeenPatti');
    Route::get('transaction/teenpatti-profit-loss/{id}', 'Transaction\ProfitLossController@listTeenPatti');

    //Summary Settlement
    Route::get('settlement/plus-account', 'Summary\SettlementController@getPlusUsers');
    Route::get('settlement/plus-account/{id}', 'Summary\SettlementController@getPlusUsers');
    Route::get('settlement/minus-account', 'Summary\SettlementController@getMinusUsers');
    Route::get('settlement/minus-account/{id}', 'Summary\SettlementController@getMinusUsers');

    Route::post('settlement/clear-settlement', 'Summary\SettlementController@clearSettlement');

    //system
    Route::get('settlement/system-plus-account', 'Summary\SystemSettlementController@getPlusUsers');
    Route::get('settlement/system-plus-account/{id}', 'Summary\SystemSettlementController@getPlusUsers');
    Route::get('settlement/system-minus-account', 'Summary\SystemSettlementController@getMinusUsers');
    Route::get('settlement/system-minus-account/{id}', 'Summary\SystemSettlementController@getMinusUsers');

    Route::post('settlement/system-clear-settlement', 'Summary\SystemSettlementController@clearSettlement');

    //Setting
    Route::get('setting/action/manage', 'Setting\ActionController@manage');
    Route::post('setting/action/create', 'Setting\ActionController@create');
    Route::post('setting/action/update', 'Setting\ActionController@update');
    Route::post('setting/action/status', 'Setting\ActionController@status');

    //Live Game
    Route::get('event/live-game/manage', 'Event\LiveGameController@manage');
    Route::post('event/live-game/create', 'Event\LiveGameController@create');
    Route::post('event/live-game/update', 'Event\LiveGameController@update');
    Route::post('event/live-game/status', 'Event\LiveGameController@status');
    Route::get('event/live-game/get-data/{id}', 'Event\LiveGameController@getData');

    //Check Profit which status is 1
    Route::get('net-profit','Admin\AdminController@netProfit');
});



