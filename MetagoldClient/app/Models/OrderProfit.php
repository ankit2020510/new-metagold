<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Redis;

class OrderProfit extends Model
{
   public $timestamps=false;
    protected $table = 'tbl_order';

    public static function getProfit($tradingSymbol,$instrumentToken,$name,$order_type,$trans_type,$lot,$buy_price,$sell_price,$price)
    {
    	$depth = self::getCurrentPrice($tradingSymbol,$instrumentToken);

          $currentBuyPrice = $depth['currentBuy'];
          $currentSellPrice = $depth['currentSell'];
  
                if($order_type == 'instant_execution'){
                      if($trans_type == 'BUY'){
                           $profit = $currentSellPrice-$buy_price;
                           $netProfit = $profit*$lot;
                      }else{
                           $profit =$sell_price-$currentBuyPrice;
                           $netProfit = $profit*$lot;
                       }//close checking of trans_type
                  }elseif($order_type == 'buy_limit'){
                      if($trans_type == 'BUY'){
                           $profit = $currentSellPrice-$price;
                           $netProfit = $profit*$lot;
                      }
                }else{
                     if($trans_type == 'SELL'){
                           $profit = $price-$currentBuyPrice;
                           $netProfit = $profit*$lot;
                      }//close checking of trans_type
                }

           $respone = ['currentBuyPrice'=>$currentSellPrice,'currentSellPrice'=>$currentBuyPrice,'netProfit'=>$netProfit];

   return $respone;

 } 

    public static function getCurrentPrice($tradingSymbol,$instrumentToken){
       
       $cache = Redis::connection();
       $key = 'Mcx_Market';
       $result=$cache->get($key);
       $arrData = json_decode($result);
       if(!empty($arrData)){
       foreach ($arrData as $key => $val) {
          
       	 if($key == $tradingSymbol && $val->instrument_token == $instrumentToken){
              $depth = $val->depth;
              $currentBuy = $depth->buy[0]->price;
              $currentSell = $depth->sell[0]->price;      
      	        	       
          }
       }
          $depth = ['currentBuy'=>$currentBuy,'currentSell'=>$currentSell];

       	  return $depth;
       	}else{

       		return 0;
       	}
       
    }

}