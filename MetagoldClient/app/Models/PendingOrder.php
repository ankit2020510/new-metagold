<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;


class PendingOrder extends Model
{
   public $timestamps=false;
    protected $table = 'tbl_order';

    public static function getList()
    {
        $query = DB::table('tbl_order')
            ->select('*')
            ->where([['status',3],['comm',0]]);
            

        $list = $query->get();

        return $list;
    }

}



