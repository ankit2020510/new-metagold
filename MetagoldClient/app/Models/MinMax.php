<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class MinMax extends Model
{
   public $timestamps=false;
    protected $table = 'tbl_lots';

    public static function checkMinMaxUnit($key_name,$lot)
    {

        $data = DB::table('tbl_lots')->where([['key_name',$key_name],['status',1]])->select('min','max')->first();

        if(!empty($data)){
	         $minUnit = $data->min;
	         $maxUnit = $data->max;
	         if($minUnit <= $lot && $maxUnit >= $lot){
	            
	            return 1;

	         }else{
	         	return 0;
	         }
        }


    }

}