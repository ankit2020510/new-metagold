<?php

namespace App\Http\Controllers\McxMarket;
use DB;
use App\Models\Mcx;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class McxController extends Controller
{

  public function mcxMarket(Request $request){

      $cache = Redis::connection();
     
        $response = [ "status" => 0 , "code" => 400 , 'data' => null , "message" => "Bad Request !!" ];

       $startTime = time();
        $endTime = time() + 60;
        while ($endTime > time()) {

            usleep(300000);
      /*  echo $startTime;
        echo "</br>";
        echo $endTime;*/
        $client = new Client();
        $response = $client->request('GET', 'http://printo-env-1.prib5q8tuu.ap-south-1.elasticbeanstalk.com/commodities');
    	$statusCode = $response->getStatusCode();
    	$body = $response->getBody()->getContents();
    	$detail = json_decode($body,true);
    	$data = $detail['data'];
   
          
         //print_r($data); 
            
        $arr = [];
        $gold=[];
        $silver=[];
        $usd=[];
        $usdMaxArr=$silverMax=$goldMax=$usdMinArr=$silverMin=$goldMin=$usdExpiryArr=[];
       
         foreach ($data as $key => $val1) {

            if($val1['name'] == 'USDINR'){
                   $usdExpiryArr[] = $val1['expiry']; 
                   $usd[$key]=$val1; 
            }
            if($val1['name'] == 'GOLD'){
                   $goldExpiryArr[] = $val1['expiry'];                 
                   $gold[$key]=$val1;            
            }  
            if($val1['name'] == 'SILVER'){
                   $silverExpiryArr[] = $val1['expiry'];
                   $silver[$key]=$val1;
            }                
         }
         
         $minGold = strtotime(min($goldExpiryArr));
         $minSilver = strtotime(min($silverExpiryArr));     
         $minUsd = strtotime(min($usdExpiryArr));
         
         // array of maximum expiry value and minimum value for gold
         foreach ($gold as $key => $value) {  
               if( strtotime($value['expiry']) == $minGold){
                  $goldMinexpiryData[$key] = $value;
                  $goldMinexpiryData[$key]['status'] = 1;
               }else{
                  $goldMaxexpiryData[$key] = $value;
                  $goldMaxexpiryData[$key]['status'] = 0;
               } 
         }
         // array of maximum expiry value and minimum value for silver
         foreach ($silver as $key => $value) {
               if( strtotime($value['expiry']) == $minSilver){
                  $silverMinexpiryData[$key] = $value;
                  $silverMinexpiryData[$key]['status'] = 1;
               }else{
                  $silverMaxexpiryData[$key] = $value;
                  $silverMaxexpiryData[$key]['status'] = 0;
               } 
         }
          // array of maximum expiry value and minimum value for silver
         foreach ($usd as $key => $value) {
               if( strtotime($value['expiry']) == $minUsd){
                  $usdMinexpiryData[$key] = $value;
                  $usdMinexpiryData[$key]['status'] = 1;
               }else{
                  $usdMaxexpiryData[$key] = $value;
                  $usdMaxexpiryData[$key]['status'] = 0;
               } 
         }
         $data = array_merge($usdMinexpiryData,$usdMaxexpiryData,$silverMinexpiryData,                         $silverMaxexpiryData,$goldMinexpiryData,$goldMaxexpiryData);
      

        if(in_array(date('D'),['Mon','Tue','Wed','Thu','Fri'])){
          
          	foreach ($data as $key1 => $val1) {
                    
              if($val1['depth']['sell'][0]['price'] != 0 && $val1['depth']['buy'][0]['price'] != 0){
             
                  if($val1['name'] == 'USDINR'){
                    $buy = round($val1['depth']['buy'][0]['price'],2);
                    $sell = round($val1['depth']['sell'][0]['price'],2);      
                    $val1['depth']['buy'][0]['price'] = $buy;
                    $val1['depth']['sell'][0]['price'] =$sell;
                  }

                    $arr[$key1]= $val1;
              }else{

                 if($val1['depth']['sell'][0]['price'] == 0 && $val1['depth']['buy'][0]['price'] == 0){

                        $key = 'Mcx_Market';
                        $result=$cache->get($key);
                        $arrData = json_decode($result);
                        foreach ($arrData as $key => $val) {

                          if($key1 == $key){
                            $arr[$key1]= $val;  
                          }
                        }
       /*iff*/  }else{

                    if($val1['depth']['sell'][0]['price'] == 0){
                        $key = 'Mcx_Market';
                        $result=$cache->get($key);
                        $arrData = json_decode($result);
                        foreach ($arrData as $key => $val) {
                          if($key1 == $key){
                            $val->depth->buy[0]->price=$val1['depth']['buy'][0]['price'];
                            $arr[$key1]= $val;  
                          }
                        }
                    }
                    if($val1['depth']['buy'][0]['price'] == 0){
                        $key = 'Mcx_Market';
                        $result=$cache->get($key);
                        $arrData = json_decode($result);
                        foreach ($arrData as $key => $val) {
                          if($key1 == $key){
                            $val->depth->sell[0]->price=$val1['depth']['sell'][0]['price'];
                            $arr[$key1]= $val;  
                          }
                        }
                    }

               }
            }
          }
               
            $key = 'Mcx_Market';
            $data =  json_encode($arr);
            if($cache->exists($key)){
              
              $cache->set($key, $data);
              $cache->publish('mcxclient', $data);
            }else{
               $cache->set($key, $data);
                $cache->publish('mcxclient', $data);
            }

         $database = $this->storeMarket();
          
        }
         

          
          $response = ['status' => 1 ,'code'=>200, 'message'=>'saved successfully!!'];
    }//while
	    
   return $response;

  }

  public function storeMarket(){

       $cache = Redis::connection();     
       $key = 'Mcx_Market';
       $result=$cache->get($key);

       $arrData = json_decode($result);
      
      $i=0;
      foreach($arrData as $key => $val){
       	$arrD[] = $val;
       	$arrD[$i]->tradingSymbol = $key;
       $i++;       	        	
      }
             
      foreach($arrData as $key1=> $data){
       	          	
           $market = new Mcx();      
           $tradingSymbol = $data->tradingSymbol;
	      	 $market->name = $data->name;
	      	 $market->instrument_token = $data->instrument_token;
	      	 $market->tradingSymbol = $tradingSymbol;
	      	 $market->timestamp = $data->timestamp;
	      	 $market->last_trade_time = $data->last_trade_time;
	      	 $market->last_price = $data->last_price;
	      	 $market->last_quantity = $data->last_quantity;
	      	 $market->buy_quantity = $data->buy_quantity;
	      	 $market->sell_quantity = $data->sell_quantity;
	      	 $market->average_price = $data->average_price;
	      	 $market->oi = $data->oi;
	      	 $market->oi_day_high = $data->oi_day_high;
	      	 $market->oi_day_low = $data->oi_day_low;
	      	 $market->net_change = $data->net_change;
	      	 $market->lower_circuit_limit = $data->lower_circuit_limit;
	      	 $market->upper_circuit_limit = $data->upper_circuit_limit;
	      	 $market->ohlc = json_encode($data->ohlc);
	      	 $market->depth = json_encode($data->depth);
	      	 $market->expiry = $data->expiry;
           $market->status = $data->status;
 
           $order =  DB::table('tbl_mcxmarket2')->whereIn('status',[1,0])->select('instrument_token','name')->get();
	      	  
           $token=[];
	      	 if(!empty($order)){
		      	  foreach ($order as  $val) {
		      	 	   $token[] = $val->instrument_token;
		      	  }
	      	 }

           if(!in_array($data->instrument_token, $token)){
		      	  if($market->save()){	 	
		      	    	$response = [ "status" => 1 , "code" => 200 , 'data' => null , "message" => "Succesfully added !!" ];
		      	  }
	      	  }else{

	      	  	$arr = [
               'name' => $data->name,
			      	 'instrument_token' => $data->instrument_token,
			      	 'timestamp' => $data->timestamp,
			      	 'last_trade_time' => $data->last_trade_time,
			      	 'last_price' => $data->last_price,
			      	 'last_quantity' => $data->last_quantity,
			      	 'buy_quantity' => $data->buy_quantity,
			      	 'sell_quantity' => $data->sell_quantity,
			      	 'average_price' => $data->average_price,
			      	 'oi' => $data->oi,
			      	 'oi_day_high' => $data->oi_day_high,
			      	 'oi_day_low' => $data->oi_day_low,
			      	 'net_change' => $data->net_change,
			      	 'lower_circuit_limit' => $data->lower_circuit_limit,
			      	 'upper_circuit_limit' => $data->upper_circuit_limit,
			      	 'ohlc' => json_encode($data->ohlc),
			      	 'depth' => json_encode($data->depth),
			      	 'tradingSymbol' =>  $tradingSymbol,
			      	 'expiry' => $data->expiry,
               'status'=>$data->status 

	      	  	  ]; 

              
	      	  	$order =  DB::table('tbl_mcxmarket2')->where([['instrument_token',$data->instrument_token]])->update($arr);

	      		$response = [ "status" => 1 , "code" => 200 , 'data' => null , "message" => "Market already exists !!" ];
	      	  }
	      }
	     

	  return $response;
	}

//display current markets
public function currentMarket(){
        
       $response = [ "status" => 0 , "code" => 401 , "message" => "Bad Request !!" ];
       
       $uid = Auth::user()->id;
       $cache = Redis::connection();     
       $key = 'Mcx_Market';
       $result=$cache->get($key);
       $data = json_decode($result);
       $mcxMarket=[];
       $arr=[];
       $newMarket = DB::table('tbl_mcxmarket_future')->where([['uid',$uid],['status',1]])->first();
       //print_r($newMarket); die('ppp');
       $market=[];
       if(!empty($newMarket)){ $market = json_decode($newMarket->market); };
       if($data != null){
          foreach($data as $key => $value) {    

                if($value->status == 1){
                   $depth = $value->depth;
                   $depth_buy= $depth->buy;
                   $depth_buy_price = $depth_buy[0]->price;
                   $depth_sell= $depth->sell;
                   $depth_sell_price = $depth_sell[0]->price;
                  $arr[]=[

                      "name"=> $value->name,
                      "expiry"=> $value->expiry,
                      "instrument_token"=> $value->instrument_token,
                      "last_price"=> $value->last_price,
                      "timestamp"=> $value->timestamp,
                      "last_trade_time"=> $value->last_trade_time,
                      "last_quantity"=>$value->last_quantity,
                      "buy_quantity"=> $value->buy_quantity,
                      "sell_quantity"=> $value->sell_quantity,
                      "average_price"=> $value->average_price,
                      "oi"=> $value->oi,
                      "oi_day_high"=> $value->oi_day_high,
                      "oi_day_low"=> $value->oi_day_low,
                      "net_change"=>$value->net_change,
                      "lower_circuit_limit"=> $value->lower_circuit_limit,
                      "upper_circuit_limit"=> $value->upper_circuit_limit,
                      "ohlc"=>$value->ohlc,
                      "depth"=>['buy'=>$depth_buy_price, 'sell'=>$depth_sell_price],
                      "status"=>$value->status,
                      "tradingSymbol"=>$key
                        ];
                 }
                if(in_array($value->instrument_token, $market)){
                   $depth = $value->depth;
                   $depth_buy= $depth->buy;
                   $depth_buy_price = $depth_buy[0]->price;
                   $depth_sell= $depth->sell;
                   $depth_sell_price = $depth_sell[0]->price;
                  $arr[]=[

                      "name"=> $value->name,
                      "expiry"=> $value->expiry,
                      "instrument_token"=> $value->instrument_token,
                      "last_price"=> $value->last_price,
                      "timestamp"=> $value->timestamp,
                      "last_trade_time"=> $value->last_trade_time,
                      "last_quantity"=>$value->last_quantity,
                      "buy_quantity"=> $value->buy_quantity,
                      "sell_quantity"=> $value->sell_quantity,
                      "average_price"=> $value->average_price,
                      "oi"=> $value->oi,
                      "oi_day_high"=> $value->oi_day_high,
                      "oi_day_low"=> $value->oi_day_low,
                      "net_change"=>$value->net_change,
                      "lower_circuit_limit"=> $value->lower_circuit_limit,
                      "upper_circuit_limit"=> $value->upper_circuit_limit,
                      "ohlc"=>$value->ohlc,
                     "depth"=>['buy'=>$depth_buy_price, 'sell'=>$depth_sell_price],
                      "status"=>$value->status,
                      "tradingSymbol"=>$key
                  ];                
                }
              
          }
         

  
       }else{
            $mcxMarket='';
       }
       $response = [ "status" => 1 , "code" => 200 , 'data'=>$arr, "message" => "Market found !!" ];

    return $response;
  }

  //display future market in add
  public function futureMarket(){
        
       $response = [ "status" => 0 , "code" => 401 , "message" => "Bad Request !!" ];
        
       $cache = Redis::connection();     
       $key = 'Mcx_Market';
       $result=$cache->get($key);
       $data = json_decode($result);
      // print_r($data);
       $market = $this->currentMarket();
       $currentMarket = $market['data'];
       $instrument_token=[];
       foreach ($currentMarket as $value) {
            $instrument_token[] = $value['instrument_token'];
       }
     
       
       $extraMarket=[];
       $goldDifference=$silverDifference=$usdDifference=0;
       if($data != null){
            foreach($data as $key => $value) {
               if($value->name == 'GOLD' && $value->status == 1){
                      $currentExpiry = $value->expiry;
                      $goldExp = $value->expiry;                  
                  }
                  if($value->name == 'SILVER' && $value->status == 1){
                      $currentExpiry = $value->expiry;
                      $silverExp = $value->expiry;                  
                  }
                  if($value->name == 'USDINR' && $value->status == 1){
                      $currentExpiry = $value->expiry;
                      $usdExp = $value->expiry;                  
                  }      
            }
//print_r($goldExp); print_r($silverExp); print_r($usdExp); //die('ppp');
             /*foreach($data as $key1 => $value) {    
             
                  if($value->name == 'GOLD' && $value->status == 0){
                        $currentDate = date('Y-m-d'); 
                        $date1=date_create($currentDate);
                        $date2=date_create($goldExp);
                        $diff=date_diff($date1,$date2); 
                        $goldDifference = $diff->format("%R%a");
                        if($goldDifference <= '+3' && $goldDifference >= 0){
                          if(!in_array($value->instrument_token, $instrument_token)){
                             $extraMarket[]=$value;
                          }
                        }            
                  }
                  if($value->name == 'SILVER' && $value->status == 0){
                        $currentDate = date('Y-m-d'); 
                        $date1=date_create($currentDate);
                        $date2=date_create($silverExp);
                        $diff=date_diff($date1,$date2); 
                        $silverDifference = $diff->format("%R%a");
                        if($silverDifference <= '+3' && $silverDifference >= 0 ){
                          if(!in_array($value->instrument_token, $instrument_token)){
                             $extraMarket[]=$value;
                          }
                        }                    
                  }
                  if($value->name == 'USDINR' && $value->status == 0){
                        $currentDate = date('Y-m-d'); 
                        $date1=date_create($currentDate);
                        $date2=date_create($usdExp);
                        $diff=date_diff($date1,$date2); 
                        $usdDifference = $diff->format("%R%a");
                        if($usdDifference <= '+3' && $usdDifference >= 0 ){
                          if(!in_array($value->instrument_token, $instrument_token)){
                             $extraMarket[]=$value;
                          }
                        }                 
                  }      
            }*/
           // print_r($extraMarket); die('jsjksjfkjk');
            foreach($data as $key1 => $value) {    
                    
                  if($value->name == 'GOLD' && $value->status == 0){
                       if(!in_array($value->instrument_token, $instrument_token)){
                          $extraMarket[]=$value;
                       }
                                    
                  }
                  if($value->name == 'SILVER' && $value->status == 0){
                       if(!in_array($value->instrument_token, $instrument_token)){
                          $extraMarket[]=$value;
                        }
                                            
                  }
                  if($value->name == 'USDINR' && $value->status == 0){
                        if(!in_array($value->instrument_token, $instrument_token)){
                          $extraMarket[]=$value;
                        }
                                        
                  }      
            }
       }else{
            $extraMarket='';
       }

       //print_r($goldExp); echo "string"; print_r($silverExp); echo "string";print_r($usdExp); die();
       $response = [ "status" => 1 , "code" => 200 , 'data'=>$extraMarket, "message" => "Market found !!" ];

    return $response;
  }
  
  //add future market on click
  public function addMarket(Request $request){
        
       $response = [ "status" => 0 , "code" => 401 , "message" => "Bad Request !!" ];
       
       $uid = Auth::user()->id;
       //$marketArr1=[];
       $marketDetail = DB::table('tbl_mcxmarket_future')->where([['uid',$uid],['status',1]])->select('market')->first();
      
       if($marketDetail != null){ 
          $markets = json_decode($marketDetail->market);
         if(!in_array($request->instrument_token, $markets)){  
             $marketArr = json_decode($marketDetail->market);
             $marketArr[] = $request->instrument_token;
             $market = [ 'market'=>$marketArr];
             DB::table('tbl_mcxmarket_future')->where([['uid',$uid],['status',1]])->update($market);

             $response = [ "status" => 1 , "code" => 200 , "message" => "Market added successfully !!" ];
          }else{
             $response = [ "status" => 1 , "code" => 200 , "message" => "Market already exixts successfully !!" ]; 
          }
       }else{
     
          $marketArr[] = $request->instrument_token;
          $marketArr = json_encode($marketArr);
          $arr = [ 'uid' => $uid, 'market' =>$marketArr ];
          DB::table('tbl_mcxmarket_future')->insert($arr);

           $response = [ "status" => 1 , "code" => 200 , "message" => "Market added successfully !!" ];
       }
      

    return $response;
  }

}












?>