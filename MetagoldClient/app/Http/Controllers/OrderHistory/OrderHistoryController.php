<?php

namespace App\Http\Controllers\OrderHistory;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;


class OrderHistoryController extends Controller
{


       public function orderHistory(Request $request)
       {   
     
             $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
     	     $uid = Auth::user()->id;

             //die('llllll');
            if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){
                  
                        $startDate = date('yy-m-d', strtotime($request->start_date));
                        $startDate =$startDate." 00:00:01";
                     
                        $endDate = date('yy-m-d', strtotime($request->end_date));
                        $endDate =$endDate." 23:59:59";
                    
            }else{
                
                 $start = new \DateTime('now +1 day');
                $endDate =  $start->format('yy-m-d h:i:s');

                $end = new \DateTime('now -5 day');
                $startDate = $end->format('yy-m-d h:i:s');  
            }


            $query =  DB::table('tbl_order')->select('*')->where([['uid',$uid],['status',5],['comm',1]]);

            if(isset($request->isFirst) && $request->isFirst == 1){
                       $order = $query->limit(15)->get();
            }else{
                      $order = $query->whereBetween('created_at',[$startDate, $endDate])->get();
                 }

            $orders = [];
            foreach ($order as $key => $val) {
            	# code...
            	$orders[]=$val;
            }
            
            $response = ['status' => 1, 'code' => 200, 'data' => $orders, 'message' => 'data found!!'];
           
   return $response;

    }



     public function newOrderHistory(Request $request)
       {   
     
             $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
             $uid = Auth::user()->id;

             
            if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){
                  
                        $startDate = date('yy-m-d', strtotime($request->start_date));
                        $startDate =$startDate." 00:00:01";
                     
                        $endDate = date('yy-m-d', strtotime($request->end_date));
                        $endDate =$endDate." 23:59:59";
                    
            }else{
                
                 $start = new \DateTime('now +1 day');
                $endDate =  $start->format('yy-m-d h:i:s');

                $end = new \DateTime('now -5 day');
                $startDate = $end->format('yy-m-d h:i:s');  
            }
           // print_r($request->route('id')); die('ppp');
                            
                if(isset($request->type) && $request->type != 'ALL'){
                    $query =  DB::table('tbl_order')->select('*')->where([['name',$request->type],['uid',$uid],['status',5],['comm',1]]);
                }else{
                    $query =  DB::table('tbl_order')->select('*')->where([['uid',$uid],['status',5],['comm',1]]);
                }
            

            if(isset($request->isFirst) && $request->isFirst == 1){
                       $orderComplete = $query->limit(15)->orderBy('created_at','DESC')->get();
            }else{
                      $orderComplete = $query->whereBetween('created_at',[$startDate, $endDate])->get();
                 }

            $orders = [];
            foreach ($orderComplete as $key => $val) {
                # code...
                $orders[]=$val;
            }

             
           
                if(isset($request->type) && $request->type != 'ALL'){
                    $queryAr =  DB::table('tbl_order')->select('*')->where([['name',$request->type],['uid',$uid],['status',1],['comm',0]]);
                }else{
                    $queryAr =  DB::table('tbl_order')->select('*')->where([['uid',$uid],['status',1],['comm',0]]);
                }
            
            

            if(isset($request->isFirst) && $request->isFirst == 1){
                       $orderPosition = $queryAr->limit(15)->orderBy('created_at','DESC')->get();
            }else{
                      $orderPosition = $queryAr->whereBetween('created_at',[$startDate, $endDate])->get();
                 }

            $position = [];
            foreach ($orderPosition as $key => $val) {
                # code...
                $position[]=$val;
            }
            
            
            $response = ['status' => 1, 'code' => 200, 'data' =>['order'=>$orders,'position'=>$position], 'message' => 'data found!!'];
           
   return $response;

    }

 }























?>