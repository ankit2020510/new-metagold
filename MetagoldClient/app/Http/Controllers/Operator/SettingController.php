<?php

namespace App\Http\Controllers\Operator;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{

    public function detail(Request $request)
    {

    try{

      $response = ['status'=>0,'code'=>404,'message'=>'Bad request !!!','data'=>null];

      $settingDetail = DB::table('tbl_common_setting')->select('key_name','value')
                         ->whereIn('key_name',['IS_MAINTENANCE','MAINTENANCE_SETTING'])
                         ->where('status',1)
                         ->get();
        
 
      	  if(!empty($settingDetail)){
           
            $response = ['status'=>1,'code'=>200, 'message'=>'Data found !!!','data'=>$settingDetail];

          }



  return $response;

}catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


 }