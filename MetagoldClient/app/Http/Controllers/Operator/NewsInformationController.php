<?php

namespace App\Http\Controllers\Operator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class NewsInformationController extends Controller
{
    /**
     * get news and add
     * Action - Get
     * Created at JUNE 2020 by metagold
     */
    public function list(Request $request)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];
           
        try{

                $data = DB::table('tbl_news')->where('status',1)->get();
                if(!empty($data)){
                   $response = ['status'=>1, 'code'=>200, 'data'=>$data, 'message'=>'Data found !!'];
                    return $response;
                }else{
                    $response = ['status'=>1, 'code'=>201, 'data'=>null, 'message'=>'Data not found !!'];
                    return $response;
                }

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
