<?php

namespace App\Http\Controllers\Lots;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\PlaceOrder;

class LotsController extends Controller
{


       public function lot(Request $request)
       {   
     
           $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!!" ];
     	   
           $lots = DB::table('tbl_lots')->where('status',1)->get();

           if(!empty($lots)){
            // print_r($arr);
           $arr = [];
            foreach ($lots as $val) {
                $arr[] = [
                    'id'=>$val->id,
                    'key_name'=>$val->key_name,
                    'min'=>$val->min,
                    'max'=>$val->max,
                    'commission'=>$val->commission,
                    'unit'=>$val->unit,
                    'status'=>$val->status,
                    'buttonsConfig'=>json_decode($val->buttonConfig)

                ];

              }
              $response = ['status' => 1, 'code' => 200, 'data' =>$arr, 'message' => 'data found!!'];
           }else{
             $response = ['status' => 1, 'code' => 200, 'data' =>null, 'message' => 'data found!!'];
           } 
           return $response;
       }


 }























