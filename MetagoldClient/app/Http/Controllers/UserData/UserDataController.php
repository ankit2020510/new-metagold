<?php

namespace App\Http\Controllers\UserData;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redis;

class UserDataController extends Controller
{

    public function userData(Request $request)
    {

     try {

            $dataArr = [];

            if( null != Auth::user()->id ){

                $uid = Auth::user()->id;
               
                $globalCommentary = $teenPattiData = 'No data!';
                $globalTimeOut = 60; $teenPattiStatus = 0;
                $eventListTiming = $eventDetailTiming = $balanceRefreshTiming = ['web' => 2000 , 'app' => 2000 ];
                $oddsTiming = ['web' => 500 , 'app' => 500 ];

                $updated_on = date('Y-m-d H:i:s');
                        

                $dataArr['balance'] = $this->getBalanceValUpdate($uid);
                $dataArr['betOption'] = $this->getBetOptions($uid);
                $dataArr['teenPattiData'] = null;
                if(null != $request->input('type')){
                $dataArr['teenPattiData'] = $this->getTeenPattiData();
                }


              
                $userName = Auth::user()->username;

                $settingData = DB::table('tbl_common_setting')->select('key_name','value')
                        ->where('status',1)
                        ->get();
               
                if( $settingData != null ){
                    $operator = 0;
                    foreach ( $settingData as $data ){
      
                       if(trim($data->key_name) == 'TEENPATTI_OPERATORID' ){
                          $operator = $data->value;
                          
                       }
  
                        if( trim($data->key_name) == 'GLOBAL_COMMENTARY' ){
                            $globalCommentary = $data->value;;
                        }elseif ( trim($data->key_name) == 'GLOBAL_TIME_OUT' ){
                            $globalTimeOut = (int)$data->value;
                        }elseif ( trim($data->key_name) == 'EVENT_LIST_TIMING' ){
                            $setting = json_decode( $data->value,true );
                            if( $setting != null ){
                                $web = $app = 2000;
                                if( isset($setting['web']) && ((int)$setting['web']) >= 1000  ){
                                    $web = (int)$setting['web'];
                                }
                                if ( isset($setting['app']) && ((int)$setting['app']) >= 1000 ){
                                    $app = (int)$setting['app'];
                                }
                                $eventListTiming = ['web' => $web , 'app' => $app ];
                            }
                        }elseif ( trim($data->key_name) == 'EVENT_DETAIL_TIMING' ){
                            $setting = json_decode( $data->value,true );
                            if( $setting != null ){
                                $web = $app = 2000;
                                if( isset($setting['web']) && ((int)$setting['web']) >= 1000  ){
                                    $web = (int)$setting['web'];
                                }
                                if ( isset($setting['app']) && ((int)$setting['app']) >= 1000 ){
                                    $app = (int)$setting['app'];
                                }
                                $eventDetailTiming = ['web' => $web , 'app' => $app ];
                            }
                        }elseif ( trim($data->key_name) == 'ODDS_TIMING' ){
                            $setting = json_decode( $data->value,true );
                            if( $setting != null ){
                                $web = $app = 500;
                                if( isset($setting['web']) && ((int)$setting['web']) >= 300  ){
                                    $web = (int)$setting['web'];
                                }
                                if ( isset($setting['app']) && ((int)$setting['app']) >= 300 ){
                                    $app = (int)$setting['app'];
                                }
                                $oddsTiming = ['web' => $web , 'app' => $app ];
                            }
                        }elseif ( trim($data->key_name) == 'BALANCE_REFRESH_TIMING' ){
                            $setting = json_decode( $data->value,true );
                            if( $setting != null ){
                                $web = $app = 5000;
                                if( isset($setting['web']) && ((int)$setting['web']) >= 1000  ){
                                    $web = (int)$setting['web'];
                                }
                                if ( isset($setting['app']) && ((int)$setting['app']) >= 1000 ){
                                    $app = (int)$setting['app'];
                                }
                                $balanceRefreshTiming = ['web' => $web , 'app' => $app ];
                            }
                        }elseif ( trim($data->key_name) == 'LIVE_GAME_STATUS' ){
                            $teenPattiStatus = (int)$data->value;
                        }

                    }

                }

                $globalCommentary_List=null;
                $globalCommentaryList = DB::table('tbl_global_commentary')->select('id','commentary')->orderBy('id', 'DESC')->limit(6)->get();
                $cnt=0;
                foreach ($globalCommentaryList as $dataList) {
                    if($cnt != 0) {
                        $globalCommentary_List[] = $dataList;
                    }
                    $cnt++;
                }
              
                $dataArr['teenpatti_operatorId'] = $operator;
                $dataArr['globalCommentary'] = $globalCommentary;
                $dataArr['globalTimeOut'] = $globalTimeOut;
                $dataArr['eventListTiming'] = $eventListTiming;
                $dataArr['eventDetailTiming'] = $eventDetailTiming;
                $dataArr['oddsTiming'] = $oddsTiming;
                $dataArr['balanceRefreshTiming'] = $balanceRefreshTiming;
                $dataArr['teenPattiStatus'] = $teenPattiStatus;
                $dataArr['globalCommentaryList'] = $globalCommentary_List;
 
                if( null == $request->route('id') || $request->route('id') == ' ' ){
                   
                    if(isset($request->plateform) && $request->plateform == 'desktop'){
                      
                     $dataArr['matchUnmatchData'] = $this->matchUnmatchData($uid,null);
                   }else{
                     $dataArr['matchUnmatchData'] = null;
                   }
                  
                }else{
            
                   $eventId = $request->route('id');
                   $dataArr['matchUnmatchData'] = $this->matchUnmatchData($uid,$eventId);
                }
                   $dataArr['userName'] = $userName;

                   
 
            $response = [ "status" => 1 ,'code'=>200, "data" => $dataArr, 'message'=>'Data Found !!'  ];

            
            }else{
                $response =  [ "status" => 0 , "code" => 400 ,'data'=>null, "message" => "Bad request!" ];
            }


          return $response;

         }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

}


 public function getBalanceValUpdate($uid)
       {

            $user = DB::table('tbl_user_info')->select('balance','pl_balance','expose','updated_on')
                        ->where('uid',$uid)
                        ->first();    

            if( $user != null ){
                 if($user->expose!=0 || $user->expose!=null || $user->expose!=''){
                     /*$user_balance = $user->balance-$user->expose+$user->pl_balance;*/
                     $user_balance = $user->balance+$user->pl_balance;
                 }else{
                     $user_balance = $user->balance+$user->pl_balance;
                 }
               

                $updatedTime = (strtotime($user->updated_on)+0);

                return [ "balance" => round($user_balance) , "expose" => round( $user->expose) , "mywallet" => round($user->balance) , "updated_time" => $updatedTime, "pl_balance" =>$user->pl_balance ];
            }

        return [ "balance" => 0 , "expose" => 0 , "mywallet" => 0 , "updated_time" => 0];
  }

  public function getBetOptions( $uid ){

         $model = DB::table('tbl_user_info')->select('bet_option')
                        ->where('uid',$uid)
                        ->first();
        
        if( $model != null ){
            return $model->bet_option;
        }else{

            $setting = DB::table('tbl_common_setting')->select('value')
                        ->where([['key_name','DEFAULT_STACK_OPTION'],['status',1]])
                        ->first();
            

            if( $setting != null ){
                return $setting->value;
            }else{
                return '10000,25000,50000,100000,200000';
            }

        }
    }

 public function getTeenPattiData(){

        $teenPattiData = null;

        $teenPattiData = DB::table('tbl_live_game')
                    ->where('status',1)
                    ->get();
      
        if( $teenPattiData != null ){
            return $teenPattiData;
        }
        return $teenPattiData;
    }

  public function matchUnmatchData($uid,$eventId)
  {
      
        $matchedDataArr = $unMatchedDataArr = [
            "data" => [],
            "count" => 0
        ];
        $betList = $betListNew = [];

        if( $eventId == null  || $eventId == ' '){
          
             $betList1 = DB::table('tbl_bet_pending_2')->select('id','price','rate','size','win','loss','bType','result','description','status','mType','runner','is_match','diff')
                  ->where([['result','PENDING'],['uid',$uid],['status',1]])
                  ->orderBy('created_on' ,'DESC')
                   ->get();
                  foreach ($betList1 as $betList1) {
                     $betList[] = $betList1;
                  }
             
             $betList2 = DB::table('tbl_bet_pending_3')->select('id','price','rate','size','win','loss','bType','result','description','status','mType','runner','is_match','diff','market')
                  ->where([['result','PENDING'],['uid',$uid],['status',1]])
                  ->orderBy('created_on' ,'DESC')
                   ->get();
                
                    foreach ($betList2 as $betList2) {
          
                     $betList[] = $betList2;

                    }

            $betList3 = DB::table('tbl_bet_pending_4')->select('id','price','rate','size','win','loss','bType','result','description','status','mType','runner','is_match','diff')
                  ->where([['result','PENDING'],['uid',$uid],['status',1]])
                  ->orderBy('created_on' ,'DESC')
                   ->get();
                    foreach ($betList3 as $betList3) {
                     $betList[] = $betList3;
                    }
        
              $betList4 = DB::table('tbl_bet_pending_1')->select('id','price','rate','size','win','loss','bType','result','description','status','mType','runner','is_match','diff')
                  ->where([['result','PENDING'],['uid',$uid],['status',1]])
                  ->orderBy('created_on' ,'DESC')
                   ->get();
                   foreach ($betList4 as $betList4) {
                     $betList[] = $betList4;
                    }
         
        }else{
         
              $betList1 = DB::table('tbl_bet_pending_2')->select('id','price','rate','size','win','loss','bType','result','description','status','mType','runner','is_match','diff')
                  ->where([['result','PENDING'],['uid',$uid],['status',1],['eid',$eventId]])
                  ->orderBy('created_on' ,'DESC')
                   ->get();
                     foreach ($betList1 as $betList1) {
                     $betList[] = $betList1;
                      }
                
             $betList2 = DB::table('tbl_bet_pending_3')->select('id','price','rate','size','win','loss','bType','result','description','status','mType','runner','is_match','diff','market')
                  ->where([['result','PENDING'],['uid',$uid],['status',1],['eid',$eventId]])
                  ->orderBy('created_on' ,'DESC')
                   ->get();
                    
                  foreach ($betList2 as $betList2) {
                    if($betList2->mType == 'ballbyball' ){
                      $market = $betList2->market;
                      $space = ' ';
                      $ball = strstr($market,$space,true);        
                      $betList2->ball= $ball;
                    }
                    if($betList2->mType == 'khado'){
                       $difference = $betList2->diff - $betList2->price ;        
                      $betList2->difference= $difference;
                    }
                      $betList[] = $betList2;
                  
                      }
             $betList3 = DB::table('tbl_bet_pending_4')->select('id','price','rate','size','win','loss','bType','result','description','status','mType','runner','is_match','diff')
                  ->where([['result','PENDING'],['uid',$uid],['status',1],['eid',$eventId]])
                  ->orderBy('created_on' ,'DESC')
                   ->get();
                    foreach ($betList3 as $betList3) {
                     $betList[] = $betList3;
                      }
             
              $betList4 = DB::table('tbl_bet_pending_1')->select('id','price','rate','size','win','loss','bType','result','description','status','mType','runner','is_match','diff')
                  ->where([['result','PENDING'],['uid',$uid],['status',1],['eid',$eventId]])
                  ->orderBy('created_on' ,'DESC')
                   ->get();
                    foreach ($betList4 as $betList4) {
                     $betList[] = $betList4;
                      }
           
             //print_r($betList); die('ewe');
        }


        $matchData = $unMatchData = [];
        if( $betList != null ){
            $i = 0;

            foreach ( $betList as $betData ){
                $betData->profit = $betData->size;
                if( $betData->mType == 'fancy' || $betData->mType == 'fancy2' ){
                    if( $betData->rate != 0 ){
                        $betData->profit = ( $betData->size*$betData->rate ) / 100;
                    }else{
                        $betData->profit = 0;
                    }
                }else if( $betData->mType == 'fancy3' ){
                    if( $betData->mType != 'yes' ){
                        if( $betData->price > 0 ){
                            $betData->profit = round( ( ($betData->size*$betData->price)-$betData->size ) , 2 );
                        }
                    }else{
                        if( $betData->price > 0 ){
                           $betData->profit = round( ( ($betData->size*$betData->price)-$betData->size ) , 2 );
                        }
                    }
                }else if( $betData->mType == 'khado' ){
                   // print_r('khado 1');
                    if( $betData->bType != 'yes' ){
                        if( $betData->price > 0 ){
                            $betData->profit = round( ( ($betData->size*$betData->rate)/ 100 ) , 2 );
                        }
                    }else{
                        if( $betData->price > 0 ){
                           $betData->profit = round( ( ($betData->size*$betData->rate)/ 100 ) , 2 );
                        }
                        //print_r($betData['profit']);
                    }
                }else if( $betData->mType == 'ballbyball' ){
                   // print_r('khado 1');
                    if( $betData->bType != 'yes' ){
                        if( $betData->price > 0 ){
                            $betData->profit = round( ( ($betData->size*$betData->rate)/ 100 ) , 2 );
                        }
                    }else{
                        if( $betData->price > 0 ){
                           $betData->profit =round( ( ($betData->size*$betData->rate)/ 100 ) , 2 );
                        }
                        //print_r($betData['profit']);
                    }
                }else if( $betData->mType == 'cricket_casino' ){

                    if( $betData->rate != 0 ){
                        $betData->profit = ( $betData->size*($betData->rate-1) );
                    }else{
                        $betData->profit = 0;
                    }

                }else if( $betData->mType == 'bookmaker' ){
                    if( $betData->bType != 'back' ){
                        if( $betData->price > 0 ){
                            $betData->profit = round( ( $betData->size*( $betData->price/100 ) ) , 2 );
                        }
                    }else{
                        if( $betData->price > 0 ){
                            $betData->profit = round( ( $betData->size*( $betData->price/100 ) ) , 2 );
                        }
                    }
                }else{
                    if( $betData->bType != 'back' ){
                        if( $betData->price > 0 ){
                            $betData->profit = round( ( ($betData->size*$betData->price)-$betData->size ) , 2 );
                        }
                    }else{
                        if( $betData->price > 0 ){
                            $betData->profit = round( ( ($betData->size*$betData->price)-$betData->size ) , 2 );
                        }
                    }
                }

               if( $betData->is_match == 1  ){
                    $matchData[] = $betData;
                }else{
                    if( $betData->status == 1  ){
                        $unMatchData[] = $betData;
                    }

                 }
              $i++;
            }


            $matchDataNew = $unMatchDataNew = [
                [ 'title' => 'Match Odd','dataList' => [] ],
                [ 'title' => 'Bookmaker','dataList' => [] ],
                [ 'title' => 'Fancy','dataList' => [] ],
                [ 'title' => 'Session','dataList' => [] ],
                [ 'title' => 'Other Market','dataList' => [] ],
                [ 'title' => 'Khado','dataList' => [] ],
                [ 'title' => 'Ballbyball','dataList' => [] ],
                [ 'title' => 'Cricket Casino','dataList' => [] ],
                [ 'title' => 'Completed Match','dataList' => [] ],
                [ 'title' => 'Tied Match','dataList' => [] ]
            ];

            if( $matchData != null || $matchData != ' ' ){
                $matchOddData = $matchOddData2 = $fancyData = $fancyData2 = $fancyData3 = $lotteryData = $khadoData = $ballbyballData = $completedMatch = $tiedMatch = [];

                foreach ( $matchData as $mData ){

                    if( $mData->mType == "match_odd"  ){
                        $matchOddData[] = $mData;
                    }

                     if( $mData->mType == "completed_match"  ){
                        $completedMatch[] = $mData;
                    }

                     if( $mData->mType == "tied_match"  ){
                        $tiedMatch[] = $mData;
                    }

                    if( $mData->mType == "bookmaker"  ){
                        $matchOddData2[] = $mData;
                    }

                    if( $mData->mType == "fancy"  ){
                        $fancyData[] = $mData;
                    }

                    if( $mData->mType == "fancy2"  ){
                        $fancyData2[] = $mData;
                    }
                    if( $mData->mType == "fancy3"  ){
                        $fancyData3[] = $mData;
                    }

                    if( $mData->mType == "cricket_casino"  ){
                        $lotteryData[] = $mData;
                    }
                    if( $mData->mType == "khado"  ){
                        $khadoData[] = $mData;
                       
                    }
                      if( $mData->mType == "ballbyball"  ){
                        $ballbyballData[] = $mData;
                      
                    }

                }

                $matchDataNew = [
                    [ 'title' => 'Match Odd','dataList' => $matchOddData ],
                    [ 'title' => 'Bookmaker','dataList' => $matchOddData2 ],
                    [ 'title' => 'Fancy','dataList' => $fancyData ],
                    [ 'title' => 'Session','dataList' => $fancyData2 ],
                    [ 'title' => 'Other Market','dataList' => $fancyData3 ],
                    [ 'title' => 'Khado','dataList' => $khadoData ],
                    [ 'title' => 'Ballbyball','dataList' => $ballbyballData ],
                    [ 'title' => 'Cricket Casino','dataList' => $lotteryData ],
                    [ 'title' => 'Completed Match','dataList' => $completedMatch ],
                    [ 'title' => 'Tied Match','dataList' => $tiedMatch ]
                ];
            }

            $matchedDataArr = [
                "dataItems" => $matchDataNew,
                "count" => count($matchData)
            ];

      
            if( $unMatchData != null){
          
                $unMatchOddData = $unMatchCompletedMatch = $unMatchTiedMatch = $unMatchOddData2 = $unMatchFancyData = $unMatchFancyData2 = $unMatchLotteryData = $unMatchKhadoData = $unMatchBallbyballData = $unMatchFancyData3 = [];
                foreach ( $unMatchData as $unData ){

                    if( $unData->mType == "match_odd"  ){
                        $unMatchOddData[] = $unData;
                    }

                    if( $unData->mType == "completed_match"  ){
                        $unMatchCompletedMatch[] = $unData;
                    }

                     if( $unData->mType == "tied_match"  ){
                        $unMatchTiedMatch[] = $unData;
                    }

                    if( $unData->mType == "match_odd2"  ){
                        $unMatchOddData2[] = $unData;
                    }

                    if( $unData->mType == "fancy"  ){
                        $unMatchFancyData[] = $unData;
                    }

                    if( $unData->mType == "fancy2"  ){
                        $unMatchFancyData2[] = $unData;
                    }
                    if( $unData->mType == "fancy3"  ){
                        $unMatchFancyData3[] = $unData;
                    }

                    if( $unData->mType == "cricket_casino"  ){
                        $unMatchLotteryData[] = $unData;
                    }
                    if( $unData->mType == "khado"  ){
                        $unMatchKhadoData[] = $unData;
                    }
                     if( $unData->mType == "ballbyball"  ){
                        $unMatchKhadoData[] = $unData;
                    }

                }
        
                $unMatchDataNew = [
                    [ 'title' => 'Match Odd','dataList' => $unMatchOddData ],
                    [ 'title' => 'Match Odd 2','dataList' => $unMatchOddData2 ],
                    [ 'title' => 'Fancy','dataList' => $unMatchFancyData ],
                    [ 'title' => 'Session','dataList' => $unMatchFancyData2 ],
                    [ 'title' => 'Other Market','dataList' => $unMatchFancyData3 ],
                    [ 'title' => 'Khado','dataList' => $unMatchKhadoData ],
                    [ 'title' => 'Ballbyball','dataList' => $unMatchBallbyballData ],
                    [ 'title' => 'Cricket Casino','dataList' => $unMatchLotteryData ],
                    [ 'title' => 'Completed Match','dataList' => $unMatchCompletedMatch ],
                    [ 'title' => 'Tied Match','dataList' => $unMatchTiedMatch ]
                ];
            }


            $unMatchedDataArr = [
                "dataItems" => $unMatchDataNew,
                "count" => count($unMatchData)
            ];

            $items = [
                'matched'   =>  $matchedDataArr,
                'unmatched' =>  $unMatchedDataArr
            ];

        }else{
            $items = [
                'matched'   =>  $matchedDataArr,
                'unmatched' =>  $unMatchedDataArr
            ];
        }

        return $items;
  }

  
}