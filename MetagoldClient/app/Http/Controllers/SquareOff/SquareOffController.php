<?php

namespace App\Http\Controllers\SquareOff;
use DB;
use App\Models\Mcx;
use App\Models\OrderProfit;
use App\Helpers\LogActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class SquareOffController extends Controller
{

  public function orderProcessing(Request $request){

   
        $response = [ "status" => 0 , "code" => 400 , 'data' => null , "message" => "Bad Request !!" ];
       
        if(isset($request->instrumentToken) && isset($request->position)){
          $orders = DB::table('tbl_order')
                    ->select('*')
                    ->where([['status',1],['id',$request->position]])->first();


          $arr = [
            
            'betId' => $orders->id,
            'uid'=> $orders->uid,
            'name' => $orders->name,
            'price' =>$orders->price,
            'buy_price' => $orders->buy_price,
            'sell_price' =>$orders->sell_price,
            'instrumentToken'=> $orders->instrumentToken,
            'tradingSymbol'=>$orders->tradingSymbol,
            'stopLoss' => $orders->stopLoss,
            'takeProfit'=>$orders->takeProfit,
            'deviation'=>$orders->deviation,
            'lot'=>$orders->lot,
            'order_type'=>$orders->order_type,
            'trans_type'=>$orders->trans_type,
            'comm'=>$orders->comm,
            'margin'=>$orders->margin,
            'status'=>$orders->status,
            'expiry'=>$orders->expiry,
            'created_at'=>$orders->created_at,
            'updated_at'=>$orders->updated_at

          ];


         if($orders->status == 1 && $orders->comm == 0 ){

               $marketBlock = DB::table('tbl_mcxmarket2')->select('*')->where('tradingSymbol',$orders->tradingSymbol)->first();
                
                if(!empty($marketBlock) && $marketBlock->is_block == 1){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Market is closed now. You cannot SquareOff this order at this time!!"];
                    return $response; exit;
                }

                if($request->checkPrice != 0){
	                $difference = $this->getCurrentPrice($request->tradingSymbol,$request->instrumentToken,$request->trans_type,$request->order_type,$request->checkPrice);
	                if($difference >= 0){$diff = $difference;}else{$diff = (-1)*$difference;}
	               
	                if($diff > 5){

	                   $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Price has been changed try to enter current price again"];
	                    return $response; exit;
	                }
                }
              

             $manualProfit = OrderProfit::getprofit($orders->tradingSymbol,$orders->instrumentToken,$orders->name,$orders->order_type,$orders->trans_type,$orders->lot,$orders->buy_price,$orders->sell_price,$orders->price);
             $netProfit = $manualProfit['netProfit'];
             $closeBuyPrice = $manualProfit['currentBuyPrice'];
             $currentSellPrice = $manualProfit['currentSellPrice'];

             //$order = DB::table('tbl_order_history')->insert($arr);
             
            

             $status = [
                    'status'=>5,
                    'profit'=>$netProfit,
                    'closeBuyPrice'=>$closeBuyPrice,
                    'closeSellPrice'=>$currentSellPrice,
                    'updated_at'=>date('Y-m-d h:i:s')
                   ];

             $orderStatus = DB::table('tbl_order')->where([['id',$request->position],['uid',$orders->uid]])->update($status);
             $log = 'Order closed successfully';
               LogActivity::addTolog($log);
             $response = [ 'status'=> 1,'code'=>200,'message'=>'Order completed successfully!!'];

         }else{

          $response = [ 'status'=> 1,'code'=>200,'message'=>'This Order is already squared off .Please check you Statement!!'];
         }
     }

       
   return $response;

  }

  public function getCurrentPrice($tradingSymbol,$instrumentToken,$trans_type,$order_type ,$checkPrice){
       
       $cache = Redis::connection();
       $key = 'Mcx_Market';
       $result=$cache->get($key);
       $arrData = json_decode($result);
       if(!empty($arrData)){
       foreach ($arrData as $key => $val) {
          
         if($key == $tradingSymbol && $val->instrument_token == $instrumentToken){
              $depth = $val->depth;
              $currentBuy = $depth->buy[0]->price;
              $currentSell = $depth->sell[0]->price;      
                         
          }
       }
          
          if($order_type == 'instant_execution'){
             if($trans_type == 'BUY'){
                $difference = $currentBuy - $checkPrice;
              }else{
                 $difference = $currentSell-$checkPrice;
              }
          }elseif($order_type == 'buy_limit'){

               $difference = $currentBuy - $checkPrice;

          }elseif($order_type == 'sell_limit'){
                $difference = $currentSell-$checkPrice;
          }
      }else{
        $difference =0;
      }
       
       return $difference;
    }



 public function cronOrderProcessing(Request $request){

   
        $response = [ "status" => 0 , "code" => 400 , 'data' => null , "message" => "Bad Request !!" ];

         $orders = DB::table('tbl_order')
                    ->select('*')
                    ->where([['status',1],['uid',122]])->get();

        $date = date('Y-m-d h:i:s');
        foreach ($orders as $key => $val) {

          print_r($val); die('ppp');
           

        }

        print_r($orders); die('aaaaaaaa');
       die('corn order procesing');
       
   return $response;

  }














}












?>