<?php
namespace App\Http\Controllers\Pdf;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class PdfController extends Controller
{
   public function removeStatement(Request $Request){

         $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];
        /* $month = date('m');
         $table_name = 'Month_'.$month;

         $fields = [
             ['name' => 'stat_id', 'type' => 'integer'],
             ['name' => 'clientId', 'type' => 'integer'],
             ['name' => 'userId', 'type' => 'integer'],
             ['name' => 'childId', 'type' => 'integer'],
             ['name' => 'parentId', 'type' => 'integer'],
             ['name' => 'eid', 'type' => 'integer'],
             ['name' => 'eType', 'type' => 'integer'],
             ['name' => 'mtype', 'type' => 'string'],
             ['name' => 'amount', 'type' => 'string'],
             ['name' => 'type', 'type' => 'string'],
             ['name' => 'balance', 'type' => 'string'],
             ['name' => 'name', 'type' => 'string'],
             ['name' => 'tradingSymbol', 'type' => 'string'],
             ['name' => 'order_type', 'type' => 'string'],
             ['name' => 'trans_type', 'type' => 'string'],
             ['name' => 'lot', 'type' => 'string'],
             ['name' => 'price', 'type' => 'string'],
             ['name' => 'buyPrice', 'type' => 'string'],
             ['name' => 'sellPrice', 'type' => 'string'],
             ['name' => 'closeBuyPrice', 'type' => 'string'],
             ['name' => 'closeSellPrice', 'type' => 'string'],
             ['name' => 'stopLoss', 'type' => 'string'],
             ['name' => 'takeProfit', 'type' => 'string'],
             
        ];

        $this->createTable($table_name, $fields);
*/
        $data = Transaction::getStatement();
       // print_r($data); die('fhgjhfgjh');
        foreach ($data as $val) {
           
           $transaction = new Transaction($table_name);
           $transaction->stat_id = $val->id;
           $transaction->clientId = $val->clientId;
           $transaction->userId = $val->userId;
           $transaction->childId = $val->childId;
           $transaction->eid = $val->eid;
           $transaction->eType =$val->eType;
           $transaction->mtype = $val->mType;
           $transaction->amount = $val->amount;
           $transaction->type = $val->type;
           $transaction->balance = $val->balance;
           $transaction->name = $val->name;
           $transaction->tradingSymbol = $val->tradingSymbol;
           $transaction->order_type = $val->order_type;
           $transaction->trans_type = $val->trans_type;
           $transaction->lot =$val->lot;
           $transaction->price = $val->price;
           $transaction->buyPrice =$val->buyPrice;
           $transaction->sellPrice = $val->sellPrice;
           $transaction->closeBuyPrice = $val->closeBuyPrice;
           $transaction->closeSellPrice =$val->closeSellPrice;
           $transaction->stopLoss = $val->stopLoss;
           $transaction->takeProfit = $val->takeProfit;
           $transaction->created_at = $val->created_on;
           $transaction->updated_at = $val->updated_on;

           $exist = DB::table($table_name)->where('stat_id',$val->id)->first();

           if(empty($exist)){
             $transaction->save(); 
             }	
        }

        	$response = ['status' => 1,'code'=>200,'data'=>null,'message' => 'Data Found !'];
       
    return $response;
   }

  public function statementGeneration(Request $request)
  {
    $response = ['status'=>1 ,'code'=>404, 'message'=>'Bad Request !!'];
    $month = date('m');
    $table_name = 'Month_07';//'Month_'.$month;
    $uid = Auth::user()->id;
    $clientName =Auth::user()->username;
    $parentId = Auth::user()->parentId;
    $openingBalance=0;
    $closingBalance=0;
    $today = date('Y-m-d');
    /*$closingDate = date('Y-m-d', strtotime($today . " - 1 day"));
    $firstDayofLastMonth = new DateTime("first day of last month");
    $openningDate = $firstDayofLastMonth->format('Y-m-d');*/
    $openningDate = $request->start_date;
    $closingDate = $request->end_date;
    $last_row = DB::table($table_name)->where('userId',$uid)->latest()->first();

    if(!empty($last_row)){
      $openingBalance = $last_row->balance;
    }
    $first_row = DB::table($table_name)->where('userId',$uid)->orderBy('id','DESC')->first();
    if(!empty($first_row)){
      $closingBalance = $first_row->balance;
    }

       if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){      
                    $startDate = date('yy-m-d', strtotime($request->start_date));
                    $startDate =$startDate." 00:00:01";
                    $endDate = date('yy-m-d', strtotime($request->end_date));
                    $endDate =$endDate." 23:59:59";
        }else{
                $start = new \DateTime('now +1 day');
                $endDate =  $start->format('yy-m-d h:i:s');
                $end = new \DateTime('now -5 day');
                $startDate = $end->format('yy-m-d h:i:s');  
        }

   // print_r($startDate); echo "string"; print_r($endDate);
    $data = DB::table($table_name)->where('userId',$uid)
                ->whereBetween('created_at',[$startDate, $endDate])
                ->get();

    $profitLoss=0;
    foreach ($data as $val) {
    
    	if($val->type == 'CREDIT'){
           $profitLoss+=$val->amount;
    	}elseif($val->type == 'DEBIT'){
           $profitLoss+=(-1)*$val->amount;
    	}
    	
    }
    //print_r($profitLoss); die('mvnbmvnbmnv');
    
    $response = ['status'=>1 ,'code'=>200, 'data'=>$data,'openingBalance'=>$openingBalance,'closingBalance'=>$closingBalance, 'profitLoss'=>$profitLoss,'startDate'=>$openningDate,'endDate'=>$closingDate,'clientName'=>$clientName,'clientId'=>$uid,'parentId'=>$parentId, 'message'=>'Data Found'];
    return $response;

  }

public function createTable($table_name, $fields = [])
{
    
    if (!Schema::hasTable($table_name)) {
        Schema::create($table_name, function (Blueprint $table) use ($fields, $table_name) {
            $table->increments('id');
            if (count($fields) > 0) {
                foreach ($fields as $field) {
                    $table->{$field['type']}($field['name'])->nullable();
                }
            }
            $table->timestamps();
        });

        return response()->json(['message' => 'Given table has been successfully created!'], 200);
    }

    return response()->json(['message' => 'Given table is already existis.'], 400);
}



}



?>