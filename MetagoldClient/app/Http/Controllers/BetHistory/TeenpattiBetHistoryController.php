<?php

namespace App\Http\Controllers\BetHistory;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;


class TeenpattiBetHistoryController extends Controller
{

  public function TeenpattiBetHistory(Request $request)
    {       
       
      try{

         $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
         $uid = Auth::user()->id;
       
        if( json_last_error() == JSON_ERROR_NONE ){
           
            if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){
               
                        $startDate = date('yy-m-d', strtotime($request->start_date));
                        $startDate =$startDate." 00:00:01";                     
                        $endDate = date('yy-m-d', strtotime($request->end_date));
                        $endDate =$endDate." 23:59:59";
             }else{
                $start = new \DateTime('now +1 day');
                $endDate =  $start->format('yy-m-d h:i:s');
                $end = new \DateTime('now -5 day');
                $startDate = $end->format('yy-m-d h:i:s'); 
             }
              
         $where = ([['status',1],['uid',$uid]]);
        
         $query = DB::table('tbl_bet_history_teenpatti')->select('id','price','size','win','rate','loss','mType','bType','result','description','created_on','updated_on')
                  ->where($where)
                  ->whereIn('status',[0,1])
                  ->whereIn('result',['WIN','LOSS'])
                  ->orderBy('created_on' ,'DESC');
              

             if(isset($request->isFirst) && $request->isFirst == 1){
                $betList = $query->limit(10)->get();
              }else{
                $betList = $query->whereBetween('created_on',[$startDate, $endDate])->get();
              }

           $models=[];    
           if( $betList != null ){
            foreach ( $betList as $data ){
                $models[]=$data;
             }
           }
  
           if( $models != null ){

              $response = [ "status" => 1 ,'code'=> 200, "data" => ['items'=> $models ] ,'message'=> 'Data Found !!' ];
           }else{
              $response = [ "status" => 1 ,'code'=> 200, "data" => null ,'message'=>'Data not found !!' ];
           }
             
    }

   return $response;
   
  }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
 }

}
