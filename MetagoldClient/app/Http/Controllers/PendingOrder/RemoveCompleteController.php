<?php

namespace App\Http\Controllers\PendingOrder;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\PlaceOrder;
use Illuminate\Support\Facades\Redis;

class RemoveCompleteController extends Controller
{


       public function removeOrder(Request $request)
       {   
     
             $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
             $order = PlaceOrder::where([['comm',1],['margin',1],['status',5]])->get();

             foreach ($order as $val) {
             
                 $arr = [
                
                    'betId' => $val->id,
                    'systemId'=> $val->systemId,
                    'uid'=> $val->uid,
                    'client'=>$val->client,
                    'master'=>$val->master,
                    'name' => $val->name,
                    'price' =>$val->price,
                    'buy_price' => $val->buy_price,
                    'sell_price' =>$val->sell_price,
                    'instrumentToken'=> $val->instrumentToken,
                    'tradingSymbol'=>$val->tradingSymbol,
                    'stopLoss' => $val->stopLoss,
                    'takeProfit'=>$val->takeProfit,
                    'deviation'=>$val->deviation,
                    'closeBuyPrice'=>$val->closeBuyPrice,
                    'closeSellPrice'=>$val->closeSellPrice,
                    'lot'=>$val->lot,
                    'order_type'=>$val->order_type,
                    'trans_type'=>$val->trans_type,
                    'orderMargin'=>$val->orderMargin,
                    'commission'=>$val->commission,
                    'comm'=>$val->comm,
                    'margin'=>$val->margin,
                    'status'=>$val->status,
                    'expiry'=>$val->expiry,
                    'ip_address'=>$val->ip_address,
                    'created_at'=>$val->created_at,
                    'updated_at'=>$val->updated_at,
                    'balanceLow'=>$val->balanceLow

                  ];

                  DB::table('tbl_order_history')->insert($arr);
                  DB::table('tbl_order')->where('id',$val->id)->delete();
         	    
             }

            $response = ['status'=>1,'code'=>200,'message'=>'Removed Successfully!!'];
            return $response;

        }
  }


?>