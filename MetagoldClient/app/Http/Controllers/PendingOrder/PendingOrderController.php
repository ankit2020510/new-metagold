<?php

namespace App\Http\Controllers\PendingOrder;
use App\User;
use DB;
use App\Models\PendingOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\PlaceOrder;
use Illuminate\Support\Facades\Redis;

class PendingOrderController extends Controller
{


       public function pendingOrder(Request $request)
       {   
     
             $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
     	  $startTime = time();
        $endTime = time() + 60;
        while ($endTime > time()) {
            usleep(200000);

     	     $orders = PendingOrder::getList();
         //echo "<pre>"; print_r($orders); 
          if(!empty($orders)){
     	     foreach ($orders as $key => $val) {

              $depth = $this->getCurrentPrice($val->tradingSymbol,$val->instrumentToken);
              $currentBuyPrice = $depth['currentBuy'];
              $currentSellPrice = $depth['currentSell'];
     	     	if($val->order_type == 'buy_limit'){

                    if($val->price > $val->buy_price){
                        if($currentBuyPrice >= $val->price){

                          $status = [ 'status'=> 1];
                          PendingOrder::where([['uid',$val->uid],['id',$val->id]])->update($status);

                         $update = $this->updateTransaction($val->uid,$val->id,$val->commission,$val->order_type,$val->trans_type,$val->lot,$val->sell_price,$val->buy_price,$val->price,$val->stopLoss,$val->takeProfit,$val->name,$val->tradingSymbol,$val->orderMargin); 
                         $update1 = $this->updateCommission($val->uid,$val->commission);
                        }
                    }else{
                      if($currentBuyPrice <= $val->price){

                           $status = [ 'status'=> 1];
                          PendingOrder::where([['uid',$val->uid],['id',$val->id]])->update($status);

                         $update = $this->updateTransaction($val->uid,$val->id,$val->commission,$val->order_type,$val->trans_type,$val->lot,$val->sell_price,$val->buy_price,$val->price,$val->stopLoss,$val->takeProfit,$val->name,$val->tradingSymbol,$val->orderMargin); 

                         $update1 = $this->updateCommission($val->uid,$val->commission);
                      }
                    }
     	     
     	     }else{
                 if($val->price > $val->sell_price){
                        if($currentSellPrice >= $val->price){

                          $status = [ 'status'=> 1];
                          PendingOrder::where([['uid',$val->uid],['id',$val->id]])->update($status);

                         $update = $this->updateTransaction($val->uid,$val->id,$val->commission,$val->order_type,$val->trans_type,$val->lot,$val->sell_price,$val->buy_price,$val->price,$val->stopLoss,$val->takeProfit,$val->name,$val->tradingSymbol,$val->orderMargin); 

                         $update1 = $this->updateCommission($val->uid,$val->commission);
                        }
                    }else{
                      if($currentSellPrice <= $val->price){

                           $status = [ 'status'=> 1];
                          PendingOrder::where([['uid',$val->uid],['id',$val->id]])->update($status);

                         $update = $this->updateTransaction($val->uid,$val->id,$val->commission,$val->order_type,$val->trans_type,$val->lot,$val->sell_price,$val->buy_price,$val->price,$val->stopLoss,$val->takeProfit,$val->name,$val->tradingSymbol,$val->orderMargin); 
                         $update1 = $this->updateCommission($val->uid,$val->commission);
                      }
                    }
                 }

     	     }
          
         $response = ['status'=>1,'code'=>200,'message'=>'successfully updated!!'];
         //return $response;
      }else{
          $response = ['status'=>1,'code'=>200,'message'=>'Something went wrong!!'];
         //return $response;
      }
    }
    return $response;
  }

public function getCurrentPrice($tradingSymbol,$instrumentToken){
       
       $cache = Redis::connection();
       $key = 'Mcx_Market';
       $result=$cache->get($key);
       $arrData = json_decode($result);
       $currentBuy=0;
       $currentSell=0;
       if(!empty($arrData)){
       foreach ($arrData as $key => $val) {
          
         if($key == $tradingSymbol && $val->instrument_token == $instrumentToken){
              $depth = $val->depth;
              $currentBuy = $depth->buy[0]->price;
              $currentSell = $depth->sell[0]->price;      
                         
          }
       }
          $depth = ['currentBuy'=>$currentBuy,'currentSell'=>$currentSell];

          return $depth;
        }else{

          return 0;
        }
       
    }

  /*public function getCurrentPrice($instrumentToken){

         
        $depth = DB::table('tbl_mcxmarket2') ->select('depth')->where([['status',1],['instrument_token',$instrumentToken]])->first();
        if(!empty($depth)){
          $newDepth = json_decode($depth->depth);
         
          $currentBuy = $newDepth->buy[0]->price;   
          $currentSell = $newDepth->sell[0]->price;
        
          $depth = ['currentBuy'=>$currentBuy,'currentSell'=>$currentSell];

          return $depth; 
       }else{
          return 'depth not found'; 
       }
      
   }*/


    public function updateTransaction($uid,$orderid,$profit,$orderType,$trans_type,$lot,$sell_price,$buy_price,$price,$stopLoss,$takeProfit,$name,$tradingSymbol,$margin) {

       $amount = round($profit,0);
       
       //if( $amount > 0 ){ $type = 'CREDIT'; }else{ $amount = (-1)*$amount; $type = 'DEBIT'; }

       $summaryData = [];

       
       $cUser = DB::table('tbl_user')->select(['parentId','systemId'])->where([['id',$uid]])->first();
       $cUserInfo = DB::table('tbl_user_info')->select(['balance','pl_balance','expose'])->where([['uid',$uid]])->first();
       
        $parentId = $cUser->parentId;
        $systemId = $cUser->systemId;
        $type = 'DEBIT';
        $balance = $this->getCurrentBalanceClientNew($uid,$profit,$type,$cUserInfo->balance,$cUserInfo->pl_balance,'CLIENT',$margin,$cUserInfo->expose);
                  

          if($orderType == 'instant_execution'){
                         if($trans_type == 'BUY'){
                           $prc = $buy_price;
                           $description = $name.' > '.$lot.' > '.$trans_type.' AT '.$prc;
                         }else{
                           $prc = $sell_price;
                           $description = $name.' > '.$lot.' > '.$trans_type.' AT '.$prc;
                         }
                       }elseif($orderType == 'buy_limit' || $orderType == 'sell_limit'){
                           if($trans_type == 'BUY'){
                             $prc = $price;
                             $description = $name.' > '.$lot.' > '.$trans_type.' AT '.$prc;
                         }else{
                              $prc = $price;
                             $description = $name.' > '.$lot.' > '.$trans_type.' AT '.$prc;
                         }
                          
                       }


        $resultArr = [
            'systemId' => $systemId,
            'clientId' => $uid,
            'userId' => $uid,
            'childId' => 0,
            'parentId' => $parentId,
            'sid' => 0,
            'eid' => $orderid,
            'mid' => 0,
            'eType' => 3,
            'mType' => 0,
            'trans_type'=>$trans_type,
            'order_type'=>$orderType,
            'lot'=>$lot,
            'buyPrice'=>$buy_price,
            'sellPrice'=>$sell_price,
            'closeBuyPrice'=>0,
            'closeSellPrice'=>0,
            'stopLoss'=>$stopLoss,
            'takeProfit'=>$takeProfit,
            'price'=>$price,
            'tradingSymbol'=>$tradingSymbol,
            'name'=>$name,
            'type' => $type,
            'amount' => round($amount),
            'p_amount' => 0,
            'c_amount' => 0,
            'balance' => $balance,
            'description' => $description,
            'remark' => 'Commission Entry',
            'status' => 1,
        ];

        DB::table('tbl_transaction_client')->insert($resultArr);

      
            $type = $type;
            $ownPl = $amount;
            $parentPl = 0;
            $parentComm = 0;
            $ownComm = 0;
       // $this->userSummaryUpdate($summaryData,'CLIENT');
        $this->userSummaryUpdateNew($uid,$ownPl,$parentPl,$type,$ownComm,$parentComm);
       // if( $type == 'CREDIT' ){ $pType = 'DEBIT'; }else{ $pType = 'CREDIT'; }
        $pType = "CREDIT";
        $parentUserData = DB::table('tbl_user_profit_loss as pl')
            ->select(['pl.userId as userId','pl.actual_profit_loss as apl','pl.profit_loss as gpl','ui.balance as balance','ui.pl_balance as pl_balance'])
            ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
            ->where([['pl.clientId',$uid],['pl.actual_profit_loss','!=',0]])->orderBy('pl.userId','desc')->get();

        $childId = $uid;
       // print_r($parentUserData); echo "strinnnnnnnnnn";
        $summaryData = [];

        foreach ( $parentUserData as $user ){

            $cUser = DB::table('tbl_user')->select(['parentId','role'])->where([['id',$user->userId]])->first();


           
            $parentId = $cUser->parentId;
            
            $transactionAmount = ( $amount*$user->apl )/100;
            $pTransactionAmount = ( $amount*(100-$user->gpl) )/100;
            $cTransactionAmount = ( $amount*($user->gpl-$user->apl) )/100;
            $balance = $this->getCurrentBalance($user->userId,$transactionAmount,$pType,$user->balance,$user->pl_balance,'PARENT');
              


            $resultArr = [
                'systemId' => $systemId,
                'clientId' => $uid,
                'userId' => $user->userId,
                'childId' => $childId,
                'parentId' => $parentId,
                'sid' => 0,
                'eid' => $orderid,
                'mid' => 0,
                'eType' => 0,
                'mType' => 0,
                'lot'=>$lot,
                'buyPrice'=>$buy_price,
                'sellPrice'=>$sell_price,
                'trans_type'=>$trans_type,
                'order_type'=>$orderType,
                'closeBuyPrice'=>$buy_price,
                'closeSellPrice'=>$sell_price,
                'stopLoss'=>$stopLoss,
                'takeProfit'=>$takeProfit,
                'price'=>$price,
                'tradingSymbol'=>$tradingSymbol,
                'name'=>$name,
                'type' => $pType,
                'amount' => round($transactionAmount),
                'p_amount' => round($pTransactionAmount),
                'c_amount' => round($cTransactionAmount),
                'balance' => $balance,
                'description' => $description,
                'remark' => 'Commission Entry',
                'status' => 1,
            ];

            if( $cUser->role == 1 ){
                DB::table('tbl_transaction_admin')->insert($resultArr);
            }else{
                DB::table('tbl_transaction_parent')->insert($resultArr);
            }

            $uid = $user->userId;
            $type = $pType;
            $ownPl = $transactionAmount;
            $parentPl = $pTransactionAmount;
            $parentComm = 0;
            $ownComm = 0;
            //$this->userSummaryUpdate($summaryData,null);
            $this->userSummaryUpdateNew($uid,$ownPl,$parentPl,$type,$ownComm,$parentComm);
            $childId = $user->userId;

        $SummaryData1 = DB::table('tbl_settlement_summary')->select('ownPl','ownComm')
                ->where( 'uid', $user->userId)->first();
            if(!empty($SummaryData1)){
                $updated_on = date('Y-m-d H:i:s');
                $ubalance=0;
                $ubalance= $SummaryData1->ownPl+ $SummaryData1->ownComm;
                DB::table('tbl_user_info')->where([['uid',$user->userId]])
                    ->update(['pl_balance' => $ubalance,'updated_on'=>$updated_on]);
            }

        }
         
        
       //print_r($cUser); echo "string"; print_r($cUserInfo); echo "string"; print_r($balance);
        
        
    }

//user Summary Update New
    public static function userSummaryUpdateNew($uid,$ownPl,$parentPl,$type,$ownComm,$parentComm){
        
        $userSummary = DB::table('tbl_settlement_summary')
            ->where([['status',1],['uid',$uid]])->first();
        if( $userSummary != null ){

            $updatedOn = date('Y-m-d H:i:s');
            if( isset( $type ) && $type != 'CREDIT' ){
                $ownPl = (-1)*round($ownPl,2);
                $parentPl = (-1)*round($parentPl,2);
                $ownComm = (-1)*round($ownComm,2);
                $parentComm = (-1)*round($parentComm,2);
            }

            $updateArr = [
                'ownPl' => DB::raw('ownPl + ' . $ownPl),
                'ownComm' => DB::raw('ownComm + ' . $ownComm),
                'parentPl' => DB::raw('parentPl + ' . $parentPl),
                'parentComm' => DB::raw('parentComm + ' . $parentComm),
                'updated_on' => $updatedOn
            ];

            if( DB::table('tbl_settlement_summary')
                ->where([['status',1],['uid',$uid]])->update($updateArr) ){
                $updateArr1 = [ 'uid' => $uid,'updated_on' => $updatedOn,
                    'ownPl' => $ownPl, 'parentPl' => $parentPl,
                    'ownComm' => $ownComm, 'parentComm' => $parentComm];

                //$array = array('function' => 'settelement', 'Query' => $updateArr1);
                //$this->activityLog(json_encode($array));
            }else{
                $userSummary = DB::table('tbl_settlement_summary')
                    ->where([['status',1],['uid',$uid]])->first();
                $updatedOn = date('Y-m-d H:i:s');
                $betTimeDiff = (strtotime($updatedOn)-strtotime($userSummary->updated_on) );
                if($betTimeDiff == 0) {
                    usleep(1000);
                    $userSummary = DB::table('tbl_settlement_summary')
                        ->where([['status',1],['uid',$uid]])->first();
                }
                $updateArr = [
                    'ownPl' => $userSummary->ownPl+$ownPl,
                    'ownComm' => $userSummary->ownComm+$ownComm,
                    'parentPl' => $userSummary->parentPl+$parentPl,
                    'parentComm' => $userSummary->parentComm+$parentComm,
                    'updated_on' => date('Y-m-d H:i:s')
                ];
                if( DB::table('tbl_settlement_summary')
                    ->where([['status',1],['uid',$uid]])->update($updateArr) ){
                    $updateArr1 = [ 'uid' => $uid,'updated_on'=>$updatedOn,
                        'ownPl' => $ownPl, 'parentPl'=>$parentPl,
                        'ownComm' => $ownComm, 'parentComm' => $parentComm];

                    //$array = array('function' => 'settelement', 'Query' => $updateArr1);
                    //$this->activityLog(json_encode($array));
                }
            }

        }else{
            $updatedOn = date('Y-m-d H:i:s');
            if( isset( $type ) && $type != 'CREDIT' ){
                $ownPl = (-1)*round($ownPl,2);
                $parentPl = (-1)*round($parentPl,2);
                $ownComm = (-1)*round($ownComm,2);
                $parentComm = (-1)*round($parentComm,2);
            }

            $insertArr = [  'uid' => $uid, 'updated_on' => $updatedOn,
                'ownPl' => $ownPl, 'ownComm' => $ownComm,
                'parentPl' => $parentPl,'parentComm' => $parentComm ];

            DB::table('tbl_settlement_summary')->insert($insertArr);

        }
    }
    
 // get Current Balance admin/Super master and master
    public static function getCurrentBalance($uid,$amount,$type,$balance,$plBalance,$role)
    {
        
          
          $updateArr = [
            'pl_balance' => DB::raw('pl_balance + ' . round($amount,0))
        ];

        DB::table('tbl_user_info')->where('uid', $uid)->update($updateArr);
        $cUserInfo = DB::table('tbl_user_info')->select(['balance','pl_balance'])->where([['uid',$uid]])->first();

        $balance = $cUserInfo->balance;
        $plBalance = $cUserInfo->pl_balance;
        if( $role != 'CLIENT' ){
            return round($plBalance,0);
        }else{
            if($plBalance > 0){
                return round(($balance + $plBalance),0);
            }else{
                return round(($balance - $plBalance),0);
            }

        }
    }
// get Current Balance New
    public static function getCurrentBalanceClientNew($uid,$amount,$type,$balance,$plBalance,$role,$margin,$expose)
    {
        //$conn = DB::connection('mysql2');
        //if( $type != 'CREDIT' ){ $amount = (-1)*$amount; }
        $updated_on = date('Y-m-d H:i:s');
        $updateArr = [
            'pl_balance' => DB::raw('pl_balance - ' . round($amount,0)),
            'expose' => DB::raw('expose + ' . round($margin)),
            'updated_on' => $updated_on
        ];

        if( DB::table('tbl_user_info')->where('uid', $uid)->update($updateArr) ){
            $cUserInfo = DB::table('tbl_user_info')
                ->select('balance','pl_balance','updated_on')->where('uid',$uid)->first();
            $balance = $cUserInfo->balance;
            $plBalance = $cUserInfo->pl_balance;

            if( $role != 'CLIENT' ){
                return round($balance,0);
            }else{
                return round(($balance + $plBalance),0);
            }
        }else{
            $updated_on = date('Y-m-d H:i:s');
            $cUserInfo = DB::table('tbl_user_info')
                ->select(['balance','pl_balance','updated_on'])->where([['uid',$uid]])->first();
            $betTimeDiff = (strtotime($updated_on)-strtotime($cUserInfo->updated_on) );
            if($betTimeDiff == 0) {
                usleep(1000);
                $cUserInfo = DB::table('tbl_user_info')
                    ->select(['balance','pl_balance','updated_on'])->where([['uid',$uid]])->first();
            }

            $balance = $cUserInfo->balance;
            $newPlBalance = $cUserInfo->pl_balance;

            $updateArr = [
                'pl_balance' => $newPlBalance,
                'expose'  => $margin,
                'updated_on' => $updated_on
            ];

            if( DB::table('tbl_user_info')
                ->where([['uid', $uid]])->update($updateArr) ){
                if( $role != 'CLIENT' ){
                    return round($balance,0);
                }else{
                    return round(($balance + $newPlBalance),0 );
                }
            }
        }
    }
public function updateCommission($uid,$profit) {

       $amount = round($profit,0);       
       $cUser = DB::table('tbl_user')->select(['parentId','systemId'])->where([['id',$uid]])->first();
       $cUserInfo = DB::table('tbl_user_info')->select(['balance','pl_balance','expose','pl_commission'])->where([['uid',$uid]])->first();
       
        $parentId = $cUser->parentId;
        $systemId = $cUser->systemId;
        $type = 'DEBIT';
        $balance = $this->getCommissionClient($uid,$profit,$type,$cUserInfo->pl_commission,'CLIENT');
                  
        $pType = "CREDIT";
        $parentUserData = DB::table('tbl_user_profit_loss as pl')
            ->select(['pl.userId as userId','pl.actual_profit_loss as apl','pl.actual_commission as aplc','pl.commission as gplc','pl.profit_loss as gpl','ui.balance as balance','ui.pl_balance as pl_balance','ui.pl_commission as pl_commission'])
            ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
            ->where([['pl.clientId',$uid],['pl.actual_commission','!=',0]])->orderBy('pl.userId','desc')->get();

        $childId = $uid;
       // print_r($parentUserData); echo "strinnnnnnnnnn";
        $summaryData = [];

        foreach ( $parentUserData as $user ){

            $cUser = DB::table('tbl_user')->select(['parentId','role'])->where([['id',$user->userId]])->first();
            $parentId = $cUser->parentId;
            
            $transactionAmount = ( $amount*$user->aplc )/100;
            $pTransactionAmount = ( $amount*(100-$user->gplc) )/100;
            $cTransactionAmount = ( $amount*($user->gplc-$user->aplc) )/100;
            $balance = $this->getCommissionParent($user->userId,$transactionAmount,$pType,$user->pl_commission,'PARENT');
        }
         
}


// update commission
    public static function getCommissionClient($uid,$amount,$type,$plCommission,$role)
    {
        $updated_on = date('Y-m-d H:i:s');
        $updateArr = [
            'pl_commission' => DB::raw('pl_commission - ' . round($amount,0)),
            'updated_on' => $updated_on
        ];

         DB::table('tbl_user_info')->where('uid', $uid)->update($updateArr);
           
    }

public static function getCommissionParent($uid,$amount,$type,$plCommission,$role)
    {
        
          
          $updateArr = [
            'pl_commission' => DB::raw('pl_commission + ' . round($amount,0))
        ];

        DB::table('tbl_user_info')->where('uid', $uid)->update($updateArr);
        $cUserInfo = DB::table('tbl_user_info')->select(['balance','pl_balance','pl_commission'])->where([['uid',$uid]])->first();

    }


 }























?>