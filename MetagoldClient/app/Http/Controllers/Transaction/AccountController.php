<?php

namespace App\Http\Controllers\Transaction;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;



class AccountController extends Controller
{

    public function accountStatement(Request $request)
    {

     try{
        
         $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
         $userId = Auth::user()->id;

            if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){
                  
                        $startDate = date('yy-m-d', strtotime($request->start_date));
                        $startDate =$startDate." 00:00:01";
                        $endDate = date('yy-m-d', strtotime($request->end_date));
                        $endDate =$endDate." 23:59:59";
              
            }else{
                
                $start = new \DateTime('now +1 day');
                $endDate =  $start->format('yy-m-d h:i:s');
                $end = new \DateTime('now -5 day');
                $startDate = $end->format('yy-m-d h:i:s');  
                
            }
              
                   
           $accountQuery = DB::table('tbl_transaction_client')->select('id','sid','description','userId','type','created_on','amount','balance','mid','mType')
                          ->where([['userId',$userId],['status',1]])
                          ->orderBy('id','DESC');
                          
              if(isset($request->isFirst) && $request->isFirst != 1){
                $accountQuery = $accountQuery->whereBetween('created_on',[$startDate, $endDate])->get();
              }else{
                $accountQuery = $accountQuery->limit(10)->get();
              }

            if(!$accountQuery->isEmpty()){

            	foreach ($accountQuery as  $acQuery) {


                         $description = $acQuery->description;

                     if(strpos($acQuery->description,'>')){
                       $desc = explode(">", $acQuery->description);
                       //print_r($desc); die();
                       $description = $desc[0].'>'.$desc[1];
                       if(isset($desc[3])){
                       $description = $desc[0].'>'.$desc[1].'>'.$desc[3];
                       }
                     }
                     if($acQuery->mType == 'cricket_casino'){
                       $description = $acQuery->description;
                     }
                 
            		$arr[] = [
                          
                          'id'  => $acQuery->id,
                          'created_on'  => $acQuery->created_on,
                          'description'  => $description,
                          'type'  => $acQuery->type,
                          'amount'  => $acQuery->amount,
                          'balance'  => $acQuery->balance,
                          'marketId'  => $acQuery->mid,
                          'sid'  => $acQuery->sid      
             
            		];
            	
            	}

            	 $response = [ "status" => 1 ,'code'=> 200, "data" => $arr ,'message'=> 'Data Found !!' ];
            }else{

            	 $response = [ "status" => 1 ,'code'=> 200, "data" => null ,'message'=> 'Data Not Found !!' ];
            }



      return $response;

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


     public function accountBetList(Request $request)
     {

         $response =  response()->json([ "status" => 0 , "code" => 400 , "message" => "Bad request!" ]);

        try{

         $userId = Auth::user()->id;

           
            if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){
                   
                        $startDate = date('yy-m-d', strtotime($request->start_date));
                        $startDate =$startDate." 00:00:01";
                     
                        $endDate = date('yy-m-d', strtotime($request->end_date));
                        $endDate =$endDate." 23:59:59";
                    
            }else{
                
                 $start = new \DateTime('now +1 day');
                $endDate =  $start->format('yy-m-d h:i:s');

                $end = new \DateTime('now -5 day');
                $startDate = $end->format('yy-m-d h:i:s');  
                
            }
            
       
         if( isset( $request->market_id )  && $request->market_id != ' ' && !isset($request->type)){
        

                $BetList =  DB::table('tbl_bet_history')->select('betId','rate','win','loss','description','bType','mType','created_on','updated_on','price','size','result','diff','market')
                          ->where([['uid',$userId],['status',1],['mid',$request->market_id]])
                          ->whereIn('result',['WIN','LOSS'])
                           //->whereBetween('created_on',[$startDate, $endDate])
                          ->get();
                      
                 if(!$BetList->isEmpty() ){

                 foreach ($BetList as $list) {

                 	if($list->result == 'WIN'){

                 		$profitLoss = $list->win;

                 	}else{
                 		$profitLoss = $list->loss;
                 	}



                 	$listArr = [

                        'betId' => $list->betId,
                        'description' => $list->description,
                        'bType' => $list->bType,
                         'price' => $list->price,
                         'size' => $list->size,
                         'profitLoss' => $profitLoss,
                         'date'  =>  $list->updated_on,
                         'result' => $list->result,
                         'win'  =>$list->win,
                         'loss' => $list->loss,
                         'rate' => $list->rate,
                         'mType' => $list->mType
                 	];
                  if($list->mType == 'khado'){
                     
                     $listArr['difference'] = $list->diff - $list->price;
                  }
                  if($list->mType == 'ballbyball'){
                        $market = $list->market;
                        $space = ' ';
                        $ball = strstr($market,$space,true);        
                        $listArr['ball']= $ball;
                     
                  }
                 	$lists[] = $listArr;
                 }

           $response = response()->json([ "status" => 1 ,'code'=> 200, "data" => $lists ,'message'=> 'Data Found !!' ]);
          }else{

         $response = response()->json([ "status" => 1 ,'code'=> 200, "data" => null ,'message'=> 'Data Found !!' ]);
         
         }

        }else{

            $BetList =  DB::table('tbl_bet_history_teenpatti')->select('id','rate','win','loss','description','bType','mType','created_on','updated_on','price','size','result','diff','market')
                          ->where([['uid',$userId],['status',1],['mid',$request->market_id]])
                          ->whereIn('result',['WIN','LOSS'])
                           //->whereBetween('created_on',[$startDate, $endDate])
                          ->get();
                      
                 if(!$BetList->isEmpty() ){

                 foreach ($BetList as $list) {

                  if($list->result == 'WIN'){

                    $profitLoss = $list->win;

                  }else{
                    $profitLoss = $list->loss;
                  }



                  $listArr[] = [

                        'betId' => $list->id,
                        'description' => $list->description,
                        'bType' => $list->bType,
                         'price' => $list->price,
                         'size' => $list->size,
                         'profitLoss' => $profitLoss,
                         'date'  =>  $list->updated_on,
                         'result' => $list->result,
                         'win'  =>$list->win,
                         'loss' => $list->loss,
                         'rate' => $list->rate,
                         'mType' => $list->mType
                  ];
               

               
                 }

           $response = response()->json([ "status" => 1 ,'code'=> 200, "data" => $listArr ,'message'=> 'Data Found !!' ]);
          }else{

         $response = response()->json([ "status" => 1 ,'code'=> 200, "data" => null ,'message'=> 'Data Found !!' ]);
         
         }

        }


return $response;

  }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }



 }