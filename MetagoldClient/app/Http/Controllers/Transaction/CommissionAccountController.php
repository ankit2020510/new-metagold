<?php

namespace App\Http\Controllers\Transaction;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;



class CommissionAccountController extends Controller
{

    public function accountStatement(Request $request)
    {

     try{

      
                 $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
         $userId = Auth::user()->id;

            if( isset( $request->start_date ) && isset( $request->end_date )
                && ( $request->start_date != '' ) && ( $request->end_date != '' )
                && ( $request->start_date != null ) && ( $request->end_date != null ) ){
                  
                        $startDate = date('yy-m-d', strtotime($request->start_date));
                        $startDate =$startDate." 00:00:01";
                        $endDate = date('yy-m-d', strtotime($request->end_date));
                        $endDate =$endDate." 23:59:59";
              
            }else{
                
                $start = new \DateTime('now +1 day');
                $endDate =  $start->format('yy-m-d h:i:s');
                $end = new \DateTime('now -5 day');
                $startDate = $end->format('yy-m-d h:i:s');  
                
            }
              
         
           $account = DB::table('tbl_transaction_client')->select('*')
                          ->where([['userId',$userId],['status',1]])
                          ->whereBetween('created_on',[$startDate, $endDate])
                          ->orderBy('id','DESC')
                          ->paginate(10);
                          //->get();
           $accountQuery = $account->items();
           $totalRecords = $account->total();   
                          /*
              if(isset($request->isFirst) && $request->isFirst != 1){
                $accountQuery = $accountQuery->whereBetween('created_on',[$startDate, $endDate])->get();
              }else{
                $accountQuery = $accountQuery->limit(10)->get();
              }*/

             //print_r($accountQuery); die('ansbsjah');

            if(!empty($accountQuery)){
              $i=0;
            	foreach ($accountQuery as  $acQuery) {

                     if($acQuery->order_type == 'instant_execution'){
                         if($acQuery->trans_type == 'BUY'){
                           $price = $acQuery->buyPrice;
                         }else{
                           $price = $acQuery->sellPrice;
                         }
                     }else{
                         $price = $acQuery->price;
                     }
                     /*if($acQuery->eType == 3){
                      $description = $acQuery->name.' > '.$acQuery->lot.' > '.$price;
                     }else{
                      $description = $acQuery->description;
                     }*/

                    $arr[] = [

                      'id' => $acQuery->id,
                      'userId' =>$acQuery->userId,
                      'type'=>$acQuery->type,
                      'lot'=>$acQuery->lot,
                      'balance' => round($acQuery->balance),
                      'mType' =>$acQuery->mType,
                      'name'=>$acQuery->name,
                      'eType'=>$acQuery->eType,
                      'order_type'=>$acQuery->order_type,
                      'trans_type'=>$acQuery->trans_type,
                      'buyPrice'=>$acQuery->buyPrice,
                      'sellPrice'=>$acQuery->sellPrice,
                      'stopLoss' => $acQuery->stopLoss,
                      'takeProfit'=>$acQuery->takeProfit,
                      'tradingSymbol'=>$acQuery->tradingSymbol,
                      'closeBuyPrice' => $acQuery->closeBuyPrice,
                      'closeSellPrice'=>$acQuery->closeSellPrice,
                      'created_on'=>$acQuery->created_on,
                      'updated_on'=>$acQuery->updated_on,
                      'status'=>$acQuery->status,
                      'description'=>$acQuery->description

                    ];
 
                    if($acQuery->eType == 3){
                      $arr[$i]['commission'] = round($acQuery->amount);
                      $arr[$i]['amount'] =  round($acQuery->amount);
                    }else{
                      $arr[$i]['amount'] = round($acQuery->amount);
                       $arr[$i]['commission'] = 0;
                       $orderId = DB::table('tbl_transaction_client')->select('*')
                          ->where([['userId',$userId],['status',1],['eType',3],['eid',$acQuery->eid]])->first();
                       if(!empty($orderId)){
                          $arr[$i]['completeOrderId']= 'Order closed against #'.$orderId->id;
                       }
                    }

                  $i++;
                       
            	}

            	 $response = [ "status" => 1 ,'code'=> 200, "data" => $arr ,'total'=>$totalRecords ,'message'=> 'Data Found !!' ];
            }else{

            	 $response = [ "status" => 1 ,'code'=> 200, "data" => null ,'message'=> 'Data Not Found !!' ];
            }



      return $response;

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


     
 }