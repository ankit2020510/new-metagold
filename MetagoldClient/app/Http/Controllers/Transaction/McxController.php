<?php

namespace App\Http\Controllers\McxMarket;
use DB;
use App\Models\Mcx;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class McxController extends Controller
{

  public function mcxMarket(Request $request){

      $cache = Redis::connection();
      /*$key='TestData';
      $data="Redis working.";
      $cache->set($key, $data);
      $result=$cache->get($key);
      echo "Demo--->".$result;
      exit;*/
        $response = [ "status" => 0 , "code" => 400 , 'data' => null , "message" => "Bad Request !!" ];

        $startTime = time();
        $endTime = time() + 60;
        while ($endTime > time()) {

            usleep(300000);
      /*  echo $startTime;
        echo "</br>";
        echo $endTime;*/
        $client = new Client();
        $response = $client->request('GET', 'http://printo-env-1.prib5q8tuu.ap-south-1.elasticbeanstalk.com/commodities');
    	$statusCode = $response->getStatusCode();
    	$body = $response->getBody()->getContents();
    	$detail = json_decode($body,true);
    	$data = $detail['data'];
   
     
         $date = new \DateTime();
         $da= date_format($date, 'H:i');
            
      $arr = [];
       
      if(date('D') != 'Sat' || date('D') != 'Sun'){
      	if(date('D') == 'Fri' && $da < '23:30' ){
      	
          $key = 'Mcx_Market';
          $data =  json_encode($data);
          if($cache->exists($key)){
          	
          	$cache->set($key, $data);
          }else{
          	 $cache->set($key, $data);
          }

        }else{
        	  $key = 'Mcx_Market';
          $data =  json_encode($data);
          if($cache->exists($key)){
          	
          	$cache->set($key, $data);
          }else{
          	 $cache->set($key, $data);
          }
        }
      }
        //}
          //$result=$cache->get($key);


          $database = $this->storeMarket();
          $response = ['status' => 1 ,'code'=>200, 'message'=>'saved successfully!!'];
    }
	    
   return $response;

  }

  public function storeMarket(){

       $cache = Redis::connection();
       $key = 'Mcx_Market';
       $result=$cache->get($key);

       $arrData = json_decode($result);
     // print_r($arrData); die();
      $i=0;
      foreach($arrData as $key => $val){
       	$arrD[] = $val;
       	$arrD[$i]->tradingSymbol = $key;
       $i++;
        	//print_r($arrD); die();
        	
        }

     /*  foreach($arrData as $key => $data){
       
        $arr[]=[
             'name' => $data['name'],
	      	 'instrument_token' => $data['instrument_token'],
	      	  'tradingSymbol' => $key,
	      	 'timestamp' => $data['timestamp'],
	      	 'last_trade_time' => $data['last_trade_time'],
	      	 'last_price' => $data['last_price'],
	      	 'last_quantity' => $data['last_quantity'],
	      	 'buy_quantity' => $data['buy_quantity'],
	      	 'sell_quantity' => $data['sell_quantity'],
	      	 'average_price' => $data['average_price'],
	      	 'oi' => $data['oi'],
	      	 'oi_day_high' => $data['oi_day_high'],
	      	 'oi_day_low' => $data['oi_day_low'],
	      	 'net_change' => $data['net_change'],
	      	 'lower_circuit_limit' => $data['lower_circuit_limit'],
	      	 'upper_circuit_limit' => $data['upper_circuit_limit'],
	      	 'ohlc' => json_encode($data['ohlc']),
	      	 'depth' => json_encode($data['depth']),
	      	 'expiry' => $data['expiry']

          ];
       }  

*/

       foreach($arrD as $data){
       
             $market = new Mcx();
             $tradingSymbol = substr($data->tradingSymbol, (strpos($data->tradingSymbol, ':') ?: -1) + 1);
	      	 $market->name = $data->name;
	      	 $market->instrument_token = $data->instrument_token;
	      	 $market->tradingSymbol = $tradingSymbol;
	      	 $market->timestamp = $data->timestamp;
	      	 $market->last_trade_time = $data->last_trade_time;
	      	 $market->last_price = $data->last_price;
	      	 $market->last_quantity = $data->last_quantity;
	      	 $market->buy_quantity = $data->buy_quantity;
	      	 $market->sell_quantity = $data->sell_quantity;
	      	 $market->average_price = $data->average_price;
	      	 $market->oi = $data->oi;
	      	 $market->oi_day_high = $data->oi_day_high;
	      	 $market->oi_day_low = $data->oi_day_low;
	      	 $market->net_change = $data->net_change;
	      	 $market->lower_circuit_limit = $data->lower_circuit_limit;
	      	 $market->upper_circuit_limit = $data->upper_circuit_limit;
	      	 $market->ohlc = json_encode($data->ohlc);
	      	 $market->depth = json_encode($data->depth);
	      	 $market->expiry = $data->expiry;

	      	 $order =  DB::table('tbl_mcxmarket2')->where('status',1)->select('instrument_token')->get();

	      	 if(!empty($order)){
		      	 foreach ($order as  $val) {
		      	 	$instrument_token[] = $val->instrument_token;
		      	 }
	      	 }

             if(!in_array($data->instrument_token, $instrument_token)){
		      	 if($market->save()){
		      	 	
		      	 	$response = [ "status" => 1 , "code" => 200 , 'data' => null , "message" => "Succesfully added !!" ];
		      	 }
	      	  }else{

	      	  	  $arr = [
                     'name' => $data->name,
			      	 'instrument_token' => $data->instrument_token,
			      	 'timestamp' => $data->timestamp,
			      	 'last_trade_time' => $data->last_trade_time,
			      	 'last_price' => $data->last_price,
			      	 'last_quantity' => $data->last_quantity,
			      	 'buy_quantity' => $data->buy_quantity,
			      	 'sell_quantity' => $data->sell_quantity,
			      	 'average_price' => $data->average_price,
			      	 'oi' => $data->oi,
			      	 'oi_day_high' => $data->oi_day_high,
			      	 'oi_day_low' => $data->oi_day_low,
			      	 'net_change' => $data->net_change,
			      	 'lower_circuit_limit' => $data->lower_circuit_limit,
			      	 'upper_circuit_limit' => $data->upper_circuit_limit,
			      	 'ohlc' => json_encode($data->ohlc),
			      	 'depth' => json_encode($data->depth),
			      	 'tradingSymbol' =>  $tradingSymbol,
			      	 'expiry' => $data->expiry 

	      	  	  ];

	      	  	$order =  DB::table('tbl_mcxmarket2')->where([['status',1],['instrument_token',$data->instrument_token]])->update($arr);

	      		$response = [ "status" => 1 , "code" => 200 , 'data' => null , "message" => "Market already exists !!" ];
	      	  }
	      }

	  return $response;
	  }

}












?>