<?php

namespace App\Http\Controllers\Complain;
use App\User;
use DB;
use App\Models\Transaction;
use App\Models\LogActivity as Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Helpers\LogActivity;

class ComplainController extends Controller
{

       public function create(Request $request)
       {   
            
             $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];

            $uid = Auth::user()->id;
            $username = Auth::user()->username;
            $parentId = Auth::user()->parentId;
            $parent = DB::table('tbl_user_info')->where('uid',$parentId)->select('pName')->first();
            if(!empty($parent)){
              $pName = $parent->pName;
            }
               $name = ' '; $img =null;
            if ($request->hasFile('input_img')) {
                $image = $request->file('input_img');
                $name = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/upload');
                $image->move($destinationPath, $name);
            }
            if($name != ' '){
               $img = url('/upload/').'/'.$name;
            }
            $time = date('Y-m-d h:i:s');
          
            if($request->title != ' ' && $request->information != ' '){

                $data = [
                      'uid'=>$uid,
                      'client_name'=>$username,
                      'parent'=>$pName,
                      'title'=>$request->title,
                      'information'=>$request->information,
                      'image'=>$img,
                      'created_at'=>$time,
                      'updated_at'=>$time

                    ];
             
                $insert = DB::table('tbl_complain')->insert($data);
               

                $response = [ "status" => 1 , "code" => 200 , "message" => "Your complain has been Filed! Our team will resolve this in sometime" ];
              }else{
                $response = [ "status" => 0 , "code" => 200 , "message" => "Something went wrong!!" ];
              }
           
           $log = 'complain created';
             LogActivity::addTolog($log);
             
        
   return $response;
    }

  public function list(Request $request)
       {   
            
             $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];           
            $uid = Auth::user()->id;
             $complainList = DB::table('tbl_complain')
                            ->where('uid',$uid)
                            ->whereIn('status',[0,1,2])
                             ->orderBy('id','DESC')
                             ->paginate(10);
                          //->get();
                 // print_r($complainList->total()); die();
           $complainQuery = $complainList->items();
           $totalRecords = $complainList->total();
                            
             if(!empty($complainList)){

               $response = [ "status" => 1 , "code" => 200 ,'data'=>$complainQuery,'total'=>$totalRecords, "message" => "Data Found !! " ];  
             }else{
                 $response = [ "status" => 0 , "code" => 200 ,'data'=>null, "message" => "Data Found !! " ];
             }

   return $response;

    }
    // delete complain after 7 days
    public function deleteComplain(Request $request)
       {   
            
             $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];           
          /*  $uid = Auth::user()->id;*/
             $list = DB::table('tbl_complain')->select('id','created_at')
                            ->where('status',0)->get();
             $newDate = date('Y-m-d h:i:s');
         
             if(!empty($list)){
               foreach ($list as $val) {
                 $date = $val->created_at;
                 $date1=date_create($date);
                 $date2=date_create($newDate);
                 $diff=date_diff($date1,$date2);
                 $difference = $diff->format("%a");
                 if($difference >= 7){
                  DB::table('tbl_complain')->where('id',$val->id)->delete();
                 }
               }

               $response = [ "status" => 1 , "code" => 200 , "message" => "Data Deleted !! " ];  
             }else{
                 $response = [ "status" => 0 , "code" => 200 ,'data'=>null, "message" => "Data Found !! " ];
             }

            $this->removeStatement();
            $this->deleteLog();

   return $response;

    }

   // remove statement to another table
    public function removeStatement(){

         $response = ['status' => 0, 'error' => ['message' => 'Something Wrong! Data not available on this moment !']];
         $month = date('m');
         $table_name = 'Month_07';//'Month_'.$month;

        $data = Transaction::getStatement();
       // print_r($data); die('fhgjhfgjh');
        foreach ($data as $val) {

           $transaction = new Transaction($table_name);
           $transaction->stat_id = $val->id;
           $transaction->clientId = $val->clientId;
           $transaction->userId = $val->userId;
           $transaction->childId = $val->childId;
           $transaction->eid = $val->eid;
           $transaction->eType =$val->eType;
           $transaction->mtype = $val->mType;
           $transaction->amount = $val->amount;
           $transaction->type = $val->type;
           $transaction->balance = $val->balance;
           $transaction->name = $val->name;
           $transaction->tradingSymbol = $val->tradingSymbol;
           $transaction->order_type = $val->order_type;
           $transaction->trans_type = $val->trans_type;
           $transaction->lot =$val->lot;
           $transaction->price = $val->price;
           $transaction->buyPrice =$val->buyPrice;
           $transaction->sellPrice = $val->sellPrice;
           $transaction->closeBuyPrice = $val->closeBuyPrice;
           $transaction->closeSellPrice =$val->closeSellPrice;
           $transaction->stopLoss = $val->stopLoss;
           $transaction->takeProfit = $val->takeProfit;
           $transaction->description = $val->description;
           $transaction->remark = $val->remark;
           $transaction->created_at = $val->created_on;
           $transaction->updated_at = $val->updated_on;

           $arr = DB::table($table_name)->where([['stat_id',$val->id],['userId',$val->userId]])->first();
           if(empty($arr)){
             $transaction->save();
           }  
        }

          $response = ['status' => 1,'code'=>200,'data'=>null,'message' => 'Data Found !'];
       
    return $response;
   }
  //delete log activity after 7 days
  public function deleteLog()
       {   
            
             $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];           
             $list = Log::select('id','created_at')->orderBy('id','DESC')->get()->toArray();
             $newDate = date('Y-m-d h:i:s');
         
             if(!empty($list)){
               foreach ($list as $val) {
                 if($val['id'] != null ){
                     $date = $val['created_at'];
                     $date1=date_create($date);
                     $date2=date_create($newDate);
                     $diff=date_diff($date1,$date2);
                     $difference = $diff->format("%a");
                     if($difference >= 7){
                      Log::where('id',$val['id'])->delete();
                     }
                 }
               }

               $response = [ "status" => 1 , "code" => 200 , "message" => "Data Deleted !! " ];  
             }else{
                 $response = [ "status" => 0 , "code" => 200 ,'data'=>null, "message" => "Data Found !! " ];
             }

           

   return $response;

    }

   

 }























?>