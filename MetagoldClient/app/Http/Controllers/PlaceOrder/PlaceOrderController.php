<?php

namespace App\Http\Controllers\PlaceOrder;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\PlaceOrder;
use App\Models\MinMax;
use App\Http\Traits\MarginTrait;
use App\Http\Traits\CommissionTrait;
use Illuminate\Support\Facades\Redis;

class PlaceOrderController extends Controller
{
       use MarginTrait;
       use CommissionTrait;

       public function placeOrder(Request $request)
       {   
            
             $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
     	     $uid = Auth::user()->id;
             $systemId = Auth::user()->systemId;
             $username = Auth::user()->username;
             //die('llllll');
             $market = new PlaceOrder();

             $market->deviation = 0;
             $market->price = 0;

		     if($request->orderType == 'instant_execution'){ 
		        $market->deviation = $request->deviation;
		        $market->price = 0;            		      
		     }elseif($request->orderType == 'buy_limit'){
                $market->price = $request->price;
                $market->status = 3;
		     }else{
		     	$market->price = $request->price;
		     	$market->status = 3;
		     }
             
             $market->uid = $uid;
             $market->systemId = $systemId;
	      	 $market->name = $request->name;
	      	 $market->tradingSymbol = $request->tradingSymbol;
	      	 $market->order_type = $request->orderType;
	      	 $market->sell_price = $request->sellPrice;
	      	 $market->buy_price = $request->buyPrice;
	      	 $market->instrumentToken = $request->instrumentToken;
	      	 $market->stopLoss = $request->SL;
	      	 $market->takeProfit = $request->TP;
	      	 $market->lot = $request->lot;
	      	 $market->trans_type = $request->trans_type;
	      	 $market->expiry = $request->expiry;
             $market->ip_address =  $this->get_client_ip();
             $market->client = $username;

             $query = DB::table('tbl_user_info')->select(['balance','pl_balance','expose','pName'])
                ->where('uid',$uid);
             $userBalArray = $query->first();
             $userBalance = $userBalArray->balance+$userBalArray->pl_balance;
             $market->master = $userBalArray->pName;

              if($request->orderType == 'instant_execution'){

              $difference = $this->getCurrentPrice($request->tradingSymbol,$request->instrumentToken, $request->sellPrice,$request->buyPrice,$request->orderType,$request->trans_type);
              if($difference >= 0){$diff = $difference;}else{$diff = (-1)*$difference;}
             
              if($diff > 10){
                 $response =  [ "status" => 0 ,"code" => 400 , "message" => "Price has been changed"];
                return $response; exit;
              }
            }

             $marketBlock = DB::table('tbl_mcxmarket2')->select('*')->where('tradingSymbol',$request->tradingSymbol)->first();
            $date = date('Y-m-d');
            if($request->expiry <= $date){
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "The expiry of this market is over if u want to trade you can add market from add market !! "];
                return $response; exit;
            }
            if(!empty($marketBlock) && $marketBlock->is_block == 1){
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "This market is closed now. You cannot make trade at this time!!"];
                return $response; exit;
            }

            if(!empty($marketBlock) && $marketBlock->is_block == 1){
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "This market is closed now. You cannot make trade at this time!!"];
                return $response; exit;
            }
           
            $query = DB::table('tbl_user_block_status')->select('*')->where('uid',$uid);
            $userStatusCheck = $query->first();
            if(!empty($userStatusCheck) && $userStatusCheck->type == 1){
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Your account is blocked by parent! Plz contact administrator!!"];
                return $response; exit;
            }
            if(!empty($userStatusCheck) && $userStatusCheck->type == 2){
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Your trade is locked by parent! Plz contact administrator!!"];
                return $response; exit;
            }

            $checkMinMax = MinMax::checkMinMaxUnit($request->name,$request->lot);
           // print_r($checkMinMax); die('ppppkk');
            if($checkMinMax == 0){
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Please enter correct unit to trade !!"];
                return $response; exit;
            }
           // print_r($userBalArray->pl_balance); die('ppop');
            if($userBalArray->pl_balance > 500000){
                $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "You have reached the max profit limit. Pls contact your admin!!"];
                return $response; exit;
            }

	      	 if($request->orderType == 'instant_execution' && $request->trans_type == 'SELL'){ 
		        //$market->deviation = $request->deviation;
		           $prce = $request->sellPrice;            		      
		     }elseif($request->orderType == 'instant_execution' && $request->trans_type == 'BUY'){
                   $prce = $request->buyPrice;
		     }else{
                   $prce = $request->price;
		     }
             $commission = $this->commission($request->name);
             $comm = $this->getcommission($uid,$request->orderType,$request->trans_type,$request->lot,$request->sellPrice,$request->buyPrice,$request->price,$commission);
             $market->commission = round($comm,0);
             

		      $margin = $this->marginAll($request->orderType,$request->trans_type,$request->lot,$market->price,$request->buyPrice,$request->sellPrice);
 
              $market->orderMargin = round($margin);

              
              $newMargin = $margin + $comm + $userBalArray->expose;
             /* print_r($newMargin); echo "string"; print_r($userBalance); die('margin');*/
              
              if($userBalance > 0){
    		      if($userBalance > $newMargin){	
             
                     $message = 'sell ' .$request->lot.'/'.$request->lot.'  '.$request->name.' at '.$prce;
    	      	     if($market->save()){

                       if($market->order_type == 'instant_execution'){
                        
                       $update = $this->updateTransaction($uid,$market->id,$comm,$request->orderType,$request->trans_type,$request->lot,$request->sellPrice,$request->buyPrice,$request->price,$request->stopLoss,$request->takeProfit,$request->name,$request->tradingSymbol,$margin);

                       $update1 = $this->updateCommission($uid,$comm);
                       }
    	      	 	
                        $log = $request->name.'Order Placed';
                        LogActivity::addTolog($log);
    	      	 	   $response = [ "status" => 1 , "code" => 200 , 'data' => $message , "message" => "Order Placed !!" ];
    	      	     }

    	      	  }else{
    	      		$response = [ "status" => 1 , "code" => 200 , 'data' => null , "message" => "You have insufficient cash in the account to make this trade !!" ];
    	      	     }
    	     }else{
    	      	$response = [ "status" => 1 , "code" => 200 , 'data' => null , "message" => "You have insufficient cash in the account to make this trade !!" ];
    	      }
            
   return $response;

    }


   public function updateTransaction($uid,$orderid,$profit,$orderType,$trans_type,$lot,$sell_price,$buy_price,$price,$stopLoss,$takeProfit,$name,$tradingSymbol,$margin) {

       $amount = round($profit,0);
       
       //if( $amount > 0 ){ $type = 'CREDIT'; }else{ $amount = (-1)*$amount; $type = 'DEBIT'; }

       $summaryData = [];

       
       $cUser = DB::table('tbl_user')->select(['parentId','systemId'])->where([['id',$uid]])->first();
       $cUserInfo = DB::table('tbl_user_info')->select(['balance','pl_balance','expose'])->where([['uid',$uid]])->first();
       
        $parentId = $cUser->parentId;
        $systemId = $cUser->systemId;
        $type = 'DEBIT';
        $balance = $this->getCurrentBalanceClientNew($uid,$profit,$type,$cUserInfo->balance,$cUserInfo->pl_balance,'CLIENT',$margin,$cUserInfo->expose);
                  

          if($orderType == 'instant_execution'){
                         if($trans_type == 'BUY'){
                           $prc = $buy_price;
                           $description = $name.' > '.$lot.' > '.$trans_type.' AT '.$prc;
                         }else{
                           $prc = $sell_price;
                           $description = $name.' > '.$lot.' > '.$trans_type.' AT '.$prc;
                         }
                       }elseif($orderType == 'buy_limit' || $orderType == 'sell_limit'){
                           if($trans_type == 'BUY'){
                             $prc = $price;
                             $description = $name.' > '.$lot.' > '.$trans_type.' AT '.$prc;
                         }else{
                              $prc = $price;
                             $description = $name.' > '.$lot.' > '.$trans_type.' AT '.$prc;
                         }
                          
                       }


        $resultArr = [
            'systemId' => $systemId,
            'clientId' => $uid,
            'userId' => $uid,
            'childId' => 0,
            'parentId' => $parentId,
            'sid' => 0,
            'eid' => $orderid,
            'mid' => 0,
            'eType' => 3,
            'mType' => 0,
            'trans_type'=>$trans_type,
            'order_type'=>$orderType,
            'lot'=>$lot,
            'buyPrice'=>$buy_price,
            'sellPrice'=>$sell_price,
            'closeBuyPrice'=>0,
            'closeSellPrice'=>0,
            'stopLoss'=>$stopLoss,
            'takeProfit'=>$takeProfit,
            'price'=>$price,
            'tradingSymbol'=>$tradingSymbol,
            'name'=>$name,
            'type' => $type,
            'amount' => round($amount),
            'p_amount' => 0,
            'c_amount' => 0,
            'balance' => $balance,
            'description' => $description,
            'remark' => 'Commission Entry',
            'status' => 1,
            'created_on'=>date('Y-m-d h:i:s'),
            'updated_on'=>date('Y-m-d h:i:s'),

        ];

        DB::table('tbl_transaction_client')->insert($resultArr);

        /*$summaryData['uid'] = $uid;
        $summaryData['ownPl'] = $amount;
        $summaryData['type'] = $type;*/
      
            $type = $type;
            $ownPl = $amount;
            $parentPl = 0;
            $parentComm = 0;
            $ownComm = 0;
       // $this->userSummaryUpdate($summaryData,'CLIENT');
        $this->userSummaryUpdateNew($uid,$ownPl,$parentPl,$type,$ownComm,$parentComm);
       // if( $type == 'CREDIT' ){ $pType = 'DEBIT'; }else{ $pType = 'CREDIT'; }
        $pType = "CREDIT";
        $parentUserData = DB::table('tbl_user_profit_loss as pl')
            ->select(['pl.userId as userId','pl.actual_profit_loss as apl','pl.profit_loss as gpl','ui.balance as balance','ui.pl_balance as pl_balance'])
            ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
            ->where([['pl.clientId',$uid],['pl.actual_profit_loss','!=',0]])->orderBy('pl.userId','desc')->get();

        $childId = $uid;
       // print_r($parentUserData); echo "strinnnnnnnnnn";
        $summaryData = [];

        foreach ( $parentUserData as $user ){

            $cUser = DB::table('tbl_user')->select(['parentId','role'])->where([['id',$user->userId]])->first();


           
            $parentId = $cUser->parentId;
            
            $transactionAmount = ( $amount*$user->apl )/100;
            $pTransactionAmount = ( $amount*(100-$user->gpl) )/100;
            $cTransactionAmount = ( $amount*($user->gpl-$user->apl) )/100;
            $balance = $this->getCurrentBalance($user->userId,$transactionAmount,$pType,$user->balance,$user->pl_balance,'PARENT');
              


            $resultArr = [
                'systemId' => $systemId,
                'clientId' => $uid,
                'userId' => $user->userId,
                'childId' => $childId,
                'parentId' => $parentId,
                'sid' => 0,
                'eid' => $orderid,
                'mid' => 0,
                'eType' => 0,
                'mType' => 0,
                'lot'=>$lot,
                'buyPrice'=>$buy_price,
                'sellPrice'=>$sell_price,
                'trans_type'=>$trans_type,
                'order_type'=>$orderType,
                'closeBuyPrice'=>$buy_price,
                'closeSellPrice'=>$sell_price,
                'stopLoss'=>$stopLoss,
                'takeProfit'=>$takeProfit,
                'price'=>$price,
                'tradingSymbol'=>$tradingSymbol,
                'name'=>$name,
                'type' => $pType,
                'amount' => round($transactionAmount),
                'p_amount' => round($pTransactionAmount),
                'c_amount' => round($cTransactionAmount),
                'balance' => $balance,
                'description' => $description,
                'remark' => 'Commission Entry',
                'status' => 1,
                'created_on'=>date('Y-m-d h:i:s'),
                'updated_on'=>date('Y-m-d h:i:s')

            ];

            if( $cUser->role == 1 ){
                DB::table('tbl_transaction_admin')->insert($resultArr);
            }else{
                DB::table('tbl_transaction_parent')->insert($resultArr);
            }

           /* $summaryData['uid'] = $user->userId;
            $summaryData['type'] = $pType;
            $summaryData['ownPl'] = $transactionAmount;
            $summaryData['parentPl'] = $pTransactionAmount;*/
            $uid = $user->userId;
            $type = $pType;
            $ownPl = $transactionAmount;
            $parentPl = $pTransactionAmount;
            $parentComm = 0;
            $ownComm = 0;
            //$this->userSummaryUpdate($summaryData,null);
            $this->userSummaryUpdateNew($uid,$ownPl,$parentPl,$type,$ownComm,$parentComm);
            $childId = $user->userId;

        $SummaryData1 = DB::table('tbl_settlement_summary')->select('ownPl','ownComm')
                ->where( 'uid', $user->userId)->first();
            if(!empty($SummaryData1)){
                $updated_on = date('Y-m-d H:i:s');
                $ubalance=0;
                $ubalance= $SummaryData1->ownPl+ $SummaryData1->ownComm;
                DB::table('tbl_user_info')->where([['uid',$user->userId]])
                    ->update(['pl_balance' => $ubalance,'updated_on'=>$updated_on]);
            }

        }
         
        
       //print_r($cUser); echo "string"; print_r($cUserInfo); echo "string"; print_r($balance);
        
        
    }

//user Summary Update New
    public static function userSummaryUpdateNew($uid,$ownPl,$parentPl,$type,$ownComm,$parentComm){
        
        $userSummary = DB::table('tbl_settlement_summary')
            ->where([['status',1],['uid',$uid]])->first();
        if( $userSummary != null ){

            $updatedOn = date('Y-m-d H:i:s');
            if( isset( $type ) && $type != 'CREDIT' ){
                $ownPl = (-1)*round($ownPl,2);
                $parentPl = (-1)*round($parentPl,2);
                $ownComm = (-1)*round($ownComm,2);
                $parentComm = (-1)*round($parentComm,2);
            }

            $updateArr = [
                'ownPl' => DB::raw('ownPl + ' . $ownPl),
                'ownComm' => DB::raw('ownComm + ' . $ownComm),
                'parentPl' => DB::raw('parentPl + ' . $parentPl),
                'parentComm' => DB::raw('parentComm + ' . $parentComm),
                'updated_on' => $updatedOn
            ];

            if( DB::table('tbl_settlement_summary')
                ->where([['status',1],['uid',$uid]])->update($updateArr) ){
                $updateArr1 = [ 'uid' => $uid,'updated_on' => $updatedOn,
                    'ownPl' => $ownPl, 'parentPl' => $parentPl,
                    'ownComm' => $ownComm, 'parentComm' => $parentComm];

                //$array = array('function' => 'settelement', 'Query' => $updateArr1);
                //$this->activityLog(json_encode($array));
            }else{
                $userSummary = DB::table('tbl_settlement_summary')
                    ->where([['status',1],['uid',$uid]])->first();
                $updatedOn = date('Y-m-d H:i:s');
                $betTimeDiff = (strtotime($updatedOn)-strtotime($userSummary->updated_on) );
                if($betTimeDiff == 0) {
                    usleep(1000);
                    $userSummary = DB::table('tbl_settlement_summary')
                        ->where([['status',1],['uid',$uid]])->first();
                }
                $updateArr = [
                    'ownPl' => $userSummary->ownPl+$ownPl,
                    'ownComm' => $userSummary->ownComm+$ownComm,
                    'parentPl' => $userSummary->parentPl+$parentPl,
                    'parentComm' => $userSummary->parentComm+$parentComm,
                    'updated_on' => date('Y-m-d H:i:s')
                ];
                if( DB::table('tbl_settlement_summary')
                    ->where([['status',1],['uid',$uid]])->update($updateArr) ){
                    $updateArr1 = [ 'uid' => $uid,'updated_on'=>$updatedOn,
                        'ownPl' => $ownPl, 'parentPl'=>$parentPl,
                        'ownComm' => $ownComm, 'parentComm' => $parentComm];

                    //$array = array('function' => 'settelement', 'Query' => $updateArr1);
                    //$this->activityLog(json_encode($array));
                }
            }

        }else{
            $updatedOn = date('Y-m-d H:i:s');
            if( isset( $type ) && $type != 'CREDIT' ){
                $ownPl = (-1)*round($ownPl,2);
                $parentPl = (-1)*round($parentPl,2);
                $ownComm = (-1)*round($ownComm,2);
                $parentComm = (-1)*round($parentComm,2);
            }

            $insertArr = [  'uid' => $uid, 'updated_on' => $updatedOn,
                'ownPl' => $ownPl, 'ownComm' => $ownComm,
                'parentPl' => $parentPl,'parentComm' => $parentComm ];

            DB::table('tbl_settlement_summary')->insert($insertArr);

        }
    }
     //user Summary Update
   /* public function userSummaryUpdate($summaryData,$role){

        if( isset( $summaryData['uid'] ) ){

            $uid = $summaryData['uid'];
            $ownPl = isset($summaryData['ownPl']) ? $summaryData['ownPl'] : 0;
            if($role == 'CLIENT'){
               $parentPl = $summaryData['ownPl']; 
            }else{
               $parentPl = $summaryData['parentPl'];    
            }

            $ownComm = isset($summaryData['ownComm']) ? $summaryData['ownComm'] : 0;
            $parentComm = isset($summaryData['parentComm']) ? $summaryData['parentComm'] : 0;

            $userSummary = DB::table('tbl_settlement_summary')
                ->where([['status',1],['uid',$uid]])->first();

            if( $userSummary != null ){

                if( isset( $summaryData['type'] ) && $summaryData['type'] == 'CREDIT' ){
                    $ownPl = $ownPl+$userSummary->ownPl;
                    $parentPl = $parentPl+$userSummary->parentPl;
                }else{
                    $ownPl = $userSummary->ownPl-$ownPl;
                    $parentPl = $userSummary->parentPl-$parentPl;
                }

                $ownComm = $userSummary->ownComm+$ownComm;
                $parentComm = $userSummary->parentComm+$parentComm;

                $updateArr = [ 'ownPl' => $ownPl, 'ownComm' => $ownComm,
                    'parentPl' => $parentPl, 'parentComm' => $parentComm ];

                DB::table('tbl_settlement_summary')->where([['status',1],['uid',$uid]])
                    ->update($updateArr);

            }else{

                $insertArr = [ 'uid' => $uid, 'ownPl' => $ownPl, 'ownComm' => $ownComm,
                    'parentPl' => $parentPl, 'parentComm' => $parentComm ];

                DB::table('tbl_settlement_summary')->insert($insertArr);

            }

        }

    }*/



     // get Current Balance
    /*public function getCurrentBalance($uid,$amount,$type,$balance,$plBalance,$role,$margin,$expose)
    {

        if( $type == 'CREDIT' ){
            $newplBalance = round($plBalance)+round($amount);
            $arr = ['pl_balance'=>$newplBalance];
        }else{
            $newplBalance = round($plBalance) - round($amount);
            $newExpose = $expose + $margin;
            $arr = ['pl_balance'=>$newplBalance, 'expose'=>$newExpose];
        }

        
        if( DB::table('tbl_user_info')->where('uid',$uid)->update($arr) ){

            if( $role != 'CLIENT' ){
                return round( ( $newplBalance ),2);
            }else{
                return round( ( $newplBalance + $balance),2);
            }

        }else{

            if( $role != 'CLIENT' ){
                return round( ( $plBalance ),2);
            }else{
                return round( ( $newplBalance + $balance ),2);
            }
        }

    }
*/
 
/* public function deductBalance($uid,$profit){

    $balance =  DB::table('tbl_user_info')->where('uid',$uid)->select('balance')->first();
    $balance = $balance->balance;
    $newplBalance = $balance-$profit;
    $updateBalance = ['balance' => $newplBalance];
    $update =  DB::table('tbl_user_info')->where('uid',$uid)->update($updateBalance);

 }*/
 // get Current Balance admin/Super master and master
    public static function getCurrentBalance($uid,$amount,$type,$balance,$plBalance,$role)
    {
        
          
          $updateArr = [
            'pl_balance' => DB::raw('pl_balance + ' . round($amount,0))
        ];

        DB::table('tbl_user_info')->where('uid', $uid)->update($updateArr);
        $cUserInfo = DB::table('tbl_user_info')->select(['balance','pl_balance'])->where([['uid',$uid]])->first();

        $balance = $cUserInfo->balance;
        $plBalance = $cUserInfo->pl_balance;
        if( $role != 'CLIENT' ){
            return round($plBalance,0);
        }else{
            if($plBalance > 0){
                return round(($balance + $plBalance),0);
            }else{
                return round(($balance - $plBalance),0);
            }

        }
    }
// get Current Balance New
    public static function getCurrentBalanceClientNew($uid,$amount,$type,$balance,$plBalance,$role,$margin,$expose)
    {
        //$conn = DB::connection('mysql2');
        //if( $type != 'CREDIT' ){ $amount = (-1)*$amount; }
        $updated_on = date('Y-m-d H:i:s');
        $updateArr = [
            'pl_balance' => DB::raw('pl_balance - ' . round($amount,0)),
            'expose' => DB::raw('expose + ' . round($margin)),
            'updated_on' => $updated_on
        ];

        if( DB::table('tbl_user_info')->where('uid', $uid)->update($updateArr) ){
            $cUserInfo = DB::table('tbl_user_info')
                ->select('balance','pl_balance','updated_on')->where('uid',$uid)->first();
            $balance = $cUserInfo->balance;
            $plBalance = $cUserInfo->pl_balance;

            if( $role != 'CLIENT' ){
                return round($balance,0);
            }else{
                return round(($balance + $plBalance),0);
            }
        }else{
            $updated_on = date('Y-m-d H:i:s');
            $cUserInfo = DB::table('tbl_user_info')
                ->select(['balance','pl_balance','updated_on'])->where([['uid',$uid]])->first();
            $betTimeDiff = (strtotime($updated_on)-strtotime($cUserInfo->updated_on) );
            if($betTimeDiff == 0) {
                usleep(1000);
                $cUserInfo = DB::table('tbl_user_info')
                    ->select(['balance','pl_balance','updated_on'])->where([['uid',$uid]])->first();
            }

            $balance = $cUserInfo->balance;
            $newPlBalance = $cUserInfo->pl_balance;

            $updateArr = [
                'pl_balance' => $newPlBalance,
                'expose'  => $margin,
                'updated_on' => $updated_on
            ];

            if( DB::table('tbl_user_info')
                ->where([['uid', $uid]])->update($updateArr) ){
                if( $role != 'CLIENT' ){
                    return round($balance,0);
                }else{
                    return round(($balance + $newPlBalance),0 );
                }
            }
        }
    }

    // Function to get commission
   public function commission($key_name) {
        
        $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];

        $data = DB::table('tbl_lots')->where([['key_name',$key_name],['status',1]])->first();
        if(!empty($data)){

            $commission = $data->commission;
        }else{
            $commission = ' ';
        }
        return $commission;
    }
// Function to get the client IP address
   public function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }







public function updateCommission($uid,$profit) {

       $amount = round($profit,0);       
       $cUser = DB::table('tbl_user')->select(['parentId','systemId'])->where([['id',$uid]])->first();
       $cUserInfo = DB::table('tbl_user_info')->select(['balance','pl_balance','expose','pl_commission'])->where([['uid',$uid]])->first();
       
        $parentId = $cUser->parentId;
        $systemId = $cUser->systemId;
        $type = 'DEBIT';
        $balance = $this->getCommissionClient($uid,$profit,$type,$cUserInfo->pl_commission,'CLIENT');
                  
        $pType = "CREDIT";
        $parentUserData = DB::table('tbl_user_profit_loss as pl')
            ->select(['pl.userId as userId','pl.actual_profit_loss as apl','pl.actual_commission as aplc','pl.commission as gplc','pl.profit_loss as gpl','ui.balance as balance','ui.pl_balance as pl_balance','ui.pl_commission as pl_commission'])
            ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
            ->where([['pl.clientId',$uid],['pl.actual_commission','!=',0]])->orderBy('pl.userId','desc')->get();

        $childId = $uid;
       // print_r($parentUserData); echo "strinnnnnnnnnn";
        $summaryData = [];

        foreach ( $parentUserData as $user ){

            $cUser = DB::table('tbl_user')->select(['parentId','role'])->where([['id',$user->userId]])->first();
            $parentId = $cUser->parentId;
            
            $transactionAmount = ( $amount*$user->aplc )/100;
            $pTransactionAmount = ( $amount*(100-$user->gplc) )/100;
            $cTransactionAmount = ( $amount*($user->gplc-$user->aplc) )/100;
            $balance = $this->getCommissionParent($user->userId,$transactionAmount,$pType,$user->pl_commission,'PARENT');
        }
         
}


// update commission
    public static function getCommissionClient($uid,$amount,$type,$plCommission,$role)
    {
        $updated_on = date('Y-m-d H:i:s');
        $updateArr = [
            'pl_commission' => DB::raw('pl_commission - ' . round($amount,0)),
            'updated_on' => $updated_on
        ];

         DB::table('tbl_user_info')->where('uid', $uid)->update($updateArr);
           
    }

public static function getCommissionParent($uid,$amount,$type,$plCommission,$role)
    {
        
          
          $updateArr = [
            'pl_commission' => DB::raw('pl_commission + ' . round($amount,0))
        ];

        DB::table('tbl_user_info')->where('uid', $uid)->update($updateArr);
        $cUserInfo = DB::table('tbl_user_info')->select(['balance','pl_balance','pl_commission'])->where([['uid',$uid]])->first();

    }






public function getCurrentPrice($tradingSymbol,$instrumentToken,$buyPrice,$sellPrice,$order_type,$trans_type){
       
       $cache = Redis::connection();
       $key = 'Mcx_Market';
       $result=$cache->get($key);
       $arrData = json_decode($result);
       if(!empty($arrData)){
       foreach ($arrData as $key => $val) {
          
         if($key == $tradingSymbol && $val->instrument_token == $instrumentToken){
              $depth = $val->depth;
              $currentBuy = $depth->buy[0]->price;
              $currentSell = $depth->sell[0]->price;      
                         
          }
        }
           if($order_type == 'instant_execution'){
             if($trans_type == 'BUY'){
                $difference = $currentBuy - $buyPrice;
              }else{
                 $difference = $currentSell-$sellPrice;
              }
          }elseif($order_type == 'buy_limit'){

               $difference = $currentBuy - $buyPrice;

          }elseif($order_type == 'sell_limit'){
                $difference = $currentSell-$sellPrice;
          }
      
          

          
        }else{

           $difference = 0;
         
        }
       return $difference;
    }


   /*public function checkUserProfit($uid){
      
      $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];

      $orderList = PlaceOrder::where([['uid',$uid],['status',1]])->get();

      if(!empty($orderList)){

              foreach ($orderList as $val) {
                  
                  $depth = $this->getCurrentPrice($val->tradingSymbol,$val->instrumentToken);
                  $currentBuyPrice = $depth['currentBuy'];
                  $currentSellPrice = $depth['currentSell'];

                  if($val->order_type == 'instant_execution'){
                    if($val->trans_type == 'BUY'){
                        $profit[] = ($currentBuyPrice - $val->buy_price) * $val->lot;
                    }else{
                        $profit[] = ($val->sell_price - $currentSellPrice) * $val->lot;
                    }
                 }elseif($val->order_type == 'buy_limit'){

                       $profit[] = ($currentBuyPrice - $val->price) * $val->lot;
                 }elseif ($val->order_type == 'sell_limit') {
                      $profit[] = ($val->price - $currentSellPrice) * $val->lot;
                 }

              }

      $profitSum = array_sum($profit);

      }else{

       $profitSum = 0;
       }

      return $profitSum;

   }
*/




 }//class

?>