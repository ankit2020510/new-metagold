<?php

namespace App\Http\Controllers\PlaceOrder;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\PlaceOrder;


class UpdateSLTPController extends Controller
{
            
     public function update(Request $request)
     {

       $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

       $uid = Auth::user()->id;

       if($request->SL != " " && $request->TP != " "){

        $data = [
           
            "stopLoss"=>$request->SL,
            "takeProfit"=>$request->TP
        ];

        PlaceOrder::where([['uid',$uid],['id',$request->orderId]])->update($data);

        $log = 'SL/TP updated succussfully';
        LogActivity::addTolog($log);
 
        $response = [ 'status' => 1, 'code'=>200, 'message' => 'Data updated succussfully !!'  ];

       }else{

         $data = [
           
            "stopLoss"=>$request->SL,
            "takeProfit"=>$request->TP
        ];

        PlaceOrder::where([['uid',$uid],['id',$request->orderId]])->update($data);

        $log = 'SL/TP updated succussfully';
        LogActivity::addTolog($log);
 
        $response = [ 'status' => 1, 'code'=>200, 'message' => 'Data updated succussfully !!'  ];
       }
      

      return $response;
     }



   



}


  



















?>