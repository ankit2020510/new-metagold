<?php

namespace App\Http\Controllers;
use App\User;
use DB;
use Illuminate\Http\Request;
use App\Helpers\LogActivity;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    
    public function register(Request $request)
    {

        
       $this->validate($request, [
            'name' => 'required|min:3',
            'username' => 'required|unique:tbl_user',
            'systemId' => 'required',
             'parentId' => 'required',
            'password' => 'required|min:6',
        ]);

        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'systemId'=>$request->systemId,
            'role' => $request->role,
            'parentId'=>$request->parentId,
            'password' => bcrypt($request->password)
        ]);

        $token = $user->createToken('TutsForWeb')->accessToken;

        return response()->json(['token' => $token], 200);
    }

   
    public function login(Request $request)
    {

        $credentials = [
            'username' => $request->username,
            'password' => $request->password,
            'systemId' => $request->systemId,
            'status' =>1
        ];



        if (auth()->attempt($credentials)) {
        
          
            $details = auth()->user();
              $userBlock = DB::table('tbl_user_block_status')->select('*')->where('uid',$details->id)->first();
              
            if((!empty($userBlock) && isset($userBlock->type) && $userBlock->type != 1 ) || (empty($userBlock) && !isset($userBlock->type))){

              if($details->role == 4){
               usleep(100000);
             if(!empty($details)){
              $accessToken = DB::table('oauth_access_tokens')->select('*')->where('user_id',$details->id)->delete();
              }
        
             $token = auth()->user()->createToken('TutsForWeb')->accessToken;
             $data = [
                "username" => $details->username,
                "token" => $token,
                "is_password_updated" => $details->is_password_updated,
                "role" => $details->role,
                "banner" => $this->getBannerData()
                ];

                $profile = User::where(['id'=>$details->id])->first();
                $profile->is_login = 1;
                $profile->update();

                 $operatorInfo = DB::table('tbl_system')->select('*')
                  ->where([['operatorId',$details->systemId],['status',1]])
                   ->first();

          
               $data['operatorId'] = $operatorInfo->operatorId;
               $data['operator_name'] = $operatorInfo->systemname;
               $data['logo'] = url('/operator_images').'/'.$operatorInfo->logo;
               $data['theme_id'] = $operatorInfo->theme_id;
               $log = 'you have LoggedIn successfully';
               LogActivity::addTolog($log);
          
             }else{
             return response()->json(['status' => 0, 'code'=> 444,'data' => null,'message' => 'Incorrect username or password !']);
             }

            return response()->json(['status'=>1,'data'=> $data,"message" => "Logged In successfully!",'code'=>200]);
          }else{
            return response()->json(['status'=>1,'data'=>null,"message" => "This user has been blocked !!",'code'=>200]);
          }
        } else {
            return response()->json(['status' => 0, 'code'=> 444,'data' => null,'message' => 'Incorrect username or password !']);
        }
    }

  public function logout(Request $request)
    {

        if (Auth::check()) {
            Auth::user()->token()->revoke();
             $details = auth()->user();
             $profile = User::where(['id'=>$details->id])->first();
                $profile->is_login = 0;
                $profile->update();

            $log = 'Logged Out successfully';
               LogActivity::addTolog($log);
            return response()->json([ "status" => 1 ,'code'=>200, "data" =>null , "message" => "Logout successfully !" ]); 
        }else{
            return response()->json([ "status" => 0 , 'code'=>401,"data" =>null , "message" => "Something went wrong !" ]);
        }


    }


//change password
public function changePassword(Request $request) {

   
     $data = $request->all();
     $user = Auth::guard('api')->user();

  if($user != null || $user != ''){
     
     if( isset($data['oldpassword']) && !empty($data['oldpassword']) && $data['oldpassword'] !== "" && $data['oldpassword'] !=='undefined') {

        if($data['oldpassword'] == $data['password']){
            return  response()->json(['status'=>0, 'code'=>200, 'data'=> null,'message' => 'old password and current password must be different!']);
            exit;
        }
         
         $check  = Auth::guard('web')->attempt([
             'username' => $user->username,
             'password' => $data['oldpassword']
         ]);
         if($check && isset($data['password']) && !empty($data['password']) && $data['password'] !== "" && $data['password'] !=='undefined') {
             $user->password = bcrypt($data['password']);
             $user->token()->revoke();
             $user->is_password_updated = 1;
             $token = $user->createToken('newToken')->accessToken;


             $user->save();

           
             $log = 'Password change successfully';
               LogActivity::addTolog($log);
         return  response()->json(['status'=>1, 'code'=>200, 'data'=> null,'message' => 'Password changed successfully!']);
             //return json_encode(array('token' => $token)); 

         }else{

            return response()->json(['status'=>0,'code'=>403,'data'=>null,"message" => "Please enter correct old password.!!"]);
         }
     }
   }else{

      return response()->json(['status'=>0,'code'=>401,'data'=>null,"message" => "Unauthorized access !!"]);   
        }
        
  }

   
public function details()
    {
        return response()->json(['user' => auth()->user()], 200);
    }

      public function getBannerData(){

        
        $banner = DB::table('tbl_banner')->select('url','width','height')
                  ->where([['status',1],['type',2]])
                  ->first();

        if( $banner != null ){
            return $banner->url;
        }else{
            return null;
        }

    }
}
