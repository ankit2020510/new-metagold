<?php

namespace App\Http\Controllers\Trade;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\LogActivity;
use App\Http\Controllers\Controller;
use App\Models\PlaceOrder;

class DeleteOrderController extends Controller
{


       public function deleteBuySellLimitMarket(Request $request)
       {   
     
             $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
     	       $uid = Auth::user()->id;

             if(isset($request->position)){

               $status = ['status' => 2];
               PlaceOrder::where([['id',$request->position],['uid',$uid]])->update($status);

               $response = [ "status" => 1 , "code" => 200 , "message" => "Deleted successfully" ];
             }else{

               $response = [ "status" => 1 , "code" => 400 , "message" => "something went wrong" ];
             }

             $log = $request->position.' ID Order deleted';
             LogActivity::addTolog($log);
             
   return $response;

    }

 }























?>