<?php

namespace App\Http\Controllers\Trade;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\PlaceOrder;

class TradeController extends Controller
{


       public function tradeHistory(Request $request)
       {   
     
             $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
     	     $uid = Auth::user()->id;
             //die('llllll');
            /*$order =  DB::table('tbl_order')->where([['uid',$uid],['comm',0]])->whereIn('status',[1,3])->select('*')->get();*/
            $order =  DB::table('tbl_order')->select('id','uid','client','master','name','price','buy_price','sell_price','instrumentToken','tradingSymbol','stopLoss','takeProfit','deviation','lot','order_type','trans_type','profit','commission','closeBuyPrice','closeSellPrice','comm','margin','expiry','ip_address','status')->where([['uid',$uid],['comm',0]])->whereIn('status',[1,3])->get();
            $orders = [];
            foreach ($order as $key => $val) {
            	# code...
            	$orders[]=$val;
            }
            
            $response = ['status' => 1, 'code' => 200, 'data' => $orders, 'message' => 'data found!!'];
           
   return $response;

    }

    public function currentOrder(Request $request)
    {

     try{

      
                 $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
         $uid = Auth::user()->id;
         $order = DB::table('tbl_order')->select('*')
                          ->where([['uid',$uid],['status',1],['comm',0]])
                          //->whereBetween('created_on',[$startDate, $endDate])
                          ->orderBy('id','DESC')
                          ->paginate(10);
                          //->get();
           $orderQuery = $order->items();
           $totalRecords = $order->total();  


                 $response = [ "status" => 1 ,'code'=> 200, "data" => $orderQuery ,'total'=>$totalRecords ,'message'=> 'Data Found !!' ];
           

      return $response;

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }
     public function orderPosition(Request $request)
    {

     try{

      
         $response = [ "status" => 0 , "code" => 400 , "message" => "Bad request!" ];
         $uid = Auth::user()->id;
         $position = DB::table('tbl_order')->select('*')
                          ->where([['uid',$uid],['status',3],['comm',0]])
                          //->whereBetween('created_on',[$startDate, $endDate])
                          ->orderBy('id','DESC')
                          ->paginate(10);
                          //->get();
           $positionQuery = $position->items();
           $totalRecords = $position->total();  


                 $response = [ "status" => 1 ,'code'=> 200, "data" => $positionQuery ,'total'=>$totalRecords ,'message'=> 'Data Found !!' ];
          



      return $response;

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


 }























?>