<?php
namespace App\Http\Traits;

trait MarginTrait {
    public function marginAll($order_type,$trans_type,$lot,$price,$buy_price,$sell_price) 
    {
                $margin=0;
                // checking  Gold
                if($order_type == 'instant_execution'){
                      if($trans_type == 'BUY'){
                           $price = $buy_price*$lot;
                           $margin = $price * 2/100; 
                      }else{
                           $price = $sell_price*$lot;
                           $margin = $price * 2/100;
                       }
                  }elseif($order_type == 'buy_limit' || $order_type == 'sell_limit' ){
                      if($trans_type == 'BUY'){
                           $price = $price*$lot;
                           $margin = $price * 2/100;  
                      }else{
                            $price = $price*$lot;
                           $margin = $price * 2/100; 
                      }
                }
        
        return $margin;
    }
}