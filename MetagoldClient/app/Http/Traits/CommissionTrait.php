<?php
namespace App\Http\Traits;
use Illuminate\Support\Facades\Auth;

trait CommissionTrait {

     public function getcommission($uid,$order_type,$trans_type,$lot,$sell_price,$buy_price,$price,$commission)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];


         $uid = Auth::user()->id;
         $comm = 0;

                 if($order_type == 'instant_execution'){
                    if($trans_type == 'SELL'){
                            $sell = $sell_price * $lot;
                            $comm = $sell * $commission ;
                    }else{
                            $buy = $buy_price * $lot;
                            $comm = $buy * $commission;
                    }

                 }elseif($order_type == 'buy_limit' || $order_type == 'sell_limit' ){
                        
                       if($trans_type == 'SELL'){
                            $price = $price * $lot;
                            $comm = $price * $commission ;
                    }else{
                            $price = $price * $lot;
                            $comm = $price * $commission ;

                    }
                 }

    return $comm;
    }
   
}