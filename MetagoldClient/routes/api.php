<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Login API
Route::post('login', 'AuthController@login');
//Register API
Route::post('register', 'AuthController@register');

Route::get('operator-detail', 'Operator\OperatorDetailController@detail');
Route::get('setting', 'Operator\SettingController@detail');
Route::get('news-list', 'Operator\NewsInformationController@list');
Route::get('lots', 'Lots\LotsController@lot');
Route::get('remove/completeOrder', 'PendingOrder\RemoveCompleteController@removeOrder');
Route::get('delete-complain', 'Complain\ComplainController@deleteComplain');
Route::get('remove/statement', 'Pdf\PdfController@removeStatement');




Route::get('android-version', 'Dashboard\AndroidController@androidVersion');



Route::get('mcx-market', 'McxMarket\McxController@McxMarket');
Route::get('store-market', 'McxMarket\McxController@storeMarket');
Route::get('pending-order', 'PendingOrder\PendingOrderController@pendingOrder');

Route::get('redis-test', 'Dashboard\RedisController@redis_test');

Route::middleware('auth:api')->group(function () {

     Route::get('user', 'PassportController@details');
//Logout API
     Route::get('logout', 'AuthController@logout')->name('logout');

   Route::post('update-sltp', 'PlaceOrder\UpdateSLTPController@update');
   Route::post('place-order', 'PlaceOrder\PlaceOrderController@placeOrder'); 
   Route::get('trade-history', 'Trade\TradeController@tradeHistory');
   Route::get('position/order', 'Trade\TradeController@orderPosition'); 
   Route::get('current/order', 'Trade\TradeController@currentOrder'); 
   Route::get('delete-order', 'Trade\DeleteOrderController@deleteBuySellLimitMarket'); 
   Route::get('get-market', 'Trade\TradeMarketController@tradeMarket'); 
   Route::get('get-margin', 'MarginTrade\MarginTradeController@tradeMargin'); 
   Route::post('order-processing', 'SquareOff\SquareOffController@orderProcessing');
   Route::post('order-history', 'OrderHistory\OrderHistoryController@orderHistory'); 
   Route::post('new-order-history', 'OrderHistory\OrderHistoryController@newOrderHistory');
   Route::post('new-order-history/{id}', 'OrderHistory\OrderHistoryController@newOrderHistory'); 
   Route::get('current-market', 'McxMarket\McxController@currentMarket'); 
   Route::post('add-market', 'McxMarket\McxController@addMarket');
   Route::post('remove-market', 'McxMarket\McxController@removeMarket');
   Route::get('future-market', 'McxMarket\McxController@futureMarket');
   Route::post('complain/create', 'Complain\ComplainController@create');
   Route::get('complain/list', 'Complain\ComplainController@list');
   Route::get('logActivity', 'Log\LogController@create');

   Route::post('statement/list', 'Pdf\PdfController@statementGeneration');
//Change password  API
    Route::post('change-password', 'AuthController@changePassword');
//UserData 
    Route::post('user-data', 'UserData\UserDataController@userData');
//UserData/{{id}} 
    Route::post('user-data/{id}', 'UserData\UserDataController@userData');

    //Front Menu list API
    Route::get('front-menu-list', 'Dashboard\MenuController@frontMenuList');

    Route::get('sharemarket-list', 'Dashboard\ZerodhaController@index');
//Market results api
    Route::post('market-result', 'Market\MarketResultController@marketResult');
//Market List
    Route::get('market-list', 'Market\MarketController@marketList');
//Market Profit Loss
    Route::post('market-profit-loss', 'Market\MarketProfitLossController@marketProfitLoss');
    Route::get('market-sports-list', 'Market\MarketProfitLossController@marketSportsList');
    Route::post('market-bet-list', 'Market\MarketProfitLossController@marketBetList');
    Route::post('get-event-list', 'Market\MarketProfitLossController@getEventList');
//Account Statement 
    Route::post('account-statement', 'Transaction\AccountController@accountStatement');
    Route::post('account-bet-list', 'Transaction\AccountController@accountBetList');

    Route::post('commission-statement', 'Transaction\CommissionAccountController@accountStatement');
    Route::post('export-statement', 'Transaction\AccountController@exportStatement');

  
});



